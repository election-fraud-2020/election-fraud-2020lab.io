---
title: Draza Smith's Election Analysis
meta_desc: Draza's statistical analysis on the 2020-2021 US elections, and evidence of algorithmic manipulation of results.
breadcrumb: "[Follow the Data](#colophon) ›"
last_updated: 20 Apr 2022
---

> The election was not counted, it was calculated, and we were on cruise control."
>
> <small>--- Draza Smith summarizes her findings, in [interview with David Clements](https://rumble.com/vjt3pj-math-wars-we-have-the-plans-to-the-dominion-death-star..html)</small>

Draza Smith is a computer engineer with two master's degrees, in electrical engineering and in computer engineering. She completed her doctoral work in computer engineering, and awaits defense of her PhD dissertation. She has worked for Sandia National Labs focusing on Cyber and Grid Security.

She explored the 2020 election count data from The New York Times (supplied mostly by Edison Research, with additions by The Associated Press) which shows the proportion of ballots voting for each candidate *as the results came in, batch by batch*. Her investigations indicate that results in many states may potentially be being manipulated or controlled via a **computer algorithm**.

She believes the algorithm has been programmed to achieve a specific percentage of votes in each location, adjusting the votes up or down during the count in order to reach that target. She likens this algorithm to that of a "[PID Controller](https://realpars.com/pid-controller/)", or a car's cruise control, where, after a specific target speed has been set, the car's engine adjusts up or down to reach that target. She identified several cases where the target was adjusted mid-way through the ballot counting process (potentially based on the results in other states, such as after Florida was announced as a win for Trump) and it appears that all batches following this adjustment show a shift in the results as the algorithm now aims for the new target value.

Since most election systems in the US are "closed source" --- meaning that their source code cannot be easily audited --- it's not clear exactly *how* this manipulation is occurring, but it deserves deeper investigation, and is yet another example of why closed-source election software run by private companies should *not* be used in elections.

Draza posts her analyses, charts and graphs on [her Telegram Channel](https://t.me/LadyDraza).


## Articles & Reports

{% include page_card url="/draza-reports/california-recall-election/" %}

<div class="post-feed full-width">
    {% include card 
        title="Draza's State-by-State Reviews &amp; Charts"
        url="https://t.me/LadyDraza"
        summary="Reviews of all 50 states and what the election counts tell us about potential algorithmic control. "
        button="Browse Draza's Telegram Channel" 
        %}

<!-- *We’ll be attempting to provide coverage of Draza’s earlier reports. Stay tuned while we get these prepared, and see her [Telegram channel](https://t.me/LadyDraza) in the meantime.* -->
</div>



## Video Presentations

* [MATH WARS: We have the plans to the DOMINION Death Star](https://rumble.com/vjt3pj-math-wars-we-have-the-plans-to-the-dominion-death-star..html) --- Draza's first interview with David K. Clements, along with Dr. Douglas Frank and Edward Solomon. July 13, 2021.

* [Draza Smith: Election Fraud on Cruise Control?](https://rumble.com/vkgtqh-draza-smith-election-fraud-on-cruise-control.html) --- further in-depth interview with David K. Clements. July 28, 2021.

* [Draza Explains Her Process](https://rumble.com/vkh96p-ladydraza-pid-cruise-control-north-dakota.html) --- by walking through North Dakota as an example, Draza walks through how she compiles the data into Excel, what assumptions she makes, and shows some examples of some anomalies. July 29, 2021.

* [Draza Smith's Presentation at Washington's Election Integrity Public Hearing](https://rumble.com/vlwjiy-draza-smith-gives-her-presentation-at-election-integrity-public-hearing.html), Aug 29, 2021

* [How PID's Play A Role In The Stolen Election In America](https://www.bitchute.com/video/rbA7FtN7nBz7/) --- Dr. Smith (no relation to Draza) summarizes and explains Draza's research. Sept 16, 2021.


## Raw Data

You can view the ballot counting time-series graphs as well as access the raw time-series data used for many of Draza's statistical analyses in our [Data Explorer](/data/explorer/).
