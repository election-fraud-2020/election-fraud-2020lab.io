---
title: Down Ballot Audit Force
breadcrumb: "[Follow the Data](#colophon) › "
meta_desc: Help us collect important data on the 2020 election.
last_updated: 05 Dec 2021
---


We believe the General Election results have been manipulated in quite a number of counties, but this can be difficult to communicate to the average person. 

Inspired by one of [Seth Keshel's Telegram posts](https://t.me/ElectionHQ2024/974) featuring the chart below, our initial research shows that when we gather the following Down Ballot election results, it clearly highlights which counties have "abnormal" General Election trends compared to other county races. The analysis can produce a simple but powerfully convincing diagram that clearly illustrates the issue.

----

[![image](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/de0e020edc861dafd04b3a57f1603bff/image.png)](https://gitlab.com/KalevN/downballotanalysis/-/wikis/uploads/de0e020edc861dafd04b3a57f1603bff/image.png)

----

{% include toc %}

# Down Ballot Audit Force

This guide will explain, step by step, how to analyze for yourself a county's election race results.

The steps are:

* Find the election results for your county online
* Save the raw data to your computer
* Collect the data into a spreadsheet (or on a piece of paper)
* Analyze the data
* Share your findings with the community

Worked examples for two counties are shown at the bottom of this guide and make up for the majority of the content in this document.

## Context

The 2020 Presidential election has come under intense scrutiny. Analyzing the election data reveals many anomalies and statistical impossibilities.

One of the simplest and most impactful analysis is the **historical down ballot election** results.

**Everyone** can participate and determine for themselves if the numbers make sense.

## Purpose

There are 3,141 counties in America. At each General election, counties will run many different electoral races simultaneously.

The idea behind the "down ballot analysis" is that voters typically vote "down ballot" according to party lines. So, if the Presidential race has significantly different percentages to other races we need to find out why. Especially if, historically speaking, the presidential percentages are always in line with other races.

Finding the data for all these counties and analyzing the data takes time. It is fairly simple, but it takes time.

The results can be very convincing and provide the opportunity to discuss these findings with family and friends, who don't believe the election was manipulated.

----

# 📒 Guide

## Introduction

Finding, collecting and analyzing the down ballot election results will provide you with all the proof you need to demand full transparency and accountability from your elected officials.

We cannot guarantee all counties will have "suspicious trends", but many of them will. 

"Suspicious trends" are not, in and of themselves, proof of any wrong doing, however they do provide a **compelling motive** to demand further investigations.

If, "We The People" can see with our own eyes that the down ballot election results do not make sense, we will demand explanations. The answers will be found in full forensic audits (that include canvassing).

We only need to carry out a few forensic audits, in smaller counties, across a few different states, to establish wide spread sophisticated and systematic fraud, and therefore decertify the whole election. We do NOT need full state wide forensic audits. Fraud is fraud. If found anywhere it vitiates everything!

**CAUTION:** Do not let your elected official and the media establish **YOUR** expectations. 

If **YOU** are convinced that something is not right, after analyzing the ballot election results, **YOU** can demand further action be taken, in order to establish the truth. 

Don't wait for politicians to be convinced... that will never happen. 

"We The People" are the one and only solution. We must unite and demand action by all peaceful and lawful means.

**NOTE:** Read through the guide once and then look at the worked examples. The worked examples should clarify the guide. If you still have questions or suggestion to improve the guide. Please let us know.

## Objective

Find Down Ballot election results for all elections going back to 2000 for the county you live in.

Namely, these years:
- 2020
- 2016
- 2012
- 2008
- 2004
- 2000

## Finding Down Ballot Election Results

### Online

- Search for your county Down Ballot Election Results page

Here are some search terms to use:

```
[county name] [state name] county election results

[county name] [state name] official website
```

Replace `[county name]` and `[state name]` with the name of the county and state you live in.

Once you find the official county website you should be able to search the site to find all historical election results.

**HINT:** Try using different search engines. DuckDuckGo, Yahoo, Bing etc...

### Offline

- Call your county clerk (or elected official) and ask them to email you the links to the down ballot election results for all the years mentioned above.

## Saving The Results

Once you find the results page you may be offered several options to view the results. Either in a pdf file or a HTML page.

Make sure to download and save the pdf files associated with the elections we are interested in (see the years in the list above.)

**Keep them in a safe place on your computer. We may need to share these files with one another later.**

For HTML pages: Save the page to your computer or take screenshots of it.

## Collecting The Results

Start by looking at the 2020 election results.

There will be a "Presidential" race section, which will list the votes for each candidate as well as the total number of votes.

You can either write these details down on a piece of paper, or type (or copy/paste) the results in an Excel spreadsheet. (You can find an Excel template [here](https://gitlab.com/KalevN/downballotanalysis/-/wikis/DownBallotElectionResults_v02.xlt) to get started.)

We are only interested in the numbers for the Republican and Democrat party. If there are several other candidates then group their numbers into an "Other" party.

**NOTE:**
- You will find many other election results for different "races"
- We are only interested in recording the results for the "races" that have similar total votes to the Presidential race
- Older elections may not provide pre-calculated "total votes" for a race. You will need to calculate it manually by adding up all the votes for each candidate

Repeat the same process for each election year. i.e. 2020, 2016, 2012, 2008, 2004, 2000

**NOTE:**

- Older elections have less races and the data becomes harder to aggregate. That is fine. Do the best you can, and don't hesitate to ask for help
- Every county reports on the results differently, however the fundamental information should be the same

## Analyzing The Results

Calculate the percentage vote for each candidate.

### What to look for

- Does the Presidential race percentages match other races? (if not, is there a good explanation?)
- Historical trends. Do the percentage gap between parties look the same from one election to the next?
- Does the voter turnout increase or decrease excessively for each party from one election to the next?
- Do the percentage votes for each party make sense form one election to the next?
- Can the change in percentage vote be explained rationally?

**NOTE:** See the worked examples to see how this plays out.

## Sharing The Results

- Share your findings with family and friends and ask them if it makes sense to them. If they don't believe your findings, tell them to do the analysis for themselves. All the data is open and free. There are no excuses.

## Worked Examples

The next page includes a full worked example, with screenshots, to explain the process.

[View Worked Examples](examples/){:.button}