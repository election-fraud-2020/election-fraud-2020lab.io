---
title: Maricopa County Forensic Audit
breadcrumb: "[Reports by State](#colophon) › [Arizona](/fraud-summary-by-state/arizona/) ›"
meta_desc: Here's what we know so far about the Maricopa County Forensic Audit
last_updated: 25 Oct 2021
---


{% include toc %}


#### Initial Obstruction

Based on several unresolved issues from the election and the county's own internal audit -- as explained in our main [Arizona](/fraud-summary-by-state/arizona/) article -- the state Senate issued several subpoenas (legally binding requests) to the Maricopa Country election board and county recorder, to access its voting machines, copies of all ballots and much more so it can perform its own audit. The election board argued vehemently against those requests, saying they were overly broad, and aimed to negotiate a settlement with the Senate. [^5] The Senate eventually received the ballots and *some* of other requested materials, and began the audit.

#### Continued Obstruction

In May, mid-way through the major forensic audit, the senate committee identified the following issues that it asked the county election board to clarify: [^9]

1. Ongoing refusal to comply with the Senate's subpoenas
2. Chain of custody and ballot organization anomalies
3. Deleted vote-counting databases

The Maricopa County Board of supervisors responded with [this letter](https://www.maricopa.gov/DocumentCenter/View/68972/20210517-Response-Letter-to-Senate-President-Fann---FINAL) on May 17, 2021, denying all wrongdoing and heavily denouncing the skills of the auditors and legitimacy of the audit, however they appear themselves to misunderstand the workings of data recovery software.

#### July Senate Hearing

On July 16, 2021, a 2-hour hearing was given in the Arizona Senate, with auditors raising several questions and issues that are preventing them from completing the audit, including:

* 73,243 mail-in ballots have no record of being sent out [^8] (Maricopa County later [responded](https://recorder.maricopa.gov/justthefacts/#tabs=1), arguing that this number also includes early in-person votes which would not have been mailed) 

* 11,326 voters were not listed on November 3 voter roll, but *were* listed on December 4 voter roll and were listed as having voted in the election [^8]

* 18,000 who voted in November but were then removed from the rolls [^8] [^12]

* Thousands of duplicated ballots that lacked serial numbers. Senate President Karen Fann noted that without serial numbers, it would be impossible to know how many times a ballot was duplicated.  [^8]

* 168,000 ballots are printed on unofficial paper that the county claims they did not use. This could be a simple mistake, or an indication of fraudulent ballots. [^8]

- An unpublicised digital breach of the election system in November [^8] [^12]

- A one-off digital event erasing the security log for the period covering the November election. The auditors reported 37,646 entries into the digital security log for the election management system – which has only eight accounts – on one day, 11 March 2021. Data being kept on a first-in, first-out principle, the digital deluge shoved enough previous data out to hide election security log events before February 2021. Rep. Finchem claims this shows criminal intent. [^8] [^12]


The full hearing was [available on YouTube](https://youtu.be/7OZmNbBDQ6k?t=10897) <span class="pill">Censored</span>, or [on Rumble, here](https://rumble.com/vjvyzi-live-arizona-az-state-senate-hearing-on-the-2020-election-audit-in-maricopa.html).

Maricopa County officials have been highly oppositional to the audit at every stage, and deny each of the issues. They have responded to common questions and allegations [on their website](https://recorder.maricopa.gov/justthefacts/).

#### Further Subpoenas

Meanwhile, the Senate issued fresh subpoenas (legally binding requests) to obtain the missing information required to complete the audit: ballot envelopes (or images of them), voter records, routers (or router images), all findings concerning systems breaches, and all usernames and passwords for election machines. [^10] After the subpoenas were refused, the Senate has referred the matter to the state's Attorney General for investigation.

#### Common Criticisms of the Audit

*Based partly on [Ryan Wiggins' article](https://www.audacy.com/971talk/news/politics/cheat-sheet-easy-answers-to-popular-criticisms-of-the-arizona-audit) from 97.1 FM Talk.*

***"This is a partisan audit only done by Republicans, so it can't be trusted."***

Democrat officials knowingly turned down being involved in the audit, instead preferring to discredit it to their media allies. [^13] Volunteers involved in the audit were recruited from all political sides --- in fact volunteers were not even asked which party they belonged to or who they voted for.

***"It was paid for by far-right-wing groups."***

The Senate chose to avoid criticism by not spending millions of taxpayer dollars on it, instead leaving private donors as the only means of funding. [^13] 56% of the funding came from *The America Project*, an organization led by Patrick Byrne, a Libertarian who did not even vote for Trump. Other funders include Former Lt. General Michael Flynn and Christina Bobb from OAN News [^14] --- hardly "far right".

***"Cyber Ninjas and other groups doing the audit have no forensic audit experience."***

This is, in fact, true. US elections have never been forensically examined at this scale, therefore nobody has any experience in doing such an audit. There has been no precedent. [^13]

***"These people are not even certified auditors."***

Despite the media use of the term "certified auditors", there is actually no such thing as accredited election auditors in America, only accredited [voting system test laboratories](/faqs/voting-system-test-laboratories-vstl/) (VSTLs). These labs are hired by voting machine companies like Dominion to grant them government <abbr title="U.S. Election Assistance Commission (eac.gov)">EAC</abbr> (US Election Assistance Commission) certification, and as such would be inherently biased against finding issues in machines that they themselves were paid to certify. 

When the Senators then approached big financial auditing companies, they all refused, saying it would put their government contracts at risk. [^15]

***"Even Republicans on the Maricopa Board of Supervisors have resisted this audit."***

Yes, they have resisted heavily! Early on they agreed with the idea, but after visits from out-of-state lawyers, they begin resisting efforts and even ignoring subpoenas. One would expect officials to comply with members of their own party, especially if it could exonerate them. They instead have resisted transparency. [^13] What do they have to hide?

[Seth Keshel](/seth-keshel-reports/) also notes that Chairman Jack Sellers won his election by only 403 votes (0.1%) out of 424,531 cast, and is unlikely to want to risk his position by having the results investigated. [^16]

***"There have already been audits that proved nothing happened in Maricopa County, AZ."***

The previous audit in Maricopa County was organized by *themselves* and utilized [Pro V&V and SLI Compliance](/faqs/voting-system-test-laboratories-vstl/) --- companies that Dominion had previously contracted to certify their machines. They would likely be inherently biased against finding issues in the same machines they, themselves, were paid to certify.

Many other audits of this nature are also small "risk limiting audits" which examine a small percentage of the ballots. The *full forensic audit* examined around 2,100,000 ballots, including the paper, the counting process, the chain of custody, and other related processes. This was a *much* deeper examination than had been done before.

***"There were blue and black pens on the tables.  They were looking for bamboo fibers in the papers.  One of the insurrectionists was there counting.  Etc..."***

Lots of small things were raised to nitpick on the audit, yet it was live-streamed 24 hours a day from more than a dozen camera angles. Security was very tight, and anyone -- regardless of party -- was held to the high standard.

***"Cyber Ninjas CEO Doug Logan pushed election conspiracy theories."***

The main accusation against him (that we could find) is that he is allegedly "Anon", the anonymous interviewee in the documentary *The Deep Rig* (something we have not yet been able to confirm) who apparently said that "If we don't fix our election integrity now, we may no longer have a democracy." The interviewee also claimed that current and former members of the Central Intelligence Agency (CIA) were involved with a "disinformation" campaign directed at the 2020 presidential election. [^17]

If true, the first statement can hardly be called "conspiracy". And while the second does allege a form of conspiracy, it's not at all baseless, as federal agencies have been clearly implicated by Department of Defense Chief of Staff Kash Patel, and US House Intelligence Committee Chairman Devin Nunes, as they explain in detail in the documentary *[The Plot Against The President](/in-detail/2020-election-fraud-documentaries/#the-plot-against-the-president)*.

If fraud *does* exist in Maricopa County, it is arguably better to have someone sceptical of the results investigate --- as long as they can legitimately prove their conclusions --- rather than someone who believes everything is fine already.

Also, if the Democrats had accepted the offer to be involved in the audit from the start, they could have participated in picking the cyber team.  Why did they choose not to participate?




#### Telling the Story

On Aug 12, at the [Cyber Symposium](/in-detail/cyber-symposium-mike-lindell/), Arizona State Representatives Mark Finchem, Sonny Borrelli, and Wendy Rogers told the story of the entire audit process, including responding to early election fraud reports and moving through with the forensic audit despite many obstructions from Maricopa County, the media and others. 

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vifrdz/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

Senate President Karen Fann also tells the story from her perspective in a 3-part video series on Twitter. See [Part 1](https://twitter.com/AZGOP/status/1427736243127738371?s=20), [Part 2](https://twitter.com/AZGOP/status/1427766848699863040?s=20), [Part 3](https://twitter.com/AZGOP/status/1427782296032235521?s=20).


#### Settlement Regarding Routers and Splunk Logs

After further pressure from the Senate to release the routers and splunk logs for auditing, the Senate and County reached a settlement agreement ([read it here](https://www.maricopa.gov/DocumentCenter/View/71353/Agreement)) where the County Supervisors will give the Splunk logs and the routers over to a third party, a trusted source which is former Arizona Congressman, John Shadegg, who will act as a caretaker of the data, a trusted 3rd party. Cyber Ninjas will then (reportedly) be able to get everything that they want from him [^18], although the terms of the agreement appear to conflict with this. [^20]

Unfortunately, it has been reported that John Shadegg has lobbied on behalf of company "Hickman's Eggs" in the past.  Hickman's Eggs is owned by the then Chair of the Maricopa County Supervisors, Clint Hickman.  Supervisor Hickman has been a hostile player in regard to the audit, and as such, John Shadegg may not be a neutral player, but instead hold a direct conflict of interest. [^19]

Senator Kelly Townsend has been posting regular updates on [her Telegam channel](https://t.me/KellyTownsend) --- despite not being allowed into the audit committee itself, she is keeping a close eye on proceedings. The [Arizona Conservatives Channel](https://t.me/ArizonaConservatives) also has regular updates.


#### Forensic Audit Results

The final forensic audit results are due to be released Friday, September 24, at 1pm Arizona time. 

The Arizona Audit team was previously posting [frequent updates on Twitter here](https://twitter.com/arizonaaudit), until [Twitter suspended their account in late July](https://www.theepochtimes.com/twitter-suspends-2020-maricopa-county-election-audit-accounts_3921097.html). Updates are now on Telegram at [@ArizonaAudit](https://t.me/ArizonaAudit).


### Voter Canvassing Effort

A part of the Maricopa Forensic Audit involved voter canvassing — directly interviewing voters and visiting registered addresses to confirm that official records match how residents actually voted. The Federal Department of Justice swiftly enacted significant pressure against this canvassing, saying that it equated to “voter intimidation”, and would be potentially illegal. This blockade of the Senate’s efforts forced them to focus on the remaining aspects of the audit.

But hundreds of private citizens such as those led by [Liz Harris](https://t.me/VoteLizHarris) had already begun their own grass-roots canvassing effort, visiting 11,708 residential properties and gathering data on 4,570 voters. We've summarized her team's findings on [the main Arizona page, here](/fraud-summary-by-state/arizona/#voter-canvassing-effort).

## Final Report

{% include maricopa_results %}


### Footnotes & References

[^5]: "[Maricopa County votes to audit machines used in November election](https://www.fox10phoenix.com/news/maricopa-county-votes-to-audit-machines-used-in-november-election)", Fox 10 Phoenix, Jan 27, 2021.

[^6]: Report: "[2020 Presidential Election Startling Vote Spikes: Statistical Analysis of State Vote Dumps in the 2020 Presidential Election](https://www.scribd.com/document/496106864/Vote-Spikes-Report)" by Eric Quinnell (Engineer); Stan Young (Statistician); Tony Cox (Statistician); Tom Davis (IT Expert); Ray Blehar (Government Analyst, ret’d); John Droz (Physicist); and Anonymous Expert.

[^7]: Patrick Byrne, traditionally a Libertarian, developed a team of computer specialists to investigate election integrity issues including the notorious one-sided "spikes" found in several states and has written extensively about them on his website *[DeepCapture.com](https://deepcapture.com)*. See "[Evidence Grows: ’20 Election Was Rigged](https://www.deepcapture.com/2020/11/election-2020-was-rigged-the-evidence/)" (Nov 24, 2020) and his book "[The Deep Rig](https://www.amazon.com/Deep-Rig-Election-friends-integrity-ebook/dp/B08X1Z9FHP)".

[^8]: From Arizona Audit Senate Hearing, July 16, 2021. A short summary of the 2 hour hearing is available from OAN [via this link](https://rumble.com/vjxz8f-bombshells-dropped-at-ariz.-audit-hearing.html). The full hearing was [available on YouTube](https://youtu.be/7OZmNbBDQ6k?t=10897) <span class="pill">Censored</span> or [on Rumble, here](https://rumble.com/vjxjni-az-senate-hearing-on-the-election-audit-in-maricopa-county-7-16-21.html).

[^9]: From the full letter from Arizona Senate to Maricopa County Board of Supervisors, May 12, 2021. [View PDF](https://cdn.donaldjtrump.com/djtweb/general/5-12-21_Letter_to_Maricopa_County_Board.pdf).

[^10]: "[Arizona Senate Issues Fresh Subpoenas for 2020 Election Audit](https://www.theepochtimes.com/arizona-senate-issues-fresh-subpoena-for-2020-election-audit_3920229.html)", The Epoch Times, July 27, 2021.

[^11]: Arizona is one of just two states in the United States that requires proof of citizenship when registering to vote. The loophole, the Federal Only ballot ([see explanation](https://www.azpm.org/s/43413-federal-only-ballots-mean-some-arizonans-cant-vote-local/)), which voters may use instead, does not require this documentation. Instead, voters are limited to federal races.

    The exploitation of this loophole of "Federal Only" voters was flagged by Senator Kelly Townsend on Telegram ([view post](https://t.me/KellyTownsend/122)) and also in [an article by The Gateway Pundit](https://www.thegatewaypundit.com/2021/06/arizona-2x-undocumented-federal-voters-biden-won/), Jun 6, 2021.

    [Arizona's Secretary of State website](https://azsos.gov/elections/voting-election/proof-citizenship-requirements) outlines the official requirements for voter registration, proof of citizenship, and how "federal only" votes supposedly work.

[^12]: The Spectator Australia: "[The Big Lie vs Stop the Steal](https://www.spectator.com.au/2021/07/the-big-lie-vs-stop-the-steal/)", July 24, 2021.

[^13]: Ryan Wiggins: "[OPINION: Cheat Sheet: Easy answers to popular criticisms of the Arizona audit](https://www.audacy.com/971talk/news/politics/cheat-sheet-easy-answers-to-popular-criticisms-of-the-arizona-audit)", Sep 15, 2021

[^14]: See funding report from Arizona Senate: <https://t.me/LibertyOverwatchChannel/5121>

[^15]: According to Representative Mark Finchem, from the presentation given at the Cyber Symposium. See Rumble video: "[AZ Leaders Expose How Election Was Stolen In Their State](https://rumble.com/vl1xj9-az-leaders-expose-how-election-was-stolen-in-their-state.html)", published Aug 12, 2021.

[^16]: From [Seth's Telegram post](https://t.me/ElectionHQ2024/1110), Aug 28, 2021

[^17]: These accusations appeared in multiple left-leaning media outlets including the Arizona Mirror: "[Audit leader Doug Logan appears in conspiracy theorist election film](https://www.azmirror.com/2021/06/26/audit-leader-doug-logan-appears-in-conspiracy-theorist-election-film/)", Jun 26, 2021

[^18]: <https://t.me/KellyTownsend/594>

[^19]: <https://t.me/KellyTownsend/596>

[^20]: <https://t.me/ArizonaConservatives/2723>