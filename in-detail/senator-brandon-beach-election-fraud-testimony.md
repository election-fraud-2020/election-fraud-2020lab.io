---
title: Georgia Senator Brandon Beach's Election Fraud Statements
last_updated: 5 Jan 2021
---

*December 30, 2020*

After 12 hours of testimony presented to Georgia's Senate Judiciary Subcommittee on Elections (December 30, 2020), Senator Brandon Beach made the following closing statements.

----

I'll be brief 'cos I know we're on a time limit. Chairman, I just wanna say thank you. You're you're on your way out, so are you chairman Heath, and y'all are here today and serving your last few days and making things happen for your constituents and I wanna thank you. 

I also wanna say that, I was just telling Chairman Jones, after 12 hours of testimony you can't make some of this stuff up. I mean it's just unbelievable what we've seen in this 12 hours of testimony. I'm embarrassed. It's an embarrassment for our state and I am more and more convinced now that this is a well-orchestrated, well-coordinated effort, by several groups to commit widespread and systemic fraud. And I just wanna go through a couple things that I wanna observe.

1. We had out-of-state people voting. Even [Gabe Sterling has admitted](https://tennesseestar.com/2020/12/24/georgia-election-official-gabriel-sterling-reveals-woman-used-his-voting-address-after-a-month-of-defending-election-integrity/) that a lady from Maryland used his address to vote in our election. He works for the Secretary of State's office.

2. We had double voters, we had 1,700 double voters. After the primary, the Secretary of State had a press conference, [and] said he was gonna investigate and prosecute over a thousand people that double-voted in the primary. As of December 23rd, there's not been one investigation [or] one prosecution. 

3. We had felons voting, dead people voting.

4. With the State Farm Arena that we've just talked about with 37,500 ballots being counted with no supervision.

5. And then we had a chain of custody [issue] with these drop boxes, and I wanna mention this: a lady sent me this from Cobb County and they're called "Dropbox Ballot Transfer Forms" and I've got four of 'em, but I'm just gonna go over one. There were over a thousand ballots total, but this one had 274 ballots. The same two people picked them all up. I'm not gonna mention their names. They picked them up on 10/22 on this one. They're supposed to then take them directly to the Cobb County Elections office and get them there that same day; they didn't arrive till 10/25. So they picked them up on 10/22 on 10/25 they go to the county Register's Office Election office. Now that is breaking the chain of custody. Where were these ballots? Were they in the trunk of a car? Were they in the back seat of a car? Were they in somebody's home? The chain of custody was broken.

So anyway, Mr Chairman, I'll end with this: people are mad. People are angry. We saw how angry this one lady that testified from Cobb County was, there's passion. These are real people. These aren't expert witnesses that Mr Smith hires for court cases, these are our constituents. They're angry and they're mad, and they want us to do the right thing just like Mayor Giuliani said. We need to do the right thing. We need to take action and we need to do something about this fraud. 

So I would suggest we focus on Fulton County and the State Farm Arena and I would like to make a motion that we request Fulton County Board of Elections to make absentee ballots from State Farm Arena from 10:30 PM to 1:30 AM on election night available to the [Chili Law Group?] and Mr Pulitzer to validate if these are legitimate ballots, and I do that in a form of a motion.


### Source

Video from the Senate Judiciary Subcommittee on Elections (December 30, 2020), by NTD Television. Quote begins from 1hr 35min 50sec into the video.

* <https://v.bajarfb.com/142237407695911>
* <https://www.facebook.com/watch/live/?v=142237407695911&ref=watch_permalink>