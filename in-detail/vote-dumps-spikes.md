---
title: Unusual Vote Dumps and Spikes During Tallying
faq_title: What caused the vote dumps (spikes) during the 2020 ballot counting?
breadcrumb: "[Follow the Data](#colophon) ›"
comments_enabled: true
last_updated: 10 Sep 2021
---

Many of the "swing states" experienced unusual and statistically improbably "dumps" or "spikes" during tallying that deserve deeper investigation.

The following three charts were published by Patrick Byrne (a Libertarian who didn't vote for Trump), who gathered a team of computer specialists to investigate election integrity issues including the notorious one-sided "spikes" and has written extensively about them on his website *[DeepCapture.com](https://deepcapture.com)*. 

<img src="/images/spike-arizona.png" class="image-border" width="3301" height="2550" alt="Arizona Election Vote Spike Chart">

<img src="/images/spike-georgia.png" class="image-border" width="3301" height="2550" alt="Georgia Election Vote Spike Chart">

<img src="/images/spike-michigan.png" class="image-border" width="3300" height="2550" alt="Michigan Election Vote Spike Chart">

{:.caption}
Source: "[Evidence Grows: ’20 Election Was Rigged](https://www.deepcapture.com/2020/11/election-2020-was-rigged-the-evidence/)" by Patrick Byrne, and also included in his book "[The Deep Rig](https://www.amazon.com/Deep-Rig-Election-friends-integrity-ebook/dp/B08X1Z9FHP)".


## The Vote Spikes Report

For a more in-depth analysis of vote spikes across many states, along with specific stats, timestamps and batch numbers, see the following report:

[2020 Presidential Election Startling Vote Spikes: Statistical Analysis of State Vote Dumps in the 2020 Presidential Election](https://www.scribd.com/document/496106864/Vote-Spikes-Report)  
<small>by Eric Quinnell (Engineer); Stan Young (Statistician); Tony Cox (Statistician); Tom Davis (IT Expert); Ray Blehar (Government Analyst, ret’d); John Droz (Physicist); and Anonymous Expert.</small>

<iframe class="scribd_iframe_embed" title="Vote Spikes Report" src="https://www.scribd.com/embeds/496106864/content?start_page=1&view_mode=scroll&access_key=key-Ei7h9msOavvxAanIwW63" tabindex="0" data-auto-height="true" data-aspect-ratio="0.7729220222793488" scrolling="no" width="100%" height="600" frameborder="0"></iframe><p  style="   margin: 12px auto 6px auto;   font-family: Helvetica,Arial,Sans-serif;   font-style: normal;   font-variant: normal;   font-weight: normal;   font-size: 14px;   line-height: normal;   font-size-adjust: none;   font-stretch: normal;   -x-system-font: none;   display: block;"   ><a title="View Vote Spikes Report on Scribd" href="https://www.scribd.com/document/492079663/Vote-Dumps-Report#from_embed"  style="text-decoration: underline;">Vote Spikes Report</a> by <a title="View John Droz, jr.'s profile on Scribd" href="https://www.scribd.com/user/3314846/John-Droz-jr#from_embed"  style="text-decoration: underline;">John Droz, jr.</a></p>