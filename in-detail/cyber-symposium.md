---
title: Cyber Symposium Detailed Summary 2021 - Mike Lindell
heading: Cyber Symposium Detailed Summary
permalink: /in-detail/cyber-symposium-mike-lindell/
meta_desc: Video highlights and summary of the key issues raised during the Symposium.
breadcrumb: "[Further Articles](#colophon) › "
last_updated: 16 Aug 2021
comments_enabled: true
comment_intro: Were there any other important points or clips from the Symposium we missed? Let us know in the comments below.
---

*August 10-12, 2021, hosted by Mike Lindell, in Sioux Falls, South Dakota.*

Mike Lindell hosted a 3-day symposium to bring election experts, security researchers, journalists and legislators together to examine the evidence of a stolen election. 

<div class="info" markdown=1>
## Watch Video Replay

* Streaming repeatedly at [FrankSpeech.com](https://home.frankspeech.com/).

* Or (mostly complete) recordings of [Day 1](https://rumble.com/vky68n--watch-live-patriot-news-outlet-mike-lindells-cyber-symposium-day-1-10am-es.html), [Day 2A](https://rumble.com/vkzy5k--watch-live-patriot-news-outlet-mike-lindells-cyber-symposium-day-2-9am-est.html), [Day 2B](https://rumble.com/vl0pki-mike-lindells-cyber-symposium-day-2-live-continued.html), or [Day 3](https://rumble.com/vl1owp--watch-live-patriot-news-outlet-mike-lindells-cyber-symposium-day-3-8122021.html) at Rumble.com.
</div>

{% include toc %}


## Presenters

**Colonel (Retired) Phil Waldron** --- Waldron served in the United States Army for 30 years and is a decorated combat veteran.  He has extensive operational experience in Surveillance and Reconnaissance; Strategic Communications; Joint Information Operations; Special Technical Operations; Cyber Computer Network Operations; OPSEC; All-Source Intelligence Operations; Unconventional Warfare and Operations Integration.  

**Dr Douglas Frank** --- Dr Frank is a physicist, chemist and mathematician with a Ph. D. in Surface Analytical Chemistry. <small>[Telegram](https://t.me/FollowTheData)</small>

**David K. Clements** --- Clements, J.D., is a professor of business law at NMSU, and serves as a trial consultant for civil litigation firms. He was a violent crimes prosecutor supervising the Lincoln County district attorney’s office and has tried over 130 cases. <small>[Telegram](https://t.me/theprofessorsrecord)</small>

**Patrick Colbeck** --- Colbeck earned his B.S. and M.S. in aerospace engineering from the University of Michigan and worked in the aerospace, defense, pharmaceutical, healthcare, telecommunications, automotive, information technology and financial services. <small>[Website](https://letsfixstuff.org/) or [Twitter](https://twitter.com/pjcolbeck)</small>

**Captain Seth Keshel** --- Keshel was a former American army captain who worked as a Military Intelligence officer for about 6 years.  Now is focused on analyzing historical election results.   <small>[Telegram](https://t.me/ElectionHQ2024)</small>

**Dr. Shiva Ayyadurai** --- SHIVA Ayyadurai MIT PhD is a scientist, inventor, entrepreneur and Fulbright Scholar who holds 4 degrees from MIT including his PhD in Biological Engineering. He started 7 successful high-tech companies in MA. <small>[Website](https://vashiva.com/) or [Telegram](https://t.me/drshivaayyadurai)</small>

**Draza Smith** --- Draza has an M.A. in Electrical Engineering and M.A. Cyber Engineering.  She completed her doctoral work in cyber engineering and awaits defense of her PhD dissertation. Draza has worked for Sandia National Labs focusing on Cyber and Grid security. <small>[Telegram](https://t.me/ladydraza)</small>



## Day 1 Summary

* Curiously, the Frank Speech web video stream breaks down right at starting time — a possible hack or DDoS attack, apparently with 4+ million incoming requests

* Opening prayer, pledge of allegiance, national anthem

* Eye-opening intro video from Retired Colonel and security expert Phil Waldron. Phil is leading a "red team" of security experts investigating the election issues.

  <div class="responsive-iframe"><iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vid807/?pub=m4ux1" frameborder="0" allowfullscreen></iframe></div>

* Panel with Mike Lindell, Col. Phil Waldron, Professor David K. Clements, Pat Colbeck -- they discuss the video and surrounding issues

* Professor David K. Clements mentions how the Democrats often have smooth, polished presentations, but lack substance. Conservatives lack the polish, but often have more substance. This is because of their foundations.

* Dr. Douglas Frank’s presentation -- explaining his team’s work with multiple county and state officials, some explosive findings across multiple states, and some secret audits going on behind the scenes. The statistics he's uncovered show very alarming patterns that indicate algorithmic fraud. His findings have been reproduced by numerous other groups and individuals.

  <div class="responsive-iframe"><iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vid7gk/?pub=m4ux1" frameborder="0" allowfullscreen></iframe></div>

* Pat Colbeck’s presentation -- he discussed his personal witness testimony (now with video evidence) of voting machines being connected via CAT-5 ethernet cable to internet, despite conflicting claims from election officials and senior government officials, and being blocked out of the building during his role as poll observer

* Pat Colbeck introduces the on-site election challenge whereby symposium attendees can vote in a simulated election, and they will demonstrate how easy it is to hack. Within 20mins an attendee apparently hacked the system and wiped out the results, requiring them to reset it to continue the simulation.

* Steve Bannon joins the panel, talking with Professor David K. Clements

  <div class="responsive-iframe"><iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vidd0h/?pub=m4ux1" frameborder="0" allowfullscreen></iframe></div>

  {:.small}
  > They [Democrats] understand they can not win elections of the American people when you go to paper ballots. So they are going to weaponize everything around it, and if you think you've seen their toughest moves, if you think you have seen the hammer drop, we haven't seen anything yet...
  > 
  > "They have sawn the wind and they are going to reap the whirlwind because the American people are going to demand, full forensic audits of 3 November 2020 in every state.
  > 
  > "That's what they feared. **If they had the receipts it would be totally open. They would rub your nose in it everyday. They would show you every ballot. There wouldn't have to be any subpoenas. They would turn the routers over. They'd say "You've not just lost, you got skunked. You lost Arizona, you lost Georgia, you lost these states... you lost, you lost, you lost."**
  > 
  > "**They can't prove it**, they don't have the receipts, so they will use every aspect of lawfare, of the media, of the criminal justice system. Of everything they possibly can... To do what? **To thwart the will of the American people...**
  > 
  > "...The future is now and the future is about [fixing] 3 November."
  >
  > <small>--- Steve Bannon</small>

* Mike Lindell discusses several state results and provides numbers for which he believes were the “honest” results. [View the 50min video where he goes through every state, on FrankSpeech.com](https://home.frankspeech.com/video/real-state-state-numbers-president-trumps-victory).

* The story of Brazil’s President Jair Bolsonaro from his son Eduardo Bolsonaro. He describes the electoral races there, and how the resulting percentages seem too uniform across counties, without the natural variation one would normally expect. He complains about a lack of transparency in election proceedings. The election machines "were rigged". Massive bike parades and boat parades became a way for the public to demonstrate.

  <div class="responsive-iframe"><iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vid8lb/?pub=m4ux1" frameborder="0" allowfullscreen></iframe></div>

* The panel (David Clements and Mike Lindell) discusses Pennsylvania and how they changed rules unlawfully using Covid as a pretext

* David Clements reminds the audience about the hundreds of [political prisoners from the January 6 riots](/in-detail/capitol-riots/#appalling-mistreatment-in-prison) who are still incarcerated harshly for mostly minor misdemeanours, most with no criminal history

* Steve Bannon shares about several outstanding issues in government agencies

* Pat Colbeck demonstrates packet captures and explains how they work. These will apparently be revealed in more detail throughout the symposium. 

* Arizona Senator Sonny Borrelli and Dr. Doug Frank discuss further issues, including how Maricopa County fought and obstructed the audits so intensely. Dr. Frank’s analysis showed the county had a *minimum* of 200,000 phantom voters. 

* David Clements and Dr. Frank discuss several issues while taking questions from the audience

* The big evening timeslot was filled with Tina Peters, on a panel with Retired US Air Force Colonel Shawn Smith and Sherronna Bishop. Tina is a Mesa County Clerk in Colorado who has been heavily persecuted for her desire to look honestly into the irregularities in the election for which she was responsible for. Only hours earlier, while she was flying to Sioux Falls for the symposium, authorities raided her office, supposedly looking to pin blame on her for leaking tabulator passwords for which she claims only the Secretary of State actually has access to. [Read more](https://americanfaith.com/mesa-county-clerk-tina-peters-says-her-office-was-wrongly-raided-by-colorado-secretary-of-state/) of the breaking story.

  <iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vid0jv/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

(There were rumours that the evening timeslot was supposed to be another significant whistleblower, but it appears that due to the office raid during that day, they decided to invite Tina Peters up instead. The whistleblower will likely coming in the next 1-2 days.)

More video clips are on the [Praying Media Telegram channel](https://t.me/praying_medic), and will also likely be posted on [Rumble.com](https://rumble.com) in coming days.


## Day 2 Summary

Presenters made the following comments:

* Patrick Colbeck describing the process of elections, setting the context for the data they will be exploring over the next 2 days. 

* While they wait to show some live / real-time investigation of evidence, Dr. Doug Frank explains some of the upcoming news:

  {:.small}
  > We have a number of friendly county clerks around the nation. We've been able to image the machines: before, during and after the elections.
  >
  > "We track *everything* that happens during the election. We found that the voting company comes in and erases everything.
  > 
  > "We now have proof of this. And this is *way* better than PCAPs."

* A question was raised: "Can you compare your data to a state where there isn't fraud?" 

  Dr. Frank spent a long time trying to do that, but actually struggled to find a place there wasn't fraud, or at least struggled in knowing for sure that it was clean. He describes that by examining historic data:

  1990 | He could detect no fraudulent algorithm running
  2008 | Voter rolls massively expanded once motor vehicle registrations automatically registered voters at the same time. Around this time, the correlation coefficient between census demographics and vote demographics was around 0.5
  2012 | Correlation 0.7--0.8
  2016 | Correlation 0.9
  2020 | Correlation almost 0.999

  He found some evidence of fraud from Republicans around 2004-2005. It's not an issue of "Oh those evil Democrats", but it happens with RINOs (Republicans in name only) also.

* Dr. Frank says "Never give power to the government. Because you'll never get it back. You'll have a huge fight to get it back. Guess what: our government have owned our elections for 20 years. It's going to be hard to undo that."

* Dr. Frank says that he uses the Edison election data not for hard evidence, but for corroborating evidence.

* Colonel (Retired) Phil Waldron adds context about how election data must be retained, by law. He also comments on George Soros' massive funding, which far outweighs a lot of Republican initiatives, however the people have the Constitution on their side.

* New Hampshire updates: LHS Associates manages/supports the election technology across the states in New England. After the election, the memory cards were mailed back to LHS and data was not preserved, breaking federal law. Workers did manage to save 4 cards in Windham during an audit, but then they were also later wiped. The staff simply replied: "Oops, they were wiped already!" No enforcement of these crucial laws is happening to date. This needs to be rectified, and some people need to be prosecuted.

  Since there may not be time to resolve these issues prior to next election, the people may just need to vote "Amish" -- on paper, not electronically.

* Dr. Frank says he ignores most of the news because it helps him stay objective to the data, and not be influenced by news.

* Question: "What happened in 2016? If Trump won, was there fraud?"  
  Dr. Frank says that Trump overwhelmed the algorithms. The algorithms can only inflate the rolls so far before it becomes very obvious.

* Dr. Frank notes that after illegitimate records are added to voter rolls, it's *very difficult* to remove them from rolls, as there are specific laws in place for how the rolls are cleaned up. He mentions an instance where 650,000 were added in an earlier election, and 300,000 were able to be challenged/removed, but there was still a net addition of around 350,000.

  Recently, when Dr. Frank and Matthew De Perno subpoenaed the voter roll data, they were told it would take 2 years to deliver it to them.

* Ron Watkins ([CodeMonkeyZ](https://t.me/CodeMonkeyZ)) joins the discussion remotely, although amidst several technical difficulties. They examine an election system drive image from Mesa County, Colorado, an election management system (EMS), which behaves like a mainframe for the voting system. They have two images: an earlier one that contains data and logs dating back to 2019, then another image that was taken after a "system update" whereby data is missing. All event logs and error logs are gone!

  [View a more detailed summary of the technical session](../cyber-symposium/codemonkeyz-technical-forensic-session/)

* Dr. Frank shares that has provided help to legislators: both technical and legal help, helping the legislators prepare what they need to tackle these issues

* Someone commented that in Montana, who uses ES&S machines, there were no serious issues. However in all likelihood, *all* of the machines have issues. Vulnerabilities or poor security in the SQL Server database appear to be common. Audits **must** check the paper ballots.

* People should be aware that there are vulnerabilities even in "air-gapped" systems -- those not connected to the internet. Leaders seem to be using "air gapping" as an excuse for not fixing vulnerabilities, but it is not solid reasoning, as machines can still be fraudulently manipulated even without internet access.

* A reminder for everyone to look very closely at the companies who manage the printing and the voter rolls.

* A movie is coming out with 300 clips of issues with the "sham audit" in New Hampshire. Look out for this.

* Dr. Frank mentions that he has caught the machines sometimes making multiple images from the same ballot, so audits should check carefully for this.

* Linda from Colorado is pushing for a full forensic audit. See [Audit50.org](https://audit50.org). She says everyone should hold onto any unused ballots. Jovan Pulitzer may need those to help identify what unused ones look like, with the correct fold lines, etc.

* Some are still saying "Where is the evidence?" There's mountains of it, but it often doesn't make it into court. Why is that? Dr. Frank says that part of the issue may be that courts don't judge on political ideas such as "the whole election has been corrupted". These issues have to go to the legislature, and *they* investigate and take action.

  If people are still looking for evidence, Dr. Frank notes that he has submitted evidence in courts  that were officially entered into the record; as has Dr. Shiva.

* Affidavits are being collected in Kansas. They're also hoping to check their machines for cell-tower modems. Anyone with an outstanding Kansas affidavit should sign and return it, see [The Jim Price Show](https://www.thejimpriceshow.com/).

* They reiterated the information from last night that Colorado Secretary of State Jena Griswold raised issues of passwords being leaked, but possibly used that as an excuse to raid the County Clerk's office.

  - She is attempting to stop *all forensic audits* in Colorado. 1,400 petitions were submitted, but she did not allow them to be publicly submitted. She cut off the public testimony that was supposed to be allowed until August 10.

  - Also, Jena Griswold (CO SOS) was the only person in the state who had the passwords, yet used the "password leak" to raid the office yesterday.

* A spokesman mentioned that Antrim County saw similar issues with the deletion of log files

* Timothy Ramthun (59th Assembly District in Wisconsin) wants to get action moving forward on these issues. He says that on June 21st, Dominion asked to update *all* the machines. The vote passed 4-to-2. "I want conspiracy out, we want fact in. We want closure. Do we have integrity in our process?"

* He asks: Is Dominion scrubbing our machines? How do we prevent this happening?

  The suggested response is to get a forensic image of every server *before* they update. Don't let them in until you have it. It should theoretically take 1 day to forensically image the servers. The security technician said he has some of these devices in his suitcase right now.

* 2 Dominion employees apparently work at the Sacramento voter offices, California. This was confirmed at a board of supervisors meeting.

* A state representative from Iowa confirms that voting machine vendor ES&S were "definitely running the show", not the county staff.

* It takes 2-3 hours to forensically image a machine. You would normally do a few simultaneously. Duplicators cost around $10k, for high-speed ones. Plus you need SSD hard drives to store the images on, an additional cost.

* There's a mention that some voter rolls are full of weird birthdates and spelling of names. These should be audited.

* Legislators should look at the certified printer, programmer, maintenance personnel, and bulk mailing company, and see them as partners with your electronic voting machines. They often have a temporary office in elections department. They may need auditing too.

> I think our third-party vendors are running our elections, not our trusted officials. This has been happening a long time. People have been trying to talk about it, but have been hushed and blocked and cancelled, and people try to discredit everything."
> 
> <small>--- Unknown</small>

* Sonny Borrelli says to remember the great patriots that gave blood, sweat and tears for our country.

* In Utah, all government agencies are required to have outside auditing *except* the education system and voting system. Why is no outside auditing mandated?

* A spokesman says: "We must go back to systems where everyone can verify [the] paper ballots and ID".

> We're trading fair elections for convenience [ease of counting/reporting]."

* The cyber security team leave the stage and Seth Keshel is joined by Pat Colbeck, David Clements, Phil Waldron, and Joe Oltmann.

  <div class="responsive-iframe"><iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vifv78/?pub=m4ux1" frameborder="0" allowfullscreen></iframe></div>

  - Some are rightly alarmed at what we've seen.
  - It was the first time the techs have looked through these files. This is the beginning of 2-3 week process. They'll start by looking at known points. They may initially look at the differences between two images. Look at anomalies.
  - But it seems clear that **in Mesa County, Dominion has actively erased forensic evidence**.

* Joe Oltmann gives his background and summarizes the evidence we are seeing:
  - Sometimes when people are blocking your investigations you need to clear the chaos, and talk about individual situations.
  - This so-called update was called "the trusted build", by Dominion.
  - Were the logs preserved? They look like they were deleted by a Dominion employee.
  - Was Dominion communicating with an outside system? (Unsure)
  - **Once you breach the public trust, that alone should trigger an audit.**

* Joe Oltmann also describes:
  - Being sued in Colorado
  - Dealing with the "deep state" mob, "it's not a joke!"
  - How a Supreme Court judge is under investigation for $2 million contract. This news came out 6-8 weeks ago.
  - Somehow his legal case was moved to Denver, the "bluest" place in the state, where "they" (his opponents) could control it
  - They're ignoring the [anti-SLAPP law](https://www.rcfp.org/resources/anti-slapp-laws/) completely.
  - Behind this is Eric Coomer, Vice President of Dominion Voting Systems, who owns the patents on the adjudication methods used in the election ([read more about Dominion](/in-detail/dominion-voting-machines/))
  - Unexpectedly, the judge ruled to grant Joe's opponents some discovery/deposition

* "A large part of the fight is to actually get the [election] data." - Col Phil Waldron

* Passwords for Dominion have been leaked online, for about 5 years. Dominion staff appear to be using the same company passwords also for their email accounts.

* Laws around elections need to be sured up with specific, active, definitive language

* David K. Clements says that there are some strange oddities about the voting machine contracts: 

  - That county clerks must not cooperate with outside sources. They need to "Let them handle it". Dominion even offer to pay the lawyer fees, strangely. They are protecting the crime syndicate.
  - They use the legal defense of protecting their "intellectual property" (IP) but this is often a way of hiding their malfeasance.
  - They cannot be allowed to enforce the result on the American people

* Question: Under what circumstances can someone ignore a subpoena?

  David says: The Senate has authority and power, and can go to judge to compel behavior. Extensions can be granted if needed, but up to a point. The judge can (and should) then issue a fine or send people to jail.

  Joe says: (unfortunately) it might be possible that the judiciary are compromised (as he is now witnessing).

<div class="info" markdown=1>

### Key Tasks for Legislators:

  - Get a copy of your Dominion / ES&S contract

  - Ask clarifying questions about their maintenance activities and get an exact itemized list

  - Review John Droz Jnr's great, [comprehensive report](https://election-integrity.info/Post_Election_Audits.pdf) on what a full forensic audit is. It breaks down the issues of canvassing, machines, ballots, etc.

</div>

* Remember it's not *just* Dominion that exhibit these issues. It's deeper than that. HART, ES&S, Smartmatic... *ALL MACHINES*.

* See also the America First Audit Chat Groups ([see the main group](https://t.me/AmericaFirstAudits), which also links to state-specific groups)

* Dr. Shiva's presentation (*summary still to come*)

  <div class="responsive-iframe"><iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vieywg/?pub=m4ux1" frameborder="0" allowfullscreen></iframe></div>

* Seth Keshel's presentation: Behind the Election Corruption Curtain  
  (*summary still to come*, but based generally on his [10 Indisputable Facts](https://www.westernjournal.com/gen-flynn-exclusive-10-indisputable-facts-2020-election-argue-audits/) article)

  <div class="responsive-iframe"><iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/viewkr/?pub=m4ux1" frameborder="0" allowfullscreen></iframe></div>

* Draza Smith's presentation (*summary and video still to come*, but probably based closely on [Election Fraud on Cruise Control](https://rumble.com/vkgtqh-draza-smith-election-fraud-on-cruise-control.html))

  <span class="pill">Video coming soon</span>

* Mike Lindell went through all states and listed the number of votes that were affected by cyber attacks (not including other types of fraud).  [View the 50min video where he goes through every state, on FrankSpeech.com](https://home.frankspeech.com/video/real-state-state-numbers-president-trumps-victory).

  (Editor's note: It's still not fully clear how exactly these numbers were calculated, as no packet capture data has yet been released.)


#### Updates from State Legislators

Several state legislators took the stage one by one to give a brief update on their attempts at tackling election fraud. [Watch video here](https://rumble.com/vl0pki-mike-lindells-cyber-symposium-day-2-live-continued.html), from 2hr 55min mark.

* Theresa Manzella from Montana:

    - Who makes law? Not judges, not governors. The Constitution says that legislative power resides in Congress.
    - She'd like to see a trained-up audit team
    - They are going to need 76 people in congress to create the election process and audit process
    - Getting past the RINOs is the challenge. She hopes to see conservative Christians rise up to help.

* Steve from Michigan:

    - Secretary of State was mandating 6ft distance for poll-watchers. Steve filed a lawsuit against this and won. But on election day, it largely wasn't followed anyway.
    - Absentees ballots were sent out en-masse, signatures weren't checked
    - Dozens of new election integrity bills are coming out, but they still haven't found out exactly what was wrong with the last one!
    - "If there's really no fraud, then prove it"

* Vicki Craft, Washington state:

    - Seen a lot of challenges
    - No reply from Secretary of State to her emailed questions
    - Mail-in ballots, early voting, harvesting... these have been done for years
    - Governor still has single authority (using their emergency powers)
    - As an elected official, she was kicked out of her own Capitol!
    - "We're in trouble, and we need the people. The people will turn it around and take it back."
    - Wants a forensic audit checklist, and model legislation
    - Talked with Wendy Rogers, and attempting to work together with her

* Steve [Unknown surname], from Washington state:

    - This is not over.
    - Full-forensic audit is planned for in next year's budget
    - Also other planning in the works
    - Came to Symposium to find the truth
    - Hold the line. "Stop with the defence, and go on the offence."
    - Pursue the truth, overcome fear, and let's restore the United States of America together

* Robert Sutherland, Washington state:

    - Called for public hearings regarding 2020 elections
    - This Sunday, will listen to public testimony
    - Affidavits coming in now
    - If testimony is compelling, will be asking the public if they have faith in the election
    - Will call for full forensic audit
    - The evidence is becoming overwhelming

* David Eastman, Alaska state house:

    > The strength of the Constitution lies entirely in the determination of each citizen to defend it. Only if every single citizen feels duty bound to do his share in this defense are the constitutional rights secure." 
    >
    > <small>― Albert Einstein</small>

    - In Alaska, the election process has been given over to a government monopoly
    - 2 weeks before election, their database was hacked
    - Government decided not to let anyone know about it. They waited until after the election was certified!
    - 100,000 citizens had their data breached

* Troy Smith, Oregon, representing Kim Thatcher

    - In Oregon, they are wanting to look into the concerns
    - Constitution says that state legislatures have their role in election, but it's currently Democrat-controlled. We really need a "We the people" movement.
    - We need to open our eyes, be open-minded, but with healthy scepticism
    - He's really thinking about the chain of custody and the vulnerabilities
    - The codes and PCAPs are over their heads, and it's too hard to look into
    - The problem: they have to *prove* there was a murder before they can even get an investigation
    - It's *reasonable* to have transparency

* Mark Alliegro, New Hampshire

    - New Hampshire was one of the first states to take action against Colonialism. First state to form government and have independent constitution.
    - He didn't need convincing of a stolen election. On election night, it was very obvious that it was stolen. He came to go home with an action plan.
    - What happened in Windham? It was an attempt to steal Windham, but a cheat-proof majority was built.
    - An audit was accomplished, but it went out of their control and into the hands of the wrong people. You must make sure you control every aspect of the audit! You must get the keys to every locked room.
    - "We're going to win"
    - "The day the yoke of Rome falls, will be the day of a great rejoicing!"
    
      <div class="responsive-iframe"><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/AJVmwyHeIXo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>


*More to come soon.*


## Day 3 Summary

The main highlights of day 3 were:

* Announcements from Mike Lindell and Col. Phil Waldron that:

  - Mike was physically attacked 10:30pm the previous night. It was [later reported](https://www.thegatewaypundit.com/2021/08/breaking-exclusive-mike-lindell-reveals-details-attack-3-men-lobby-sioux-falls-hotel/) that 3 men approached wanting a photo with Mike. One of them tried to either pierce, stab or tightly pinch a pressure point under Mike's arm, either to create a reaction for their camera, or as a threatening act.

  - The man that attended along with Tina Peters from Colorado had his home raided last night, while his wife and kids were there, and they confiscated a lot of equipment.

  - Mike and Phil are not able to release the promised packet captures due to "finding a credible threat within the data streams, a poison pill inserted into the data", and not wanting to release that to the public. It's not clear whether this "poison pill" was some kind of intentional corruption of the data or some form of malware, or possibly just the presence of government passwords and email addresses embedded inside the IP packets that were deemed as not ready for public release.

  - The information is being handled in accordance with the Cybersecurity Act of 2015, [Presidential Executive Order (EO) 13636](https://obamawhitehouse.archives.gov/issues/foreign-policy/cybersecurity/eo-13636) (by Obama in 2013), [Presidential Policy Directive 21](https://obamawhitehouse.archives.gov/the-press-office/2013/02/12/presidential-policy-directive-critical-infrastructure-security-and-resil/) (also Obama in 2013), and several other executive orders. These orders were not given recently, so it appears as though they are simply following the law regarding the reporting of credible cybersecurity threats.

  - An important update from the "red team" which has been in the crowd, detecting those who have infiltrated to cause trouble at the symposium. Several people have been detected, and some asked to leave. There's been attacks from multiple angles. Also intruders in the event trying to hack information from people attending as well as compromise the activities of the event.

  - The data has been handed over to these authorities, namely an [Information Sharing & Analysis Organization](https://www.cisa.gov/frequently-asked-questions-about-information-sharing-and-analysis-organizations-isaos) (ISAO, although it wasn't named which one) for them to process it on behalf of the US Government. "We'll see what they do with it."

  <div class="responsive-iframe"><iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vig0cr/?pub=m4ux1" frameborder="0" allowfullscreen></iframe></div>

  These announcements still leave many unanswered questions, so we're hoping more will be explained soon.

* An excellent presentation from Arizona State Representatives: Mark Finchem, Sonny Borrelli, and Wendy Rogers. They walk through the entire process of responding to early election fraud reports and moving through with the forensic audit despite many obstructions from Maricopa County, the media and others.

  <div class="responsive-iframe"><iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vifrdz/?pub=m4ux1" frameborder="0" allowfullscreen></iframe></div>

* Lawyer Alan Dershowitz shares about why he's supporting Mike Lindell in the lawsuit battle with Dominion Voting Systems

* Cyber experts Draza Smith and Mark Cook, and Retired US Air Force Colonel Shawn Smith hosted an excellent technical session that demonstrated an actual vote tabulation system and how easy it was for an attacker to alter the vote counts. They discuss the *many, many* vulnerabilities and why computerized elections are *not* secure. States need to return to paper-based voting (with the secure ballots mentioned by Mark Finchem in the earlier presentation). 

  <iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vils16/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

  To see the entire presentation, view the entire day 3 recording, [here](https://rumble.com/vl1owp--watch-live-patriot-news-outlet-mike-lindells-cyber-symposium-day-3-8122021.html).

* Joe Oltmann did a deeper dive into how Dominion is connected with both Serbia and China

  <div class="responsive-iframe"><iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vigioz/?pub=m4ux1" frameborder="0" allowfullscreen></iframe></div>

  [View slides](https://tsarizm.com/news/balkans/2021/08/12/oltmann-report-dominion-serbian-technology-with-chinese-characteristics/)

* Professor David K. Clements presented on "The Vote Trafficking Parable"

  <div class="responsive-iframe"><iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vigk6e/?pub=m4ux1" frameborder="0" allowfullscreen></iframe></div>

  [View a summary of his session](../cyber-symposium/david-clements-vote-trafficking-parable/)

*Further details and presentation notes coming soon.*

[Watch the entire day 3 recording here](https://rumble.com/vl1owp--watch-live-patriot-news-outlet-mike-lindells-cyber-symposium-day-3-8122021.html)




## News Feeds

The following Telegram channels were posting frequent updates during the symposium:

[@AmericaFirstAudits](https://t.me/AmericaFirstAudits)  
[@joeoltmann](https://t.me/joeoltmann)  
[@AuditWatch](https://t.me/AuditWatch)  
[@praying_medic](https://t.me/praying_medic)  
[@theprofessorsrecord](https://t.me/theprofessorsrecord) (presenter Professor David K. Clements)  
[@FollowTheData](https://t.me/FollowTheData) (presenter Dr. Doug Frank)  
[@maninamerica](https://t.me/maninamerica)  
[@ElectionHQ2024](https://t.me/ElectionHQ2024)  
[@drawandstrikechannel](https://t.me/drawandstrikechannel)  
[@CodeMonkeyZ](https://t.me/CodeMonkeyZ)  
[@electionevidence](https://t.me/electionevidence)  
[@KanekoaTheGreat](https://t.me/KanekoaTheGreat)  
...along with our own channel [@ElectionFraud20_org](https://t.me/ElectionFraud20_org)

