---
title: Initial Forensic Analysis of Mesa County Machine Images at Cyber Symposium
meta_desc: Key points from the technical forensic analysis session at Cyber Symposium, August 2021.
last_updated: 27 Sep 2021
---

{:.info}
Since this article was published, a full forensic report of the Mesa County machine images has been released, proving that crucial election data was deleted in violation of state and federal laws. See *[Colorado: Forensic Evidence of Dominion Deleting Election Data](/fraud-summary-by-state/colorado/#forensic-evidence-of-dominion-deleting-election-data)*.

*From August 11, 2021*

During Mike Lindell's Cyber Symposium, several cybersecurity experts were given access to two forensic images from a Dominion voting machine, in Mesa County, Colorado. One image was from an earlier date, and another image was apparently from the same machine on a later date, *after* Dominion had performed a "system update" which appears to have wiped 90% of the log files and election data from the system.

Note that this session was very introductory analysis as the technicians had not had a chance to review the data prior to the session. It was all happening "live". More results are expected in the coming weeks.

BitTorrent links for downloading the images are listed below.

----

The entire session went for several hours, although stalled a couple of times due to technical issues. It begins in the video below, around the 2hr 4min mark:

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vids0a/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

Continues in this video, around 1hr 44min mark:

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vie2fk/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

* Ron Watkins ([CodeMonkeyZ](https://t.me/CodeMonkeyZ)) joins the discussion remotely, although amidst several technical difficulties. They examine an election system drive image from Mesa County, Colorado, an election management system (EMS), which behaves like a mainframe for the voting system. They have two images: an earlier one that contains data and logs dating back to 2019, then another image that was taken after a "system update" whereby data is missing. All event logs and error logs are gone!

  What the technicians are trying to determine: "Why did they make these changes?"

  The previous data was "***zeroed out and deleted***". The law of 22-month data retention is not happening. This appears to be a common issue.

  Every step should be completely tracked and made transparent. Hardly anyone can see (currently) what happens between when they vote and when it is stored and counted.


* These systems should not be proprietary! Election systems are not covered by NIST standards. Why not?

* The server image appears to contain a file created in 2016 and has been vulnerable since 2017, with a remote code execution (RCE) vulnerability. Voting systems containing this vulnerability were placed online. In total, he found 53 files that are vulnerable with a score of 7 or above on CVE chart - remote code executions.

* When 3 security researchers reported the security issues to the vendor (possibly Dominion, or ES&S, or other), instead of the vulnerabilities being fixed, they were sent "Cease and Desist" letters.

* Question is asked: How do we prove that machines were connected to the internet?
  - This can be done by creating a forensic image of a machine (prior to the logs being wiped, as they have in some other cases)
  - Otherwise it's possible to capture the traffic live, as it's happening, for which they have some data


* Apparently the security experts presenting are having issues with their sharing platform being currently hacked, so they're struggling to present


- The technical presenters discover a VBS file in the `inetpub` folder that appears to be from 1997-1999 and contains commands to create processes (execute a program). This might be a  concerning discovery.

- They also look at log files that seem to show remote connects and HTTP requests.

- They also discover a "remote file manager", which is a potential security vulnerability, as it may allow a remote attacker to upload/download/change files on the machine.

- A "RankChoiceStyle" file. Does Mesa County use rank choice voting? If not, why does this file reference RankChoiceStyle? Mesa County confirms that they do not use it, but have been considering it for future use.

- The log files show HTTP POST requests logged on election day, with a `200 OK` response indicating it succeeded. Filename `/EmsApplicationServer/RemoteFileManager.soap`. This may be a normal part of the closed-network communication of the EMS server, or possibly contain a vulnerability -- unconfirmed.

- *Editor's note:* the following Maricopa County EMS network diagram shows how it's supposed to be connected: <https://recorder.maricopa.gov/justthefacts/pdf/Maricopa%20EMS%20Diagram%20V2.pdf>

    An EMS server (which it appears to be what is being examined) is connected to ballot tabulation machines and adjudication workstations, plus scanners and printers. These are all over a LOCAL network, but are not supposed to be internet connected. We can't tell if it's internet-connected at this point.

- Regardless of internet access, it seems that the issue that they're trying to flag is that there were a lot of log files present on one date, and a lot of log files were gone after Dominion performed their "update".

    "The logs folder is almost entirely GONE. At least 3 years of log files have been wiped. After the update, there's only 3!"

- Someone asks: "What about the deleted files, can they be accessed? Have the techs tried a file recovery yet?" No, since today's the first time they've seen the data, it has not been attempted yet.

- It appears as though when Dominion performed this so-called "system update" they may have decided to install a complete new disk image rather than just a "patch" update. This does seem fairly suspicious. If they wanted to cover their tracks this would be an excellent method and convenient excuse for doing so. It *appears* (although not confirmed) that they performed a full-disk image.

    If there was any intrusion or malfeasance involved, this is the exact area of the disk that would be manipulated. 

- They ask: "Why would they perform a full-disk image during the recent update, but not on earlier updates?" That is particularly curious. The earlier log files are dated back 3 years, so previous updates did not appear to destroy them.

- Ron looked into the adjudication client, and the configuration file and it's parameters. It wasn't clear from the presentation, but it's possible that some or all of the adjudication client was removed in the "update".

- Officials need to ask: "Was the machine certified after the update?" If not, it is being run illegally on uncertified machines.

- "There's so much smoke here. There must be a fire. Something is wrong. And they're blocking every attempt at figuring out what is wrong."

- A question was asked about whether the team was running a full virtual machine (VM) or just looking at a disk image. The image is currently just being explored and not "mounted" or running. As such they can't run any programs (yet). They might, however, be able to explore the Windows registry if they exported the files and opened them in another tool or editor.

- They discover that there was previously 2 disk partitions and now there are 4.

- There were claims that a machine like this appears open to having additional operating systems installed, creating a back-door entry into the system

- Strangely Dominion seem to have deleted the old SQL Server databases in the second image, meaning most (or all) of the election data may be missing from the second one.

- They find a script (a Windows batch file) named "dehardening" which appears to reduce the security on the system for some (yet unknown) reason. The script was dated 10-19-2020, a couple of weeks prior to the election.

    A dehardening script would normally exist alongside a hardening script too (since you normally reduce a system's security for a brief time only to apply some kind of upgrade). It's quite unusual to not have a combination of scripts, or to leave a system unhardended.

- It's clear that there needs to be a detailed cyber forensic audit of these servers.

- It was Dominion employees who visited the county -- and they were the ones who coordinated the "updates" of the machines

- According to the whistleblower, Dominion was visiting *every county* in late spring / early summer

- They come across a file `Remove restrictions.bat` - that sounds bad.

- Someone finds an "adjudication key" file present on the Windows desktop (for which user was not clear, but it seems very insecure).

    Editor's note: Researchers can obtain the 17GB disk images via the following torrent magnet links (you will need a Bittorrent client).

    [magnet:?xt=urn:btih:dc654b50ec08a8ad5d8f6275f9cd4fcae29686c1&dn=CnuDA4EHJS0glXNC.zip&tr=udp%3a%2f%2ftracker.openbittorrent.com%3a80%2fannounce&tr=udp%3a%2f%2ftracker.opentrackr.org%3a1337%2fannounce](magnet:?xt=urn:btih:dc654b50ec08a8ad5d8f6275f9cd4fcae29686c1&dn=CnuDA4EHJS0glXNC.zip&tr=udp%3a%2f%2ftracker.openbittorrent.com%3a80%2fannounce&tr=udp%3a%2f%2ftracker.opentrackr.org%3a1337%2fannounce)

    [magnet:?xt=urn:btih:ef534e78bbe71b3908ccf074d6d40077a3a63074&dn=ic9WLQaUKTRWV2Sv.zip&tr=udp%3a%2f%2ftracker.openbittorrent.com%3a80%2fannounce&tr=udp%3a%2f%2ftracker.opentrackr.org%3a1337%2fannounce](magnet:?xt=urn:btih:ef534e78bbe71b3908ccf074d6d40077a3a63074&dn=ic9WLQaUKTRWV2Sv.zip&tr=udp%3a%2f%2ftracker.openbittorrent.com%3a80%2fannounce&tr=udp%3a%2f%2ftracker.opentrackr.org%3a1337%2fannounce)


* During the presentation Ron Watkins receives a call from his lawyer and makes a strange announcement that "Conan James Hayes took physical hard drives from Mesa County, without authorization from County Clerk. Return them to the clerk."

  Tina Peters, Mesa County Clerk tells the audience that no physical hard drives were taken, well... not unless they were removed yesterday while she was travelling interstate.