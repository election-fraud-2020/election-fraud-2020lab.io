---
title: Dominion Voting Machines
breadcrumb: "[Further Articles](#colophon) › "
faq_title: Were the Dominion voting machines involved in election irregularities?
last_updated: 15 Feb 2022
comments_enabled: true
---

{% include toc %}

According to [Verified Voters](https://verifiedvoting.org/verifier/#mode/visualization/year/2020), 25.1% of US voters were in a region containing Dominion voting equipment.

As a potential avenue for fraud in the US 2020 Presidential Election, researchers have raised the following concerns about Dominion:

* A 2019 report by Texas's Attorney General recommended that Dominion machines were fragile, error-prone, extremely complex to install, and should not be used [^8]

* Reports of Dominion's VP and Director of Security and Strategy, Eric Coomer, telling Antifa activists *"Don't worry about the election, Trump's not gonna win. I made f**king sure of that!"* Coomer showed a history of strong anti-Trump and anti-American sentiment. He also held the patents for "adjudication" used by Dominion Voting Systems, which defines the process for how an unclear ballot is judged. [^11]

  📺 [Watch Joe Oltmann's interview](https://rumble.com/vfudkf-joe-oltmann-exposes-dominion-votings-eric-coomer-ties-to-antifa-skinheads-a.html) about Eric Coomer

  📰 Further in-depth investigative articles about this are also on AsheInAmerica.com [here](https://asheinamerica.com/2021/08/26/the-ultimate-gaslight-susan-dominus-eric-coomer-the-mainstream-fairy-tales-of-the-new-york-times/) and [here](https://asheinamerica.com/2021/09/04/the-ultimate-gaslight-2-what-do-eric-joe-vodka-nance-the-kraken-all-have-in-common/). *Recommended reading.*

* The way in which they permit tampering and overriding of results. [^3] This was clearly demonstrated on video. [^15] 

* A forensic audit of the Antrim County vote tabulation found that the Dominion system had an astonishing error rate of 68 percent. By way of comparison, the Federal Election Committee requires that election systems must have an error rate no larger than 0.0008 percent. [^4]

* A [forensic report](/fraud-summary-by-state/colorado/#forensic-evidence-of-dominion-deleting-election-data) in Mesa County Colorado proved that a "system update" applied by Dominion after the election wiped dozens of log files, databases and other election data from their machines, in breach of state and federal laws

* Dominion's Democracy Suite machines typically use "commercial off-the-shelf" (COTS) hardware which has no special security hardening [^14]

* Connection to the internet/wifi. [^1] Patrick Colbeck's affidavit cites evidence that internet connectivity was present during voting in Detroit, contradicting Dominion CEO testimony, given under oath, to Congress. [^7] System manuals also explicitly refer to internet and ethernet connectivity. [^7]

* Ownership issues -- potentially being owned by foreign entities, outside the US who may have motive to sway the results of an election

* Potential connections to Smartmatic (Sequoia) and ES&S, all of which may have been influenced or undermined by partisan bias. [^9]

* The contracts that Dominion makes with US counties have restrictive rules such that the county must: [^13]
  
  - Resist Freedom of Information (FOIA) requests wherever possible
  - Inform Dominion whenever these occur
  - Not reverse engineer or even *analyze* the software -- something that would be necessary when tally errors occur

* According to Professor David K. Clements, Dominion's legal contracts specify that counties must not cooperate with outside organizations, including auditors. They need to “Let them handle it”. Dominion, strangely, even offer to pay the legal fees in such scenarios. [^12] 

* Dominion repeatedly refuse in-depth audits based on the legal defense of "protecting their intellectual property" (IP). They've been accused of using this as a way of hiding their malfeasance. [^12]

* Counties that started using Dominion machines, on average, saw a 2 to 3 percentage point vote migration toward the Democrat presidential candidate and away from the Republican opponent even when adjusted for demographic variances. [^5]

* Emerging reports that Dominion, through their lobbyist Brownstein Hyatt Farber Schreck, have made over 30 donations to Colorado Secretary of State Jena Griswold who has been implicated in election malpractice including deletion of data and log files from the 2020 election in violation of both state and federal laws for retention of election-related materials. [^19] [^19d] They also donated to Colorado Attorney General Phil Weiser's re-election campaign. [^19d]

* Democrats Elizabeth Warren and Amy Klovich expressed concern about the machines: lack of transparency, security, and the ability to change votes. [^2]

* Dominion CEO John Poulos testified under oath that Ranked Choice Voting module which allows fractional voting was *not* enabled in Antrim County.  This testimony conflicts with forensic analysis findings. Fractional votes were evident in a data stream from Dominion servers to Edison servers. [^7]

* Encryption keys stolen [^7]

* Dominion Systems passwords were seen to be available on the Dark Web. [^7] But worse than that, the passwords between Antrim County, Michigan, and Maricopa County, Arizona were not only shared between multiple users, but followed the same basic structure, meaning that knowing the passwords in one state would allow you to reasonably guess the passwords for another state. [^doug3]

* Other audit logs deleted [^7]

* Original ballot images deleted [^7]

* Alleged reports of a large, coordinated hacking attempt in Italy with the aim of modifying election results (nicknamed "ItalyGate") [^10]

> I test [the security of] applications for a living --- I've probably tested 3 or 4 thousand applications --- and [the Dominion software I audited in Antrim County] was some of the worst software I've seen, in the way it was chosen to be designed. Just stupid, bad programming practices." 
>
> <small>--- Doug Logan, Cyber Ninjas CEO, Maricopa County Forensic Audit Lead [^doug1] [^doug2]</small>

[^doug1]: This quote was from Rumble Video: <https://rumble.com/vnf092-cyber-ninjas-ceo-tells-congress-to-fork-off-and-joins-us-instead.html>, 13min 30sec mark.

[^doug3]: This quote was from Rumble Video: <https://rumble.com/vnf092-cyber-ninjas-ceo-tells-congress-to-fork-off-and-joins-us-instead.html>, 1hr 31min mark.

[^doug2]: View the [Cyber Ninjas Report on Antrim County's Dominion software](https://www.scribd.com/document/530979936/Antrim-County-Michigan-Election-Management-System-Application-Security-Analysis-by-Cyber-Ninjas-040921), released Apr 9, 2021. The original PDF was obtained from [DepernoLaw.com here](https://www.depernolaw.com/all-expert-reports.html).

Dominion has released [statements](https://www.dominionvoting.com/election2020-setting-the-record-straight/) on their website that deny many of the above allegations, however most have not been presented to the courts, under oath, in order to determine the truth.


## Video demonstration of vote switching

The following 7-minute video is one (of several) that demonstrates how the Dominion voting machines can be easily manipulated and corrupted, via direct SQL queries to the database.

<div class="responsive-iframe"><iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vdxt2l/?pub=m4ux1" frameborder="0" allowfullscreen></iframe></div>

A similar demonstration was also run live at [Mike Lindell's Cyber Symposium](/in-detail/cyber-symposium-mike-lindell/), showing how easy the system is to manipulate via SQL queries:

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vils16/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

Matthew DePerno, an attorney involved with the Antrim County, Michigan, Allied Security Operations Group (ASOG) audit, has released [a detailed report](https://electionevidence.com/wp-content/uploads/2021/05/Deperno-Lawsuit.pdf) on how fraudulent actors can utilize this process, and how they likely did during the November 2020 election.

There is also a clear video demonstration of how an election worker, sitting solo in a secluded office can scan ballots multiple times, adjudicate and override votes, all without oversight, and without the actions being logged to them individually. [^15]


## J. Alex Halderman's Report on Dominion Vulnerabilities, July 2021

J. Alex Halderman produced a report on numerous vulnerabilities within Dominion ICX ballot-marking devices (BMDs) as used in [Georgia](/fraud-summary-by-state/georgia/), [Nevada](/fraud-summary-by-state/nevada/), [Louisiana](/fraud-summary-by-state/louisiana/), [Arizona](/fraud-summary-by-state/arizona/), [California](/fraud-summary-by-state/california/), [Colorado](/fraud-summary-by-state/colorado/), [Michigan](/fraud-summary-by-state/michigan/), [Illinois](/fraud-summary-by-state/illinois/), [Kansas](/fraud-summary-by-state/kansas/), [Ohio](/fraud-summary-by-state/ohio/), [Missouri](/fraud-summary-by-state/missouri/), [New Jersey](/fraud-summary-by-state/new-jersey/), [Pennsylvania](/fraud-summary-by-state/pennsylvania/), [Tennessee](/fraud-summary-by-state/tennessee/) and [Washington State](/fraud-summary-by-state/washington/).

Unfortunately in Aug 2021, a Georgia District Court judge sealed the report and prevented it from being released to the public:

{% include tg-embed url="https://t.me/LibertyOverwatchChannel/5276" %}

Then in Feb 2022, the report was suppressed further in a federal court:

{% include tg-embed url="https://t.me/LibertyOverwatchChannel/6471" %}

Here is a short court excerpt that describes the report, and what we know about it so far:

<iframe src="https://frankspeech.com/sites/default/files/2021-12/092121%20Halderman%20Decl..pdf" width="100%" height="600"></iframe>


## Which states used Dominion voting systems during 2020 election?

The following states use Dominion systems (although not all counties within these states do):

![](https://i.stack.imgur.com/dxbYR.png)

<small class="muted">Source: [Politics Stack Exchange](https://politics.stackexchange.com/a/60136)</small>

<!-- New Hampshire, Utah and Vermont might not actually use Dominion. TBC -->

In list form, these states are: [Alaska](/fraud-summary-by-state/alaska/), [Arizona](/fraud-summary-by-state/arizona/), [California](/fraud-summary-by-state/california/), [Colorado](/fraud-summary-by-state/colorado/), [Florida](/fraud-summary-by-state/florida/), [Georgia](/fraud-summary-by-state/georgia/), [Illinois](/fraud-summary-by-state/illinois/), [Iowa](/fraud-summary-by-state/iowa/), [Kansas](/fraud-summary-by-state/kansas/), [Louisiana](/fraud-summary-by-state/louisiana/), [Massachusetts](/fraud-summary-by-state/massachusetts/), [Michigan](/fraud-summary-by-state/michigan/) (in [65 out of 83 counties](https://bkq.639.myftpupload.com/wp-content/uploads/2021/01/Michigan-Voting-Systems-1.pdf)), [Minnesota](/fraud-summary-by-state/minnesota/), [Missouri](/fraud-summary-by-state/missouri/), [Nevada](/fraud-summary-by-state/nevada/), [New Hampshire](/fraud-summary-by-state/new-hampshire/), [New Jersey](/fraud-summary-by-state/new-jersey/), [New Mexico](/fraud-summary-by-state/new-mexico/), [New York](/fraud-summary-by-state/new-york/), [Ohio](/fraud-summary-by-state/ohio/), [Pennsylvania](/fraud-summary-by-state/pennsylvania/), [Tennessee](/fraud-summary-by-state/tennessee/), <!-- [Texas](/fraud-summary-by-state/texas/),--> [Utah](/fraud-summary-by-state/utah/), [Vermont](/fraud-summary-by-state/vermont/), [Virginia](/fraud-summary-by-state/virginia/), [Washington](/fraud-summary-by-state/washington/), & [Wisconsin](/fraud-summary-by-state/wisconsin/). [^6]

A review of 10 key states ([Arizona](https://azsos.gov/sites/default/files/2020_0709_Election_Cycle_Voting_Equipment.pdf), [Colorado](https://www.westword.com/news/former-secretary-of-state-defends-dominion-voting-systems-resultsin-colorado-11847518), [Florida](https://files.floridados.gov/media/704803/voting-systems-in-use-by-county-20210902.pdf), [Georgia](https://sos.ga.gov/securevoting/), [Michigan](https://michigan.maps.arcgis.com/apps/webappviewer/index.html?id=f3d6f6232f4f4ae3b0c74e661b599c2f), [Minnesota](https://sos.minnesota.gov/media/3802/2018-voting-equipment-map.pdf), [Nevada](https://www.nvsos.gov/sos/elections/election-resources/voting-system), [North Carolina](https://www.ncsbe.gov/voting/voting-equipment), [Pennsylvania](https://www.timesonline.com/story/news/2020/11/20/republicans-move-election-audit-grove-says-dominion-hiding/6363261002/) and [Wisconsin](https://elections.wi.gov/elections-voting/voting-equipment/voting-equipment-use)) finds that Dominion systems were used in 351 of 731 counties.

[MSN reports](https://www.msn.com/en-us/news/politics/swing-state-counties-that-used-dominion-voting-machines-mostly-voted-for-trump/ar-BB1bxn03) that Trump won 283 of those counties, 81 percent of the total, while he won 79 percent of the counties that didn’t use Dominion systems. However this statistic hides the fact that Democrats appear to have injected votes in far greater numbers in the most populated counties (they only need to do so in the largest 1 or 2 counties in each state in order to swing the results). Read *[The Battle for the Largest Counties](/in-detail/battle-for-largest-counties/)* for more on this phenomenon.

## Further Background

The article *[Colorado: The Election Fraud Test Kitchen](https://hollyataltitude.com/2021/01/27/colorado-the-election-fraud-test-kitchen/)* by Holly at Altitude (Jan 27, 2021) covers the history of how Dominion entered the US from Canada, became entrenched in the state of Colorado, and eventually spread to many other states that adopted Colorado's broken-but-well-branded strategy.

<div class="info" markdown=1>
Several additional reports on Dominion and other voting machine companies have been collated by researchers at the link below.

[View Reports](https://e1.pcloud.link/publink/show?code=kZR8cJZcJz7woyCh9uqxmYCLP57a5pqzf6V){:.button}
</div>


### Footnotes & References

[^1]: FlashPoint: Voting Machines Exposed, <https://youtu.be/ViJt9gvM1wk?t=1410>. Unfortunately, this video has been censored by YouTube, and no longer appears to be accessible via this link. We will update the address once we have a new link.

[^2]: FlashPoint: Voting Machines Exposed, <https://youtu.be/ViJt9gvM1wk?t=1904>. Unfortunately, this video has been censored by YouTube, and no longer appears to be accessible via this link. We will update the address once we have a new link.

[^3]: "The Current State Of Our Country", Rudy Giuliani, Trump Legal Team, <https://youtu.be/NAzCECx8XeE?t=1320> (around 22min mark). Unfortunately, this video has been censored by YouTube, and no longer appears to be accessible via this link. We will update the address once we have a new link.

[^4]: “[The Immaculate Deception: Six Key Dimensions of Election Irregularities](https://bannonswarroom.com/wp-content/uploads/2020/12/The-Immaculate-Deception-12.15.20-1.pdf)", Peter Navarro, Director of the Office of Trade and Manufacturing Policy and Assistant to the President.

    Dominion [refutes these claims](https://www.dominionvoting.com/election2020-setting-the-record-straight/).
    
    See also Navarro's follow-up reports:  
    "[The Art of the Steal](https://www.priestsforlife.org/elections/pdf/navarro-art-of-the-Steal.pdf)" (Volume 2 of 3)  
    "[Yes, President Trump Won](https://mdrnrepublican.files.wordpress.com/2021/04/thenavarroreportvolumeiiifinal1.13.21-0001.pdf)" (Volume 3 of 3)

[^5]: [Fraud Analyst Finds Average of 2 to 3 Percent Shift for Biden in Counties That Used Dominion](https://www.theepochtimes.com/fraud-analyst-flags-pro-biden-shift-in-counties-that-used-dominion_3619566.html), The Epoch Times, December 17, 2020

[^6]: StackExchange: [What states used voting equipment by Dominion Voting Systems for the 2020 election (if any)?](https://politics.stackexchange.com/questions/60135/what-states-used-voting-equipment-by-dominion-voting-systems-for-the-2020-electi)

[^7]: Patrick J. Colbeck, former elected Michigan Senator (2011-2018), and former candidate for governor in Michigan, released a report on election fraud and irregularities that he either personally witnessed (in some cases) or has become aware of: "[The Case for Decertification of Michigan Election Results](https://letsfixstuff.org/2021/01/the-case-for-decertification-of-michigan-election-results/)"

[^8]: "[Voting System Examination Dominion Voting Systems Democracy Suite 5.5-A](https://www.scribd.com/document/483722661/TX-Dominion-Report)", Prepared for the Secretary of State of Texas, by James Sneeringer, Ph.D. Designee of the Attorney General.

[^9]: Patrick Byrne, traditionally a Libertarian, developed a team of computer specialists to investigate election integrity issues and has written extensively about them on his website *[DeepCapture.com](https://deepcapture.com)*. See "[Evidence Grows: ’20 Election Was Rigged](https://www.deepcapture.com/2020/11/election-2020-was-rigged-the-evidence/)" (Nov 24, 2020) for more background on Dominion, as well as his book "[The Deep Rig](https://www.amazon.com/Deep-Rig-Election-friends-integrity-ebook/dp/B08X1Z9FHP)".

[^10]: Rumble Video: "[ITALY STOLE U.S. ELECTION – HERE’S WHY THEY DID IT](https://rumble.com/vcz2lr-italy-stole-u.s.-election-heres-why-they-did-it.html)", Jan 18, 2021.

[^11]: The Professor's Record with David K. Clements: "[Joe Oltmann EXPOSES Dominion Voting's Eric Coomer --- Ties to Antifa, Skinheads, and Election LIES](https://rumble.com/vfudkf-joe-oltmann-exposes-dominion-votings-eric-coomer-ties-to-antifa-skinheads-a.html)", Rumble video, April 20, 2021.

[^12]: This is according to David K. Clements, as shared at [Mike Lindell's Cyber Symposium](/in-detail/cyber-symposium-mike-lindell/) on August 10-12, 2021, as part of a panel, joined by Joe Oltmann, Phil Waldron and Patrick Colbeck. View the panel discussion on [Rumble.com](https://rumble.com/vl21ci-cyber-symposium-joe-oltmann-josh-merritt-phil-waldron-david-clements-and-pa.html).

[^13]: [Read the full Dominion contract](https://edgarcountywatchdogs.com/wp-content/uploads/2021/01/Dominion-Software-contract-1.pdf). As reported by Illinois Leaks: "[DuPage County Clerk signed anti-transparency contract with Dominion Voting Systems - Dominion encouraged County to resist disclosure of information](https://edgarcountywatchdogs.com/2021/01/dupage-county-clerk-signed-anti-transparency-contract-with-dominion-voting-systems-dominion-encouraged-county-to-resist-disclosure-of-information/)", Jan 15, 2021.

[^14]: U.S. Election Integrity Project: [Briefing Slides](https://useipdotus.files.wordpress.com/2021/09/useip-co-election-integrity-briefing_2.pdf), April 2021, page 16

[^15]: An election supervisor in Coffee County, Georgia demonstrates how the counting, tabulation and adjudication process works, and how easy it is to (a) scan the same ballots multiple times, and (b) modify the votes as they come through the scanner. 

    [Dominion Voting Machine Flaws -- 2020 Election Coffee County, Georgia Video Part 1](https://www.youtube.com/watch?v=46CAKyyObls)  
    [Dominion Voting Machine Flaws -- 2020 Election Coffee County, Georgia Video Part 2](https://www.youtube.com/watch?v=ijjwS6h-PyU)

    Observers in the video can be overheard saying "Why would anyone approve such a system?" and "It works fine for honest people; but those who wish to do wrong though, certainly can."

[^18]: Brannon Howse Live: "[Exclusive: FBI Raids Home of County Clerk Tina Peters in Attempt to Cover Up Voter Crimes?](https://frankspeech.com/tv/video/exclusive-fbi-raids-home-county-clerk-tina-peters-attempt-cover-voter-crimes)", Nov 16, 2021. Tina Peters and Shawn Smith are introduced at 14min 40sec mark.

[^19]: See item [^18], at 4:30min mark. Additionally, Tina Peters reports that Jena Griswold is also funded by George Soros, see [^20] at 42min mark.

[^19d]: See item [^18], at 54min 40sec mark.

[^20]: Bannon's War Room: "[FBI Attempts To Intimidate Moms, Dads, and Gold Star Mothers](https://frankspeech.com/tv/video/fbi-attempts-intimidate-moms-dads-and-gold-star-mothers), Nov 17, 2021. Tina Peters is interviewed from 41min 15sec mark.

[^21]: <https://t.me/LibertyOverwatchChannel/4453>