---
title: Michigan Election Analysis by Seth Keshel
state: Michigan
abbr: mi
last_updated: 23 May 2023
---

{% include see_main_article %}

{% include seth_report_intro.html %}

{% include toc %}


<div class="responsive-iframe"><iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vhpn03/?pub=m4ux1" frameborder="0" allowfullscreen></iframe></div>

<div class="responsive-iframe"><iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vi89om/?pub=m4ux1" frameborder="0" allowfullscreen></iframe></div>

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

{% include seth_state_details.html %}


Michigan tracks [Pennsylvania](../pennsylvania/) in terms of voter tendencies; since 2008, declining Democrat participation and increasing Republican, especially under Trump.  Obama lost 303k in 2012, Clinton another 296k.  Trump gained 370k amidst this coalition shift in a shrinking state, only to be lapped by Biden with 535k after no new net positive Democrat votes in 12 years.

As a general rule, states in population decline with a massive coalition shift (party switching) going on are not going to show massive gains for both sides.  The counties in Michigan all track with [Pennsylvania](../pennsylvania/) counties.  Not one clean county here – an easy Trump win as we saw on 11/3 in progress.

If Biden is 527k heavy, Trump margin of victory in Michigan was about 52.9% to 45.4%, or 7.5% overall (373k votes).

**Best targets for audits:** Macomb, Allegan, Bay, Berrien, Calhoun, Clinton, Eaton, Jackson, Kent, Livingston, Monroe, Muskegon, Ottawa, Saginaw, St. Clair

{:.caption}
Source: <https://t.me/CaptainKMapsandStats/79>


{% include seth_worst_counties_for_state heading=true %}


## Stats By County

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table2.jpg)

{:.caption}
Source: <https://t.me/CaptainKMapsandStats/79>



## Detailed Report

{% include pdf_box.html 
    url = "https://radiopatriot.files.wordpress.com/2021/07/keshel-michigan-disparities-1.pdf"
    title = "Improbable Voting Trend Reversals in Michigan"
    description = "by Seth Keshel, MBA"
%}


## Grand Rapids Presentation

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="ElectionHQ2024/576" data-width="100%"></script>

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="ElectionHQ2024/577" data-width="100%"></script>

Also available to [download as a PDF](/images/seth-keshel/mi-grand-rapids-presentation.pdf).


## Allegan County

The following PDF shows a map of which precincts in the county had the most abnormal results in 2020:

{% include tg-embed url="https://t.me/ElectionHQ2024/3563" %}


## Berrien County

{% include tg-embed url="https://t.me/ElectionHQ2024/3599" %}


## Genesee County

{% include tg-embed url="https://t.me/ElectionHQ2024/3438" %}

{% include tg-embed url="https://t.me/ElectionHQ2024/3318" %}


## Ingham County

{% include tg-embed url="https://t.me/ElectionHQ2024/3598" %}


## Jackson County

{% include tg-embed url="https://t.me/ElectionHQ2024/3446" %}


## Kalamazoo County

{% include tg-embed url="https://t.me/ElectionHQ2024/3447" %}


## Kent County

{% include tg-embed url="https://t.me/ElectionHQ2024/3419" %}


## Livingstone County

<iframe width="100%" height="700" src="https://e.pcloud.link/publink/show?code=XZefJFZ7gvMLCOPS2Qo7qlls7LvEjc00tQ7"></iframe>

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="ElectionHQ2024/2434" data-width="100%"></script>


## Macomb County

The following chart shows an odd anomaly whereby instead of the votes for Democrats (blue) and Republicans (red) moving in opposite directions, they both went upward in 2020. Seth predicts that the Democrat numbers should have trended downward based on party registration numbers.

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-summary.jpg)

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="ElectionHQ2024/2111" data-width="100%"></script>

See also [the canvassing findings in Macomb County](/canvassing/#michigan) which show significant issues with votes.


## Muskegon County

{% include tg-embed url="https://t.me/ElectionHQ2024/2596" %}

<!-- Heat map to be embedded here later, when time permits -->

{% include tg-embed url="https://t.me/ElectionHQ2024/2623" %}


## Oakland County

{% include tg-embed url="https://t.me/ElectionHQ2024/3348" %}



## Ottawa County

{% include tg-embed url="https://t.me/ElectionHQ2024/3346" %}


## Washtenaw County

{% include tg-embed url="https://t.me/ElectionHQ2024/3350" %}


## Wayne County

{% include tg-embed url="https://t.me/ElectionHQ2024/3183" %}



{% assign events = site.data.events | where: "state", page.abbr %}
{% include events %}


## Further Updates

{% include tg-embed url="https://t.me/ElectionHQ2024/6065" %}

{% include tg-embed url="https://t.me/ElectionHQ2024/6823" %}


{% include seth_reports_methodology %}

{% include raw_data %}

## Other Reports

* A team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the report "[2020 Presidential Election Startling Vote Spikes](https://election-integrity.info/Vote_Spikes_Report.pdf)" which includes a section on Michigan

* See also the main [Michigan](/fraud-summary-by-state/michigan/) article for more

## Seth's Endorsement of Matthew DePerno for Attorney General

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="ElectionHQ2024/1359" data-width="100%"></script>


{% include canvass/get-involved %}

{% include state_telegram_channels %}
