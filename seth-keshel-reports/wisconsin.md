---
title: Wisconsin Election Analysis by Seth Keshel
state: Wisconsin
abbr: wi
last_updated: 23 May 2023
---

{% include see_main_article %}

{% include seth_report_intro.html %}

{% include toc %}

<div class="responsive-iframe"><iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vhqzcx/?pub=m4ux1" frameborder="0" allowfullscreen></iframe></div>

{:.caption}
[Watch on Rumble](https://rumble.com/vkd5i7-wisconsin-election-audit-debrief-captain-seth-keshel.html)


<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/v1f0dlh/?pub=4" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Watch on Rumble](https://rumble.com/v1hmjhb-episode-53-wisconsin-state-review.html)


![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}


{% include seth_state_details.html %}


Wisconsin is a little swingy with third party and independents, with more play in those numbers than [Pennsylvania](../pennsylvania/) and [Michigan](../michigan/).  Additionally, with a much smaller state, excess vote totals are lower.  Estimating Biden at 139k statewide excess in a state he “won” by 21k, on the strength of a 3:42 AM vote drop on 11/4 of 168,000 ballots going 85/15% for him.

Dane and Milwaukee both appear to be 15k heavy (which is lenient).  The state, mostly within these two counties, had 244k “indefinitely confined” voters due to COVID, 4x the amount they had in the previous election.  This allows a workaround to voter ID.  Trump still trended western Wisconsin, which was key to his 2016 win.

If Biden is 139k heavy, Trump margin in Wisconsin was about 51.0% to 47.2%, or 3.8% overall (118k votes).

Best counties to target for audits:

 St. Croix | (5k excess)
 Outagamie | (6k) 
 Brown | (7k)
 Waukesha | (10k)
 Calumet | (3k)
 Winnebago | (3k)
 Oneida | (2k)

{:.caption}
Source: <https://t.me/CaptainKMapsandStats/76>

{% include seth_worst_counties_for_state heading=true %}


## Stats By County

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table2.jpg)


## Written Reports

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="ElectionHQ2024/490" data-width="100%"></script>

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="ElectionHQ2024/491" data-width="100%"></script>

Or you can download the reports from this [alternate link](https://e.pcloud.link/publink/show?code=kZTApJZ8NOmqAm1Lx8DKbI93McR6F83XEWV#folder=1836308875&tpl=publicfoldergrid).


## Brown County

The following heat-map graphic shows the precincts in Brown County with the most irregularities:

<!-- Heat map to be embedded here later, when time permits -->

{% include tg-embed url="https://t.me/ElectionHQ2024/2760" %}


## Dane County

Seth highlights some strange irregularities with the voter rolls in this Telegram post:

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="ElectionHQ2024/1727" data-width="100%"></script>


## Kenosha County

Seth highlights some further specific issues with voter rolls that may indicate some link to the 2010 Census data, similar to what has been suggested by [Dr. Doug Frank](/dr-frank-reports/):

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="ElectionHQ2024/1755" data-width="100%"></script>


## Milwaukee County

{% include tg-embed url="https://t.me/ElectionHQ2024/4547" %}


## Outagamie County

Outagamie showed numerous anomalies in their voter registration list:

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="ElectionHQ2024/2027" data-width="100%"></script>


## Rock County

The following video shows how Seth discovered 8,000 excess Biden votes, based on a 16-year trend line and identification of populist coalition shift.

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vjlm01/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
Source: [Telegram Post](https://t.me/ElectionHQ2024/1242), Sep 7, 2021


# Sheboygan County

{% include tg-embed url="https://t.me/ElectionHQ2024/4365" %}



## St. Croix County

{:.small}
> I have long believed St. Croix County, would be an ideal target for a conservative county audit in Wisconsin, since Milwaukee, Dane, and Rock would give so much resistance.
> 
> It appears the locals have allowed for a 14-to-5 Democrat-Republican split in the county government, unfortunately.  
> 
> But here is a county that is 5k high for Biden based on registrations and trends, likely tied to Twin Cities activities.
> 
> This county has a Trump trend identifiable due to larger margin than Romney in 2016.  All counties in PA, NC, FL showing that all expanded GOP registration advantage.

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vj8kzm/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
Source: [Telegram Post](https://t.me/ElectionHQ2024/1107), Aug 28, 2021

The following PDF shows a map of which precincts within the county had the most abnormal results:

{% include tg-embed url="https://t.me/ElectionHQ2024/3601" %}


Seth also flagged an issue whereby St. Croix voter rolls have 5,728 voters on the roll with a registration date of 1/1/1918, of which 1,430 are active. 

{% include large_image width="950" url="/images/seth-keshel/wi-roll.jpg" %}

It's possible that this date was used as some kind of default when the date was unknown, but that seems like very poor database management. Passports don't allow an unknown date, drivers licenses don't allow an unknown date, credit cards don't allow an unknown date, so why should voter registrations allow one?


## Seth on the Ramthun Report

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/ceEKCYZaWbY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

{:.caption}
Published Oct 31, 2021


## Concerns with "Indefinitely Confined" Category

Seth provides some stats on the use (and possible abuse) of the "indefinitely confined" category for requesting an absentee ballot:

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="ElectionHQ2024/1810" data-width="100%"></script>


{% assign events = site.data.events | where: 'state', page.abbr %}
{% include events %}


## Further Updates

{% include tg-embed url="https://t.me/ElectionHQ2024/6823" %}

{% include tg-embed url="https://t.me/ElectionHQ2024/6877" %}



{% include seth_reports_methodology %}

{% include raw_data %}

## Other Reports

* A team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the following reports:

  * "[2020 Presidential Election Startling Vote Spikes](https://election-integrity.info/Vote_Spikes_Report.pdf)" which includes a section on Wisconsin

  * "[Milwaukee 2020 Election Analyses](http://wiseenergy.org/Energy/Election/Milwaukee_Voter_Analysis.pdf)"

* [WI 2020 Election Fingerprints](https://wwrkds.net/wp2/new-wi-2020-election-fingerprints-rev-2-0/), by wwrkds

* See also the main [Wisconsin](/fraud-summary-by-state/wisconsin/) article for more

{% include canvass/get-involved %}

{% include state_telegram_channels %}
