---
title: Ohio Election Analysis by Seth Keshel
state: Ohio
abbr: oh
last_updated: 7 Dec 2021
---

{% include see_main_article %}

{% include seth_report_intro.html %}

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table2.jpg)

{% include seth_state_details.html %}


Ohio was a blowout in 2016, higher than most predicted, with the margin slightly lighter this year, but with more votes won by.  This state, regionally, is clean, compared to the cesspools that are MI and PA.  I estimate Biden to be 88k higher than trends, population growth, previous records, and population growth would suggest.

Most curiously (sarcasm), Cuyahoga County (Cleveland) didn’t appear to have nearly the level of interest in candidate Biden as Wayne County ([Michigan](../michigan/)) and nearby Allegheny County ([Pennsylvania](../pennsylvania/)) did.  Biden gained just 18k votes, but with declining voter roll and a solid Trump gain, I still flagged 8k as class yellow.

Franklin is the only red, 15k excess, 19% over Obama ’08 new vote growth with solid Trump gain opposing.  

If Biden is 88k heavy, Trump’s margin would have been 9.7% (54.0-44.3), a win of 564k votes.

Best targets for audits: Butler, Delaware, Licking, Lake, Union, Warren


{:.caption}
Source: <https://t.me/CaptainKMapsandStats/90>

----

{% include seth_worst_counties_for_state heading=true %}

{% include seth_reports_methodology %}

{% include raw_data %}

## Other Reports

* A team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the report "[2020 Presidential Election Startling Vote Spikes](https://election-integrity.info/Vote_Spikes_Report.pdf)" which includes a section on Ohio

{% include canvass/get-involved %}

{% include state_telegram_channels %}
