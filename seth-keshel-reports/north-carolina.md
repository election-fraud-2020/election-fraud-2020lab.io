---
title: North Carolina Election Analysis by Seth Keshel
state: North Carolina
abbr: nc
last_updated: 23 May 2023
---

{% include see_main_article %}

{% include seth_report_intro.html %}

{% include toc %}

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/v1j4m9z/?pub=4" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Watch on Rumble](https://rumble.com/v1lqs6x-episode-93-north-carolina.html)


![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

{% include seth_state_details.html %}

North Carolina drifted another 4% toward Republican registration, but mysteriously, Trump gained a massive 396,000 votes, while Biden gained 495,000 votes, with two previous elections showing 35k (2012) and 11k (2016) vote gains as state moved right.

My estimates suggest minimum of 257k excess Biden votes, slightly more than Roy Cooper’s margin of victory.

If accurate, Trump’s margin in NC would have been 6.3%, or 52.3% to 46.0%, which is in keeping with the registration trend of the past two decades.

**Best targets for audits:** Johnston, Union, Iredell, New Hanover, Lincoln, Onslow, Brunswick, Gaston

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-summary.jpg)

{:.caption}
Source: <https://t.me/CaptainKMapsandStats/51>


{% include seth_worst_counties_for_state heading=true %}


## Stats By County

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table2.jpg)

{:.caption}
Source: <https://t.me/CaptainKMapsandStats/51>



## Brunswick & New Hanover Counties

One on one with Seth Keshel as he answers questions specific to Brunswick County and New Hanover County:

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vl2pet/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>


## Henderson County

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="ElectionHQ2024/899" data-width="100%"></script>

Here's the attached video:

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vihrqd/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

Following Seth's post we've also done some of our own analysis on Henderson County, NC. 

[View the draft report](https://gitlab.com/election-fraud-2020/election-fraud-2020.gitlab.io/-/issues/7){:.button}


## Wake County

The following PDF shows a map of which precincts in the county had the most abnormal results in 2020:

{% include tg-embed url="https://t.me/ElectionHQ2024/3035" %}


## Down Ballot Races

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="ElectionHQ2024/2158" data-width="100%"></script>


## Precinct-Level Data

Seth has produced detailed precinct-level heat maps for several dozen counties in other states but has found it difficult to do so for North and South Carolina:

{% include tg-embed url="https://t.me/ElectionHQ2024/3607" %}



## Possible Voter Roll Manipulation

A number of states, both key competitive states, and not, have machined/trimmed/manipulated their voter rolls for the desired outcomes.  See [New Hampshire](../new-hampshire/), [Alaska](../alaska/), [California](../california/), and [Arizona](../arizona/).

There are other states with bloated rolls, like [Florida](../florida/) and [Texas](../texas/). [Colorado](../colorado/) doesn’t show much movement in the two parties but tons of new registered indies/others, along with [Washington](../washington/) and [Oregon](../oregon/). [Pennsylvania](../pennsylvania/) and **North Carolina** appear to have let their party rolls run clean, showing a tremendous beating getting ready to be done by Trump, but pulled things off differently.  

Phantoms.

{:.caption}
Source: <https://t.me/ElectionHQ2024/788>



{% assign events = site.data.events | where: 'state', page.abbr %}
{% include events %}

{% include seth_reports_methodology %}

{% include raw_data %}

{% include canvass/get-involved %}

{% include state_telegram_channels %}
