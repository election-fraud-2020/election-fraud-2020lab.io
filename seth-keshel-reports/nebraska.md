---
title: Nebraska Election Analysis by Seth Keshel
state: Nebraska
abbr: ne
last_updated: 23 May 2023
---

{% include see_main_article %}

{% include seth_report_intro.html %}

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

{% include seth_state_details.html %}

Nebraska geographically is mostly cleanly trended (green), but the major population centers appear 33k heavy for Biden.  In a blowout state like this, why would it matter?  Because that appears to have swung the NE-2 electoral vote that covers all of Douglas County and part of Sarpy.

I have Douglas 15k high (conservative estimate) and Sarpy 8k high.  Ironically, after arriving at those numbers, I saw the NE-2 gap is 23k.  Local knowledge indeed needed.  Sarpy is 1/3 the size of Douglas but has an equal number of net new registered voters.  It wouldn’t surprise me if the parts of Sarpy in NE-2 are being manipulated with registration-wise.

Lancaster (college county) also appears 8k heavy, flagged yellow due to larger population base.

If Biden is 33k heavy, an accurate margin is 23.4%, or 60.6% to 37.2%.  

Best GOP county audit targets: Sarpy

Need to audit: Douglas, Lancaster

{:.caption}
Source: <https://t.me/CaptainKMapsandStats/110>

----

{% include seth_worst_counties_for_state heading=true %}

## Presentation of Seth's Findings by Nebraska Voter Accuracy Project

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vqbsqk/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.small}
[View on Rumble](https://rumble.com/vsxyug-nebraska-voter-accuracy-project-explains-seth-keshels-findings.html) \| [View Full 90min Presentation](https://rumble.com/vsjrua-nebraska-voter-accuracy-project-presentation-to-omaha-liberty-ladies.html) \| [Download Presentation Slides](https://dl.dropboxusercontent.com/s/8ggyvcetl4c1jaj/NebVAPPresentation17Dec21v15.pdf)


## Douglas County

{% include tg-embed url="https://t.me/ElectionHQ2024/3195" %}


## Sarpy County

{% include tg-embed url="https://t.me/ElectionHQ2024/3216" %}


## Further Updates

{% include tg-embed url="https://t.me/ElectionHQ2024/7226" %}

{% include tg-embed url="https://t.me/ElectionHQ2024/6837" %}

{% include tg-embed url="https://t.me/ElectionHQ2024/6838" %}



{% include seth_reports_methodology %}

{% include raw_data %}

{% include canvass/get-involved %}

{% include state_telegram_channels %}
