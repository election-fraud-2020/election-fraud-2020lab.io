---
title: Colorado Election Analysis by Seth Keshel
state: Colorado
abbr: co
last_updated: 8 Feb 2022
---

{% include see_main_article %}

{% include seth_report_intro.html %}

{% include toc %}

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

{% include seth_state_details.html %}


The increasing trend of Democrat registrations favors a Biden win; however, massive increases in voter rolls like in [Oregon](../oregon/) with unaffiliated party voters is curious.  Biden is +465k in a small state over Clinton, with previous high Democrat gain of 287k for Obama (35k and 15k in 2 elections following).  Trump is +162k.

Colorado Secretary of State decision to ban audits forced a lot of people to take a good look here, when many weren’t watching it.  Biden’s vote totals in a state that already voted by mail anyway make little sense.  I estimate 183k excess votes statewide.

Red | Obviously Ugly | 8 counties
Yellow | Suspect/Likely Fraud | 20 counties
Green | Clean | 36 counties

Estimates for excess votes:

Arapahoe | 20k
Denver | 25k
El Paso | 20k
Jefferson | 25k
Larimer | 12k
Mesa | 5k
Weld | 10k

Best audit targets are Mesa and Weld.  

With excess votes only accounted for, 52.7% to 44.4% (8.3%) is an accurate Biden number in keeping with registration trends.  If votes are being flipped, look out.  Would have made the Gardner race very tight.


{:.caption}
Source: <https://t.me/CaptainKMapsandStats/14>


{% include seth_worst_counties_for_state heading=true %}


## Stats By County

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table2.jpg)

{:.caption}
Source: <https://t.me/CaptainKMapsandStats/14>



## Possible Voter Roll Manipulation

A number of states, both key competitive states, and not, have machined/trimmed/manipulated their voter rolls for the desired outcomes.  See [New Hampshire](../new-hampshire/), [Alaska](../alaska/), [California](../california/), and [Arizona](../arizona/).

There are other states with bloated rolls, like [Florida](../florida/) and [Texas](../texas/). **Colorado** doesn’t show much movement in the two parties but tons of new registered indies/others, along with [Washington](../washington/) and [Oregon](../oregon/). [Pennsylvania](../pennsylvania/) and [North Carolina](../north-carolina/) appear to have let their party rolls run clean, showing a tremendous beating getting ready to be done by Trump, but pulled things off differently.  

Phantoms.

{:.caption}
Source: <https://t.me/ElectionHQ2024/788>


This chart shows how the voter rolls tend to increase prior to each election but then drop immediately afterward.

![](/images/seth-keshel/co-roll-chart.jpg)

{:.caption}
Source: <https://t.me/ladydraza/439>


## Mesa County Deep Dive

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="ElectionHQ2024/904" data-width="100%"></script>

Here is the attached video:

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/viifo3/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="ElectionHQ2024/983" data-width="100%"></script>

Our team at ElectionFraud20.org has also analyzed some additional stats for Mesa County, discussed in our Gitlab discussion area:

  [View the draft report](https://gitlab.com/election-fraud-2020/election-fraud-2020.gitlab.io/-/issues/9){:.button}



## Unexpected Surge in Mail-In Ballots

{% include tg-embed url="https://t.me/ElectionHQ2024/3237" %}



{% assign events = site.data.events | where: 'state', page.abbr %}
{% include events %}

{% include seth_reports_methodology %}

{% include raw_data %}

{% include canvass/get-involved %}

{% include state_telegram_channels %}
