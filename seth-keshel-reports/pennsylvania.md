---
title: Pennsylvania Election Analysis by Seth Keshel
state: Pennsylvania
abbr: pa
last_updated: 23 May 2023
---

{% include see_main_article %}

{% include seth_report_intro.html %}

{% include toc %}

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

{% include seth_state_details.html %}


Even worse than I thought.  PA was a slam dunk for Trump, with a 244k-12k (21 to 1 ratio) of net new registrations in previous 4 years that previously predicted Trump flip in 2016.

![{{ page.state }} - Republicans out-registered Democrats 21:1 between 2016-2020](/images/seth-keshel/{{ page.abbr }}-banner.jpg)

This is horrific.  Only 3 counties I see as relatively clean, with 27 classified as major.  60 of 67 counties should have been more GOP than last time per registration trends, but only 20 trended more GOP.  This flipped Erie and Northampton, which should have been slam dunks.

Trump crushed Obama ’08 total in a shrinking state, only to be passed on strength of what I assess to be 504k excess votes!

The worst counties:

Allegheny | 50k
Berks | 20k | <small>[see further analysis below](#berks-county)</small>
Bucks | 20k
Chester | 15k
Erie | 15k | <small>[see further analysis below](#erie-county)</small>
Lackawanna | 12k
Lehigh | 12k
Luzerne | 15k
Montgomery | 30k
Northampton | 20k
Philadelphia | 40k
Washington | 15k
Westmoreland | 18k | <small>[see further analysis below](#westmoreland-county)</small>
York | 25k

Trump margin, if accurate, should have been 52.0% to 46.1% (5.9%), or 424k votes.  A bludgeoning in keeping with registration trend.

{:.caption}
Source: <https://t.me/CaptainKMapsandStats/73>


{% include seth_worst_counties_for_state heading=true %}


## Videos Covering Pennsylvania

<div class="responsive-iframe"><iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vhpoyp/?pub=m4ux1" frameborder="0" allowfullscreen></iframe></div>

{:.caption}
[Watch on Rumble](https://rumble.com/vkbv3z-pennsylvania-election-audit-debrief-captain-seth-keshel.html)

<div class="responsive-iframe"><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/xXMW9VNMPT4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>

{:.caption}
[Watch on YouTube](https://www.youtube.com/watch?v=xXMW9VNMPT4)

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vhfttm/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Watch on Rumble](https://rumble.com/vk1zyw-bombshell-by-seth-keshel-trump-won-pennsylvania-by-6-8.html?mref=lzerp&mrefc=2)




## The Trends Visualized

The following slide shows how in 2016 (and in many elections prior), the trend in voter registrations (towards Republican) also paralleled the trend in election results; however in 2020, although the state had many more Republicans registering to vote, the final results trend in the opposite direction:

![](/images/seth-keshel/pa-graphic.jpg)


## Stats By County

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table2.jpg)




## November 2020 Presentation

Seth Keshel gave the following presentation to the Pennsylvania Republican Party on 11 Nov 2020:

<iframe class="scribd_iframe_embed" title="Voter Registration Trends (Seth Keshel)" src="https://www.scribd.com/embeds/498212060/content?start_page=1&view_mode=scroll&access_key=key-8Duk2jvmCLXz3SoILwtX" tabindex="0" data-auto-height="true" data-aspect-ratio="1.7790927021696252" scrolling="no" width="100%" height="600" frameborder="0"></iframe>

Followed by this official, signed declaration of the results he discovered:

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="ElectionHQ2024/466" data-width="100%"></script>

<iframe class="scribd_iframe_embed" title="Seth Keshel - Declaration on Pennsylvania, 17 Nov 2020" src="https://www.scribd.com/embeds/532067829/content?start_page=1&view_mode=scroll&access_key=key-SdvyGBF1QLgsyO5VGzmy" data-auto-height="true" data-aspect-ratio="0.7729220222793488" scrolling="no" id="doc_99207" width="100%" height="700" frameborder="0"></iframe>

He later expanded upon and revised his initial analysis, as per the sections below.


## Bellwether Counties in Pennsylvania

This is from the scrap heap, and is complicated, but if you want some of my Pre-election work to chew on, here is some good stuff. 

[![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table3.jpg)](/images/seth-keshel/{{ page.abbr }}-table3.jpg)

Luzerne, Erie, Northampton were three Trump flips that moved a lot in GOP registration. Two were stolen back to blue. GOP control of those three shows strong working class support and a win in MI. 

Lackawanna is another story, similar to Mahoning in Ohio (won by Trump), lost by 3% in 2016 and trended hard R (Baris had Trump up 6 there) suggesting a flip this year, Scranton Joe effect be damned. 

A GOP win there resonates all the way to Minnesota in terms of working class depth. 

The slide reads itself in the bottom but keep in mind, DJT working class support tightens a lot of states not thought of looking back at history.



{:.caption}
Source: <https://t.me/ElectionHQ2024/651>


## Berks County

County Commissioner Leinbach of Berks County says this was a great election.  Best in history, maybe *ever*.

Here, Seth shows you the 20,000 extra Biden votes in that county.  Make sure you know the difference between phony, surface-skimming audits and real ones that identify phantom voters.

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vjbwxe/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
Source: [Telegram Post](https://t.me/ElectionHQ2024/1165), Aug 31, 2021


## Butler County

[![](/images/seth-keshel/pa-butler-county.jpg){:width="500"}](/images/seth-keshel/pa-butler-county.jpg)

Seth provides some further stats and analysis on Butler County in the following video:

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vnqh75/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
Source: [Telegram Post](https://t.me/ElectionHQ2024/1806), Oct 23, 2021

Thanks to Butler County (PA) Patriots, the following post shows county officials communicating before and after election about various issues, mostly related to their elections equipment (machines and scanners), pondering why votes are being subtracted and totals erroneous. 

Also admitting issues with the voter list, even though we are told this is the most secure election in history. Also here, a results page showing DJT with over 80% of in person vote for him, but losing mail in vote nearly 2 to 1.

{% include tg-embed url="https://t.me/ElectionHQ2024/3017" %}

{% include tg-embed url="https://t.me/ElectionHQ2024/3022" %}

{% include tg-embed url="https://t.me/ElectionHQ2024/3023" %}

{% include tg-embed url="https://t.me/ElectionHQ2024/3025" %}


## Delaware County

{% include tg-embed url="https://t.me/ElectionHQ2024/2951" %}

{% include tg-embed url="https://t.me/ElectionHQ2024/4388" %}

{% include tg-embed url="https://t.me/ElectionHQ2024/7225" %}


## Erie County

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vijep8/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

We've also analyzed some additional stats for Erie County, being discussed in our Gitlab discussion area:

  [View the draft report](https://gitlab.com/election-fraud-2020/election-fraud-2020.gitlab.io/-/issues/11){:.button}


## Philadelphia County

{% include tg-embed url="https://t.me/ElectionHQ2024/4415" %}


## Westmoreland County

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vj8il8/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
Source: [Telegram Post](https://t.me/ElectionHQ2024/1055), Aug 25, 2021


## Possible Voter Roll Manipulation

A number of states, both key competitive states, and not, have machined/trimmed/manipulated their voter rolls for the desired outcomes.  See [New Hampshire](../new-hampshire/), [Alaska](../alaska/), [California](../california/), and [Arizona](../arizona/).

There are other states with bloated rolls, like [Florida](../florida/) and [Texas](../texas/). [Colorado](../colorado/) doesn’t show much movement in the two parties but tons of new registered indies/others, along with [Washington](../washington/) and [Oregon](../oregon/). **Pennsylvania** and [North Carolina](../north-carolina/) appear to have let their party rolls run clean, showing a tremendous beating getting ready to be done by Trump, but pulled things off differently.  

Phantoms.

{:.caption}
Source: <https://t.me/ElectionHQ2024/788>

Seth also reports that, strangely, 25.7% of registered voters in York County are recorded as registering on January 1. Either this is lazy record-keeping, or a possible indication of manipulation of voter rolls.

{:.caption}
Source: <https://t.me/ElectionHQ2024/814>

{% include pdf_box.html 
    url = "/images/seth-keshel/pa-voter-roll-analysis.pdf"
    title = "Pennsylvania Voter Roll Analysis"
    description = "Here is the promised voter registration rolls with analysis on *five counties* holding an estimated 92,000 extra Biden votes (enough to flip the state!):

Erie, Luzerne, Franklin, Montgomery, York"
%}


## Further Updates on Pennsylvania

{% include tg-embed url="https://t.me/ElectionHQ2024/3129" %}

{% include tg-embed url="https://t.me/ElectionHQ2024/6132" %}

{% include tg-embed url="https://t.me/ElectionHQ2024/6167" %}

{% include tg-embed url="https://t.me/ElectionHQ2024/6823" %}




## So What Should We Do?

For those who asked “what do we do?” in PA in response to those five counties I posted the registration problems with:

1. Put the heat on the entire state Republican party, specifically Corman, Benninghoff, Cutler, and Grove, the worst of the RINO coalition there. 

2. Ask Lawrence Tabas (PA GOP chair) to get busy demanding answers. These issues with the registration info demand answers, even if the info posted yesterday matches real voters to some degree (I’m saying I’m confident it’s not all malfeasance).

3. Support Senator Mastriano. Call his office and thank him for his stand. 

4. Melt down the phone lines of the county commissioners in those five counties. You have the information now. Put them on the spot to defend their filthy voter rolls. This is particularly useful for counties insisting they had great elections, like York did recently when they rebuffed an audit.

{:.caption}
Source: <https://t.me/ElectionHQ2024/836>


{% assign events = site.data.events | where: 'state', page.abbr %}
{% include events %}


{% include seth_reports_methodology %}

{% include raw_data %}

## Other Reports

* A team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the report "[2020 Presidential Election Startling Vote Spikes](https://election-integrity.info/Vote_Spikes_Report.pdf)" which includes a section on Pennsylvania

* [AuditTheVotePA.com](https://www.auditthevotepa.com/) has further information, data, petitions and interviews

* See also the main [Pennsylvania](/fraud-summary-by-state/pennsylvania/) article for more

{% include canvass/get-involved %}

{% include state_telegram_channels %}
