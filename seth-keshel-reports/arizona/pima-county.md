---
title: Pima County Election Analysis by Seth Keshel
breadcrumb: false
parent_page: /seth-keshel-reports/arizona/
last_updated: 13 Jan 2022
---

*View [Seth's Arizona Report](../) for an overview of what occurred in the state.*

The following 40min video is Seth's presentation of his analysis of Pima County and some wider issues in Arizona, Dec 14, 2021:

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/voa6ac/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

[See here](https://www.scribd.com/document/547765361/Seth-Keshel-Pima-County-Presentation-Slides#download&from_embed) for the slides from the above presentation. [^1]

The following heat map shows which precincts within Pima County appear to have the highest number of anomalies:

<iframe src="https://e.pcloud.link/publink/show?code=XZIJGJZ1BVJn1HNAozjV4UMj5m5V59plKEV" width="100%" height="600"></iframe>

{:.caption}
Source: [Telegram Post](https://t.me/ElectionHQ2024/1990), Nov 7, 2021


## Top Ten Precincts to Canvass

90 | (Crimson Category)
127 | (Crimson Category)
150 | (Republican Category)
131 | (Republican Category)
142 | (Republican Category)
88 | (Competitive Category)
125 | (Competitive Category)
107 | (Democrat Category)
55 | (Democrat Category)
100 | (Democrat Category)


## Top Ten Crimson Precincts For Canvass

A "Crimson Precinct" is one won by Romney, and won by Trump by a bigger margin in 2016.  Per corresponding registrations in other areas, we would expect precincts to be more Republican in 2020.

<table>
<tr>
<th rowspan="7">RED (HIGH)</th>
<td>90</td><td> (11.9% margin loss for Trump)</td></tr>
<tr><td>127</td><td> (11.8% margin loss for Trump with 16.2% Trump vote increase)</td></tr>
<tr><td>109</td><td></td></tr>
<tr><td>168</td><td></td></tr>
<tr><td>39</td><td></td></tr>
<tr><td>147</td><td></td></tr>
<tr><td>185</td><td></td></tr>

<tr><th rowspan="3">YELLOW (MODERATE)</th>
<td>83</td><td></td></tr>
<tr><td>277</td><td></td></tr>
<tr><td>1335</td><td></td></tr>
</table>

## Top Ten Republican Precincts For Canvass

A "Republican Precinct" is one won by Romney, and won by Trump in 2016, but with a smaller margin of victory.  Per corresponding registrations, some precincts are likely to become more Republican, while others are likely to continue to trend left.

<table>
<tr><th rowspan=10>RED (HIGH)</th>
<td>150</td><td> (16.2% margin loss for Trump, precinct flipped)</td></tr>
<tr><td>131</td><td> (14.5%, flipped)</td></tr>
<tr><td>142</td><td> (12.4%, flipped)</td></tr>
<tr><td>217</td><td> (11.9%, flipped)</td></tr>
<tr><td>130</td><td> (11.5%, flipped)</td></tr>
<tr><td>114</td><td> (11.4%, flipped)</td></tr>
<tr><td>104</td><td> (11.4%, flipped)</td></tr>
<tr><td>95</td><td> (10.9%)</td></tr>
<tr><td>205</td><td> (10.9%)</td></tr>
<tr><td>141</td><td> (10.7%, flipped)</td></tr>
</table>

## Data Table

To accompany the Pima County Canvassing Map, here are the precincts that rate as "high" for statistical evidence of fraud.

![](/images/seth-keshel/az-pima-county-table.jpg)

Classification refers to the voting behavior of the precinct (trend) since 2012, when there was the RINO to MAGA shift within the GOP.

**Trump Trend** | Dem precinct that is trending toward Trump
**Republican** | Romney and Trump 2016 won Precinct, but Trump won by less.  Possible to trend in either direction.
**Democrat** | Democrat typically receives 65% plus in precinct.
**Competitive** | Democrat under 65% and GOP over 35% with room for fluctuation.
**Crimson** | Won by Romney and Won by Trump 2016, with greater margin than 2012.  Almost all counties in America with this behavior are becoming more Republican in registration.  Should be more favorable to Trump in 2020.

{:.caption}
Source: [Telegram Post](https://t.me/ElectionHQ2024/1993), Nov 8, 2021


### Footnotes & References

[^1]: Source: [Telegram Post](https://t.me/ElectionHQ2024/2509), Dec 14, 2021