---
title: New Hampshire Election Analysis by Seth Keshel
state: New Hampshire
abbr: nh
last_updated: 23 May 2023
---

{% include see_main_article %}

{% include seth_report_intro.html %}

{% include toc %}

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/v1izu6v/?pub=4" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Watch on Rumble](https://rumble.com/v1lm043-episode-89-new-hampshire.html)


![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)


{% include seth_state_details.html %}

New Hampshire was almost won by Trump in 2016, with just under 3,000 vote difference.  Trump’s anti-war stance made him a perfect fit to flip New Hampshire, as the Iraq War is a key reason Bush lost it in 2004 after carrying it in 2000.  Working class shift in effect as seen in the Midwestern states, Democrat peak being in 2008, with a heavy share of independent voters.

The trends in voter registrations indicate that Biden has 40,000 more votes than what seems reasonable. Rockingham (14k) and Hillsborough (12k) are the most glaring culprits. The issue appears to lie in registrations. The trend is similar to other stagnant population states:

* Better for Republican in 2012
* Flip/near flip in 2016
* Republican vote gain in 2020

But Biden's gain is nearly 80k in a tiny state. 200k new registrations since 2016, after just 13k new from 2012-2016 seems questionable.

![New Hampshire Voting Trend Chart, Seth Keshel](/images/seth-keshel/nh-chart.jpg)

As state trended more Republican from 2008:

**2012** | -7k Republican registrations, -32k Democrat registrations, leaving the state 4.1% more Republican in the vote
**2016** | +23k Republican registrations, +21k Democrat registrations, leaving the state 5.2% more Republican in the vote
**2020** | A big Democrat surge to push the margin to 7.35% for Biden (as certified) would normally require a drop in the Republican rolls as seen in 2004-2008; but instead, Republicans gained 37k (14k more than 2016), only to be smashed by 77k new Democrat registrations and 51k new "undeclared" independent voters

Incredible how Biden reversed two cycles of Democrat decline amidst a Trump favoring coalition shift in NH to post a gain nearly 2x larger than Obama’s in 2008. 

Registration numbers alone suggest the state would drift left, but based on the above, the method of registering voters appears far different from previous years. The 15-fold increase in new registered voters since 2016 needs investigating.

If we remove the 40,000 excess votes, Biden's lead would dwindle to 50.2% to 47.7% (2.5% lead), or just 19k votes.  If there is additional fraud via votes being flipped electronically, then this state should have been a win for Trump.  I had it two or less to either candidate (tilt) leading up to the election.

**Best targets for audits:** Hillsborough, Rockingham, Merrimack, Strafford

Here is a breakdown of individual counties:

[![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table2.jpg)](/images/seth-keshel/{{ page.abbr }}-table2.jpg)

{:.caption}
Source: <https://t.me/CaptainKMapsandStats/86>

----

{% include seth_worst_counties_for_state heading=true %}

## Possible Voter Roll Manipulation

A number of states, both key competitive states, and not, have machined/trimmed/manipulated their voter rolls for the desired outcomes.  For example, in New Hampshire:

From 2008-16, the state only added 13,000 net new registered voters.  Yet in the past four years, they added *200,000*, pushing the state a couple points to the left. This would make an analyst looking at party registration shifts assume “New Hampshire moves left.”

Republicans registered 37k, but were out-registered 2:1 by Democrats overall, with another 97k Independent/Other.  So, 200,000 new registered voters in a state of 1.37 million.  For perspective, neighboring [Vermont](../vermont/), with close to half of the population of New Hampshire, registered just 34k net new voters (also probably heavy based on county analysis) – meaning NH is 3x higher in net new registered voters if we set populations to equal.

These look a lot like phantom voters -- fake records added to the rolls -- and need investigating.

{:.caption}
Source: <https://t.me/ElectionHQ2024/788>


## Seth's Presentation in New Hampshire

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vmzgn7/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

<small>Nov 19, 2021</small>


{% assign events = site.data.events | where: "state", page.abbr %}
{% include events %}


## Further Updates

{% include tg-embed url="https://t.me/ElectionHQ2024/6823" %}


{% include seth_reports_methodology %}

{% include raw_data %}

## Other News

* Windham County [previously held an audit](https://www.washingtonexaminer.com/news/new-hampshire-windham-election-audit-concludes), but there are claims that the audit was influenced by auditors with close-ties to the Democratic party. [See article by The Gateway Pundit](https://www.thegatewaypundit.com/2021/05/breaking-exclusive-windham-new-hampshire-2020-election-audit-started-two-three-auditors-conflicts-pelosi-schumer/).

* Mark Alliegro, New Hampshire State Representative, spoke at the [Cyber Symposium](/in-detail/cyber-symposium-mike-lindell/) ([view video](https://rumble.com/vl0pki-mike-lindells-cyber-symposium-day-2-live-continued.html), starting around 2hr 55min mark) saying:

    - New Hampshire was one of the first states to take action against Colonialism. First state to form government and have independent constitution.
    - He didn't need convincing of a stolen election. On election night, it was very obvious that it was stolen. He wanted to go home with an action plan.
    - What happened in Windham? It was an attempt to steal Windham, but a cheat-proof majority was built.
    - An audit was accomplished, but it went out of their control and into the hands of the wrong people. You must make sure you control every aspect of the audit! You must get the keys to every locked room.
    - "We're going to win"
    - "The day the yoke of Rome falls, will be the day of a great rejoicing!"
    
      <div class="responsive-iframe"><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/AJVmwyHeIXo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>

* Another attendee at [Mike Lindell's Cyber Symposium](/in-detail/cyber-symposium-mike-lindell/) shares that [LHS Associates](https://www.lhsassociates.com/) manages and supports the election technology across the states in New England. After the election, the memory cards were mailed back to LHS and data was not preserved, breaking federal law. Workers did manage to save 4 cards in Windham during an audit, but then they were also later wiped. The staff simply replied: “Oops, they were wiped already!” No enforcement of these crucial laws is happening to date. They said that this needs to be rectified, and some people need to be prosecuted.

* Also shared at the Symposium: apparently a movie is coming out with 300 clips of issues with the “sham audit” in New Hampshire. Look out for this.

* [Dallas, Texas 2020 Election Fingerprints](https://wwrkds.net/wp2/dallas-tx-election-fingerprint/), is a report on election votes vs turnout patterns (or "fingerprints") by wwrkds.

{% include canvass/get-involved %}

{% include state_telegram_channels %}
