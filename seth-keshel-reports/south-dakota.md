---
title: South Dakota Election Analysis by Seth Keshel
state: South Dakota
abbr: sd
last_updated: 23 May 2023
---

{% include see_main_article %}

{% include seth_report_intro.html %}

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

{% include seth_state_details.html %}


South Dakota provides party registration data which makes it easier to highlight reversals of trend.  

In **Minnehaha County**, home of the Cyber Symposium, the Democrats lost 1,100 from the voter rolls while Republicans added 4,400, a sizable shift in one of the states largest counties; however, Biden somehow gained 10k votes after two consecutive elections of dumping votes – I’ve tagged that for an excess of 6k, allowing for a 13% vote gain.

**Pennington** (Rapid City) is a little bit heavy, and so is **Lincoln**, but I can’t see any more than 11k excess Biden votes here.  Minnehaha (Sioux Falls) absolutely should audit.

Audit target: Minnehaha


{:.caption}
Source: <https://t.me/ElectionHQ2024/719>


## Stats By County

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)


## Minnehaha County

The following PDF shows a map of which precincts in the county had the most abnormal results in 2020:

{% include tg-embed url="https://t.me/ElectionHQ2024/3644" %}



{% include seth_worst_counties_for_state heading=true %}


## Further Updates

{% include tg-embed url="https://t.me/ElectionHQ2024/7226" %}


{% include seth_reports_methodology %}

{% include raw_data %}

{% include canvass/get-involved %}

{% include state_telegram_channels %}
