---
title: Maine Election Analysis by Seth Keshel
state: Maine
abbr: me
last_updated: 7 Dec 2021
---

{% include see_main_article %}

{% include seth_report_intro.html %}

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table2.jpg)

{% include seth_state_details.html %}

Statewide, Maine’s election numbers trended as registration indicates, with the state moving 2.3% left in registration, with most third party voters coming back to support Biden, at least on paper.  New Hampshire’s main suspicious issue was in mass volume of registrations dwarfing those from 2012-16.  The one that stood out this year was Cumberland, a 27k to 3k Dem registration advantage.  

Trump is up 25k from 2016, Biden up over Clinton by 77k, and above Obama’s 422k from 2008’s 17.3% blowout.  Trump took the single electoral vote from ME-2 on both campaigns.

Cumberland and York, in southern area of the state are about 15k and 6k over expected outcomes.

If Biden is 31k heavy, an accurate victory margin for him would be 5.6%, or about 51.6% to 46.0%.  If there are votes being flipped, it could be a different story.

Best audit target – Androscoggin (3k heavy), Penobscot (2k heavy)


{:.caption}
Source: <https://t.me/CaptainKMapsandStats/95>

----

{% include seth_worst_counties_for_state heading=true %}

{% include seth_reports_methodology %}

{% include raw_data %}

## Other Reports

* A team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the report "[2020 Presidential Election Startling Vote Spikes](https://election-integrity.info/Vote_Spikes_Report.pdf)" which includes a section on Maine

{% include canvass/get-involved %}

{% include state_telegram_channels %}
