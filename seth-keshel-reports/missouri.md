---
title: Missouri Election Analysis by Seth Keshel
state: Missouri
abbr: mo
last_updated: 7 Dec 2021
---

{% include see_main_article %}

{% include seth_report_intro.html %}

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

{% include seth_state_details.html %}

Missouri showed me a few surprises, namely that St. Louis City and Jackson County trended clean.  This does not mean no fraud, but rather the trends/registration/population project accurate trend/vote alignment.  The same cannot be said for the suburban counties (St. Charles, 10k heavy), and Clay (8k), both with substantial Trump gains and competing Biden record gains to cut margins.  I assess 49k excess Biden votes statewide.

Refer to the spreadsheet chart for the yellow counties and remaining counties over 40,000 population.  Many of the middle-tier counties are coming in slightly heavy, but not overwhelmingly so.

If Biden is 49k heavy, an accurate margin is 17.3%, or 57.7% to 40.4% - 515k votes.

**Best targets for audits (Republican-controlled):** Clay, St. Charles, Jefferson, Greene


{:.caption}
Source: <https://t.me/CaptainKMapsandStats/114>

----


{% include seth_worst_counties_for_state heading=true %}


## Further Updates

According to Seth, St. Charles County, Missouri, is showing some movement towards an audit of the election. See [his Telegram post](https://t.me/ElectionHQ2024/1162) from Aug 31, 2021.


{% include seth_reports_methodology %}

{% include raw_data %}

## Other Reports

* A team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the report "[2020 Presidential Election Startling Vote Spikes](https://election-integrity.info/Vote_Spikes_Report.pdf)" which includes a section on Missouri

{% include canvass/get-involved %}

{% include state_telegram_channels %}
