---
title: Vermont Election Analysis by Seth Keshel
state: Vermont
abbr: vt
last_updated: 7 Dec 2021
---

{% include see_main_article %}

{% include seth_report_intro.html %}

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

{% include seth_state_details.html %}

Vermont had 35k new registered voters.  Tiny [New Hampshire](../new-hampshire/) next door had 200k new registered voters, validating my belief voter rolls are being manipulated in certain states.

What a weird state.  2016 had some Democrat issues, Bern-outs throwing away their votes to third party; some gains are expected.  Trump up nearly 20% from 2016 in votes in a state where 10 of 14 counties have population decline.  

I am not confident third party votes aren’t being allocated to Biden at a certain clip given Clinton’s 55% share in Vermont.  Trump won national bellwether Essex, which trends clean.  13 of the counties show suspect trends, and Chittenden (min. 10k) and Washington (4k)  are obvious based on registration, pop. Decline, and Trump growth.

Biden appears to be 34k heavy in Vermont.

If 34k heavy, his margin would be 62.8% to 33.9%, or 97k.

Most in need of audits: Chittenden, Washington, Rutland, Windsor


{:.caption}
Source: <https://t.me/ElectionHQ2024/703>

----

{% include seth_worst_counties_for_state heading=true %}

{% include seth_reports_methodology %}

{% include raw_data %}

{% include canvass/get-involved %}

{% include state_telegram_channels %}
