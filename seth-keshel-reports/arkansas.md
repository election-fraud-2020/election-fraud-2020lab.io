---
title: Arkansas Election Analysis by Seth Keshel
state: Arkansas
abbr: ar
last_updated: 23 May 2023
---

{% include see_main_article %}

{% include seth_report_intro.html %}

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

{% include seth_state_details.html %}


Arkansas nearly joined Iowa with perfect green counties.  I have tagged Benton (5k) and Washington (3k) as the only viable audit targets in Arkansas.  Both have tremendous growth, but also did in 2000-10 timeframe without the sharp increases in votes.  Democrats are up 50% in Benton, which a vote gain should be expected, but 5k is the previous high, and now we have a gain of 14k – in the face of a massive record GOP vote gain.  It is certainly not rampant, and I’m still affording Biden a double previous vote gain in this county.

Washington, same dynamics, just a smaller county.  Pulaski (Little Rock) trended clean.  This doesn’t mean it is clean, but the trends don’t provide a mandate for an audit.

Overall, as seen in Iowa, a modest 10% Biden vote gain, mainly returning third party throwaways and accounting for higher turnout.  Arkansas looks good.

Best Trump county audit targets – Benton, Washington


{:.caption}
Source: <https://t.me/ElectionHQ2024/644>



{% include seth_worst_counties_for_state heading=true %}


## Further Updates

{% include tg-embed url="https://t.me/ElectionHQ2024/7226" %}


{% include seth_reports_methodology %}

{% include raw_data %}

{% include canvass/get-involved %}

{% include state_telegram_channels %}
