---
title: Alaska Election Analysis by Seth Keshel
state: Alaska
abbr: ak
last_updated: 23 May 2023
---

{% include see_main_article %}

{% include seth_report_intro.html %}

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

[![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)](/images/seth-keshel/{{ page.abbr }}-table.jpg)

{% include seth_state_details.html %}


Notes: apologies for the crude map.  AK is divided electorally into 40 State House districts, and that is how they track presidential results.  These are new as of 2016, so trends are not available except for Party ID shifts and new voter numbers.

Fairbanks is districts 1-3, and Anchorage is 15-28, with 7-14 being surrounding areas.  Trump trended heavily 37-40, which are largely native and reflects growth with minority voters.

Biden is 38k over Clinton in one cycle, with Trump up 27k. Trump is still not higher than McCain 2008.

Alaska has a lot of parties and has some indies, but I made some serious discoveries that cannot be captured fully in this caption.  I am going to stop at the bottom line, and YOU NEED TO READ THE ATTACHMENT CALLED “AK INTEL SUMMARY.”

Biden appears 19k heavy, meaning a more likely margin in Alaska counting only excess votes is 55.7% to 37.4%, or 18.3% instead of 10.0%.

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-summary.jpg)


{:.caption}
Source: <https://t.me/CaptainKMapsandStats/37>

----

{% include seth_worst_counties_for_state heading=true %}


## Possible Voter Roll Manipulation

A number of states, both key competitive states, and not, have machined/trimmed/manipulated their voter rolls for the desired outcomes.  Here is an example from Alaska:

From 2012-16, there were just 23,000 net new registered voters, split 51% for a party and 49% Independent/other.  From 2016-20, 3x the amount (68,000), at 4.5% Republican, 3.5% Democrat, and *NINETY-TWO PERCENT* Independent/Other.  This forces most of their voting districts left thanks to the amount of indies diluting the math, suggesting a left lurch to an analyst.  You may remember that Alaska took 3 days to call even with the artificially shortened 10% margin.  I believe it was the 3-electoral vote putter in the proverbial golf bag in case Biden got hung up at 268 electoral votes.

{:.caption}
Source: <https://t.me/ElectionHQ2024/788>

## Further Analysis on Alaska

[This video](https://rumble.com/vlukw3-the-daily-302-show-2a-with-guest-captain-seth-keshel.html), featuring Seth Keshel, discusses some of the issues with Alaska, starting at 22min mark. He says that it's very odd and possibly suspicious that Alaska took so long to count and submit their final results, considering the relatively small number of votes.


## Further Updates

* David Eastman, Alaska State Representative, spoke at the [Cyber Symposium](/in-detail/cyber-symposium-mike-lindell/) ([view video](https://rumble.com/vl0pki-mike-lindells-cyber-symposium-day-2-live-continued.html), starting around 2hr 55min mark) saying:

    - In Alaska, the election process has been given over to a government monopoly
    - 2 weeks before election, their database was hacked
    - Government decided not to let anyone know about it. They waited until after the election was certified!
    - 100,000 citizens had their data breached
    
    > The strength of the Constitution lies entirely in the determination of each citizen to defend it. Only if every single citizen feels duty bound to do his share in this defense are the constitutional rights secure." 
    >
    > <small>― Albert Einstein</small>


{% include tg-embed url="https://t.me/ElectionHQ2024/4815" %}

{% include tg-embed url="https://t.me/ElectionHQ2024/4831" %}

{% include tg-embed url="https://t.me/ElectionHQ2024/7226" %}



{% include seth_reports_methodology %}

{% include raw_data %}

{% include canvass/get-involved %}

{% include state_telegram_channels %}
