---
title: Delaware Election Analysis by Seth Keshel
state: Delaware
abbr: de
last_updated: 7 Dec 2021
---

{% include see_main_article %}

{% include seth_report_intro.html %}

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

{% include seth_state_details.html %}


Estimating 27k excess votes on a lenient evaluation.  We will have a house effect for Biden of at least a few points in his home state.

Party registrations moved slightly Democrat in Kent, which Trump flipped in 2016.  That is tagged yellow (5k excess) – acknowledging slight Democrat gain – flagging for strong Trump growth and massive Biden gain.

New Castle is the main driver for Democrat power in Delaware; has declined in raw Democrat votes since Obama’s first run; Biden up 16k over Obama ’08, 32k over Clinton.  Flagged yellow for 10k excess.

Sussex is 2.7% more GOP than it was in 2016 registration, when Trump blew it out.  This is a MAGA county.  Trump gained another 9k, only this time instead of Democrat column staying the same, Biden gained 17k, well against reg trends and above any reasonable expectation for home state advantage.  Flagged red, 12k excess.

Trump’s margin of loss would have been 14.3%, or 56.4% to 42.1%, a margin of 69k votes.


{:.caption}
Source: <https://t.me/CaptainKMapsandStats/64>


{% include seth_worst_counties_for_state heading=true %}


## Detailed Analysis

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="AmericasAuditForce/183" data-width="100%"></script>

If anyone has a PDF of the above report, please let us know in the comments.


## Initial Canvass Results

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="ElectionHQ2024/1547" data-width="100%"></script>

Releasing some preliminary results on canvassing in Delaware, from Patriots for Delaware. 

1. The state's “final voted” file is 20k higher than state elections office final total. Oops.

2. See photos above, with:

   - 25% adjudication rate for mail votes 
   - More votes from nursing homes than beds to vote from. 
   - Flagrant violations of the law pertaining to overseas/military ballots coming straight out of government addresses. 

And this... in Biden’s home state. 

{:.caption}
Source: [Telegram post](https://t.me/ElectionHQ2024/1547?single), Sep 29, 2021


{% include seth_reports_methodology %}

{% include raw_data %}

{% include canvass/get-involved %}

{% include state_telegram_channels %}
