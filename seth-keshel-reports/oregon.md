---
title: Oregon Election Analysis by Seth Keshel
state: Oregon
abbr: or
last_updated: 23 May 2023
---

{% include see_main_article %}

{% include seth_report_intro.html %}

{% include toc %}

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}


{% include seth_state_details.html %}

Oregon statewide moved slightly Republican in terms of party registration numbers --- 30 out of 36 counties had more Republican party registrations than Democrat ones, since 2016. As such, we should have seen the Democrats have an even smaller winning margin than the 10.98% margin seen in 2016. But instead it jumped to over 16%! How did Trump lose another 5+ percentage points while gaining 176,000 votes from 4 years earlier?

I estimate that Biden has 162k more votes than would be reasonably expected, and that's before even counting Multnomah! Here are the worst ones:

Clackamas | 15k
Jackson | 12k
Lane | 20k
Marion | 17k
Washington | 31k
Multnomah | 55k
Others... | 67k
**Total** | **217k**

Biden was +84k in Multnomah after gains of +18k and -5k in last two elections.  Estimate 55k there, for total of 217k excess.  If correct, that give us an accurate election tally in that state with Biden's margin at +7.6%. That's only considering excess Biden votes in opposition to established registration trends.

The best targets for audits are Umatilla, Linn, Jackson, and Yamhill.  Blue states had to be pushed back out to double digit margins to distract from Rust Belt operation.

{:.caption}
Source: <https://t.me/CaptainKMapsandStats/4>


## Stats By County

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

{:.caption}
Source: <https://t.me/CaptainKMapsandStats/4>


{% include seth_worst_counties_for_state heading=true %}


## Jackson County

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="ElectionHQ2024/2387" data-width="100%"></script>


## Linn County

Seth explains the registration vs vote trends since 2004 and why he estimates that Linn County has 8,000 more votes for Biden than seems rational:

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vjthip/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
Source: [Telegram Post](https://t.me/ElectionHQ2024/1323), Sep 13, 2021


<iframe width="100%" height="640" src="https://e.pcloud.link/publink/show?code=XZUfJFZbDMC1aEIN7R1L6utfMCPaY4uKQT7"></iframe>


<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="ElectionHQ2024/2465" data-width="100%"></script>


## Umatilla County

Seth explains how, with the aid of motor voter and mail-in voting, Biden is at least 3,000 votes higher than any reasonable forecast would allow for.

County Type: CRIMSON (Trump's win was bigger than Romney's win)

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vrnwdd/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
Source: [Telegram post](https://t.me/ElectionHQ2024/2887), Jan 14, 2022

{% include tg-embed url="https://t.me/ElectionHQ2024/2898" %}

{% include tg-embed url="https://t.me/ElectionHQ2024/3284" %}




## Washington County

<!-- Heat map to be embedded here later, when time permits -->

{% include tg-embed url="https://t.me/ElectionHQ2024/2633" %}



## Possible Voter Roll Manipulation

A number of states, both key competitive states, and not, have machined/trimmed/manipulated their voter rolls for the desired outcomes.  See [New Hampshire](../new-hampshire/), [Alaska](../alaska/), [California](../california/), and [Arizona](../arizona/).

There are other states with bloated rolls, like [Florida](../florida/) and [Texas](../texas/). [Colorado](../colorado/) doesn’t show much movement in the two parties but tons of new registered indies/others, along with [Washington](../washington/) and **Oregon**. [Pennsylvania](../pennsylvania/) and [North Carolina](../north-carolina/) appear to have let their party rolls run clean, showing a tremendous beating getting ready to be done by Trump, but pulled things off differently.  

Phantoms.

{:.caption}
Source: <https://t.me/ElectionHQ2024/788>



## Unexpected Surge in Mail-In Ballots

{% include tg-embed url="https://t.me/ElectionHQ2024/3237" %}



{% assign events = site.data.events | where: 'state', page.abbr %}
{% include events %}


## Further Updates

* Troy Smith, representing Kim Thatcher, Oregon State Representative, spoke at the [Cyber Symposium](/in-detail/cyber-symposium-mike-lindell/) ([view video](https://rumble.com/vl0pki-mike-lindells-cyber-symposium-day-2-live-continued.html), starting around 2hr 55min mark) saying:

    - In Oregon, they are wanting to look into the concerns
    - Constitution says that state legislatures have their role in election, but it's currently Democrat-controlled. We really need a "We the people" movement.
    - We need to open our eyes, be open-minded, but with healthy skepticism
    - He's really thinking about the chain of custody and the vulnerabilities
    - The codes and PCAPs are over their heads, and it's too hard to look into
    - The problem: they have to *prove* there was a murder before they can even get an investigation
    - It's *reasonable* to have transparency

{% include tg-embed url="https://t.me/ElectionHQ2024/3139" %}

{% include tg-embed url="https://t.me/ElectionHQ2024/6601" %}

{% include tg-embed url="https://t.me/ElectionHQ2024/6823" %}



{% include seth_reports_methodology %}

{% include raw_data %}

{% include canvass/get-involved %}

{% include state_telegram_channels %}
