---
title: West Virginia Election Analysis by Seth Keshel
state: West Virginia
abbr: wv
last_updated: 7 Dec 2021
---

{% include see_main_article %}

{% include seth_report_intro.html %}

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

{% include seth_state_details.html %}

Complete and total Democrat extinction.  Every single county trended more Republican in registration.  Have made some allowances for third party returns to Democrats, so again estimating generously.  College counties are coming in heavy, lots of small counties with minor fraud likely of some sort.

Kanawha and Berkeley are the most obvious, with Democrats getting destroyed in the voter rolls yet making massive gains in the face of huge Trump gains.  

Biden appears to be 23k heavy statewide.

If Biden is 23k heavy, Trump margin would be 70.6% to 27.6%, or 332k.

Best audit targets: Kanawha, Berkeley, Monongalia


{:.caption}
Source: <https://t.me/ElectionHQ2024/696>

----

{% include seth_worst_counties_for_state heading=true %}

{% include seth_reports_methodology %}

{% include raw_data %}

{% include canvass/get-involved %}

{% include state_telegram_channels %}
