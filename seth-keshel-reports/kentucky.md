---
title: Kentucky Election Analysis by Seth Keshel
state: Kentucky
abbr: ky
last_updated: 7 Dec 2021
---

{% include see_main_article %}

{% include seth_report_intro.html %}

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

{% include seth_state_details.html %}

Kentucky is riddled with problems, showing 22 counties with issues, totaling roughly 62k excess Biden votes.  Jefferson County (Louisville) is tied for the most with 10k (lenient estimate), but classifies as yellow due to population size.  Fayette (Lexington) is coded red, estimated at about 8k excess for Biden.

Refer to the spreadsheet chart to see the network throughout the north and central Kentucky of yellow-class counties diverting from a very strong GOP trend and Democrat die-off.

Kenton County is easily the most lucrative target in the state, strong GOP trend in the county, with about 10k excess Biden votes.

If Biden is 62k heavy, an accurate margin for Trump is 64.0%, or 64.0% to 34.2%, or 617k votes.

Best Trump county audit targets: Boone, Kenton, Warren


{:.caption}
Source: <https://t.me/CaptainKMapsandStats/116>

----

{% include seth_worst_counties_for_state heading=true %}

{% assign events = site.data.events | where: "state", page.abbr %}
{% include events %}

{% include seth_reports_methodology %}

{% include raw_data %}

{% include canvass/get-involved %}

{% include state_telegram_channels %}
