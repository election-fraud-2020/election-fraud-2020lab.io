---
title: Connecticut Election Analysis by Seth Keshel
state: Connecticut
abbr: ct
last_updated: 7 Dec 2021
---

{% include see_main_article %}

{% include seth_report_intro.html %}

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

{% include seth_state_details.html %}


8 very ugly colored counties.  Every single county with exception of Fairfield is in population decline.

Biden supposedly increased 183k votes over Clinton, after the Dem nominee lost a ton of votes in 2012, and continued losing in 2016.  Remember, my estimates are light, and they still suggest 117k excess votes in Connecticut, which had a very good chance of being under 10% blue this year based on working class performance for Trump.

The big counties have blown away Obama landslide 2008 totals despite rapid population decline and even increased Trump support.  Hartford and New Haven have GOP trajectory in registration, albeit very slight.

Worst counties:

Fairfield | (25k)
Hartford | (25k)
New Haven | (25k)
New London | (14k)

If Biden is 117k heavy, an accurate margin is 14.6%, or 56.5% to 41.9%, and 249k votes.

Best GOP county audit targets – Litchfield, Windham


{:.caption}
Source: <https://t.me/CaptainKMapsandStats/105>

----

{% include seth_worst_counties_for_state heading=true %}

{% include seth_reports_methodology %}

{% include raw_data %}

## Other Reports

* A team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the the report "[2020 Presidential Election Contrast Analysis](https://election-integrity.info/Contrast_Report.pdf)" which discusses how Connecticut unexpectedly gained a significant number of overall votes, despite a decrease in population

{% include canvass/get-involved %}

{% include state_telegram_channels %}
