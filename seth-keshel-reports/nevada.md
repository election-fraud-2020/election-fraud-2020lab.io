---
title: Nevada Election Analysis by Seth Keshel
state: Nevada
abbr: nv
last_updated: 23 May 2023
---

{% include see_main_article %}

{% include seth_report_intro.html %}


![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}


{% include seth_state_details.html %}


I've done several deep dives that included Nevada, so won't go into too much detail here.  14 counties + Independent Carson City trended roughly as expected, no real demand for forensics in those unless a statewide audit happened.

Washoe is a little heavy on votes but not smoking gun like Clark County is.  

Please review Nevada attachment for more details.

Estimate of 100k+ excess votes in Clark County for Joe Biden.

A forensic audit is a must for Clark County, but governor, Sec. State, and legislature are major issues.


![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table2.jpg)

The counties highlighted green, above, had vote counts very close to Seth's predictions based on party registration numbers. Clark, Washoe and a number of others (flagged yellow and orange) trended unexpectedly, which deserve closer inspection.

{:.caption}
Source: <https://t.me/CaptainKMapsandStats/17>



{% include seth_worst_counties_for_state heading=true %}


## Clark County

The following chart displays the precincts within Clark County with the highest number of anomalous Biden votes:

<!-- Heat map to be embedded here later, when time permits -->

{% include tg-embed url="https://t.me/ElectionHQ2024/2799" %}


## Washoe County

{% include tg-embed url="https://t.me/ElectionHQ2024/4518" %}



{% assign events = site.data.events | where: 'state', page.abbr %}
{% include events %}


## Further Updates

{% include tg-embed url="https://t.me/ElectionHQ2024/6823" %}



{% include seth_reports_methodology %}

{% include raw_data %}

One searchable database of voter registration data is being published online at [VoteRef.com](https://voteref.com/) where visitors are be able to "crowd-source" any errors.



{% include canvass/get-involved %}

{% include state_telegram_channels %}
