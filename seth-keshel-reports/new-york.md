---
title: New York Election Analysis by Seth Keshel
state: New York
abbr: ny
last_updated: 23 May 2023
---

{% include see_main_article %}

{% include seth_report_intro.html %}

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table2.jpg)

{% include seth_state_details.html %}


New York drifted 1.5% left in voter registrations, and another 0.7% left in vote, but it appears Biden is 292k votes heavy just based on trends.  It is clear Trump did well with registered Democrats in New York with 432k vote gain.  Clinton improved just 70k over Obama; Biden is strangely 689k over Clinton and 440k over Obama in 2008.

Trump’s biggest improvements were in New York City, with 4 of 5 trending Republican (Richmond would have if clean).  Trump won NEW TWO PARTY votes in New York City 180k to 146k.  It appears New York did not account for Trump gains in New York City so Bronx at 0.1% and Manhattan at 4% Democrat increase paints a picture of "true city enthusiasm" for Biden throughout the US.

Manhattan, Bronx, Queens are in green DUE TO TREND.  My analysis doesn’t detect what is baked in for decades.

If Biden is 292k heavy, an accurate margin for him would be 59.5% to 39.0%.

Best audit targets:  
Richmond, Suffolk, Nassau, Saratoga, Oneida, Onondaga


{:.caption}
Source: <https://t.me/CaptainKMapsandStats/98>

----

{% include seth_worst_counties_for_state heading=true %}


## Further Updates

{% include tg-embed url="https://t.me/ElectionHQ2024/6823" %}



{% include seth_reports_methodology %}

{% include raw_data %}

## Other Reports

A team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the following reports:

  * "[2020 Presidential Election Contrast Analysis](https://election-integrity.info/Contrast_Report.pdf)" which lists New York as the state with the 4th highest number of unexpected Biden votes, despite a decrease in population

  * "[Statistical Voting Analysis of the 2020 NY-22 Congressional Contest](https://election-integrity.info/NY-22nd-2020-Report.pdf)"

{% include canvass/get-involved %}

{% include state_telegram_channels %}
