---
title: Florida Election Analysis by Seth Keshel
state: Florida
abbr: fl
last_updated: 23 May 2023
---

{% include see_main_article %}

{% include seth_report_intro.html %}

{% include toc %}


![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}


{% include seth_state_details.html %}

## Summary

Trump is up a staggering 1,040,000 votes from 2016, more than double his gain over Romney.

Biden is up 792k, a Democrat record in Florida as the state drifts further right.

Bottom Line: Biden appears at least 296k votes heavy based on registration trends and historical data tracking with population increases.  If accurate, counting only excess votes, Trump’s margin is FL is more accurately represented at 52.6% to 46.4%, or a 6.2% margin of victory, and margin of 667k votes.

Does that seem too high?  It shouldn’t – these are typical margins for someone like Marco Rubio, who gets Trumpian numbers with Cuban and Venezuelan voters and pours it on in the red counties.

**Best targets for audits:** Lake, Pasco, Santa Rosa, Volusia, Walton.

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-summary.jpg)

{:.caption}
Source: [Telegram post](https://t.me/CaptainKMapsandStats/41)


## Stats By County

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table2.jpg)

{:.caption}
Source: [Telegram post](https://t.me/CaptainKMapsandStats/41)


{% include seth_worst_counties_for_state heading=true %}


## Broward County

{% include tg-embed url="https://t.me/ElectionHQ2024/4148" %}


## Collier County

{% include tg-embed url="https://t.me/ElectionHQ2024/4609" %}



## Duval County

{% include tg-embed url="https://t.me/ElectionHQ2024/3682" %}

The following PDF shows a map of which precincts in the county had the most abnormal results in 2020:

{% include tg-embed url="https://t.me/ElectionHQ2024/3768" %}




## Hillsborough County (Tampa)

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vixyek/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
Source: [Telegram post](https://t.me/ElectionHQ2024/1031)

<!-- Heat map to be embedded here later, when time permits -->

{% include tg-embed url="https://t.me/ElectionHQ2024/2863" %}


## Lake County

{% include tg-embed url="https://t.me/ElectionHQ2024/3630" %}


According to Seth, Lake County, Florida, is showing some movement towards an audit of the election. See [his Telegram post](https://t.me/ElectionHQ2024/1162) from Aug 31, 2021.


## Marion County

{% include tg-embed url="https://t.me/ElectionHQ2024/4177" %}


## Nassau County

{% include tg-embed url="https://t.me/ElectionHQ2024/4178" %}


## Palm Beach County

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vkokkt/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
Source: [Telegram post](https://t.me/ElectionHQ2024/1583), Oct 2, 2021


## Polk County

{% include tg-embed url="https://t.me/ElectionHQ2024/2945" %}


## Pasco, Volusia, and Lake Counties

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vkoncj/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
Source: [Telegram post](https://t.me/ElectionHQ2024/1592), Oct 2, 2021


## Volusia County

{% include tg-embed url="https://t.me/ElectionHQ2024/3061" %}



## Seth's Predictions vs Actual Results

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="ElectionHQ2024/2260" data-width="100%"></script>


## Possible Voter Roll Manipulation

A number of states, both key competitive states, and not, have machined/trimmed/manipulated their voter rolls for the desired outcomes.  See [New Hampshire](../new-hampshire/), [Alaska](../alaska/), [California](../california/), and [Arizona](../arizona/).

There are other states with bloated rolls, like **Florida** and [Texas](../texas/). [Colorado](../colorado/) doesn’t show much movement in the two parties but tons of new registered indies/others, along with [Washington](../washington/) and [Oregon](../oregon/). [Pennsylvania](../pennsylvania/) and [North Carolina](../north-carolina/) appear to have let their party rolls run clean, showing a tremendous beating getting ready to be done by Trump, but pulled things off differently.  

Phantoms.

{:.caption}
Source: <https://t.me/ElectionHQ2024/788>


## Further Updates

{% include tg-embed url="https://t.me/ElectionHQ2024/2826" %}

{% include tg-embed url="https://t.me/ElectionHQ2024/6271" %}

{% include tg-embed url="https://t.me/ElectionHQ2024/6842" %}



{% include seth_reports_methodology %}

{% include raw_data %}


## Other Reports

* A team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the following reports:

  * "[2020 Presidential Election Startling Vote Spikes](https://election-integrity.info/Vote_Spikes_Report.pdf)" which includes a section on Florida

  * "[2020 Presidential Election Contrast Analysis](https://election-integrity.info/Contrast_Report.pdf)" which includes a section on {{ page.state }}

{% include canvass/get-involved %}

{% include state_telegram_channels %}
