---
title: Iowa Election Analysis by Seth Keshel
state: Iowa
abbr: ia
last_updated: 23 May 2023
---

{% include see_main_article %}

{% include seth_report_intro.html %}

{% include toc %}

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/v1psgoq/?pub=4" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Watch on Rumble](https://rumble.com/v1semf0-episode-134-iowa-state-review.html)

Compared to surrounding states, Iowa has relatively little election fraud. However, it is likely that in the National Field of Screams, Donald Trump's winning margin was cut short by 3.6%, giving cover for serious fraud in [Wisconsin](../wisconsin/) and [Minnesota](../minnesota/).

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

{% include seth_state_details.html %}

While [Ohio](../ohio/) & Iowa were never in play, Iowa matches more with [Minnesota](../minnesota/) & [Wisconsin](../wisconsin/), and [Ohio](../ohio/) goes with [Pennsylvania](../pennsylvania/) & [Michigan](../michigan/).  Iowa is swingy.  Registrations in medium to large counties all check out, and even the statewide number, just a hair in favor of Democrats, accurately forecasted a 1% drift left.  Reagan won Iowa by less in 1984 than in his first campaign.

Iowa is the cleanest state I’ve seen yet.  Keep in mind, “all green” doesn’t mean there is no fraud.  It just means that trends are in keeping with recent political shifts, and also something Iowa provides us – registration by party.

One critical point, magnifying the insane Biden numbers elsewhere in the region, especially [Minnesota](../minnesota/), with a “7% victory" (228k excess), and about 139k in [Wisconsin](../wisconsin/).

#### IA Biden vote % over:  
Obama ’08: -8.4%  
Obama ’12: -7.7%  
Clinton ’16: +16.1%

#### MN Biden vote % over:  
Obama ’08: +9.1%  
Obama ’12: +11.1%  
Clinton ’16: +25.5%

#### WI Biden vote % over:  
Obama ’08: -2.8%  
Obama ’12: 0.6%  
Clinton ’16: +18.0%


{:.caption}
Source: <https://t.me/CaptainKMapsandStats/93>

----


{% include seth_worst_counties_for_state heading=true %}


## Further Updates

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="ElectionHQ2024/2468" data-width="100%"></script>

{% include tg-embed url="https://t.me/ElectionHQ2024/6572" %}

{% include tg-embed url="https://t.me/ElectionHQ2024/6805" %}

{% include tg-embed url="https://t.me/ElectionHQ2024/7226" %}


{% assign events = site.data.events | where: 'state', page.abbr %}
{% include events %}

{% include seth_reports_methodology %}

{% include raw_data %}

{% include canvass/get-involved %}

{% include state_telegram_channels %}
