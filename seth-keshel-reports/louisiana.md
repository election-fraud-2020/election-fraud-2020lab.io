---
title: Louisiana Election Analysis by Seth Keshel
state: Louisiana
abbr: la
last_updated: 23 May 2023
---

{% include see_main_article %}

{% include seth_report_intro.html %}

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

{% include seth_state_details.html %}

Louisiana resembles [Kentucky](../kentucky/) and [West Virginia](../west-virginia/) in registration, being states with heavy historical Democrat registrations but have been going red federally for two decades.  Now the party switching is coming but with less effect (but still pertinent) on results than in other states.

Each gained 76k from 2016 totals, but Biden appears 36k heavy.  Orleans has a clean trend in keeping with population and registration, so nothing visible from my discipline there.  East Baton Rouge, Jefferson, Lafayette, and Caddo are chipping in with some slight excesses, but not majorly compared to population.

Best target for a forensic audit in the state is St. Tammany -- way over norms, a 10k Democrat vote bump as they’ve been stagnant for several cycles in a growing parish.  Republicans had *41 times* the net number of voter registrations there, which leads me to estimate a minimum of 7,000 excess/suspicious votes for Biden.

If Biden is 36k heavy, Trump margin would be 59.5% to 38.8%, or 436k.

**Best target for an audit:** St. Tammany


{:.caption}
Source: <https://t.me/ElectionHQ2024/693>

----

## St. Tammany Parish

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vj8kie/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
Source: [Telegram Post](https://t.me/ElectionHQ2024/1105), Aug 28, 2021

{% include seth_worst_counties_for_state heading=true %}


## Further Updates

{% include tg-embed url="https://t.me/ElectionHQ2024/7226" %}


{% include seth_reports_methodology %}

{% include raw_data %}

{% include canvass/get-involved %}

{% include state_telegram_channels %}
