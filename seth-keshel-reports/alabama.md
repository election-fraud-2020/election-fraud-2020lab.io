---
title: Alabama Election Analysis by Seth Keshel
state: Alabama
abbr: al
last_updated: 23 May 2023
---

{% include see_main_article %}

{% include seth_report_intro.html %}

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

{% include seth_state_details.html %}


Alabamians, as well as the millions of bandwagon Tide fans throughout the world, will value this information.

Alabama trends pretty decently, including in Jefferson, Mobile, and Tuscaloosa counties.

However, Madison County is your prime audit candidate. 13% growth in past decade, so I forecast both to gain, even though Democrats have gone 64k-62k-63k since Obama 08.  This year, Biden pulled a 2x double vote gain up to 87k; this sort of thing can happen if the other party tanks, but Trump gained 13k, which is the most in the current turnout model (2004-present) and just 2k short of Bush’s gain from 2000 (low turnout to 2004).  Madison is at least 12k heavy, if not much more (this still affords the record vote gain for Dem).

Biden appears to be 32k heavy statewide.

If Biden is 32k heavy, Trump would be 62.9% to 35.7%, or 591k.

Most in need of audits – Madison, Shelby, Baldwin, Lee


{:.caption}
Source: <https://t.me/ElectionHQ2024/707>


{% include seth_worst_counties_for_state heading=true %}


## Jefferson County

{% include tg-embed url="https://t.me/ElectionHQ2024/4445" %}



{% assign events = site.data.events | where: "state", page.abbr %}
{% include events %}

{% include seth_reports_methodology %}

{% include raw_data %}

{% include canvass/get-involved %}

{% include state_telegram_channels %}