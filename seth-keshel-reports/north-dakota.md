---
title: North Dakota Election Analysis by Seth Keshel
state: North Dakota
abbr: nd
last_updated: 23 May 2023
---

{% include see_main_article %}

{% include seth_report_intro.html %}

![Seth Keshel County Trend Map for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-map.jpg)

{% include seth_chart_legend.html %}

![Seth Keshel County Trends for {{ page.state }}](/images/seth-keshel/{{ page.abbr }}-table.jpg)

{% include seth_state_details.html %}

North Dakota doesn’t give me party registration information but has trended heavily GOP since it was within ten points and Obama won Cass (Fargo) in 2008.  

Fargo is a big energy city, so working classes are pushing away and gravitating to the right – Biden is at least 6k heavy based on recent trends and the inverse drift of the two parties, though growth could be expected with population growth and high turnout – otherwise that could be as much as 10k.

Burleigh has been in an inverse Trend since Obama carried it in 2008, and appears about 2k heavy.  All other counties trend clean.

Overall, one of the top 5 cleanest trending states.

Audit targets: Cass, Burleigh


{:.caption}
Source: <https://t.me/ElectionHQ2024/717>

----

{% include seth_worst_counties_for_state heading=true %}

## Further Updates

{% include tg-embed url="https://t.me/ElectionHQ2024/7226" %}


{% include seth_reports_methodology %}

{% include raw_data %}

{% include canvass/get-involved %}

{% include state_telegram_channels %}
