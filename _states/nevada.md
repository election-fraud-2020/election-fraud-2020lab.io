---
title: Nevada
state: Nevada
abbr: nv
heading: Election Fraud in Nevada
# subtitle: ...
meta_desc: An overview of key election integrity issues in Nevada during the 2020 US Presidential elections.
number_of_disputed_ballots: At *least* 130,000 votes
biden_winning_margin: 33,596 votes (2.4%)
electoral_college_votes: 6 votes
forensic_audit: ❌ Not yet
summary: |
    - Over 42,000 people voted more than once
    - 1,500 deceased persons voted
    - 42,000 voter address anomalies
    - 8,952 more votes than individuals recorded as voting
    - Obstruction of audits
    - Courts blocking the presentation of evidence
battleground_state: true
last_updated: 4 May 2022
---

{% include state_stats.html %}

{% include state_summary_points %}

{% include toc %}


## Key Allegations

The following evidence of election fraud in Nevada was presented to the US Senate Committee on Homeland Security and Governmental Affairs, Full Committee Hearing, examining irregularities in the 2020 Election, December 16, 2020. Chaired by Senator Ron Johnson. [Jesse Binnall presented testimony](https://www.hsgac.senate.gov/imo/media/doc/Testimony-Binnall-2020-12-16.pdf) that:

* Over 42,000 people voted more than once. Experts reviewed the list of actual voters, comparing it to other voters with the same name, address, and date of birth. They also detected instances of people using different variations of their first name. [^1]

* 1,500 dead people were detected as having voted [^1]

* 19,000 voted in Nevada even though they didn’t live in Nevada. This was discovered by data matching with US Postal Service’s national change of address database, amongst other sources (taking care to exclude military voters and students). [^1] 

* 8,000 voted from non-existent addresses  [^1]

* 15,000 votes came from commercial or vacant addresses (based on US Postal Service’s data on commercial addresses or addresses vacant for more than 90 days) [^1]

* 4,000 non-citizens voted, as determined by comparing official DMV (driver’s licence) records of non-citizens  [^1]

* The legal team were denied further access to perform forensic audits

   "We were denied access to almost anything meaningful that would allow us to verify [results, including] the paper backups for the electronic machines--we were denied any access to those, except from one machine in the entire state of Nevada. We brought forensic experts all the way to Nevada--people that could have discovered this information, people that could have told us what happened with these machines, and we weren't allowed near them, we weren't allowed any forensic audit--nothing that could have given us any transparency. That’s what we were denied, in Nevada." [^1]


> “All in all, experts identified 130,000 unique instances of voter fraud in Nevada, but the actual number is almost certainly higher. This was not statistical sampling, but by analyzing and comparing the list of actual voters with other lists, most of which are publicly available.
>
> “...We can’t pretend we have a clean election when there’s evidence to the contrary.”
> 
> <small>--- Jesse Binnall, Nevada lawyer  [^1]</small>


{% include 2000-mules-summary %}


{% include election_integrity_scorecard %}


## Court Cases

Lawyer Jesse Binnall has testified that:

* Nevada's lower court used technicalities to limit the presentation of evidence and limit the amount of witnesses we could bring forward saying that we "couldn't introduce any live testimony but only 15 depositions ... only 15 depositions to show 130,000 instances of voter fraud [^1]

* When it later went to the Supreme Court of Nevada they "gave us two hours to brief the issues before immediately coming down with a decision. We were never fully considered by those courts." [^1]

{% include court_case_link %}


{% include seth_state_page_summary %}


## Vote Count Irregularities

In August 2021, reports emerged that there were 8,952 more votes counted than were officially cast. 15 of Nevada's 17 counties certified more ballots cast than there were individual voters recorded as voting. Clark (5,869 more) and Washoe (2,191) had the largest discrepancies, according to the analysis. [^3]

Data is being published online at [VoteRef.com](https://voteref.com/) where visitors are be able to "crowd-source" any errors.


{% include zuckerberg_interference.html %}

{% include bloated_voter_rolls %}

{% include canvass/get-involved %}

{% include news %}

{% include state_telegram_channels %}

{% include state_rep_support %}

{% include raw_data %}

{% include further_leads %}


### Footnotes & References


[^1]: Jesse Binnall, lawyer. [youtu.be/fq8OPTIEf2U?t=915](https://youtu.be/fq8OPTIEf2U?t=915) (around 15:15 mark, onwards). [Read his full statement](https://www.hsgac.senate.gov/imo/media/doc/Testimony-Binnall-2020-12-16.pdf) (PDF). This was during the US Senate Committee on Homeland Security and Governmental Affairs, Full Committee Hearing, see [^2].
   
    His conclusion was that:
    
	*"Thousands upon thousands of Nevada voters had their voices cancelled out by election fraud and invalid ballots, legislators made drastic changes to election law, and evidence from data scientists and brave whistleblowers confirms this.*
	
	*"We cannot ignore voter fraud away we can't just wish it away unfortunately that's what the media these past weeks has been trying to do in the most biased reporting I think I've ever seen---even in headlines they try to claim that the evidence I've seen with my own eyes is somehow not there. We can't wish it away, it is just simply right now a gaslighting attempt on America. This is real, this happened, and we have to address it."*  

[^2]: US Senate Committee on Homeland Security and Governmental Affairs, Full Committee Hearing, examining irregularities in the 2020 Election, December 16, 2020. Chaired by Senator Ron Johnson. Summary video available at [youtu.be/fq8OPTIEf2U](https://youtu.be/fq8OPTIEf2U), additional full testimonies available at [hsgac.senate.gov/examining-irregularities-in-the-2020-election](https://www.hsgac.senate.gov/examining-irregularities-in-the-2020-election). 

[^3]: Silver State Times: "[More votes counted than cast in Nevada 2020 General Election, analysis of voting files shows](https://silverstatetimes.com/stories/606414657-more-votes-counted-than-cast-in-nevada-2020-general-election-analysis-of-voting-files-shows)", Aug 5, 2021.