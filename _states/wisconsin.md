---
title: Wisconsin
heading: Election Fraud in Wisconsin
# subtitle: ...
state: Wisconsin
abbr: wi
meta_desc: An overview of key election integrity issues in Wisconsin during the 2020 US Presidential elections.
number_of_disputed_ballots: 221,323 votes (approx.)
biden_winning_margin: 20,682 votes (0.6%)
electoral_college_votes: 10 votes
forensic_audit: ❌ Subpoenas were issued, but investigations were heavily obstructed via lawsuits and eventually paused
summary: |
    - Up to 200,000 ballots counted without independent observation
    - Poor identity verification practices
    - Approx. 226,000 absentee votes tainted by illegal solicitation and invalid applications
    - Incomplete/altered ballot certificates
    - 205,000 voters removed from rolls in the months following the election
battleground_state: true
last_updated: 21 May 2023
---


{% include state_stats.html %}

{% include state_summary_points %}

{% include toc %}


## Key Issues Raised During US Senate Committee Hearing

Absentee voting in Wisconsin is treated quite differently than other parts of the country. The law clearly states that “Results which do not comply with those regulations may not be included in the certified results of any election”, and yet, after an examination of ballots and envelopes:

* More than 3,000 certificates were incomplete or altered. These were identified during the recount. [^2]

* In Madison and Milwaukee, Wisconsin, 28,395 people allegedly voted without
identification [^6]

* 2,000 ballots in Dane and Milwaukee counties were counted despite no clerk initials being present [^2]

* Breaches of the "Indefinitely Confined" category. Many voted, without identification, with reasons that were not valid according to law. 28,395 were explicitly identified, including a state elector, poll workers, and many others. [^2]

* Madison County created their own system: events in parks, 5 weeks before election, boxes spread around with lax rules, against state laws. 17,271 in this category. Then, in a rather obvious attempt to avoid later scrutiny, the City took those ballots and mixed them in with all the other absentee ballots so that it would be nearly impossible to identify all the illegal votes cast. [^2]

* Poor security practices by clerks:  [^2]
   - Pre-printing their initials on ballot certifications
   - Certifying unknown persons
   - Envelopes witnessed by unauthorized poll workers
   - Certifications submitted that failed to indicate in-person voters
   - 386 sealed envelopes opened by clerks after the election in Milwaukee County and others in Dane County
   - Nearly 170,000 ballots cast without a separate application

> 3 million *properly* voted in the state of Wisconsin. More than 200,000 identified during this recount, did not, but those votes got counted, and our statute says they should not have been. That, in our view, is a taint on our election in Wisconsin.”
>
> <small>--- James R. Troupis, testifying to US Senate Committee on Homeland Security and Governmental Affairs hearing, examining irregularities in the 2020 Election. [^4]</small>


{% include 2000-mules-summary %}


## Key Allegations from Trump Legal Team

* Vote counting rooms were announced as “closed down”, and yet counting continued, without independent observers present, in violation of law. It's estimated that up to 200,000 votes may have been counted in this illegal manner. [^1]

* A significant number of out of state voters  [^1]

* A number of votes linked to deceased people (less than most states, but still some) [^1]

* Lax absentee ballot rules. Despite rejection from the state's Supreme Court, the Secretary of State relaxed the rules due to Covid-19. [^1] This included:

	- Votes being illegally solicited
	- Absentee votes without applications
	- Absentee votes filled out improperly
	- Absentee votes by people claiming indefinitely confined, while they were out playing sports, etc.

   Approximately 226,000 votes fell into this category, in Dane County and Milwaukee County alone.


{% include election_integrity_scorecard %}


{% include seth_state_page_summary %}


{% include dr-frank/state-summary
    correlation=nil
    registrationsOver100=nil
    extra1=nil
    exampleCounty=nil
    exampleDate=nil
%}


## Further Issues

* Significant statistical anomalies or "red flags" in the election data, including at least one major "vote dump" with an abnormally high swing to Biden that placed him in the lead [^8]

* Payments to Native Americans to vote were supposedly “orchestrated by the Biden campaign . . . [with] Visa gift cards, jewelry, and other ‘swag.’” [^7]

* In August 2021, ten months after the election, Wisconsin removed 205,000 voters from its official rolls after they were deemed inactive and did not responding to mailings. [^9] Donald Trump questioned why this wasn't done prior to the election. [^10] Reportedly, the election commission voted in 2019 to remove voters *after* the 2021 elections, and decided at a June meeting not to change that decision.


{% include zuckerberg_interference.html %}


## Wisconsin Supreme Court Ruling

A narrowly divided Wisconsin Supreme Court on Dec 14, 2020 rejected Trump’s lawsuit about an hour before the Electoral College cast Wisconsin’s 10 votes for Democratic presidential candidate Joe Biden. In the 4-3 ruling, the court’s three liberal justices were joined by Hagedorn, who said three of Trump’s four claims were filed too late and the other was without merit. [^5]

According to James R. Troupis's testimony to the US Senate Committee, the Wisconsin Supreme Court was urged by Biden campaign not to address any substantive issues. As a result, Troupis reported that:

> The [court's] decision itself is premised not on an analysis of the law, nor the analysis of the claims, [but] an idea that we should not have a transparent system, that does not address these things. It's a sad day, frankly, when the whole opposition doesn't argue we're wrong, it argues we shouldn't be heard. That's a strange thing in a state that's so transparent as ours."
>
> <small>--- James R. Troupis, testifying to US Senate Committee on Homeland Security and Governmental Affairs hearing, examining irregularities in the 2020 Election. [^4]</small>

[Read the Court's Full Statement](https://www.wicourts.gov/sc/opinion/DisplayDocument.pdf?content=pdf&seqNo=315395)

{% include court_case_link heading="Further Court Results" %}


## Ballot Recounts 

Dane and Milwaukee Counties performed recounts at the request of the Trump campaign, involving recounting the ballots, mostly by hand, using a different team of people from those counting on election night.  These were completed by Nov 29, 2020, and awarded an extra 87 votes to Trump and 132 to Biden. [^recount1]

[Read the State Commission's Report](https://elections.wi.gov/elections-voting/recount)

Note that a recount does *not* tend to identify chain of custody issues and the alleged injection or manipulation of fraudulent ballots, such as those noted in the above sections.

In response Donald Trump tweeted that “The Wisconsin recount is not about finding mistakes in the count, it is about *finding people who have voted illegally*, and that case will be brought after the recount is over, on Monday or Tuesday. We have found many illegal votes. Stay tuned!”

[^recount1]: Yahoo News: "[Trump falls short in Wisconsin recount he paid $3 million for](https://news.yahoo.com/trump-falls-short-wisconsin-recount-204200261.html), Nov 30, 2020.


## Election Audits

Both the Wisconsin State Assembly and the State Senate commissioned election investigations.

Why are there three audits underway? Representative Timothy Ramthun explains in [this Rumble video](https://rumble.com/vm21tk-ramthun-report-episode-28-why-are-there-3-audits-in-wisconsin-which-one-is-.html) that the first two are not what the people are demanding.

#### 1. Michael Gableman Investigation

The State Assembly commissioned Michael Gableman, former Supreme Court Justice, assigning him to be Special Counsel, which gives him more authority in carrying out investigations. Gableman attended both the Arizona Forensic Audit and Lindell's Cyber Symposium, to learn and gather information. [^15] 

Despite multiple obstructions to his subpoenas and investigative questioning, he has found numerous examples of unlawful activity in the state. Some investigations are still underway, while some of his findings have been released which you can find the [*Further Updates*](#further-updates) section below.


#### 2. Legislative Audit Bureau

The Senate commissioned the Legislative Audit Bureau to perform an audit investigation, about which not much is publicly known. It's expected to be finished around fall in 2021.

#### 3. Wisconsin State Assembly Elections Committee Forensic Audits

State Representative Janel Brandtjen (Republican), head of Wisconsin State Assembly's Elections Committee travelled to [Arizona](../arizona/) in order to watch the [forensic audit in Maricopa County](../arizona/#2-senate-forensic-audit-of-maricopa-county) and has led the push for similar forensic audits in Wisconsin.

Around Aug 6, 2021, she issued official subpoenas to two different counties, demanding that they turn over all ballots and voting equipment for the state to conduct a top-to-bottom cyber investigation into the results. [^14] [^11] Earlier this year, the assembly committee voted to give Brandtjen's committee both investigatory powers as well as the power to issue subpoenas, which she is now doing.

{:.small}
> Legislators have been hearing from thousands of disgruntled constituents regarding their concerns with the November 2020 election. From outside money pouring into Democrat controlled communities, to individuals engaging in questionable activities; from improper guidance given to clerks from the Wisconsin Elections Commission, to clerks illegally prompting voters to declare themselves "indefinitely confined". The list goes on and on."
>
> "I understand the Legislative Audit Bureau is conducting an audit, however, many constituents have raised objections to it due to the length of time it has taken and the lack of specifics, as it is open-ended.
> 
> "I sincerely welcome and applaud these efforts, however, the people of Wisconsin demand and deserve a transparently, full, cyber-forensic audit as described in the subpoenas submitted to both Milwaukee and Brown Counties. The clock is ticking as the 2022 elections will commence in just a matter of months."
>
> <small>--- Rep. Janel Brandtjen, Aug 6, 2021, explaining her reasoning for the subpoenas [^14] [^11]</small>

The state's Elections Commission (a different department to The Campaigns and Elections Committee) has denied some of Brandtjen's claims about voter registration databases and voter ID checks [^denial1] [^denial2] , but their responses do not mention the other issues raised.

The two subpoenas were sent to:

* **Milwaukee County**, where results posted early in the morning catapulted Biden to victory in the state (with 69% of the vote), however Republicans have questioned the timing of the release. The clerks say there was nothing suspicious, but that was just how long it took to count the ballots. [^11]

* **Brown County**, which includes city of Green Bay where Trump won with 52% of the vote. Republicans have been accusing the Mayor of Green Bay of letting a Facebook-funded consultant run that city's election. This consultant had the keys to the city's ballot counting location. [^11]

Clerks for these counties must appear before the committee on **Sept 7th, 2021** and they must turn over all the ballots, voter names and addresses, election equipment, tabulators, servers, etc. 

It's not clear whether these two counties will readily comply with the subpoenas or whether they will push back and challenge them in court, as happened in Arizona. Milwaukee County has not yet issued a response. Brown County is currently reviewing the subpoena.

While there has been a lot of support for her actions, Democrats have criticized the subpoenas, calling Ms Brandtjen "an enemy of our democratic system". Minority Leader Gordon Hintz said of the forensic audit: "It really should be viewed as an attack on our country and an attack on our election system". [^11]

*Why would Democrats describe a forensic audit, done lawfully, as "an attack on our election system?"* 

State Representatives Janel Brandtjen and Timothy Ranthum continue to lead the efforts towards forensic audits.

## Racine County Sheriff's Report

On Oct 28, 2021, the Racine County Sheriff announced at a press conference that had evidence of widespread election law violations across the state, including class I felony crimes being committed. [^16]

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">BREAKING: Racine Co., WI Sheriff&#39;s Office says the statute broken by election officials is a &quot;Class I felony,&quot; plus multiple misdemeanors <a href="https://t.co/k9otx0BdhU">pic.twitter.com/k9otx0BdhU</a></p>&mdash; RSBN 🇺🇸 (@RSBNetwork) <a href="https://twitter.com/RSBNetwork/status/1453767412126400521?ref_src=twsrc%5Etfw">October 28, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

In the report, the investigation determined that workers in long-term care facilities were told to vote for any patients who were not lucid enough to vote on their own. They were advised to do this by Wisconsin election officials.

Despite the Racine Sheriff attempting to send the reports and investigation notes to the Wisconsin Attorney General, they were rejected. The Wisconsin AG has been scrutinized for attempting to protect WEC officials who are being investigated as part of another election integrity case. [^16]

See [*Further Updates*](#further-updates) below.



{% include canvass/get-involved %}

{% include news %}

{% include state_telegram_channels %}

{% include state_rep_support %}

{% include raw_data %}

{% include further_leads %}


### Footnotes & References


[^2]: James R. Troupis, Wisconsin Attorney. [Read his full statement](https://www.hsgac.senate.gov/imo/media/doc/Testimony-Troupis-2020-12-16.pdf) (PDF). This was during the US Senate Committee on Homeland Security and Governmental Affairs, Full Committee Hearing, see [^2].

[^3]: US Senate Committee on Homeland Security and Governmental Affairs, Full Committee Hearing, examining irregularities in the 2020 Election, December 16, 2020. Chaired by Senator Ron Johnson. Summary video available at [youtu.be/fq8OPTIEf2U?t=560](https://youtu.be/fq8OPTIEf2U?t=560), additional full testimonies available at [hsgac.senate.gov/examining-irregularities-in-the-2020-election](https://www.hsgac.senate.gov/examining-irregularities-in-the-2020-election). 

[^4]: James R. Troupis, Wisconsin Attorney. [youtu.be/fq8OPTIEf2U?t=1114](https://youtu.be/fq8OPTIEf2U?t=1114) (around 18:34 mark). [Read his full statement](https://www.hsgac.senate.gov/imo/media/doc/Testimony-Troupis-2020-12-16.pdf) (PDF). This was during the US Senate Committee on Homeland Security and Governmental Affairs, Full Committee Hearing, see [^2].

[^1]: <span class="pill">Censored</span> "The Current State Of Our Country", Rudy Giuliani, Trump Legal Team, <https://youtu.be/NAzCECx8XeE?t=990> (around 16m 30s mark). Unfortunately, this video has been censored by YouTube, and no longer appears to be accessible via this link. We will update the address once we have a new link.

[^5]: The Epoch Times: "[Wisconsin Supreme Court Judge Explains Ruling Against Trump Election Lawsuit](https://www.theepochtimes.com/wisconsin-supreme-court-judge-explains-ruling-against-trump-election-lawsuit_3627184.html)"

[^6]: Mentioned by John R. Lott, Senior Advisor for Research and Statistics at the US Department of Justice, in his paper "[A Simple Test for the Extent of Vote Fraud with Absentee Ballots in the 2020 Presidential Election: Georgia and Pennsylvania Data](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3756988&download=yes)". He provides analysis of the statistics which point to the likelihood of fraud. He references allegations made during the Senate Hearing on Election Security and Administration, December 16, 2020 <https://www.cspan.org/video/?507292-1/senate-hearing-election-security-administration>

[^7]: Peter Navarro: “[The Immaculate Deception: Six Key Dimensions of Election Irregulaties](https://bannonswarroom.com/wp-content/uploads/2020/12/The-Immaculate-Deception-12.15.20-1.pdf)”, December 15, 2020; and  
    Paul Bedard, “[Pro-Biden effort offered Native Americans $25-$500 Visa gift cards and jewelry to vote](https://www.washingtonexaminer.com/washington-secrets/pro-biden-effort-offered-native-americans-25-500-visa-gift-cards-jewelry-to-vote)”, Washington Examiner, December 3, 2020.

    See also Navarro's follow-up reports:  
    "[The Art of the Steal](https://www.priestsforlife.org/elections/pdf/navarro-art-of-the-Steal.pdf)" (Volume 2 of 3)  
    "[Yes, President Trump Won](https://mdrnrepublican.files.wordpress.com/2021/04/thenavarroreportvolumeiiifinal1.13.21-0001.pdf)" (Volume 3 of 3)

[^8]: Report: "[2020 Presidential Election Startling Vote Spikes: Statistical Analysis of State Vote Dumps in the 2020 Presidential Election](https://www.scribd.com/document/496106864/Vote-Spikes-Report)" by Eric Quinnell (Engineer); Stan Young (Statistician); Tony Cox (Statistician); Tom Davis (IT Expert); Ray Blehar (Government Analyst, ret’d); John Droz (Physicist); and Anonymous Expert.

    The report explains that the election data anomolies are “red flags” based on the rate at which votes were added, which is exceedingly high compared to the rate observed at all other time intervals.

[^9]: The Epoch Times: "[Wisconsin Election Officials Remove Over 205,000 From Voter Rolls](https://www.theepochtimes.com/wisconsin-election-officials-remove-over-205000-from-voter-rolls_3934775.html)", Aug 5, 2021.

[^10]: Donald J. Trump: "Wisconsin has just cancelled 205,000 voter registrations because they say they could not find the voters. Why did they wait until AFTER the election? Would this mean that we would have won Wisconsin? Congratulations!"  
    From [this Telegram post](https://t.me/real_DonaldJTrump/13824), Aug 8, 2021.

[^11]: Facts Matter with Roman Balmakov: "[Lawmaker Issues Official Subpoenas for Ballots and Election Machines from 2 Counties](https://www.youtube.com/watch?v=X99DELK21zc)", Aug 14, 2021. [Alternate link](https://www.theepochtimes.com/facts-matter-aug-13-election-materials-demanded-from-2-counties-lawmaker-issues-official-subpoenas_3948300.html).

[^denial1]: Urban Milwaukee: "[State Elections Commission Refutes Rep. Brandtjen](https://urbanmilwaukee.com/2021/07/28/state-election-commission-refutes-rep-brandtjen/)", Jul 28, 2021.

[^denial2]: The Wisconsin Elections Commission posted a [Statement on Recent Election Misinformation](https://elections.wi.gov/node/7478), Jul 27, 2021. It covers voter registration database and voter ID checks, but not the other issues raised by Janel Brandtjen.

[^14]: Press Release from Janel Brandtjen: "[Brandtjen Moves Forward with Audit](https://www.thewheelerreport.com/wheeler_docs/files/080621brandtjen.pdf)" (PDF), Aug 6, 2021.

    Also reported in:

    - Trending Politics: "[Wisconsin Republican Announces Subpoenas, Launches ‘Top- to-Bottom’ Review of 2020 Election in Select Counties](https://trendingpolitics.com/breaking-wisconsin-republican-announces-subpoenas-launches-top-to-bottom-review-of-2020-election-in-select-counties-knab/)", Aug 6, 2021. 

    - Associated Press: "[Republican issues subpoenas for Wisconsin election info](https://apnews.com/article/joe-biden-elections-wisconsin-election-2020-subpoenas-3952211bd482a785d3a8fbe9cc4d7c86)", Aug 7, 2021.

    - Some further details are also provided by [The Gateway Pundit](https://www.thegatewaypundit.com/2021/08/wisconsin-rep-janel-brandtjen-issues-subpoenas-audit-2-counties/)

[^15]: Associated Press: "[Lead investigator in 2020 Wisconsin election audit travels to Arizona, election fraud symposium](https://www.weau.com/2021/08/12/lead-investigator-2020-wisconsin-election-audit-travels-arizona-election-fraud-symposium/)", Aug 12, 2021.

[^16]: The Liberty Loft: "[Racine County Sheriff announces multiple violations of election law in Wisconsin](https://thelibertyloft.com/2021/10/28/racine-county-sheriff-announces-multiple-violations-of-election-law-in-wisconsin/)", Oct 28, 2021.