---
title: Georgia
state: Georgia
abbr: ga
heading: Election Fraud in Georgia
# subtitle: Subtitle to go here
meta_desc: An overview of key election integrity issues in Georgia during the 2020 US Presidential elections.
number_of_disputed_ballots: Up to 300,000 votes
biden_winning_margin: 11,779 votes (0.25%)
electoral_college_votes: 16 votes
forensic_audit: ❌ Partial ballot inspection for 1 county only
summary: |
    - Unobserved ballot counting
    - Outstanding/unresolved integrity issues with Dominion counting machines
    - Ballots appearing to have been duplicated by copy machine 
    - Massive spike in number of ballots manually adjudicated
    - High numbers of underage voters, unregistered voters, deceased voters, incarcerated voters, and out-of-state voters
    - Widespread lack of verification of absentee ballots
    - Significant statistical anomolies
    - Over 40 witnesses gave testimony in Georgia on December 9-14
    - Massive errors found during a ballot audit
battleground_state: true
last_updated: 4 May 2022
---

*NOTE: The following issues relate primarily to the November 2020 Presidential election in Georgia. A [run-off election](https://www.findlaw.com/voting/how-u-s--elections-work/what-is-a-runoff-election-.html) occurred subsequently on Jan 5, 2021. It's not yet clear whether the following issues persisted into that election also.*


{% include state_stats.html %}

{% include state_summary_points %}

{% include toc %}


The chairman of the judiciary subcommittee in Georgia that examined evidence of fraud in the 2020 election, released a [scathing 15-page report](http://www.senatorligon.com/THE_FINAL%20REPORT.PDF) calling the results of the 2020 election, “untrustworthy” and recommending that the certification of the results be rescinded. [^4]

> The November 3, 2020 General Election was chaotic and any reported results must be viewed as untrustworthy.  The Subcommittee took evidence from witnesses and received affidavits sworn under oath.  The Subcommittee heard evidence that proper protocols were not used to ensure chain of custody of the ballots throughout the Election, after the opening of ballots prior to the Election, and during the recounts. The Subcommittee heard testimony that it was possible or even likely that large numbers of fraudulent ballots were introduced into the pool of ballots that were counted as voted; there is no way of tracing the ballots after they have been separated from the point of origin."
>
> <small>--- The Honorable William T. Ligon, Chairman Of The Election Law Study Subcommittee of The Georgia State Standing Senate Judiciary Committee [^5]</small>


> After 12 hours of testimony [I conclude that] you can’t make some of this stuff up. I mean it’s just unbelievable what we’ve seen in this 12 hours of testimony. I’m embarrassed. It’s an embarrassment for our state and I am more and more convinced now that this is a well-orchestrated, well-coordinated effort, by several groups to commit widespread and systemic fraud."
> 
> <small>--- Georgia Senator Brandon Beach, Georgia’s Senate Judiciary Subcommittee on Elections [^7]


A 5-hour segment of the Senate's hearing, including eyewitness accounts is shown below:

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/u5ZP_HpBKos?start=404" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


## Key Allegations

* Failures in ballot "chain of custody" procedures, where ballot batches were taken for same-day transport, yet took three days to arrive at their destination [^7]

* During counting in Fulton County, the Election Director stated that out of 113,130 votes scanned up until that point, 106,000 of them had to be manually adjudicated (a massive 93.7%). Either the machines were faulty, or a large number of paper ballots had issues that prevented them from passing the computerized scan. [^6]

* Vote counting rooms were announced as “closed down”, and yet counting continued, without independent observers present, in violation of law. [^1] [^10] [^25]

   At State Farm Arena, evidence of this was streamed live over the internet. After observers left, ballots were pulled out from underneath a covered desk. Trump's Legal Team estimates this could account for 37,500--120,000 votes. [^12] [^7] [^2]
   
* Ballots appearing to have been duplicated by copy machine [^15] ([further details below](#machine-duplicated-ballots))

* 1,700 instances of double-voting. [^7] 

  (Georgia's Secretary of State promised to investigate and prosecute over a thousand people that double-voted in the primary election. As of December 23rd, it was reported that there has not been one investigation nor one prosecution.) [^7]

* Auditing of records found a high number of underage voters (66,247), unregistered voters (2,423), and deceased voters (10,315) [^1] [^3] [^7] [^25]

* Independent video evidence showing how, in Coffee County, votes could be easily counted multiple times via the machine [^1]

* Outstanding/unresolved integrity issues with Dominion counting machines. Learn more about [Dominion](/in-detail/dominion-voting-machines/).

* Statistical anomalies, including:

  - Three notable "vote dumps" with unexpected swings to Biden, with the largest being over 107,040 votes. These instances were labelled “red flags” not merely based on the one-sided swings, but also based on the rate at which votes were added to the tally, far higher than the rate observed at all other time intervals. [^14]

    <img src="/images/spike-georgia.png" class="image-border" width="3301" height="2550" alt="Georgia Election Vote Spike Chart">

    {:.caption}
Source: DeepCapture.com [^16]

  - In one period of counting, 120,000--138,000 votes were tallied, of which only 2,000 were for Trump (a highly unusual low rate of 1.7%) [^1]
  
  - Trump’s percentage of absentee votes was consistently lower in Fulton County border precincts (where it's alleged significant fraud occurred) than in the precincts just across the street in neighboring counties (a difference of 5.8% -- 17.3%). [^11]

  - Mathematician Edward Solomon alleges via a sworn affidavit that hundreds of ballot batch tallies across multiple precincts suspiciously all came out to be 5.5555% (or 1/18th) for Trump, which suggests there may have been a fraudulent computer algorithm switching votes to reach a specific target fraction. The algorithm appeared to be switched on for short periods, and then switched off and moved to another precinct to avoid detection. He predicts that 25,630 votes were stolen using this method in Atlanta alone. [See his 12min video](https://rumble.com/vdnfcf-edward-solomon-geometric-proof-for-georgia.html). [^19]
  
* There were 4,926 out-of-state voters (those registered in another state, yet voted in Georgia). A further 15,000 changed their address last-minute, after registration deadlines. 395 people voted in more than 1 state. [^1] [^3] [^8]

* 2,560 felons with incomplete sentences voted, illegally [^3] [^7] [^25]

* 305,701 absentee ballots were applied for too early. 2,664 absentee ballots were sent to voters too early. [^3]

* Absentee ballots were not checked for validity. [^1] The rejection rate for absentee ballots has traditionally been 3% in 2016, 3.5% in 2018, and yet in the November 2020 election, after a 6x increase in absentee ballots requested, was a mere 0.34%. Had the statutory procedure for signature matching, voter eligibility and voter identity verification been followed, there should have been 38,250--45,620 absentee ballots rejected--far more that Biden's victory margin. [^3]

* Staff at Atlanta's State Farm Arena polling center describe:

  * Witnessing absentee ballots arriving "in rolling bins 2,000 at a time" rather than in sealed, numbered boxes [^22]

  * Disagreeing on counts, sometimes with a difference of hundreds of ballots [^22]

  * Seeing overvotes being judged inconsistently (ballots where more than one candidate is marked). Apparently, no written instructions were provided on how to adjudicate ballots, only verbal instructions. [^22] [^23]

* Eye-witness testimony of highly-secretive and suspicious activity at Wigwam Resort, Litchfield, Arizona in Sept 2020, two months prior to the election, that appeared to be a significant election fraud and ballot harvesting operation, with two large ballrooms full of equipment, and transporting ballots between Arizona and Georgia. The witness was former Police Detective Gerald Buglione. [^24]

* Testimony with video and photographic evidence of ballots being shredded in Cobb County. [^9] 

  Furthermore, according to witness Susan Knox, when she called 911 to report the shredding, police never responded. She was told later that police officials were under instruction not to respond to anything related to election fraud, or anything at Cobb County regarding the election, but to pass it on to GBI or SoS, yet there was no response from them either. [^9]
  

* Despite ongoing disputes and incomplete audits, vote tallies were certified by Governor Brian Kemp, Secretary of State Raffensperger and Lieutenant Governor Geoff Duncan [^1]

* Georgia officials in Fulton County (Democratic) [have fired](https://twitter.com/paulsperry_/status/1340143044507668487?s=27) whistleblower election officials.

* In total, approx. 300,000 disputed votes


{% include 2000-mules-summary %}


## Allegations from President Trump

President Trump also elaborated on the figures he is questioning on a phone call to Georgia Secretary of State Brad Raffensperger which was leaked to the Washington Post. [Newsmax provides a more detailed transcript](https://www.newsmax.com/politics/trump-georgia-raffensperger/2021/01/03/id/1004057/) of the call with his statistics.


## Witness Statements, Dec 2020

Over 40 witnesses gave testimony during a media conference at Cobb Country Republican Party Headquarters in Georgia on Dec. 9--14. The witnesses presented evidence of election irregularities and fraud they had collected during the 2020 election ballot counts and recount.

NTD News has published a [1-hour compilation video](https://www.youtube.com/watch?v=3RV57sFsojA) of the key witness testimonials, shown below, as well as a detailed [archive of over 40 witness statements](https://www.ntd.com/election-witnesses-speak).

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/3RV57sFsojA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

{:.small.muted}
YouTube has censored the above clip. Shorter clips are available direct on [the NTD website here](https://www.ntd.com/election-witnesses-speak). If you aware of an alternate link to the full 1-hour video, please [let us know on Telegram]({{ site.data.globals.telegramChatLink }}) or via the comments section below.


## Machine-Duplicated Ballots

Numerous poll workers who observed the counts allege that hundreds (if not thousands) of ballots appeared to have been duplicated by copy machine. This was evidenced by:

  1. Being printed on different paper
  2. Being without fold marks (which would be required when requesting a mail-in ballot via mail)
  3. Having identical black marks on large numbers of ballots
  4. The marks not appearing to have been made by an ink pen 

The claims involve six witnesses with [affidavits](https://www.legalzoom.com/articles/what-is-an-affidavit-and-how-is-it-used):

  * Fulton County poll manager Suzi Voyles discovered 107 ballots for Joe Biden that appeared to have been duplicated by a copy machine. Her reports were ignored, and she was later fired by the county. At least three other poll workers (Robin Hall, Judy Aube, and Barbara Hartman) observed the same thing and have joined Voyles in swearing under penalty of perjury that they looked fake. [^15] [^17]

  * Carlos E. Silva, a registered democrat, declared in a Nov. 17 affidavit that he observed a similar “perfect black bubble” in thousands of absentee ballots for Biden during recounts in both DeKalb County and Cobb County. "All of these ballots had the same characteristics: they were all for Biden and had the same perfect bubble ... and no other markings in the rest of the ballot.” [^15]

  * Mayra Romera, another registered Democrat, testified that while monitoring the Cobb County recount, she noticed that “hundreds of these ballots seemed impeccable, with no folds or creases. The bubble selections were perfectly made … and all happened to be selections for Biden.” [^15]

Certified poll watcher Garland Favorito ([VoterGA.org](https://voterga.org)) claims that this issue could potentially amount to 10,000–20,000 ballots, more than Biden's winning margin in the state. [^15] 


{% include seth_state_page_summary %}


{% include election_integrity_scorecard %}


{% include zuckerberg_interference.html %}

{% include bloated_voter_rolls %}

{% include trend-analysis-summary %}



## Voter Canvassing

{% include canvass/georgia-braynard %}

{% include canvass/link %}


## Court Cases

{% include court_case_link %}

It does not appear that Georgia courts have heard evidence on the above issues, despite two attempts:

1. A challenge filed on December 4 was rejected by the Fulton County Superior Court because "the paperwork was improperly completed and it lacked the appropriate filing fees". [^13]

2. The case was subsequently appealed directly to the state Supreme Court, asking justices to consider the case before Monday’s meeting of the Electoral College. In a brief order, justices refused to take original juristiction of election fraud claims, stating that “petitioners have not shown that this is one of those extremely rare cases that would invoke our original jurisdiction.” [^13]


## Missing Chain-of-Custody Documentation

In June 2021, reports surfaced about missing documents that were supposed to verify the chain-of-custody of ballots.

> New revelations that Fulton County is unable to produce all ballot drop box transfer documents will be investigated thoroughly, as we have with other counties that failed to follow Georgia rules and regulations regarding drop boxes. This cannot continue.”
> <small>-- Secretary of State Brad Raffensperger [^18]</small>


## VoterGA Ballot Audit

Following the above revelations, and armed with witness affidavits, Garland Favorito and [VoterGA.org](https://voterga.org) pursued litigation, demanding access to the ballots. [^15a] [^15] 

After analysis, they announced the ballot images were "riddled with massive errors and provable fraud", including:
  
- A 60 per cent error rate: 923 of the 1,539 mail-in ballot batch files contained incorrect results [^20]

- Seven  falsified  tally  sheets  containing  850  votes  for  Joe  Biden  but  0  for Donald Trump and Jo Jorgenson. [^21] Examples:

  [![Georgia potentially fraudulent tally sheet](/images/ga-tally-sheet.jpg){:width="200"}](/images/ga-tally-sheet.jpg)
  [![Georgia potentially fraudulent tally sheet](/images/ga-tally-sheet2.jpg){:width="200"}](/images/ga-tally-sheet2.jpg)

- Duplication of at least 4,255 votes, with a large majority going to Biden [^20]

- Three days of missing drop box chain of custody forms for over 5,000 ballots [^21]

- Missing  tally  sheets  for  over  50,000  ballots  that  were  not  uploaded  until months after the audit results were initially published [^21]

These issues are highly significant considering Biden only won Georgia by 11,779 votes.

Full copies of the ballot and tally images as inspected by VoterGA are available at [this Dropbox link](https://www.dropbox.com/sh/jbdazeu1pcxmqqr/AADZBElcG318ebBqfHDIbhqwa?dl=0).


{% include canvass/get-involved %}

{% include news %}

{% include state_rep_support %}

{% include state_telegram_channels %}

{% include raw_data %}

{% include further_leads %}


### Footnotes & References

[^1]: <span class="pill">Censored</span> "[The Current State Of Our Country](https://youtu.be/NAzCECx8XeE?t=990)", Rudy Giuliani, Trump Legal Team (around 16m 30s mark). Unfortunately, this video has been censored by YouTube, and no longer appears to be accessible via this link. We will update the address once we have a new link.

[^2]: <span class="pill">Censored</span> Rudy Giuliani, Trump Legal Team, Episode 98, <https://youtu.be/7vRckA6PqGA?t=210> (around 3:30 mark onwards). Unfortunately, this video has been censored by YouTube, and no longer appears to be accessible via this link. We will update the address once we have a new link.

[^3]: <span class="pill">Censored</span> Rudy Giuliani, Trump Legal Team, Episode 98, <https://youtu.be/7vRckA6PqGA?t=770> (around 12:50 mark onwards). Unfortunately, this video has been censored by YouTube, and no longer appears to be accessible via this link. We will update the address once we have a new link.

[^4]: "[Georgia State Senate Report: Election Results Are ‘Untrustworthy;’ Certification Should Be Rescinded](https://amgreatness.com/2020/12/21/georgia-state-senate-report-election-results-are-untrustworthy-certification-should-be-rescinded/)", American Greatness

[^5]: "The Chairman’s Report Of The Election Law Study Subcommittee of The Standing Senate Judiciary Committee, Summary Of Testimony From December 3, 2020 Hearing", Honorable William T. Ligon, Chairman, <http://www.senatorligon.com/THE_FINAL%20REPORT.PDF>

[^6]: Richard Barron, Fulton County Georgia State Election Director made the statement on television that "We have scanned thus far 113,130 votes and have adjudicated 106,000 of those" (see [video and transcript](https://news.yahoo.com/georgia-spotlight-vote-count-continues-015726478.html)). This adjudication rate of 93.67% was a massive increase from 1.2% in 2016 and 2.1% in 2018, as flagged by Jovan Hutton Pulitzer in his testimony to the Georgia Senate Judiciary Committee, 30 Dec, 2020 ([see YouTube video](https://youtu.be/_PpyoYlGqBg?t=2280), 38min mark).

[^7]: Georgia Senator Brandon Beach, after 12 hours of testimony presented to Georgia’s Senate Judiciary Subcommittee on Elections (December 30, 2020). Brandon Beach's full closing statements are [published here](/in-detail/senator-brandon-beach-election-fraud-testimony/).

[^8]: "[Georgia Election Official Gabriel Sterling Reveals Woman Used His Voting Address After a Month of Defending Election Integrity](https://tennesseestar.com/2020/12/24/georgia-election-official-gabriel-sterling-reveals-woman-used-his-voting-address-after-a-month-of-defending-election-integrity/)", The Tennessee Star

[^9]: Witness Susan Knox alleges this ballot shredding occurred on Fri Nov 20, 2020 at Jim R. Miller Park, Cobb County, Georgia, in testimony given to Senate Judiciary Subcommittee on Elections (December 30, 2020), <https://www.youtube.com/watch?v=Rn0zBbV7miQ>. Her reports to police, the Georgia Bureau of Investigation, and Georgia's Secretary of State (Brad Raffensperger) went unanswered.

[^10]: According to the Chair of the Georgia Republican Party, David J. Shafer, “counting of ballots took place in secret after Republican Party observers were dismissed because they were advised that the tabulation center was shutting down for the night” (Letter dated November 10, 2020 from Doug Collins and David Shafer to Georgia Secretary of State Brad Raffensperger, p. 3). This was noted by John R. Lott, Senior Advisor for Research and Statistics at the US Department of Justice, in his paper "[A Simple Test for the Extent of Vote Fraud with Absentee Ballots in the 2020 Presidential Election: Georgia and Pennsylvania Data](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3756988&download=yes)". He provides analysis of the statistics which point to the likelihood of fraud.

[^11]: John R. Lott, Senior Advisor for Research and Statistics at the US Department of Justice, "[A Simple Test for the Extent of Vote Fraud with Absentee Ballots in the 2020 Presidential Election: Georgia and Pennsylvania Data](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3756988&download=yes)". He provides analysis of the statistics which point to the likelihood of fraud.

[^12]: Detailed analysis with photos and timeline of events is provided by The Epoch Times: [Infographic: What Happened in Atlanta on Election Night](https://www.theepochtimes.com/what-happened-in-atlanta-on-election-night-2_3607130.html)

[^13]: "[Georgia Supreme Court rejects latest Trump election appeal](https://thegrio.com/2020/12/14/georgia-supreme-court-rejects-trump-election-appeal/), The Grio / Associated Press, December 14, 2020

[^14]: Report: "[2020 Presidential Election Startling Vote Spikes: Statistical Analysis of State Vote Dumps in the 2020 Presidential Election](https://www.scribd.com/document/496106864/Vote-Spikes-Report)" by Eric Quinnell (Engineer); Stan Young (Statistician); Tony Cox (Statistician); Tom Davis (IT Expert); Ray Blehar (Government Analyst, ret’d); John Droz (Physicist); and Anonymous Expert.

[^15a]: In a further turn of events, Favorito and other petitioners were ordered to meet at the ballot warehouse on May 28, 2021 to settle the terms of the inspection of the absentee ballots. But the day before the scheduled meeting, the Democrat-controlled county filed a flurry of motions to dismiss the case, delaying the inspection until the judge can hear the county's arguments on June 21. A day later, on May 29, the sheriff's deputies left the warehouse unattended and unlocked for several hours, in violation of their court order. [^15] President Trump subsequently [released a statement](https://www.donaldjtrump.com/news/statement-by-donald-j-trump-45th-president-of-the-united-states-of-america-05.31.21-2) that said the ballots must be protected.

[^15]: RealClear Investigations: "[Why a Judge Has Georgia Vote Fraud on His Mind: ‘Pristine’ Biden Ballots That Looked Xeroxed](https://www.realclearinvestigations.com/articles/2021/06/08/why_a_judge_has_georgia_vote_fraud_on_his_mind_pristine_biden_ballots_that_look_xeroxed_779795.html)", 8 June 2021.

[^16]: Patrick Byrne, traditionally a Libertarian, developed a team of computer specialists to investigate election integrity issues including the notorious one-sided "spikes" found in several states and has written extensively about them on his website *[DeepCapture.com](https://deepcapture.com)*. See "[Evidence Grows: ’20 Election Was Rigged](https://www.deepcapture.com/2020/11/election-2020-was-rigged-the-evidence/)" (Nov 24, 2020) and his book "[The Deep Rig](https://www.amazon.com/Deep-Rig-Election-friends-integrity-ebook/dp/B08X1Z9FHP)".

[^17]: Suzi's story is explained in detail in this detailed investigative report: "[Georgia Conducting Secret 2020 Ballot Review -- Keeping Plaintiffs in the Dark](https://www.realclearinvestigations.com/articles/2021/06/22/georgia_conducting_secret_20_ballot_review_-_keeping_plaintiffs_in_the_dark_782204.html)", by RealClear Investigations, June 22, 2021.

    She also gave an interview on Real America's Voice: [GA veteran election worker speaks to John Frederick about fraud witnessed](https://rumble.com/vhmtfp-ga-veteran-election-worker-speaks-to-john-frederick-about-fraud-witnessed.html), May 26, 2021.

[^18]: From [Brad Raffensperger's tweet](https://twitter.com/GaSecofState/status/1404520023717257218?s=20), reported on in more detail in "[Georgia to review county’s ballot drop box forms from 2020](https://apnews.com/article/government-and-politics-donald-trump-joe-biden-georgia-3f5223b15bb5336fa552a24473fde313)" by Associated Press, June 16, 2021.

[^19]: Edward Solomon's 12min video "[Geometric Proof for Georgia](https://rumble.com/vdnfcf-edward-solomon-geometric-proof-for-georgia.html)" is available on Rumble. See also [his official report and affidavit](https://drive.google.com/file/d/1-j3msC3EQ0-dxpbN2lsKQm_SoaRrvXSo/view) or [screenshots of his spreadsheets](https://imgbb.com/m5YG58c). Statistician Dr Frank [also believes that Edward Solomon's numbers are correct](https://t.me/FollowTheData/552).

[^20]: VoterGA.org: "[New Evidence Reveals GA Audit Fraud and Massive Errors](https://voterga.org/wp-content/uploads/2021/07/Press-Release-New-Evidence-Reveals-Georgia-Audit-Fraud-and-Massive-Errors.pdf)", July 13, 2021. Also available via [press release on YouTube](https://www.youtube.com/watch?v=J6W0FfGZam8) while the censorship permits it.

    Also reported by The Spectator Australia: "[The Big Lie vs Stop the Steal](https://www.spectator.com.au/2021/07/the-big-lie-vs-stop-the-steal/)", July 24, 2021.

    Full copies of the ballot and tally images as inspected by VoterGA are available at [this Dropbox link](https://www.dropbox.com/sh/jbdazeu1pcxmqqr/AADZBElcG318ebBqfHDIbhqwa?dl=0).
    
[^21]: From a follow-up report from VoterGA.org: "[GA Audit Would Reveal Other Counties Much Like Fulton](https://voterga.org/wp-content/uploads/2021/07/Press-Release-GA-Audit-Would-Reveal-Other-Counties-Worse-than-Fulton.pdf)", July 19, 2021.

[^22]: Just the News: "[Georgia ballots rejected by machines were later altered by election workers to count](https://justthenews.com/politics-policy/elections/georgia-ballot-adjudication-spoiled)", Aug 8, 2021.

[^23]: Just the News: "[See disputed Georgia ballots where election workers decided a vote was for Biden, not Trump](https://justthenews.com/politics-policy/elections/see-georgia-ballot-images-where-election-workers-decided-vote-was-biden)", Aug 8, 2021.

[^24]: CDC Media: "[Evidence To Soon Be Presented To Citizens Grand Jury Of Interstate Conspiracy To Manufacture/Harvest Counterfeit Ballots For Use In 2020 Election](https://creativedestructionmedia.com/investigations/2021/07/21/breaking-evidence-to-soon-be-presented-to-citizens-grand-jury-of-interstate-conspiracy-to-manufacture-harvest-counterfeit-ballots-for-use-in-2020-election/)", July 21, 2021.

[^25]: The Daily Signal: "[4 Highlights From Georgia Senate’s Election Fraud Hearing](https://www.dailysignal.com/2020/12/03/4-highlights-from-georgia-senates-election-fraud-hearing/)", Dec 3, 2020.