---
title: Arizona
state: Arizona
abbr: az
heading: Election Fraud in Arizona
# subtitle: During US 2020 Election
meta_desc: An overview of key election integrity issues in Arizona during the 2020 US Presidential elections.
number_of_disputed_ballots: 300,000 votes (approx.)
biden_winning_margin: 10,457 votes (0.3%)
electoral_college_votes: 11 votes
forensic_audit: Partial investigations performed in 2 counties, although heavily obstructed
summary: |
    - Unobserved ballot counting
    - Many nonimmigrants and non-citizens voting without verification
    - Statistical impossibilities
    - 29,326 voter roll anomalies
    - Thousands of duplicated ballots lacking serial numbers
    - Use of unofficial ballot paper
    - Outstanding/unresolved integrity issues with Dominion counting machines, including the erasure of security log files
    - Maricopa County's obstruction of the forensic audit and refusal to comply with state Senators' requests
battleground_state: true
last_updated: 4 May 2022
collapse_headings: h2
collapse_headings_exclude: "#further-updates"
---

{% include state_stats.html %}

{% include state_summary_points %}

<!-- {% include toc %} -->


## Initial Allegations

#### Unobserved Ballot Counting

Vote counting rooms were announced as “closed down”, and yet counting continued, without independent observers present. This occurred in Pima County, Coconino County, and Maricopa County -- which combined, reach into hundreds of thousands of ballots. [^1]

#### Many Non-Citizens Voted In Election

Arizona's Governor Doug Ducey [^15] submitted an official vote count and statement that claims there were *zero* non-immigrant and non-citizen votes. However, based on a survey of voters by Trump legal team, a conservative estimate of 36,000 non-citizens voted in the election (or possibly as high as 237,000). [^1]

Arizona also reportedly had over 21,000 “federal only voters” who did not provide proof of citizenship when registering to vote. Senator Kelly Townsend alleges that "These voters cannot prove they are a citizen or that they even exist. They are not in the MVD system and all they need is a bank statement (easily doctored) to get a ballot." [^11] These alone are more than double Biden's winning margin.

#### Ballot Harvesting

Eye-witness testimony of highly-secretive and suspicious activity at Wigwam Resort, Litchfield, Arizona in Sept 2020, two months prior to the election, that appeared to be a significant election fraud and ballot harvesting operation, with two large ballrooms full of equipment, and transporting ballots between Arizona and Georgia. The witness was former Police Detective Gerald Buglione. [^14]


{% include 2000-mules-summary %}


## Statistical Anomalies

#### Vote Dump

A massive, abnormal "vote dump" with an unexpected swing to Biden: [^6] [^7]

  <img src="/images/spike-arizona.png" class="image-border" width="3301" height="2550" alt="Arizona Election Vote Spike Chart">

  {:.caption}
Source: DeepCapture.com [^7]

{% include seth_state_page_summary headingLevel=4 %}

#### Bobby Piton's Analysis

Mathematician Bobby Piton reviewed election data and stated during the state legislature hearing that he believes the numbers in Arizona are fraudulent, saying he would “stake his life” on it based on the mathematically problematic data. Piton estimated there are between 160,000 and 400,000 phantom voters. [^4] Piton’s Twitter account was suspended during his testimony. [^3]

#### Out-of-State Voters

Trump's Legal Team has alleged that the number of suspicious out-of-state voters were higher than Joe Biden's winning margin. [^1]

<!-- No longer true.
Some reports have noted that, contrary to other states, Arizona was *not* affected as much by mail-in ballot fraud, because of their stricter laws. The laws state that any absentee ballot without a valid application must be discarded. [^1]
-->


{% include election_integrity_scorecard %}


{% include zuckerberg_interference.html %}

{% include bloated_voter_rolls %}




## Court Cases

*Summary by The American Spectator, "[Election 2020: Can Classic Fraud Evidence Save The Day?](https://spectator.org/election-2020-can-classic-fraud-evidence-save-the-day/)":*

"In a lawsuit filed by Arizona Elector Kelli Ward on behalf of Team Trump, the Arizona Supreme Court [denied](https://www.azcourts.gov/Portals/0/OpinionFiles/Supreme/2020/2020_12_08_03939735-0-0000-AscDecisionOrder.PDF) her petition. The decision rested upon a trial court finding after a small sample of ballots were audited, with presentations from each side’s expert witness, that the election had seen far too little fraud to alter the outcome. Arizona’s election law requires that the margin be within 0.01 percent — in Arizona’s 2020 election, 3,339 votes — for an automatic recount. Further, unlike in Wisconsin, the Arizona Election Commission’s administrative guidelines, per legislative delegation, have the force of law. Given the extremely stringent threshold for contesting the election, the Court’s ruling appears reasonable." 

{% include court_case_link %}
   


## Maricopa County Election Audits

### 1. Maricopa County Internal Audit

On Jan 28, 2021, Maricopa County officials voted unanimously to approve their own "forensic audit" by hiring two supposedly-independent companies to conduct a review into whether voting machines counted votes correctly, and whether they were tampered with or hacked in any way. 

The Maricopa Board of Supervisors demanded that the auditor be an EAC-accredited [Voting System Test Laboratory (VSTL)](/faqs/voting-system-test-laboratories-vstl/), of which there were only two: Pro V&V and SLI Compliance. Although the Senate disagreed, the Maricopa BoS went ahead anyway and unilaterally contracted both firms to audit the Dominion voting systems and equipment (while ignoring other important aspects of the election). [^13]

The major concern with this was that Dominion had previously contracted these two companies to certify their machines, and as such they would be inherently biased against finding issues in the same machines they, themselves, were paid to certify. There were also reports that *neither* of the two firms above were actually certified at the time Maricopa County claimed they were. When this was reported by The Gateway Pundit, within four hours the <abbr title="U.S. Election Assistance Commission (eac.gov)">EAC</abbr> issued their certifications. [^13]

Arizona state Senator Warren Peterson also believed that it lacked the depth and breadth required, stating that "A county audit will not prevent the Senate from doing their own audit. *My concern with the county audit is that the scope of the audit is an inch deep. With the limited scope they have asked to be audited, they are guaranteed to find nothing.*” [^5]

### 2. Senate Forensic Audit of Maricopa County

*For a more detailed overview, see [Maricopa Forensic Audit](/in-detail/maricopa-arizona-forensic-audit-report/).*

The state Senate issued several subpoenas (legally binding requests) to the Maricopa Country election board and county recorder, to access its voting machines, copies of all ballots and much more so it can perform its own audit. The election board repeatedly resisted and refused a number of the requests, saying they were overly broad, and aimed to negotiate a settlement with the Senate. [^5] The Senate eventually received the ballots and *some* of the other requested materials, and began the audit.

Several preliminary issues were raised during the audit process:

* Ongoing refusal to comply with the Senate's subpoenas [^9]

* Chain of custody and ballot organization anomalies [^9]

* Deleted vote-counting databases [^9]

* 73,243 mail-in ballots have no record of being sent out [^8]  
(Maricopa County later [responded](https://recorder.maricopa.gov/justthefacts/#tabs=1), arguing that this number also includes early in-person votes which would not have been mailed) 

* 11,326 voters were not listed on November 3 voter roll, but *were* listed on December 4 voter roll and were listed as having voted in the election [^8]

* 18,000 who voted in November but were then removed from the rolls [^8] [^12]

* Thousands of duplicated ballots that lacked serial numbers. Senate President Karen Fann noted that without serial numbers, it would be impossible to know how many times a ballot was duplicated.  [^8]

* 168,000 ballots are printed on unofficial paper that the county claims they did not use. This could be a simple mistake, or an indication of fraudulent ballots. [^8]

* An unpublicized digital breach of the election system in November [^8] [^12]

* A one-off digital event erasing the security log for the period covering the November election. The auditors reported 37,646 entries into the digital security log for the election management system – which has only eight accounts – on one day, 11 March 2021. Data being kept on a first-in, first-out principle, the digital deluge shoved enough previous data out to hide election security log events before February 2021. Rep. Finchem claims this shows criminal intent. [^8] [^12]

The full Senate hearing from July 16 was [available on YouTube](https://youtu.be/7OZmNbBDQ6k?t=10897) <span class="pill">Censored</span>, or [on Rumble, here](https://rumble.com/vjvyzi-live-arizona-az-state-senate-hearing-on-the-2020-election-audit-in-maricopa.html).

Maricopa County officials have been highly oppositional to the audit at every stage, and deny each of the issues. They have responded to common questions and allegations [on their website](https://recorder.maricopa.gov/justthefacts/).

After the county continued to refuse the Senate's subpoenas, the matter has been referred to the State Attorney General for investigation.

#### Telling the Story

On Aug 12, at the [Cyber Symposium](/in-detail/cyber-symposium-mike-lindell/), Arizona State Representatives Mark Finchem, Sonny Borrelli, and Wendy Rogers told the story of the entire audit process, including responding to early election fraud reports and moving through with the forensic audit despite many obstructions from Maricopa County, the media and others. 

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vifrdz/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

In the video, they respond to the common attack against the audit: that "Cyber Ninjas are not an accredited audit company", reporting that there is no such thing as accredited election auditors in America, only accredited voting system test laboratories ([VSTL](/faqs/voting-system-test-laboratories-vstl/)). These labs are hired by voting machine companies like Dominion to grant them government <abbr title="U.S. Election Assistance Commission (eac.gov)">EAC</abbr> certification, and as such would be inherently biased against finding issues in machines that they themselves were paid to certify. When the Senators then approached big financial auditing companies, they all refused, saying it would put their government contracts at risk. The media has often ignored these crucial points.

See also: *[Common Criticisms of the Audit](/in-detail/maricopa-arizona-forensic-audit-report/#common-criticisms-of-the-audit)*

Senate President Karen Fann also tells the story from her perspective in a 3-part video series on Twitter. See [Part 1](https://twitter.com/AZGOP/status/1427736243127738371?s=20), [Part 2](https://twitter.com/AZGOP/status/1427766848699863040?s=20), [Part 3](https://twitter.com/AZGOP/status/1427782296032235521?s=20).


#### Forensic Audit Results

{% include maricopa_results %}


### Forensic Ballot Analysis by Erich Speckin

{% include speckin-report %}

[Read More About the Speckin Report](/in-detail/speckin-report/){:.button}


## Maricopa County Canvassing Results

{% include canvass/arizona %}

{% include canvass/link %}


## Pima County

On Dec 13, 2021, Pima County also came under the spotlight with a 7-hour election integrity hearing. The full video can be seen below:

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vo3yew/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Watch on Rumble](https://rumble.com/vqq4j2-pima-county-arizona-election-integrity-hearing-121321.html)

We hope to provide a summary of the findings in the coming days.



{% include canvass/get-involved %}

{% include news %}

{% include state_telegram_channels %}

{% include state_rep_support %}

{% include raw_data %}

{% include further_leads %}


### Footnotes & References

[^1]: <span class="pill">Censored</span> "The Current State Of Our Country", Rudy Giuliani, Trump Legal Team, <https://youtu.be/NAzCECx8XeE?t=990> (around 16m 30s mark). Unfortunately, this video has been censored by YouTube, and no longer appears to be accessible via this link. We will update the address once we have a new link.

[^3]: "[Arizona Election Fraud Investigation Continues Despite Governor’s Certification](https://uncoverdc.com/2020/12/02/arizona-election-fraud-investigation-continues-despite-governors-certification/)", Uncover DC, Dec 2, 2020.

[^2]: We're aiming to uncover further details and witness statements and post them here, in the near future.

[^4]: "[UPDATE: Arizona Citizens Investigation Discovers Thousands of Phantom Voters in State – Up to 30% of Addresses in Investigation Were Fraudulent](https://djhjmedia.com/steven/update-arizona-citizens-investigation-discovers-thousands-of-phantom-voters-in-state-up-to-30-of-addresses-in-investigation-were-fraudulent/)", DJHJ Media, Jan 1, 2021.

[^5]: "[Maricopa County votes to audit machines used in November election](https://www.fox10phoenix.com/news/maricopa-county-votes-to-audit-machines-used-in-november-election)", Fox 10 Phoenix, Jan 27, 2021.

[^6]: Report: "[2020 Presidential Election Startling Vote Spikes: Statistical Analysis of State Vote Dumps in the 2020 Presidential Election](https://www.scribd.com/document/496106864/Vote-Spikes-Report)" by Eric Quinnell (Engineer); Stan Young (Statistician); Tony Cox (Statistician); Tom Davis (IT Expert); Ray Blehar (Government Analyst, ret’d); John Droz (Physicist); and Anonymous Expert.

[^7]: Patrick Byrne, traditionally a Libertarian, developed a team of computer specialists to investigate election integrity issues including the notorious one-sided "spikes" found in several states and has written extensively about them on his website *[DeepCapture.com](https://deepcapture.com)*. See "[Evidence Grows: ’20 Election Was Rigged](https://www.deepcapture.com/2020/11/election-2020-was-rigged-the-evidence/)" (Nov 24, 2020) and his book "[The Deep Rig](https://www.amazon.com/Deep-Rig-Election-friends-integrity-ebook/dp/B08X1Z9FHP)".

[^8]: From Arizona Audit Senate Hearing, July 16, 2021. A short summary of the 2 hour hearing is available from OAN [via this link](https://rumble.com/vjxz8f-bombshells-dropped-at-ariz.-audit-hearing.html). The full hearing was [available on YouTube](https://youtu.be/7OZmNbBDQ6k?t=10897) <span class="pill">Censored</span> or [on Rumble, here](https://rumble.com/vjxjni-az-senate-hearing-on-the-election-audit-in-maricopa-county-7-16-21.html).

[^9]: From the full letter from Arizona Senate to Maricopa County Board of Supervisors, May 12, 2021. [View PDF](https://cdn.donaldjtrump.com/djtweb/general/5-12-21_Letter_to_Maricopa_County_Board.pdf).

[^10]: "[Arizona Senate Issues Fresh Subpoenas for 2020 Election Audit](https://www.theepochtimes.com/arizona-senate-issues-fresh-subpoena-for-2020-election-audit_3920229.html)", The Epoch Times, July 27, 2021.

[^11]: Arizona is one of just two states in the United States that requires proof of citizenship when registering to vote. The loophole, the Federal Only ballot ([see explanation](https://www.azpm.org/s/43413-federal-only-ballots-mean-some-arizonans-cant-vote-local/)), which voters may use instead, does not require this documentation. Instead, voters are limited to federal races.

    The exploitation of this loophole of "Federal Only" voters was flagged by Senator Kelly Townsend on Telegram ([view post](https://t.me/KellyTownsend/122)) and also in [an article by The Gateway Pundit](https://www.thegatewaypundit.com/2021/06/arizona-2x-undocumented-federal-voters-biden-won/), Jun 6, 2021.

    [Arizona's Secretary of State website](https://azsos.gov/elections/voting-election/proof-citizenship-requirements) outlines the official requirements for voter registration, proof of citizenship, and how "federal only" votes supposedly work.

[^12]: The Spectator Australia: "[The Big Lie vs Stop the Steal](https://www.spectator.com.au/2021/07/the-big-lie-vs-stop-the-steal/)", July 24, 2021.

[^13]: The Gateway Pundit: "[Both “Accredited” Voting System Test Laboratories Hired by Maricopa County Forgot To Audit Something In Their Audits](https://www.thegatewaypundit.com/2021/07/accredited-voting-system-test-laboratories-forgot-audit-something-az-audit/), July 4, 2021.

[^14]: CDC Media: "[Evidence To Soon Be Presented To Citizens Grand Jury Of Interstate Conspiracy To Manufacture/Harvest Counterfeit Ballots For Use In 2020 Election](https://creativedestructionmedia.com/investigations/2021/07/21/breaking-evidence-to-soon-be-presented-to-citizens-grand-jury-of-interstate-conspiracy-to-manufacture-harvest-counterfeit-ballots-for-use-in-2020-election/)", July 21, 2021.

[^15]: Governor Doug Ducey not only certified the tainted results (as a win for Joe Biden), but appears to have repeatedly opposed election integrity investigations and reforms. Donald Trump issued at least two [public](https://www.thegatewaypundit.com/wp-content/uploads/President-Trump-Stmt-6-24.jpg) [statements](https://www.thegatewaypundit.com/wp-content/uploads/Trump-message-4-26-Ducey.jpg), criticizing Ducey's unwillingness to fix election integrity. Ducey has also stated that it's legally impossible for Arizona to decertify the 2020 election, but this has been [disputed](https://wendyrogers.org/attorney-matt-deperno-responds-to-az-senate-lawyer-who-claimed-senate-could-not-decertify/) by others including attorney Matt DePerno.
