---
title: Pennsylvania
state: Pennsylvania
abbr: pa
heading: Election Fraud in Pennsylvania
# subtitle: Subtitle to go here
meta_desc: An overview of key election integrity issues in Pennsylvania during the 2020 US Presidential elections.
number_of_disputed_ballots: 500,000 votes (approx.)
biden_winning_margin: 81,660 votes (1.2%)
electoral_college_votes: 20 votes
forensic_audit: ❌ Audit process was initiated, but stonewalled by Senate President Corman
summary: |
    - Poor (and illegal) security controls for mail-in ballots
    - 170,830 more votes counted than individuals recorded as voting
    - Unobserved ballot counting
    - A large, questionable batch of 580,000 ballots that went 99.5% to Biden
    - No chain of custody documentation
    - A trailer containing 144,000--288,000 completed ballots disappeared, mysteriously
    - Lack of transparency
    - Conflicting records of absentee ballot numbers (including mysterious changes to the numbers, post-election)
    - Significant statistical anomalies
    - Courts blocking the presentation of evidence
battleground_state: true
last_updated: 4 May 2022
---

{% include state_stats.html %}

{% include state_summary_points %}

{% include toc %}


## Key Allegations

* The state supreme court permitted lax security controls for mail-in ballots, contravening the US constitution [^1] [^9]

* The state's DoS/SURE system records indicate that only 6,760,230 total voters actually voted, and yet 6,962,607 total ballots were cast (a discrepancy of 202,377 votes). 6,931,060 of these were eventually counted in the presidential race (a discrepancy of 170,830). [^11] [^13]

* 882,777 ballots were entered without any independent observation (uninspected), against the law of Pennsylvania - all done in the middle of the night, when Biden was behind by 600,000 votes. [^3]

* Significant statistical anomalies. [See below for examples](#statistical-anomolies).

* Ballot counts reached impossible numbers, and were later corrected without transparent explanation [^1]

* In the early morning hours after election night, the votes for Joe Biden increased by 578,014 while Trump's only increased 3,290 during that same period (a ratio of 99.5% to Biden, 0.5% to Trump). [^3] This has not been explained.

* A USPS-contracted trucker alleged in a sworn affidavit that he transported about 144,000 to 288,000 completed ballots from New York to Pennsylvania on Oct. 21 and the trailer disappeared. He wasn’t given any notice about his trailer. “The next day, it just got weirder. As I arrived at the Lancaster USPS with my tractor, I went to hook up to my trailer and the trailer was gone,” Jesse Morgan said in a media statement. [^7]

* The day prior to the election, ballot records indicated that 2.7 million ballots had been sent out. The day after the election, this number mysteriously increased by 400,000. This also has not been explained. [^1]

* So-called “naked ballots”---mail-in/absentee ballots arriving without being sealed in an envelope, as required by law---were a major problem. [^11]

* 75,505 ballots were added to the data 12 days after the election, with no explanation [^1]

* 154,585 ballots were either dated impossibly early, or mailed too late, and yet were still counted [^1]

* 1,573 ballots came from voters over 100 years old [^1]

* Attempts by state and federal members to obtain data have been obstructed [^1]

* Courts have refused to hear the evidence [^3]

* Philadelphia has had the nation's highest rates of fraud during past elections and deserves critical examination [^3]


## Senate Committee Report by Francis X. Ryan [^1]

On December 16, 2020, the US Senate Committee on Homeland Security and Governmental Affairs held a full committee hearing, examining irregularities in the 2020 Election. Representing Pennsylvania, Francis X. Ryan stated:

> The mail in-ballots system for the general election of 2020 in Pennsylvania was so fraught with inconsistencies and irregularities that the reliability of the mail-in votes in the Commonwealth of Pennsylvania is impossible to rely upon."
>
> <small>--- The Honorable Francis X. Ryan, State Representative, Commonwealth of Pennsylvania [^1]</small>

Francis X. Ryan also submitted the following specific concerns:


### 1. State Supreme Court undermining of authenticity controls

Pennsylvania supreme court undermined the controls inherent in [Act 77 of 2019](https://lwvccpa.org/details-of-the-new-pa-voting-law-act-77-of-2019/) which included:
	
- Extended mail-in ballot deadline to 3 days after election [^12]
- Mandated that ballots mailed without a postmark would be presumed to be received
- Allowed use of drop-boxes for collection of votes
- Skipped authentication of mail-in ballots, treating them dissimilarly and eliminating a critical safeguard against fraud

It's believed that relaxing these controls permitted the issues that follow.


### 2. Counting irregularities, and lack of oversight controls

“At the county level the pattern of inconsistencies is easily seen. For instance, Over-vote in Philadelphia County -- On November 4th at 11:30am, the Department of State (DOS) posted updated mail in vote counts for Philadelphia County. **The number of ballots reported to have been counted was an impossible 508,112 ballots despite the fact that only 432,873 ballots had been issued to voters in that county.** Later that day, the ballots counted number was reduced but this begs the question, who had the authority to add and subtract votes on the ballot counts reported to the Department of State? Even if this was simply a data entry error, the **lack of internal controls** over such reporting necessitates a review of the numbers, the process and system access.” [^1]
  
### 3. Unexplained changes in ballot records 

“Additionally, in a data file received on November 4, 2020, the Commonwealth’s PA Open Data sites reported over 3.1 million mail in ballots sent out. The CSV file from the state on November 4 depicts 3.1 million mail in ballots sent out but on November 2, the information was provided that only 2.7 million ballots had been sent out. This discrepancy of approximately 400,000 ballots from November 2 to November 4 has not been explained.

"Furthermore, a newly available voter dataset available on data.pa.gov which had been offline for weeks indicated that it was last updated on 11/16/2020. The download of 11/16 shows 75,505 more ballots returned on 11/16 than the download from 11/15. Therefore, from 11/15 to 11/16, **75,505 ballots were added to the dataset with no explanation**."

“Due to the magnitude of the potential discrepancies and closeness to the elections the results of the 2020 presidential election of pennsylvania would just be completely difficult if not impossible to determine with conclusiveness.”

### 4. Ballots with invalid dates

“Mail Date irregularities to include ballots mailed before the ballot was finalized, ballots mailed late and ballots mailed inconsistent with enacted legislation relative to mail in ballots: 154,584 ballots.”

### 5. Number of votes over 100 years old

“Voter Date of Birth irregularities: (voters over age 100): 1573 ballots”

### 6. Lack of transparency

“Before and after the election of November 3, 2020, the efforts by the State Government Committee and other members of the PA Legislature to obtain oversight information and relevant data to confirm or deny claims of improprieties were stymied. For instance, a hearing sought by Dominion Voting Systems with the State Government Committee of the PA House, which was scheduled for November 20, 2020, was cancelled by Dominion at the last-minute citing litigation concerns. Right to know requests were similarly not responded to except by delay from the Executive Branch until well into January 2021.”

“Without knowing the answer to these questions and due to the magnitude of the discrepancies and the closeness of the election, the results of the 2020 presidential election in Pennsylvania cannot be conclusively determined.”


{% include 2000-mules-summary %}


## Statistical Anomalies


{% capture description %}
For a detailed analysis of the statistics and anomalies uncovered within the 2020 election in Pennsylvania, we recommend reading this report, provided by [Election-Integrity.info](https://election-integrity.info){:target="_blank"}.
{% endcapture %}
{% include pdf_box.html 
  url="https://election-integrity.info/PA_2020_Voter_Analysis_Report.pdf"
  title="Pennsylvania 2020 Voter Analysis Report"
  description=description
%}


* John R. Lott, the Senior Advisor for Research and Statistics at the US Department of Justice, wrote a paper that outlines several major statistical issues, especially within Allegheny and Philadelphia Counties. [^8] As one example, he notes that Trump’s percentage of absentee votes was 3.4% lower in Allegheny County border precincts (where fraud is alleged to have occurred) compared with precincts just across the street in neighboring counties.

* Convoluted election data that appeared to show four instances where votes were *subtracted* from the totals, and another four batches (or "vote dumps") where the counts were abnormally weighted towards Biden [^14]

* After finding large statistical impossibilities in Georgia [^15], Mathematician Edward Solomon found that Pennsylvania also had similar indications of a fraudulent algorithm, although with a few twists.

  His [detailed report and affidavit](https://drive.google.com/file/d/1es0uSqp_AmuvicH-R1me-PIIRjKeSNJ2/view) alleges that hundreds of ballot batch tallies across multiple precincts had mathematically improbable results, which suggests there may have been a fraudulent computer algorithm switching votes to reach a specific target fraction. The algorithm appeared to be switched on for short periods, and then switched off and moved to another precinct to avoid detection. He predicts that 124,649 votes were stolen from President Trump using this method in these four counties alone: Allegheny, Chester, Montgomery and Philadelphia.

* A declaration and a photo suggest that a poll worker used an unsecured USB flash drive to dump an unusually large cache of votes onto vote tabulation machines.  The resultant tabulations did not correlate with the mail-in ballots scanned into the machines. [Unverified] [^10]

* Mathematician Bobby Piton, who testified at an election integrity hearing in Arizona (Nov 30, 2020) also later examined voting data from Pennsylvania. [^6] He reported that out of 9 million records:

   * There were 521,879 unique last names (an unusually high number)
   * Of those, 245,033 or just under 47 percent of the total “only belong to 1 and only 1 person!” In other words, these people have “no parents, siblings, aunts, uncles or cousins that share this particular last name,” Piton noted.

* ElectionFraud20.org has also reviewed the voting and registration trends from 2000 to 2020 and noted anomalies in Westmoreland County, Pennsylvania, among others. [Read our article](/trend-analysis/historical-county-analysis-registration-data/) that explains the method and the observations.


## Eyewitness Accounts

The following 20min video contains a number of eyewitness testimonies of fraud witnessed in Pennsylvania:

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vmvtg3/?pub=4" frameborder="0" allowfullscreen></iframe>

{:.caption}
Source: [Election Investigation Law](https://electioninvestigationlaw.org/)


{% include seth_state_page_summary %}


{% include election_integrity_scorecard %}


{% include dr-frank/state-summary
    showTitle=nil
    correlation=0.996
    extra1=nil
    registrationsOver100=false
    exampleCounty=nil
    exampleDate=nil
%}

## Audit The Vote PA's Anomalies By County

<div markdown=1 style="column-width: 11.7em" class="paragraph-margin-bottom">
  <a href="https://auditthevotepa.com/adams-county">Adams County</a>  
  <a href="https://auditthevotepa.com/allegheny-county">Allegheny County</a>  
  <a href="https://auditthevotepa.com/armstrong-county">Armstrong County</a>  
  <a href="https://auditthevotepa.com/beaver-county">Beaver County</a>  
  <a href="https://auditthevotepa.com/bedford-county">Bedford County</a>  
  <a href="https://auditthevotepa.com/berks-county">Berks County</a>  
  <a href="https://auditthevotepa.com/blair-county">Blair County</a>  
  <a href="https://auditthevotepa.com/bradford-county">Bradford County</a>  
  <a href="https://auditthevotepa.com/bucks-county">Bucks County</a>  
  <a href="https://auditthevotepa.com/butler-county">Butler County</a>  
  <a href="https://auditthevotepa.com/cambria-county">Cambria County</a>  
  <a href="https://auditthevotepa.com/cameron-county">Cameron County</a>  
  <a href="https://auditthevotepa.com/carbon-county">Carbon County</a>  
  <a href="https://auditthevotepa.com/centre-county">Centre County</a>  
  <a href="https://auditthevotepa.com/chester-county">Chester County</a>  
  <a href="https://auditthevotepa.com/clarion-county">Clarion County</a>  
  <a href="https://auditthevotepa.com/clearfield-county">Clearfield County</a>  
  <a href="https://auditthevotepa.com/clinton-county">Clinton County</a>  
  <a href="https://auditthevotepa.com/columbia-county">Columbia County</a>  
  <a href="https://auditthevotepa.com/crawford-county">Crawford County</a>  
  <a href="https://auditthevotepa.com/cumberland-county">Cumberland County</a>  
  <a href="https://auditthevotepa.com/dauphin-county">Dauphin County</a>  
  <a href="https://auditthevotepa.com/delaware-county">Delaware County</a>  
  <a href="https://auditthevotepa.com/elk-county">Elk County</a>  
  <a href="https://auditthevotepa.com/erie-county">Erie County</a>  
  <a href="https://auditthevotepa.com/fayette-county">Fayette County</a>  
  <a href="https://auditthevotepa.com/forest-county">Forest County</a>  
  <a href="https://auditthevotepa.com/franklin-county">Franklin County</a>  
  <a href="https://auditthevotepa.com/fulton-county">Fulton County</a>  
  <a href="https://auditthevotepa.com/greene-county">Greene County</a>  
  <a href="https://auditthevotepa.com/huntingdon-county">Huntingdon County</a>  
  <a href="https://auditthevotepa.com/indiana-county">Indiana County</a>  
  <a href="https://auditthevotepa.com/jefferson-county">Jefferson County</a>  
  <a href="https://auditthevotepa.com/juniata-county">Juniata County</a>  
  <a href="https://auditthevotepa.com/lackawanna-county">Lackawanna County</a>  
  <a href="https://auditthevotepa.com/lancaster-county">Lancaster County</a>  
  <a href="https://auditthevotepa.com/lawrence-county">Lawrence County</a>  
  <a href="https://auditthevotepa.com/lebanon-county">Lebanon County</a>  
  <a href="https://auditthevotepa.com/lehigh-county">Lehigh County</a>  
  <a href="https://auditthevotepa.com/luzerne-county">Luzerne County</a>  
  <a href="https://auditthevotepa.com/lycoming-county">Lycoming County</a>  
  <a href="https://auditthevotepa.com/mckean-county">McKean County</a>  
  <a href="https://auditthevotepa.com/mercer-county">Mercer County</a>  
  <a href="https://auditthevotepa.com/mifflin-county">Mifflin County</a>  
  <a href="https://auditthevotepa.com/monroe-county">Monroe County</a>  
  <a href="https://auditthevotepa.com/montgomery-county">Montgomery County</a>  
  <a href="https://auditthevotepa.com/montour-county">Montour County</a>  
  <a href="https://auditthevotepa.com/northampton-county">Northampton County</a>  
  <a href="https://auditthevotepa.com/northumberland-county">Northumberland County</a>  
  <a href="https://auditthevotepa.com/perry-county">Perry County</a>  
  <a href="https://auditthevotepa.com/philadelphia-county">Philadelphia County</a>  
  <a href="https://auditthevotepa.com/pike-county">Pike County</a>  
  <a href="https://auditthevotepa.com/potter-county">Potter County</a>  
  <a href="https://auditthevotepa.com/schuylkill-county">Schuylkill County</a>  
  <a href="https://auditthevotepa.com/snyder-county">Snyder County</a>  
  <a href="https://auditthevotepa.com/somerset-county">Somerset County</a>  
  <a href="https://auditthevotepa.com/sullivan-county">Sullivan County</a>  
  <a href="https://auditthevotepa.com/susquehanna-county">Susquehanna County</a>  
  <a href="https://auditthevotepa.com/tioga-county">Tioga County</a>  
  <a href="https://auditthevotepa.com/union-county">Union County</a>  
  <a href="https://auditthevotepa.com/venango-county">Venango County</a>  
  <a href="https://auditthevotepa.com/warren-county">Warren County</a>  
  <a href="https://auditthevotepa.com/washington-county">Washington County</a>  
  <a href="https://auditthevotepa.com/wayne-county">Wayne County</a>  
  <a href="https://auditthevotepa.com/westmoreland-county">Westmoreland County</a>  
  <a href="https://auditthevotepa.com/wyoming-county">Wyoming County</a>  
  <a href="https://auditthevotepa.com/york-county">York County</a>  
</div>

A helpful explanation on how to understand the county data above is provided in [this YouTube video](https://www.youtube.com/watch?v=D1h8YrBXj0o).


{% include electronic_voting_machines %}

[AuditTheVotePA.com](https://AuditTheVotePA.com) has also provided a separate list of all the voting machines used in the state, along with [an explanation video](https://rumble.com/vupg0q-machine-info-what-does-it-mean.html):

[View Voting Machine List](https://static1.squarespace.com/static/60a8b01a8bac0413e6ac618e/t/6203f66033fc5310654fc967/1644426848563/2020+PA+Voting+Equipment.xlsx+-+original+data.pdf){:.button}


{% include zuckerberg_interference.html %}

{% include trend-analysis-summary %}


## Court Hearings

It appears that evidence of fraud has not yet been heard by a court, despite two attempts:

1. The Pennsylvania Supreme Court denied a petition by state lawmakers, seeking review of the election result. The [Court’s order](https://www.pacourts.us/assets/opinions/Supreme/out/68%20MAP%202020%20Per%20Curiam%20Order.pdf?cb=2) invoked "[laches](https://byjus.com/free-ias-prep/doctrine-of-laches/)" — the legal doctrine that calls for dismissal of complaints filed too late.

2. Despite the above issues, the US Supreme Court [declined to hear](https://noqreport.com/2020/12/08/breaking-justice-alito-denies-pennsylvania-elector-injunction/) the case. [^11]

{% include court_case_link %}


## Election Audits

A small, but state-wide "risk-limiting" audit of 45,000 randomly selected ballots was performed in February 2021. [^19] The process was a smaller version of a "hand recount" which checks that the paper ballots match the official tally. It concluded that the counts were accurate. 

Note that a recount does *not* tend to identify chain of custody issues and the alleged injection or manipulation of fraudulent paper ballots, such as those noted in the above sections. It may simply recount the same fraudulent ballots.

Philadelphia County rejected attempts at further audits claiming that it would duplicate "extensive efforts already undertaken by the Philadelphia County Board of Elections, the Pennsylvania Department of State, state and federal courts, and ... the Pennsylvania State Senate". [^18] It's not clear, however, whether these previous investigations went any deeper than the risk-limiting audit.

*If you have any further information, let us know in the comments below.*

#### Forensic Audit

 Pennsylvania's Intergovernmental Operations Committee followed [Arizona]({% link _states/arizona.md %})'s lead and decided a deeper forensic audit was necessary and initiated this process on July 7, 2021. 

Senator Doug Mastriano (a Republican, and also the chairman of the Senate Intergovernmental Operations Committee) requested three counties hand over all of their election material in order for them to be audited. All three refused: [^16]

**County** | **Response**
======|=======
Philadelphia County | "We will not cooperate as this duplicates previous audits" [^22]
York County | "We will not send materials or provide access to voting machines" [^24]
Tioga County | "We will not provide access to machines unless the Senate pays for a new set of machines" [^23]

Governor Tom Wolf and Attorney General Josh Shapiro, both Democrats, instructed counties not to participate in the audits. The Department of State then gave a directive to all 67 counties that they must block unauthorized third party access to their voting machines, or otherwise face decertification. [^25] 

<div class="info" markdown="1">
This again highlights a crucial problem with electronic election machines:  
Nobody seems to be able to verify whether or not they have been tampered with. As such, state leaders threaten to decertify them out of mistrust of the auditor, and seemingly, to prevent any inspection at all. 

{:style="font-size: 110%"}
**If they cannot be validated as being safe and secure, they should not be used at all.**
</div>

The deadline passed, and none of the counties complied. The Senator sought official subpoenas (legally binding requests) to force the counties to hand over all ballots, records, documents, voting machines and tabulators in order to conduct a forensic audit, but this was blocked. The majority of the Senate Committee supported this move.

Republican Senate President Jake Corman has repeatedly blocked Senator Mastriano's forensic investigation by demanding that he *not* send requests to the counties, blocking subpoenas, blocking committee meetings, stripping Mastriano of his investigation and role on the committee, and even removing his entire staff. [^26] [^27]

> [This was] a move many of my fellow senators feel was an outrageous and unprecedented move unbecoming of a senate leader.
> 
> "I don’t know why Senator Corman obstructed my investigation for so long or why he has now hijacked it. What I do know is that if it was up to Senator Corman, there would be no investigation for him to steal. What I also know is that no matter who leads it, the investigation must go forward. I know that the people of Pennsylvania will never settle for a symbolic investigation. They deserve nothing less than a full forensic investigation that gets to the bottom of what happened in the 2020 election, and I’ll be a relentless voice to make sure that’s exactly what happens. I haven’t, and will not retreat.
> 
> "The question is why Jake Corman went from ten months of obstructing a forensic investigation to suddenly hijacking it and maligning the hard work we put into making this happen. The people need answers."
>
> <small>--- Senator Doug Mastriano, Aug 26, 2021 [^26]</small> 

<div class="info" markdown="1">
**[AuditTheVotePA.com](https://www.auditthevotepa.com/)** has collected over 100,000 signatures demanding an audit. Their website contains further information, data, and interviews. 

*"The people in Pennsylvania want to know what happened, who did it, and who's going to jail."*

They've expressed zero confidence in Corman's effort to lead the audits. [^28] 
</div>


## Voter Canvassing Results

{% include canvass/pennsylvania %}

{% include canvass/link %}


{% include canvass/get-involved %}

{% include news %}

{% include state_telegram_channels %}

{% include state_rep_support %}

{% include raw_data %}

{% include further_leads %}



### Footnotes & References

[^1]: The Honorable Francis X. Ryan, State Representative, Commonwealth of Pennsylvania. [Read his full statement](https://www.hsgac.senate.gov/imo/media/doc/Testimony-Ryan-2020-12-16.pdf) (PDF). This was during the US Senate Committee on Homeland Security and Governmental Affairs, Full Committee Hearing, see [^2].

[^2]: US Senate Committee on Homeland Security and Governmental Affairs, Full Committee Hearing, examining irregularities in the 2020 Election, December 16, 2020. Chaired by Senator Ron Johnson. Summary video available at [youtube.com/watch?v=fq8OPTIEf2U](https://www.youtube.com/watch?v=fq8OPTIEf2U), additional full testimonies available at [hsgac.senate.gov/examining-irregularities-in-the-2020-election](https://www.hsgac.senate.gov/examining-irregularities-in-the-2020-election). 

[^3]: <span class="pill">Censored</span> "The Current State Of Our Country", Rudy Giuliani, Trump Legal Team, <https://youtu.be/NAzCECx8XeE?t=328>. Unfortunately, this video has been censored by YouTube, and no longer appears to be accessible via this link. We will update the address once we have a new link.

[^6]: See a [Facebook post by Bobby Piton](https://www.facebook.com/BobbyPitonMakeUSGreat/posts/254162049550376?__tn__=-R). Also reported in [a World Tribune article](https://www.worldtribune.com/mathematician-1-million-people-in-pennsylvania-had-their-votes-stolen/).

[^7]: The Epoch Times: "[Ex-CIA Officer Alleges That Election Irregularities Could Be Part of a Big Scheme](https://www.theepochtimes.com/ex-cia-officer-alleges-that-election-irregularities-could-be-part-of-a-big-scheme_3603583.html)"

[^8]: John R. Lott, Senior Advisor for Research and Statistics at the US Department of Justice, "[A Simple Test for the Extent of Vote Fraud with Absentee Ballots in the 2020 Presidential Election: Georgia and Pennsylvania Data](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3756988&download=yes)". He provides analysis of the statistics which point to the likelihood of fraud.

[^9]: The president’s lawyers say that in Pittsburgh and Philadelphia, voters with invalid mail-in/absentee ballots received a notification and were allowed to correct that defect by using a provisional ballot on Election day, whereas election officials in Republican leaning counties followed election law more strictly and did not give similar notifications to voters with invalid mail-in/absentee ballots. -- Rudy Giuliani, “Trump Campaign News Conference on Legal Challenges,” C-SPAN, November 19, 2020 <https://www.c-span.org/video/?478246-1/trump-campaign-alleges-voter-fraud-states-plans-lawsuits>

[^10]: Declaration of John Doe, Delaware County Pennsylvania, November 9, 2020.

[^11]: The American Spectator: "[Election 2020: Can Classic Fraud Evidence Save The Day?](https://spectator.org/election-2020-can-classic-fraud-evidence-save-the-day/)", Jan 4, 2021.

[^12]: "Justice Samuel Alito’s Nov. 6 order to segregate ballots received after Nov. 3 was ignored, while the election officials assert that only 10,000 such ballots — a suspiciously low number — arrived. As they were commingled there is no way to verify this." [^11]

[^13]: <https://thejewishvoice.com/2020/12/pa-lawmakers-more-votes-counted-than-actual-ballots-cast-in-pennsylvania/>

[^14]: Report: "[2020 Presidential Election Startling Vote Spikes: Statistical Analysis of State Vote Dumps in the 2020 Presidential Election](https://www.scribd.com/document/496106864/Vote-Spikes-Report)" by Eric Quinnell (Engineer); Stan Young (Statistician); Tony Cox (Statistician); Tom Davis (IT Expert); Ray Blehar (Government Analyst, ret’d); John Droz (Physicist); and Anonymous Expert.

    In the report they note that Pennsylvania and Virginia both had incredibly convoluted election data that was difficult to examine. In Pennsylvania, there were four instances where votes were *subtracted* from the totals. In addition, there were four notable batches or periods where the counts were abnormally weighted towards Biden, when compared with the other batches.

[^15]: Edward Solomon's 12min video "[Geometric Proof for Georgia](https://rumble.com/vdnfcf-edward-solomon-geometric-proof-for-georgia.html)" is available on Rumble. See also [his official report and affidavit](https://drive.google.com/file/d/1-j3msC3EQ0-dxpbN2lsKQm_SoaRrvXSo/view) or [screenshots of his spreadsheets](https://imgbb.com/m5YG58c). Statistician Dr Frank [also believes that Edward Solomon's numbers are correct](https://t.me/FollowTheData/552).

[^16]: Facts Matter with Roman Balmakov: "[Lawmaker Issues Official Subpoenas for Ballots and Election Machines from 2 Counties](https://www.youtube.com/watch?v=X99DELK21zc)", Aug 14, 2021. [Alternate link](https://www.theepochtimes.com/facts-matter-aug-13-election-materials-demanded-from-2-counties-lawmaker-issues-official-subpoenas_3948300.html).

[^17]: NorthcentralPA.com: "['Unnecessary chaos': Tioga County commissioners respond to election audit inquiries lead by State Sen. Doug Mastriano](https://www.northcentralpa.com/news/unnecessary-chaos-tioga-county-commissioners-respond-to-election-audit-inquiries-lead-by-state-sen-doug/article_19e8e0bc-fb18-11eb-a251-bbaed093b7ec.html)", Aug 12, 2021.

[^18]: The Epoch Times: "[Pennsylvania Lawmaker Intends to Issue Subpoenas After 3 Counties Reject Audit Requests](https://www.theepochtimes.com/pennsylvania-senator-prepares-to-issue-subpoenas-after-three-counties-reject-audit-requests_3927471.html)", July 31, 2021, which quotes a July 30 letter from Commissioner Lisa Deeley to Mastriano.

[^19]: VotesPA: "[Post-Election Audits](https://www.votespa.com/About-Elections/Pages/Post-Election-Audits.aspx)"

[^20]: Pennsylvania State Media: "[Risk-Limiting Audit Pilot of November 2020 Presidential Election Finds Strong Evidence of Accurate Count](https://www.media.pa.gov/pages/State-details.aspx?newsid=453)", Feb 5, 2021.

[^21]: Tioga Free Press: "[STATEMENT FROM TIOGA COUNTY COMMISSIONERS](https://www.facebook.com/TiogaCountyFreedom/posts/3864986746940880)", August 10, 2021, in a Facebook post.

[^22]: Philadelphia County has rejected attempts at further audits claiming that it would duplicate "extensive efforts already undertaken by the Philadelphia County Board of Elections, the Pennsylvania Department of State, state and federal courts, and ... the Pennsylvania State Senate". [^18] It's not clear, however, whether these previous investigations went any deeper than the risk-limiting audit.

[^23]: Tioga County issued a rebuke of the entire process: "We wanted to take this opportunity to address the unnecessary chaos created by Senator Mastriano. The Senator began his one-man quest with a false accusation, saying that if Tioga County did not give him what he wanted, it was because we 'had something to hide'. The people of this county have nothing to hide, and Mr. Mastriano knows it." [^21]

    They said they weren't legally able to hand over materials without subpoenas, and that they were not able to hand over their voting machines because the Secretary of State of Pennsylvania would decertify them if they did, which would present them with a problem ahead of the next election. 

[^24]: The Epoch Times: "[Second Pennsylvania County Raises Objections to Senator’s Election Investigation](https://www.theepochtimes.com/second-pennsylvania-county-raises-objections-to-senators-election-investigation_3908218.html)", July 19, 2021.

[^25]: Just the News: "[Pennsylvania GOP senators' internal battle over imperiled forensic election audit goes public](https://justthenews.com/nation/states/pennsylvania-gop-senators-internal-battle-over-imperiled-forensic-election-audit-goes)", Aug 21, 2021

[^26]: [Senator Doug Mastriano's Press Release](https://twitter.com/SenMastriano/status/1430570528008462339/photo/1), via Twitter, Aug 26, 2021. Mastriano also provided an update on [OAN News](https://rumble.com/vlhue2-pa.-state-senate-president-jake-corman-fires-sen.-mastrianos-entire-staff.html).

[^27]: Bannon's War Room: "[Senator Mastriano Gives Stunning Update On PA Subpoenas](https://rumble.com/vlg9xj-senator-mastriano-gives-stunning-update-on-pa-subpoenas.html)", Aug 20, 2021.

[^28]: Bannon's War Room: "[PA Senator Corman Is Lying](https://rumble.com/vm250a-pa-senator-corman-is-lying.html)", Sept 3, 2021.