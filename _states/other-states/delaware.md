---
title: Delaware
heading: Election Fraud in Delaware
state: Delaware
abbr: de
last_updated: 4 May 2022
---

{% include state_stub_1 %}

{% include election_integrity_scorecard %}

{% include seth_state_page_summary %}

{% include electronic_voting_machines %}

## Preliminary Audits

{:.small}
> Preliminary Audits of the Delaware 2020 elections are damning. Only 10% of the votes have been audited and they’ve already found over 20k fraudulent ballots. Internal polling before the elections were *EXTREMELY* close. They couldn’t let Joe Biden’s home state seem even remotely competitive while they were busy stealing the swing states --- [it] would have raised a few eyebrows. Ladies and gentlemen, it’s looking like I pulled in at least 47% of the vote --- I maybe even won. Thank you to Seth Keshel’s Team for all of their hard work. More on this to come. Stay tuned!”
> 
> <small>--- Delaware U.S. Senate candidate Lauren Witzke [^delaware1]</small>

{% include canvass/delaware %}

* Dead people voting

* Almost 30 thousand voters were added to the voter rolls in the months leading up to the Nov 3 election. Oddly enough, over 11 thousand of those voters were removed from the rolls in August 2021 alone. Why would 11 thousand people unregister to vote in one month? Were the elections offices “cleaning up” their voter rolls? If so, why wait until after a historic election to remove voters?

* The astronomically high 47,205 ballots sent to adjudication --- equal to 25% of all mail-in/absentee ballots cast in the state. The FEC allows 0.0008% of ballots to be sent to adjudication. Also, how did that many ballots get manually reviewed by the required three reviewers in such a short time?

{% include canvass/link %}


## June 2021 Presentation

Patriots for Delaware delivered a presentation in June 2021 that included a summary of potential election fraud discovered in the state so far:

<iframe class="scribd_iframe_embed" title="Patriots for Delaware: Election Integrity Meeting Presentation, Jun 28, 2021 " src="https://www.scribd.com/embeds/532065179/content?start_page=1&view_mode=scroll&access_key=key-O5plFFMh5kNpPQk4qRrD" tabindex="0" data-auto-height="true" data-aspect-ratio="1.7790927021696252" scrolling="no" width="100%" height="600" frameborder="0"></iframe>


## Further Election Reports

[Patriots for Delaware](https://www.patriotsfordelaware.com/MEDIA-AND-DOCS.html) has further election integrity updates and press releases on their website.



{% include canvass/get-involved %}

{% include news %}

{% include state_rep_support %}

{% include state_telegram_channels %}

{% include raw_data %}

{% include further_leads %}


### Footnotes & References

