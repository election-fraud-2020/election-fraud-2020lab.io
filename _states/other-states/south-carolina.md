---
title: South Carolina
heading: Election Fraud in South Carolina
state: South Carolina
abbr: sc
last_updated: 4 May 2022
---

{% include state_stub_1 %}

{% include election_integrity_scorecard %}

{% include seth_state_page_summary %}

{% include dr-frank/state-summary
    correlation=nil
    registrationsOver100=false
    extra1=nil
    exampleCounty=nil
    exampleDate=nil
%}

{% include bloated_voter_rolls %}

{% include electronic_voting_machines %}


## Calls for Forensic Audit

At the September 13th 2021 Lexington County Republican Executive Committee meeting the committeemen voted and called for a full forensic audit.

Laura Scharr of the Election Fraud and Audit Committee (now the SC Safe Elections Group) gave a 30 minute presentation of how election fraud had taken place in SC elections and where that most likely happened.

[Watch the 90min video on YouTube](https://youtu.be/vo-_xfuvk4o) <span class="pill">Censored</span>


## South Carolina Safe Elections Group

The SC Safe Elections Group (formerly The Election Fraud and Audit Committee, or EFAC) is a grassroots non-partisan movement dedicated to fighting election fraud in South Carolina, working together since November 2020. Their concerned volunteer citizens are fighting for truth, justice, and a free and fair voting process. They began by investigating issues with the ES&S machines and documenting and exploring the election process via FOIAs.

The following presentation was given by EFAC regarding York County, SC (1hr 40min):

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/viuyzw/?pub=4" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Watch on Rumble](https://rumble.com/vlh556-york-county-sc-efac-presentation.html)

Learn more and get local updates via [their website](https://www.auditsouthcarolina.org/).

[Visit AuditSouthCarolina.org](https://www.auditsouthcarolina.org/){:.button} [Join Telegram Group](https://t.me/joinchat/sRfrsM-iuvoxODAx){:.button}


## Grassroots Canvassing Results

{% include canvass/south-carolina %}

{% include canvass/link %}


{% include canvass/get-involved %}

{% include news %}

{% include state_rep_support %}

{% include state_telegram_channels %}

{% include raw_data %}

{% include further_leads %}

### Footnotes & References
