---
title: Nebraska
heading: Election Fraud in Nebraska
state: Nebraska
abbr: ne
last_updated: 4 May 2022
---

{% include state_stub_1 %}

{% include election_integrity_scorecard %}

{% include seth_state_page_summary %}


## The Nebraska Voter Accuracy Project

After the 2020 election, over 2,200 citizens connected via [Telegram](https://t.me/NebraskaVAP) to share data and discuss reports from the election, forming the [Nebraska Voter Accuracy Project](https://www.nevoterap.com/).

The following 60min video presentation explains many of the findings to date, given by Larry Ortega, Dec 17, 2021:

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vqixzw/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

The full 90min presentation including introductions is [available here](https://rumble.com/vsjrua-nebraska-voter-accuracy-project-presentation-to-omaha-liberty-ladies.html). Slides from the presentation can be [downloaded here](https://dl.dropboxusercontent.com/s/8ggyvcetl4c1jaj/NebVAPPresentation17Dec21v15.pdf).


#### Highlights from the Presentation

Larry Ortega explains that there are numerous clear indications of fraud from the 2020 election --- some are mathematical, but others are clear enough to see without a mathematical background. This appears to be an issue much wider than just Nebraska, however it likely included the cooperation of at least some people on the ground within the state.

The findings thus far include:

* Four times during the vote count in Nebraska, votes for Trump went backwards. No explanation for this has yet been provided by the Secretary of State, the primary government official responsible for certifying elections.

* In Nebraska, ES&S voting machines are not checked for the possible presence of modems or wireless connectivity.

* Government contracts with ES&S, the voting machine manufacturer, prevent anyone from performing a thorough hardware or software audit of the machines. This contravenes the Nebraska Constitution which states that elections must be free and open for citizens to view and understand.

* Vote counts are reported in near-real-time via the use of memory sticks to copy results from one machine to another, although these are known to be a potential avenue for fraud and manipulation and have not been appropriately audited.

* In Nebraska, 58,000+ voters were added to the voter registration rolls in 2020 before the election, and 26,000+ were then removed in Feb and March 2021 after the election. Who were these people?

* There was an increase of 55,629 of people who voted in 2020, when the increase to the eligible voter population was only 41,850.

* County Election Officials have stated that they don't have or keep election ballot images from 2020, contravening Nebraska Election Law. Perhaps ES&S has the records?

* In Lancaster County, NE, votes are stored by ES&S using software from a
company called *Crowdstrike*, owned and operated by a Russian national, the same company hired by Hillary Clinton and the DNC that "investigated" and alleged Russian hacking of DNC servers as part of the Trump/Russia collusion that was later proved false by the FBI.

* Lancaster County accepted $400,000 in donations from Mark Zuckerberg.
  - $13,000+ was spent on voter Education
  - $266,000+ on mail-in voter supplies 
  - $10,000+ for drop boxes

  What was the voter education like? Who got the mail-in voter supplies? Where did they place the drop-boxes? It appears as though they made it easier for Democrats to get to a drop-box than for a Republican.

* Ten counties had more registered voters than citizens eligible to vote.

* Highly-unusual increases in the voter registration and turnout rates, as also noted by Dr Doug Frank, see below.

* More votes than voters --- 4,001 more votes counted in the state than voters who were recorded as having voted. Over 800 of these were detected in Cass County, and close to 700 in Douglas County.

* The voting trends in certain suburban precincts, especially in Sarpy County are highly questionable.

* In Sarpy County, Andrew LaGrone (conservative) lost his seat by only 266 votes out of 25,000 (1%), and Rich Holdcroft (conservative) lost his race by only 160 votes out of 18,000 (0.9%). Yet Sarpy County appears to have over 8,000 "excess" votes.

* The Nebraska Voter Accuracy Project has collected dozens of affidavits from residents that declare that they received unsolicited mailed ballots with wrong addresses, wrong names, or addressed to people who did not request them.

* All 93 County Clerks could not printout fundamental reports from their tabulating machines as described in their operating manuals. They responded with not knowing, often referring us to ES&S, showing their incompetence.

Further updates from The Nebraska Voter Accuracy Project can be found on their [website](https://www.nevoterap.com/), [Telegram channel](https://t.me/NebraskaVAP), [Rumble video channel](https://rumble.com/c/c-1270342), or [other social channels](https://lnk.bio/NEVoterAP).


## Sworn Affidavits from Residents

Here's a small sample of residents that reported irregularities with mail-in ballots:

{:.hint}
Prior to the 2020 election we received 3 ballots for Lindsey F. at our address. Lindsay is a daughter but has not
lived at this address for more than 15 years. Additionally, Lindsay’s name changed to Lindsay S. Lindsay was 16
years of age when she last resided at our address and was NEVER A REGISTERED VOTER WHILE LIVING AT THIS
ADDRESS. When we contacted Lindsay she said at no time did she request a mail in ballot. She further stated
that she is a registered voter (Democrat) but has never been registered at our address, she also stated that she
did not vote in the 2020 election.  
--- *Cheree F.*

{:.hint}
Kristine S. does not live here and has not for 20 years.  
--- *Charleen S.*

{:.hint}
My name is Justin T. I reside at [address redacted] with Ashley T. We each received 2 ballots with our names on them for each of us, then a week to a week and a half later we each received 2 more and then I think we received 1 more. We are both registered Republicans and have never requested any ballots.  
--- *Justin T.*

{:.hint}
I spoke to Dennis G he said his father Harley G died 11 years before the election, his mother died 1 year 6 months before the election and they both received ballots in the Nov. 3, 2020 election. Their address is [redacted] then they were in assisted living!  
--- *David D.*

{:.hint}
I did receive one ballot I requested from Fred Mytty’s office in Dodge County NE, for the Nov. 3rd 2020 Election, but I also received 3 other ballots from whoever, one of them came months after the 2020 Presidential Election. The additional 3 ballots that came were not on as heavy of paper.  
--- *Debra*

{:.hint}
I spoke to Myron he said he did not receive a mailed ballot for the Nov. 3rd, 2020 Election. In Thayer
County they mailed out ballots. He went to the courthouse to see why he did not receive a ballot and the election staff told him he had already voted, he had NOT VOTED. He turned around and left the
courthouse! HE DID NOT VOTE.  
--- *David D.*

{:.hint}
I received 4 ballots for the Nov. 3rd 2020 election. I do not know the person that’s name was listed on all 4 ballots. We are both registered Republicans and have not requested any ballots. We opened the ballots we received and then threw them away.  
--- *Anne*




{% include dr-frank/state-summary
    correlation=nil
    registrationsOver100=true
    extra1=nil
    exampleCounty=nil
    exampleDate=nil
%}

Dr Frank also found hundreds of errors in the voting records. {% include dr-frank/nebraska-variations %}


{% include bloated_voter_rolls %}

{% include electronic_voting_machines %}

{% include canvass/get-involved %}

{% include news %}

{% include state_rep_support %}

{% include state_telegram_channels %}

{% include raw_data %}

{% include further_leads %}

### Footnotes & References
