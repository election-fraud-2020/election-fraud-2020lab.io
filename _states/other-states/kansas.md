---
title: Kansas
heading: Election Fraud in Kansas
state: Kansas
abbr: ks
last_updated: 4 May 2022
---

{% include state_stub_1 %}


## Senate Hearing on Election Issues

On Feb 1, 2022 the Kansas Legislature heard presentations about election integrity issues in the state, as shown in the following 90min video:

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/5oIRTmKhQt0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

A follow-up interview with Thad Snider is [available on FrankSpeech.com](https://frankspeech.com/video/thad-snider-how-democrats-stole-johnson-county-kansas-first-time-1916).


{% include election_integrity_scorecard %}

{% include seth_state_page_summary %}

{% include dr-frank/state-summary
    correlation=0.97
    extra1=nil
    registrationsOver100=true
    exampleCounty=nil
    exampleDate=nil
%}

{% include bloated_voter_rolls %}

{% include electronic_voting_machines %}

{% include trend-analysis-summary %}

{% include canvass/get-involved %}

{% include news %}

Further coverage is also available via [FrankSpeech: Cause of America: Kansas](https://frankspeech.com/cause-america-kansas).

{% include state_rep_support %}

{% include state_telegram_channels %}

{% include raw_data %}

{% include further_leads %}

### Footnotes & References
