---
title: New York
heading: Election Fraud in New York
state: New York
abbr: ny
last_updated: 4 May 2022
---

{% include state_stub_1 %}

{% include election_integrity_scorecard %}

{% include seth_state_page_summary %}

{% include bloated_voter_rolls %}

{% include electronic_voting_machines %}

{% include trend-analysis-summary %}

## Other Reports

* A team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the following reports:

  * [2020 Presidential Election Contrast Analysis](https://election-integrity.info/Contrast_Report.pdf) which lists New York as the state with the 4th highest number of unexpected Biden votes, despite a decrease in population

  * [Statistical Voting Analysis of the 2020 NY-22 Congressional Contest](https://election-integrity.info/NY-22nd-2020-Report.pdf)

* [TMS Solutions Election Research: Monroe County NY Blank Ballot Analysis](https://www.election-research.com/Monroe-County/index.html#issue/1)

* [TMS Solutions Election Research: The 22nd Congressional District Research](https://www.election-research.com/Congressional-22nd/Menu-22nd/index.html#issue/1)


{% include canvass/get-involved %}

{% include news %}

{% include state_rep_support %}

{% include state_telegram_channels %}

{% include raw_data %}

{% include further_leads %}


### Footnotes & References
