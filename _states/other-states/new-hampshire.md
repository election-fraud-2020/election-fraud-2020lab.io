---
title: New Hampshire
heading: Election Fraud in New Hampshire
state: New Hampshire
abbr: nh
last_updated: 4 May 2022
---

{% include state_stub_1 %}

{% include election_integrity_scorecard %}

{% include seth_state_page_summary %}


## Windham Township, Rockingham County District 7

#### Hand Recount

Following the Nov 3 election results, a hand recount was performed in Windham on Nov 12 to validate the results. They found a discrepancy of 399 votes which was the largest numerical discrepancy in New Hampshire's senate race history. [^5]

#### Detailed Audit

Concerned citizens including Ken Eyring and Tom Murray [^5] gathered grassroots support for a full audit of Windham, and Bill 43 was presented to the state legislature. 

An original version of the bill designated three groups of auditors: [^6] [^5]

1. Either Harri Hursti, Ron Rivest (MIT), or Andrew Appel (picked by Secretary of State)
2. Either Retired Colonel Phil Waldron, or Dr. Shiva Ayyadurai
3. Jovan Hutton Pulitzer

The bill was later amended (or some would say "corrupted") to have a different three auditors: one chosen by the Board of Selectmen in Windham, another chosen collaboratively by the Secretary of State and Attorney General, and a third auditor to be selected by the first two auditors. [^5] The auditors requested by the citizens were sidelined.

The Board of Selectmen selected Mark Lindeman, Co-Director of Verified Voting [^vv1] --- a choice that was hotly protested by citizens at town meetings. The Secretary of State and Attorney General selected Harri Hursti, Cybersecurity Consultant and Co-Founder of Nordic Innovation Labs. Those two then co-selected Philip B. Stark --- a member of the Electoral Assistance Commission's Board of Advisors and close associate of Hursti's --- as the third audit lead. All three had associations with Verified Voting [^vv1], top-ranking Democrat officials, and George Soros. [^5] [^vv2] 

Hursti famously demonstrated a memory card hack on the Diebold AccuVote OS machines back in 2006. The machines have reportedly received software updates and fixes, but the same hardware is still in use in New Hampshire, and would be a major target of the audit.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/t75xvZ3osFg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

The intellectual property for these Diebold machines is now owned by *Dominion Voting Systems*, of which there has been [significant, alarming concerns](/in-detail/dominion-voting-machines/).

In April 2021, the New Hampshire Senate voted unanimously to approve Bill 43 to perform a forensic audit of Windham Township. This law authorized the audit of:

- The results of the House of Representatives, Rockingham District 7 race, from the November 3, 2020 General Election
- The ballot counting machines and their memory cards from Windham
- Hand tabulations of the ballots from the above-described race
- The recount done by the Secretary of State
- All ballots cast in the races for governor and United States senate, via hand-tallying

Strangely, Stark's profile was removed from the EAC website sometime in December--April [^10], while Hursti's was removed from Verified Voting immediately after being selected for the audit. [^9] It's not clear if this was an attempt at hiding associations, or some benign update.

The audit found 400 ballots that were incorrectly counted, but the final report downplayed the issue as being local and unlikely to affect the rest of New Hampshire, and thus the election outcome. Since the same machines are used in 137 towns across the state, this claim has been debated.

Here is one set of results comparing the counts on Nov 3, 2020, a recount on Nov 12, 2020, and a set of four machines during the audit. Notice how the machines rarely agree on the ballot counts, with consistent errors:

<!--
![New Hampshire Ballot Count Anomaly](https://i2.wp.com/uncoverdc.com/wp-content/uploads/2021/06/Windham-and-Beyond.jpg?fit=1204%2C1280&ssl=1)

{:.caption}
Source: [UncoverDC: Windham, NH Update: Voters Rally For Statewide Audit](https://uncoverdc.com/2021/06/02/windham-nh-update-voters-rally-for-statewide-audit/)
-->

<div class="force-wide" markdown=1>

![](https://i1.wp.com/uncoverdc.com/wp-content/uploads/2021/05/TableForGraph-1-2048x1245-1.jpg?resize=1024%2C623&ssl=1)

{:.caption}
Source: [Uncover DC](https://uncoverdc.com/2021/05/17/windham-nh-audit-update-early-results-are-troubling-as-dominion-reps-presence-continues/). Note that this chart does not include the count of blanks, as shown [here](https://uncoverdc.com/2021/06/02/windham-nh-update-voters-rally-for-statewide-audit/), which numbered over 5,627, more than any single candidate.

</div>

The final audit report is available below, with further audit documents available from [The New Hampshire Department of Justice](https://www.doj.nh.gov/sb43/index.htm).

<iframe class="scribd_iframe_embed" title="SB 43 Forensic Audit Report: Rockingham County District 7 (Town of Windham)" src="https://www.scribd.com/embeds/549512520/content?start_page=1&view_mode=scroll&access_key=key-eS43aEDTuPT5ACKyOQLp" tabindex="0" data-auto-height="true" data-aspect-ratio="0.7729220222793488" scrolling="no" width="100%" height="600" frameborder="0"></iframe>

Secretary of State William M. Gardner and Attorney General John M. Formella followed up with their joint report, [available here](https://www.doj.nh.gov/sb43/documents/20210826-joint-report-sb43-sos-ag.pdf).


#### Criticisms of the Audit

Several criticisms of the audit have been raised:

* The close ties of the auditors with top-ranking Democrats Nancy Pelosi and Chuck Schumer who appointed both Stark, and Lindeman's colleague Barbara Simons to the powerful Electoral Assistance Commission [^vv2]

* The close ties of Verified Voting with George Soros' Democracy Alliance Board [^vv2]

* Harri Hursti previously attempted to warn the world about the dangers of vulnerable voting machines in the documentaries *Hacking Democracy* and *[Kill Chain: The Cyber War on America's Elections](/in-detail/2020-election-fraud-documentaries/#kill-chain-the-cyber-war-on-americas-elections)* (and here's [an insightful 20min compilation](https://t.me/KanekoaTheGreat/221) of Hursti and Stark's machine vulnerability claims) but upon their involvement in the Windham NH audit, proceeded to claim that the election appeared relatively secure and was unaffected by fraud

* Memory cards used in the Nov 2020 election were “reset” (wiped) during the audit, losing valuable forensic data. Hursti and other auditors prevented concerned citizens from forensically imaging (securely copying) the memory cards prior to them being reset. The auditors further claimed that they could not themselves copy the Nov 2020 data from the cards in a secure-enough way, despite them having write-protect switches which would prevent alteration. [^1]

* Harri Hursti invited LHS Associates President Jeff Silvestro --- the company that sold the voting machines to Windham --- directly onto the floor of the forensic audit, in close proximity to the Diebold AccuVote Optical Scan (AV-OS) tabulators and the removable memory cards which were used in the 2020 election, outraging Windham residents who believed he should not be involved in the audit [^7]

* Philip B. Stark left the audit part-way through the process, leaving only two auditors, which appears to violate the SB 43 law requiring three auditors [^2]

* Of the three auditors, only Hursti was present at the audit when the memory cards were being inspected, another potential violation of SB 43 [^3] [^3b]

* The audit report focused on the "ballot folding" issue --- whereby a fold in the paper through a candidate's name could sometimes be recorded as a vote --- but deflected away from the other vulnerabilities in the machines, exploitable via the memory cards 

* Issues with machine #2 flipping votes from one party to another (across all races, except the governor race, strangely) have not been adequately explained. [^11] The auditors claimed that this was likely to be because machine #2 processed mostly absentee ballots which could likely be more Democrat-leaning, although this explanation does not explain why the governor's race was skewed differently, nor why the non-absentee ballots failed to have a corresponding Republican-skew. [^5] 

* The auditors claimed the machines had zero possibility of *any* internet or phone-line access whatsoever, nor even a TCP/IP stack within the operating system, yet this appears to contradict the equipment manual which includes extensive coverage of [dial-up modems](https://en.wikipedia.org/wiki/Modem#Dial-up) designed for connecting over telephone networks. [^1a] [^1] Video footage of the machine's internals appear to show severed wires, which may be how the networking ports were disabled at some point in time [^8], although this has not been clearly explained.


## Merrimack, Hillsborough County

As NH citizens made FOIA requests for the machine tapes across the state, more anomalies were discovered. Here is one from the town of Merrimack:

![Merrimack Voting Machine Tape](/images/new-hampshire/merrimack.jpg)

{:.caption}
Source: [Nick Moseder: BREAKING: THOUSANDS Of Votes Lost In New Hampshire](https://rumble.com/vmmkah-breaking-thousands-of-votes-lost-in-new-hampshire.html)

It shows that out of 1,738 total votes, a large percentage were irregular. 396 (22.8%) were overvotes, where the machine rejected the ballot as voting for too many candidates, and 322 (18.5%) were undervotes. Overvotes and undervotes are often excluded from the counts unless they pass a separate adjudication process (a process which can be easily manipulated).

Deeper analysis of the tapes also revealed that the machines that generated the most overvotes also demonstrated a strange flipping of Democrat-to-Republican percentages compared with other machines in the town. [^11]

We have yet to receive confirmation on what exactly occurred in Merrimack, but it deserves further investigation. Similar issues likely occurred across the state.


## Claremont

Similar to Merrimack, above, 727 (12.5%) of the votes in Claremont were flagged as overvotes and not counted. [^11]


{% include electronic_voting_machines %}


## Voter Roll Verification Tool

In many states, anomalies are being uncovered in the voter rolls: voters not being removed when moving interstate or passing away, and fake or "phantom" voters being added to the rolls. Citizens of New Hampshire can verify their voter registration record, and the records of known addresses via the link below:

[NH Voter Integrity: Voter Search](https://nhvoterintegrity.org/voter_search.html){:.button}


## Other Reports

* [TMS Solutions Election Research: The State of New Hampshire Voting Analysis](https://www.election-research.com/New-Hampshire-Review/index.html#issue/1)

* [TMS Solutions Election Research: New Hampshire Ballot Analysis Research](https://www.election-research.com/New-Hampshire-Summary/index.html#issue/1)


{% include canvass/get-involved %}

{% include news %}

{% include state_rep_support %}

{% include state_telegram_channels %}

Credit is also due to State Senator Bob Giuda for supporting the initial request of a forensic audit in Windham, despite the disappointing process. [^6]

{% include raw_data %}

{% include further_leads %}


### Footnotes & References

[^1a]: CannCon: "[Does an expert's affidavit contradict Harri Hursti and Philip Stark?](https://rumble.com/vhxu8h-does-an-experts-affidavit-contradict-harri-hursti-and-philip-stark.html)", Jun 2, 2021. He shows video of Philip Stark and Harri Hursti clearly stating that the machines have zero connectivity, and yet the manual has several sections on setting up modems.

[^1]: Nick Moseder: "[BREAKING - WE CAUGHT THEM! CITIZENS AUDIT! w_ Marylyn Todd & Cann Con](https://rumble.com/vhvj5n-breaking-we-caught-them-citizens-audit-w-marylyn-todd-and-cann-con.html)", May 31, 2021.

[^2]: The Gateway Pundit: "[BREAKING: One of Three Auditors Exits Windham, NH Election Audit – Law Requires a Team of Three Auditors](https://www.thegatewaypundit.com/2021/05/breaking-one-three-auditors-exits-windham-nh-election-audit-law-requires-team-three-auditors/)", May 27, 2021.

[^3]: Uncover DC: "[Windham Update: Hursti Alone Conducts Forensic Audit of Machines](https://uncoverdc.com/2021/05/27/windham-update-hursti-alone-conducts-forensic-audit-of-machines/)", May 27, 2021.

[^3b]: The Gateway Pundit: "[EXCLUSIVE: Why is Auditor Harry Hursti, Who Uncovered the Fatal Flaws in Diebold Memory Cards in 2005, Hiding These Flaws in the Windham, NH Audit This Week?](https://www.thegatewaypundit.com/2021/05/exclusive-auditor-harry-hursti-uncovered-fatal-flaws-diebold-memory-cards-2005-hiding-flaws-windham-nh-audit-week/)", May 29, 2021.

[^4]: Nick Moseder: "[BREAKING: THOUSANDS Of Votes Lost In New Hampshire](https://rumble.com/vmmkah-breaking-thousands-of-votes-lost-in-new-hampshire.html)", Sep 17, 2021.

[^5]: Rumble Interview: "[Smoking Gun in New Hampshire?](https://rumble.com/vhu4l3-smoking-gun-in-new-hampshire.html)" with CanCon, Professor David Clements, Tom Murray, and Ken Eyring, May 30, 2021.

[^6]: Ken Eyring explains the discussions he had with Secretary of State Bill Gardner and the initial agreement they came to: <https://granitegrok.com/mg_windham/2021/03/big-news-the-windham-incident-agreement-thank-you-nh-sos-bill-gardner>

[^7]: The Gateway Pundit: "[The Auditors of the Windham, New Hampshire 2020 Election Results Audit Invited the President of the Voting Machine Company to the Audit – Why?](https://www.thegatewaypundit.com/2021/05/auditors-windham-new-hampshire-2020-election-results-audit-invited-president-voting-machine-company-audit/)", May 30, 2021.

[^8]: [This video tweet from Heather Mullins](https://twitter.com/TalkMullins/status/1397653122168725505?s=20), Real America's Voice, May 27, 2021.

[^9]: Verified Voting’s current webpage shows he is not a member. However, the Internet Archive shows that [on April 29, Hursti was shown as being on the Board of Advisors](https://web.archive.org/web/20210429233013/https://verifiedvoting.org/team/). On May 3, the NH AG and SoS hired him to be a member of their audit team. Two days later, on May 5, any evidence of Hursti’s participation as an advisor with Verified Voting had been deleted. This evidence was reported by The Gateway Pundit, see [^9b].

[^9b]: The Gateway Pundit: "[BREAKING EXCLUSIVE: The Windham, New Hampshire 2020 Election Audit Was Over Before It Started – Two of Three Auditors Have Conflicts with Pelosi and Schumer](https://www.thegatewaypundit.com/2021/05/breaking-exclusive-windham-new-hampshire-2020-election-audit-started-two-three-auditors-conflicts-pelosi-schumer/)", May 26, 2021.

[^10]: Philip Stark was previously listed on the EAC website as being on their Board of Advisors, but has since been removed. See [before](https://web.archive.org/web/20201211161948/https://www.eac.gov/about/staff-directory/philip-stark) and [after](https://www.eac.gov/about/staff-directory/philip-stark).

[^11]: Nick Moseder: "[New Hampshire Election Fraud](https://rumble.com/vhnka7-new-hampshire-election-fraud.html)", May 26, 2021.