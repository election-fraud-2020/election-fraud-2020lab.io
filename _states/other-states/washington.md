---
title: Washington
heading: Election Fraud in Washington State
state: Washington
abbr: wa
last_updated: 4 May 2022
---

{% include state_stub_1 %}

{% include election_integrity_scorecard %}

{% include seth_state_page_summary %}

{% include dr-frank/state-summary
    correlation=nil
    registrationsOver100=false
    extra1=nil
    exampleCounty=nil
    exampleDate=nil
%}

{% include electronic_voting_machines %}


## Skagit County

### Public Hearing

Over 500 people showed up on Sunday, Aug 29, 2021 to witness a Washington State Public Hearing on Election Integrity at Heritage Ranch, Bow, Skagit County, WA. Presenters included:

Dr. Douglas Frank on manipulated voter rolls and statistical anomalies | 📹&nbsp;[watch&nbsp;video](https://rumble.com/vmjuoo-dr-franks-presentation-at-washington-election-integrity-public-hearing.html)
Draza Smith on vote count statistical anomalies | 📹 [watch video](https://rumble.com/vmkc4q-draza-smiths-presentation-at-washington-election-integrity-public-hearing.html)
Government Watchdog Cody Hart | 📹 [watch video](https://rumble.com/vm1ne1-cody-hart-presentation-at-election-integrity-public-hearing.html)
San Juan whistleblower, a former county elections official who witnessed 400 completely pristine envelopes and ballots in one shift that had no marks or folds, as well as other anomalies | 📹 [watch video](https://rumble.com/vlxdxy-san-juan-canvasser-delivers-a-powerful-testimonial-at-public-hearing.html)
A former Department of Licensing employee with a powerful testimonial about the severe lack of verification that allowed non-citizens and felons to illegally register to vote, and that department supervisors actively and knowingly encouraged it | 📹 [watch video](https://rumble.com/vlxddo-patti-johnson-delivers-a-powerful-testimonial-at-public-hearing.html)
Numerous other presentations | 📄 [see here](https://waelectionintegrity.com/)

The event was livestreamed. After garnering over 2,700 views, many videos were censored by YouTube and Facebook, but the videos are now available at the links below.

Watch the hearing videos at [WAElectionIntegrity.com](https://waelectionintegrity.com/), or [on Rumble here](https://rumble.com/c/c-1081394). Or read an overview from the [Skagit County Republican Party](https://skagitrepublicans.com/SkagitCOPublicHearingonElectionIntegrityExposesWAStateVotingIrregularitiesin2020Elections).


### Skagit Voter Integrity Project Summary Report

A month later, on Sep 30, 2021, the Skagit Voter Integrity Project released a [summary report](https://www.scribd.com/document/529679746/Skagit-County-Washington-Voter-Integrity-Project-Summary-Report-09-30-21). It highlights some major concerns about the 2020 election, in the county and beyond:

* More than half of state county voting machines had not been properly certified for the 2020 elections [^1]

* The number of county registered voters skyrocketed from 75,699 in 2018 to 85,682 in 2020 (during years of minimal county population increases and building moratoriums), yet 90,590 mail-out ballots were issued county-wide --- 5,000 more than there were registered voters! [^3]

* An alarming 21% increase in voter turnout compared with 2 and 4 years earlier [^2]

* Over 300 suspected forged ballot signatures during the 
2020 general election [^4]

* Over 1,000 Skagit voters had their ballot credited to Anacortes drop box locations, despite voters not voting in Anacortes and not placing their ballots in Anacortes drop boxes [^5]

* Destruction of critical video surveillance records of the 2020 Federal election [^6]

* {% include canvass/washington %}

{:.highlight}
> From the door-to-door canvass they discovered that 27.6% of ballots cast in 2020 came from voters who did not exist at their registered address. Many other anomalies were also uncovered.

[Read the Full Report](https://www.scribd.com/document/529679746/Skagit-County-Washington-Voter-Integrity-Project-Summary-Report-09-30-21){:.button}

{% include canvass/link %}


{% include bloated_voter_rolls %}

{% include trend-analysis-summary %}



## Other Reports & Updates

* A team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the report "[2020 Presidential Election Contrast Analysis](https://election-integrity.info/Contrast_Report.pdf)" which lists Washington as the state with the 6th highest number of unexpected Biden votes

* Vicki Craft, Washington State Representative spoke at the [Cyber Symposium](/in-detail/cyber-symposium-mike-lindell/), saying:
    - She has seen a lot of challenges
    - She received no reply from Secretary of State to her emailed questions
    - Mail-in ballots, early voting, harvesting... these have been done for years
    - Governor still has single authority (using their emergency powers)
    - As an elected official, she was kicked out of her own Capitol!
    - "We're in trouble, and we need the people. The people will turn it around and take it back."
    - She wants to see a forensic audit checklist, and model legislation
    - She talked with Wendy Rogers, and attempting to work together with her

    [View video](https://rumble.com/vl0pki-mike-lindells-cyber-symposium-day-2-live-continued.html) (legislators began sharing around 2hr 55min mark)

* Vicki Craft later sent a letter ([view PDF](https://www.clarkcountytoday.com/wp-content/uploads/2021/08/Letter-to-Sec.-Wyman-and-39-WA-County-Auditors.pdf)) to Secretary of State Kim Wyman requesting that forensic images be taken of all election machines before they are modified with any future updates. Aug 17, 2021. This followed Kraft seeing firsthand, proof of Dominion destroying logs in Mesa, [Colorado](../colorado/), a violation of federal law.

* Robert Sutherland, Washington State Representative, also spoke at the [Cyber Symposium](/in-detail/cyber-symposium-mike-lindell/), saying:
    - He has called for public hearings regarding 2020 elections
    - This Sunday (August 15, 2021), will listen to public testimony
    - Affidavits are coming in now
    - If testimony is compelling, will be asking the public if they have faith in the election
    - Will call for full forensic audit
    - The evidence is becoming overwhelming

* [WEiCU](https://weicu.org/) (We See You) Director Tamborine Borrelli announced a lawsuit for a full forensic audit in WA, Sept 3, 2021. [See YouTube announcement](https://youtu.be/6lhXTKWmLS8). They are also collecting affidavits from anyone who personally witnessed election issues.


{% include canvass/get-involved %}

{% include news %}

{% include state_rep_support %}

{% include state_telegram_channels %}

{% include raw_data %}

{% include further_leads %}

### Footnotes & References

[^1]: This happened because voting machine testing laboratory’s [U.S. Election Assistance Commission](https://www.eac.gov/voting-equipment/voting-system-test-laboratories-vstl) two-year accreditation certifications expired on February 25, 2017 and was not renewed until February 1, 2021 violating [RCW 29A.12.080](https://app.leg.wa.gov/RCW/default.aspx?cite=29A.12.080) and [WAC 434-335-040](https://apps.leg.wa.gov/WAC/default.aspx?cite=434-335-040). 

[^2]: [Skagit Voter Integrity Project Summary Report](https://www.scribd.com/document/529679746/Skagit-County-Washington-Voter-Integrity-Project-Summary-Report-09-30-21), Sep 30, 2021

[^3]: US Representative-Candidate Cody Hart: "[Audit Of Skagit County Records Finds Serious Election Concerns!](https://codyhart.org/blog/f/audit-of-skagit-county-records-finds-serious-election-concerns)", Jan 2, 2021.

[^4]: US Representative-Candidate Cody Hart: "[Nearly 500 Forged Ballot Signatures Reported to Sheriff McDermott](https://codyhart.org/blog/f/nearly-500-forged-ballot-signatures-reported-to-sheriff-mcdermott)", Sep 30, 2021.

[^5]: [See full list (PDF)](https://img1.wsimg.com/blobby/go/39d6f76e-c6b0-4dd9-a6b7-1fd799ca34ea/downloads/SUSPECTED%20TAMPERED%202020%20BALLOTS%20COLLECTED%20IN%20A.pdf?ver=1627756816856) of suspected tampered ballots

[^6]: US Representative-Candidate Cody Hart: "[Investigation Finds 2020 Skagit County Election Video's Destroyed](https://codyhart.org/blog/f/investigation-finds-2020-skagit-county-election-videos-destroyed)", Mar 2, 2021. This potentially breaches [Federal Law 52 USD 2071](https://uscode.house.gov/view.xhtml?req=granuleid:USC-prelim-title52-section20701&num=0&edition=prelim).

