---
title: Minnesota
heading: Election Fraud in Minnesota
state: Minnesota
abbr: mn
last_updated: 4 May 2022
---

{% include state_stub_1 %}

{% include election_integrity_scorecard %}


## Illegal Ballot Harvesting

In September 2020, two months prior to the election, investigative journalists from Project Veritas exposed an illegal ballot harvesting operation in the City of Minneapolis, thanks to Omar Jamal, employee of the Ramsey County Sheriff Department and chairman of the Somali Watchdog Group. Their reports [^4] [^1] indicate that:

* Ballots were collected or "harvested" for the benefit of City Councillor Jamal Osman, with video footage of his brother Liban Osman boasting about having 300 ballots in his car [^4]

* Somali Congresswoman Ilhan Omar (Democrat), her campaign manager Ali Isse Gainey, and other associates were also implicated, allegedly paying approximately $250,000 in cash payments to at least forty workers on the ground (of which Liban was just one) [^1] [^4]

* The scheme operated in at least three locations inside Hennepin County, Ward 6, as a "ballot harvesting triangle": the <a href="https://www.sherman-associates.com/properties/riverside-plaza/" target="_blank">Riverside Plaza apartments</a>,  the senior citizen community at <a href="https://www.apartmentfinder.com/Minnesota/Minneapolis-Apartments/Horn-Towers-Apartments" target="_blank">Horn Towers</a> and the <a href="https://vote.minneapolismn.gov/" target="_blank">Minneapolis Elections and Voter Services office</a> at 980 E. Hennepin Ave., which also functions as a voting location and ballot drop-off site. Harvesters allegedly knocked on doors and collected unopened mail-in ballots from every resident. [^4]

* The operation targeted the 80,000--100,000 Somali immigrants in Minnesota (while many in the Somali community were aware of it) [^1]

* It also involved corrupt election judges and voting booth interpreters that accompanied voters and filled out their ballots for them [^1]

{:.small}
> The voter turnout in Hennepin County in 2018 was shockingly high. We may now have the explanation. If what appears in the Project Veritas video is accurate the results of every election that touched Hennepin County in 2018 are in question."
>
> <small>--- Jennifer Carnahan, chairwoman of the Republican Party of Minnesota, Sep 29, 2020 [^5]</small>

The Omar campaign released a statement that "the amount of truth to this story is...zero" [^5] and several mainstream media channels were quick to dismiss the report as incorrect and "deceptive" (including Fox9 which got the story quite wrong [^6]), however several of them later issued retractions. Despite media attempts to discredit him, The New York Times themselves appear to view Omar Jamal as a reliable source, having published at least 10 articles quoting him on matters relating to the Somali community. [^2] After the The New York Times refused to retract their inaccurate and defamatory reporting, Project Veritas issued a lawsuit. Documents later revealed that The New York Times considers their articles about Project Veritas mere "unverifiable expression of opinion" and not to be taken seriously. The court decisions thus far appear to lean in favor of Project Veritas. [^3]


{% include seth_state_page_summary %}

{% include electronic_voting_machines %}

{% include zuckerberg_interference.html %}


## Other Reports

A team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the following reports:

  * "[2020 Presidential Election Startling Vote Spikes](https://election-integrity.info/Vote_Spikes_Report.pdf)" which includes a section on Minnesota

  * "[2020 Presidential Election Contrast Analysis](https://election-integrity.info/Contrast_Report.pdf)" which lists Minnesota as the state with the 9th highest number of unexpected Biden votes

{% include canvass/get-involved %}

{% include news %}

{% include state_rep_support %}

{% include state_telegram_channels %}

{% include raw_data %}

{% include further_leads %}

### Footnotes & References 

[^1]: Project Veritas: "[Caught In The Act: Ballot Harvester On Camera Exchanging Cash For General Election Ballot… “She's [Ilhan Omar] The One Who Came Up With All This … We Are Taking The Money And We’ll Vote For You … We Don’t Care Illegal.”](https://www.projectveritas.com/news/caught-in-the-act-ballot-harvester-on-camera-exchanging-cash-for-general/)", Sept 29, 2020

[^2]: Project Veritas: "[Project Veritas Sues The New York Times For Defamation Over Labeling Our Videos “Deceptive," “Coordinated Disinformation," Using Solely "Unidentified Sources,” And "No Verifiable Evidence."](https://www.projectveritas.com/news/project-veritas-sues-the-new-york-times-for-defamation-over-labeling-our/)", Oct 30, 2021

[^3]: Project Veritas: "[VICTORY: Court Delivers Huge Win for Project Veritas Against The New York Times](https://www.projectveritas.com/news/victory-court-delivers-huge-win-for-project-veritas-against-the-new-york/)", Mar 19, 2021

[^4]: Project Veritas: "[Ilhan Omar Connected Cash-For-Ballots Voter Fraud Scheme Corrupts Elections: 'These Here Are All Absentee Ballots...Look...My Car Is Full..." 'Money Is The King Of Everything'](https://www.projectveritas.com/news/ilhan-omar-connected-cash-for-ballots-voter-fraud-scheme-corrupts-elections/)", Sep 27, 2021

[^5]: KSTP Eyewitness News: "[Hennepin County attorney says no reports of 'ballot harvesting' prior to allegations from Project Veritas, MPD evaluating 'validity'](https://kstp.com/politics/hennepin-county-attorney-says-no-cases-of-ballot-harvesting-reported-following-allegations-from-project-veritas-mpd-evaluating-claim-september-28-2020/5877146/)", Sep 29, 2020

[^6]: Fox9 released a video report and [article](https://www.fox9.com/news/subject-of-project-veritas-voter-fraud-story-says-he-was-offered-bribe) attempting to debunk the Project Veritas investigation, however James O'Keefe from Project Veritas subsequently issued a video response identifying the misinformation and selective editing done by Fox: "[James O'Keefe goes NUCLEAR in debunking bogus Fox 9 report that defends illegal ballot harvesting](https://www.youtube.com/watch?v=tAZfQ7CpdKk)", Oct 7, 2020

