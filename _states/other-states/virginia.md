---
title: Virginia
heading: Election Fraud in Virginia
state: Virginia
abbr: va
last_updated: 4 May 2022
---

{% include state_stub_1 %}

{% include election_integrity_scorecard %}

{% include seth_state_page_summary %}

{% include electronic_voting_machines %}

## Strange Anomalies with Ballot Counting

The graph of ballot counting for the election exhibits some highly strange activity:

{% include large_image url="/images/virginia/vote-count.png" maxWidth="1200" %}

{:.caption}
Source: [wwrkds.net: A Midnight Spike in VA, and generation of Election Fingerprints over time](https://wwrkds.net/wp2/a-midnight-spike-in-va-and-generation-of-election-fingerprints-over-time/)

It shows Biden initially losing the in the state, but after some uncharacteristically steep increases, he overtakes Trump, followed by votes getting removed and readded twice (why were votes being *removed* from the counts?), resulting in a *huge* lead over Trump. This activity appears highly questionable and deserves further investigation.


{% include bloated_voter_rolls %}

{% include zuckerberg_interference.html %}

{% include trend-analysis-summary %}


## Other Reports

* A team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the following reports:

  * "[2020 Presidential Election Startling Vote Spikes](https://election-integrity.info/Vote_Spikes_Report.pdf)" which includes a section on Virginia

  * "[2020 Presidential Election Contrast Analysis](https://election-integrity.info/Contrast_Report.pdf)" which lists Virginia as the state with the 7th highest number of unexpected Biden votes

* [wwrkds.net](https://wwrkds.net/wp2/blog/) has also published several statistical analysis reports on Virginia, looking at voter turnout rates vs winning margins, and generating heatmaps ("fingerprints") to identify anomalies


## November 2021 Election

Anomalies continued during the November 2021 election in Virginia. Here's one eyewitness testimony:

{:.small}
> My wife and I went for early voting in Virginia today, and something very disturbing happened.
> 
> We were both told we couldn't vote since we had requested and been sent absentee ballots. Since we had done nothing of the sort, we "raised hell" and they finally produced affidavits we could sign saying that we had lost the ballots we requested. We demanded affidavits saying that we had never requested absentee ballots and absentee ballots had never been received by us. We were told there was no such thing. We could either sign the affidavits saying we had lost the ballots we had requested or go home without voting. We signed.
>
> During the time we were there and engaged in this dispute, a matter of about fifteen minutes, I overheard *three* other people going through exactly the same thing.
>
> We live in a heavily Democrat-controlled precinct in a Democrat-controlled state in which the Republican candidate for governor is running even with the Democrat candidate. A not-inconsiderable number of people in just one precinct during a very brief period of time were complaining that the voter rolls showed they had requested and received absentee ballots they did not request or receive."
>
> <small>--- Shared on [Telegram](https://t.me/ElectionHQ2024/1902)</small>


{% include canvass/get-involved %}

{% include news %}

{% include state_rep_support %}

{% include state_telegram_channels %}

{% include raw_data %}

{% include further_leads %}


### Footnotes & References