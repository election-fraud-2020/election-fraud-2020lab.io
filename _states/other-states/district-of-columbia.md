---
title: District of Columbia
heading: Election Fraud in District of Columbia
state: District of Columbia
abbr: dc
last_updated: 4 May 2022
---

{% include state_stub_1 %}

{:.info}
We have very little information thus far on District of Columbia. If you know of any analysis, reports, or eyewitness accounts of election anomalies or fraud, let us know in the comments below.

{% include election_integrity_scorecard %}

{% include seth_state_page_summary %}

{% include electronic_voting_machines %}

{% include trend-analysis-summary %}

{% include canvass/get-involved %}

{% include news %}

{% include state_telegram_channels %}

{% include state_rep_support %}

{% include raw_data %}

{% include further_leads %}

### Footnotes & References
