---
title: Texas
heading: Election Fraud in Texas
state: Texas
abbr: tx
last_updated: 4 May 2022
---

{% include state_stub_1 %}

{% include 2000-mules-summary %}

{% include election_integrity_scorecard %}

{% include seth_state_page_summary %}

Seth's estimates were later validated by [preliminary election audit results](https://t.me/ElectionHQ2024/2740) that surfaced in Jan 2022.


## Further Allegations

[TarrantElectionIntegrity.com](https://tarrantelectionintegrity.com/) published the following summary of election issues across numerous Texas counties:

<iframe class="scribd_iframe_embed" title="2020 Election Issues in Texas: Harris, Dallas and Central Texas, Jan 27" src="https://www.scribd.com/embeds/556836610/content?start_page=1&view_mode=scroll&access_key=key-Rt2k7ivFkBbYbvrGbx9O" tabindex="0" data-auto-height="true" data-aspect-ratio="0.7729220222793488" scrolling="no" width="100%" height="600" frameborder="0"></iframe>



{% include dr-frank/state-summary
    correlation=0.999
    registrationsOver100=false
    extra1=nil
    exampleCounty=nil
    exampleDate=nil
%}




## Call for Forensic Audit in Tarrant County

Tarrant County Precinct Chairwoman Susan Valliant drafted the following resolution that has been submitted to the Tarrant County GOP Executive Committee. 

<iframe class="scribd_iframe_embed" title="Resolution to Call for a Tarrant County Election Audit, Sept 2021" src="https://www.scribd.com/embeds/545193839/content?start_page=1&view_mode=scroll&access_key=key-p1PZUr8aWGrL6u3j4Y5q" tabindex="0" data-auto-height="true" data-aspect-ratio="0.7729220222793488" scrolling="no" width="100%" height="700" frameborder="0"></iframe>

Editor's Note: Unfortunately we do not have a copy of the attached appendices. If you have a link to these, please let us know in the comments.

Further efforts to restore election integrity are documented at [TarrantElectionIntegrity.com](https://tarrantelectionintegrity.com/).


{% capture text %}

#### Lack of Certification

Texas, [Colorado](/fraud-summary-by-state/colorado/#use-of-non-certified-voting-machines), [Arizona](/fraud-summary-by-state/arizona/), and numerous other states were affected by Voting System Test Laboratory Pro V&V certifying their electronic voting and tabulation machines despite not having an active accreditation during 2020. The US Election Assistance Commission (EAC) tried to cover for this mishap saying that accreditation "cannot be revoked unless the EAC Commissioners vote to revoke the accreditation", when in fact this appears to violate federal law which stipulates that accreditation expires after 2 years unless specifically renewed. [^2] [^3] [^4] [^5]

The counties shown in blue below used ES&S machines that were likely affected by this lack of certification:

[![Texas counties with ES&S uncertified machines](/images/texas/ess-counties.jpg){:width="450"}](/images/texas/ess-counties.jpg)

{:.caption}
Source: [ES&S Voting Machine Certification Failures](https://rumble.com/vphbiz-es-and-s-certification-failures.html)

For further details and explanatory videos, see our article *[Voting Machines Lacking EAC Accreditation](/in-detail/vstl-labs-not-eac-accredited/)*.


#### Lack of Security

{% endcapture %}


{% include bloated_voter_rolls %}

{% include electronic_voting_machines 
    prepend=text %}

{% include zuckerberg_interference.html %}

{% include trend-analysis-summary %}


## Other Reports

* A team of scientists from [Election-Integrity.info](https://election-integrity.info/) produced the report "[2020 Presidential Election Contrast Analysis](https://election-integrity.info/Contrast_Report.pdf)" which includes a section on {{ page.state }}

* [Bexar County Election Integrity](https://bexarcountyei.wixsite.com/bcei-site), a grassroots group, has information on election issues in the county and is recruiting volunteers to assist with the cause

* Texas Attorney General Ken Paxton reported on Sep 24, 2021 that he is still prosecuting over 500 cases of election fraud in the state. [^1]  
(If you have further details on this statistic, please let us know in the comments below.)


{% include canvass/get-involved %}

{% include news %}

{% include state_rep_support %}

{% include state_telegram_channels %}

Analyst Seth Keshel also [reports](https://t.me/ElectionHQ2024/1673) that he has had meetings with several candidates for Texas Governor: Don Huffines, Col. Allen West, and Chad Prather, all of whom have showed at least some interest in fixing election problems in the state. Seth has not had the same interest from Governor Greg Abbott.

{% include raw_data %}

{% include further_leads %}


### Footnotes & References

[^1]: Ken Paxton interview on Bannon's War Room: "[Democrats Fighting Against Legitimate Elections](https://rumble.com/vmwxwj-democrats-fighting-against-legitimate-elections.html)", Sep 24, 2021

[^2]: Rumble.com: "[ES&S Voting machine Certification failures](https://rumble.com/vphbiz-es-and-s-certification-failures.html)", published Nov 19, 2021.

[^3]: EAC: "[VSTL Certificates & Accreditation](https://www.eac.gov/sites/default/files/voting_system_test_lab/files/VSTL%20Certificates%20and%20Accreditation.pdf)"

[^4]: EAC, Jerome Lovato Memo: "[Pro V&V EAC VSTL Accreditation](https://www.eac.gov/sites/default/files/voting_system_test_lab/files/Pro_VandV_Accreditation_Renewal_delay_memo012721.pdf)", Jan 27, 2021.

[^5]: See also [Colorado › Use of Non-Certified Voting Machines](/fraud-summary-by-state/colorado/#use-of-non-certified-voting-machines)