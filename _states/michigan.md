---
title: Michigan
state: Michigan
abbr: mi
heading: Election Fraud in Michigan
# subtitle: Subtitle to go here
meta_desc: An overview of key election integrity issues in Michigan during the 2020 US Presidential elections.
number_of_disputed_ballots: 270,000 votes (approx.)
biden_winning_margin: 154,188 votes (2.8%)
electoral_college_votes: 16 votes
forensic_audit: ❌ Internal audits only
summary: |
    - Padded voter rolls
    - Suspicious vote spikes going 95%+ to Biden
    - 270,000 votes counted without independent observation 
    - Outstanding/unresolved integrity issues with Dominion counting machines
    - Eyewitness accounts of illegal ballot handling
    - Numerous counties where ballot tallies did not match records
    - Overuse of tally adjudication within Dominion software
    - Audits being performed by the same county staff who performed the elections
    - Obstruction of further audits
battleground_state: true
last_updated: 4 May 2022
---

{% include state_stats.html %}

{% include state_summary_points %}

{% include toc %}


## Key Allegations From The Patrick Colbeck Report

Patrick J. Colbeck, former elected Michigan Senator (2011-2018), and former candidate for governor in Michigan, released a report on election fraud and irregularities that he either personally witnessed during poll-watching (in some cases) or later become aware of. 


> We have heard persistent refrains from the media and complicit elected officials that “there is no evidence of election fraud” in the 2020 election. That is simply not true.
>
> "The best way to dispel the brewing division in our nation over the election results is to pursue an emergency forensic audit of the paper *and* electronic election materials for the battleground states.
>
> "If the audit reveals that all of the concerns cited in this [Case for MI Decertification](https://secureservercdn.net/198.71.233.150/bkq.639.myftpupload.com/wp-content/uploads/2021/01/Case-for-MI-Decertification.pdf) are invalidated, so be it. ... If the audit reveals that these concerns are indeed valid, though, the results of the 2020 Election should be *decertified*."
>
> <small>--- Patrick J. Colbeck</small>

Read [his summary article](https://letsfixstuff.org/2021/01/the-case-for-decertification-of-michigan-election-results/), or his [86 page PDF presentation](https://secureservercdn.net/198.71.233.150/bkq.639.myftpupload.com/wp-content/uploads/2021/01/Case-for-MI-Decertification.pdf).

{% include large_image alt="Michigan Election Report Summary" url="/images/michigan-report-summary.png" maxWidth="1100" %}

In summary, it alleges that there were *dozens* of breaches of election integrity, including:

#### Chain of Custody Broken

  - Based on the Qualified Voter File (QVF) or Voter Registration Database, 616,648 ineligible voters were allowed to vote, and 12.23% of absentee voters did not request an absentee ballot.
  - At least 210 dead voters (Possible 1,005 additional dead voters)
  - At least 317 voters cast votes in multiple states
  - At least 13,248 absentee or early voters were not residents of Michigan when they voted
  - 2,474 voters had invalid addresses (additional 857 unverifiable)
  - Fake birthdays entered demonstrating no validation of voting age criteria being met
  - Multiple versions of poll books per precinct
  - Double voting occurred due to multiple poll books
  - Unsupervised ballot duplication
  - Suspicious drops of "tens of thousands" of ballots
  - At least 289,866 illegal votes cast
  - Evidence of internet connectivity [^11]
  - Evidence of fractional vote tallies
  - Dominion election system featured a 68% error rate resulting in suspicious adjudication rate. Learn more about [Dominion](/in-detail/dominion-voting-machines/).
  - Data anomalies indicate fraud
    
#### Statutory Violations

  - Deliberate interference with duties of poll challengers
  - Republican poll workers rejected by election officials
  - Election processes executed without representatives of both major political parties
  - Poll workers left polling location prior to closure of polls
  - Destruction of election artifacts prior to end of 22 month archival requirement
  - Interference with recount efforts
  - Secretary of State Benson allowed online voter registration without signature verification
    
#### Constitutional Violations    

  - Unconstitutional delegation of legislative authority to certify election results to the Governor in [MCL 168.46](https://www.legislature.mi.gov/(S(1ubhebc0t3rquz3i1heoszxe))/mileg.aspx?page=getObject&objectName=mcl-168-46)
  - Unequal protections of law provided in favor of Democrats
  - Unlawful restrictions upon Freedom of Assembly
  - Denial of access to audit
  - Privatization of elections by left-leaning group Center for Tech and Civic Life

#### Board of Canvassers

  - Wayne County
       - No signatures of Republican canvassers were affixed to statement of certification
       - Evidence of “Cooking the Books” prior to Wayne County certification vote
       - Official vote results for Wayne County shows 0 registered voters against a tally of 172,337 votes
  - State Board of Canvassers certified results with one Republican canvasser abstaining.  Wayne County certification issue was not addressed.
  - In Kent, Macomb, Oakland and Wayne County, there is evidence that 289,866 illegal votes were cast
  - Antrim County
       - Evidence that 6,000 Trump votes were allocated to Biden out of 22,000 votes
       - Adjudication enabled vote flipping without a paper trail.

#### Dominion Voting Systems Vulnerabilities

Dominion was used in [65 out of 83 counties in Michigan](https://bkq.639.myftpupload.com/wp-content/uploads/2021/01/Michigan-Voting-Systems-1.pdf), including the troublesome Antrim County. 

  - Dominion CEO John Poulos testified under oath that Ranked Choice Voting module which allows fractional voting was not enabled in Antrim County.  This testimony conflicts with forensic analysis findings.
  - Fractional votes were evident in data stream from Dominion servers to Edison servers
  - Evidence that internet connectivity present during voting in contrast to Dominion CEO testimony  [^11]
  - System manuals explicitly refer to internet and ethernet connectivity
  - Encryption keys stolen
  - DVS passwords available on Dark Web
  - NIST posted DVS file attributes
  - Audit logs deleted
  - Original ballot images deleted

See also the [full article about Dominion Systems]({% link in-detail/dominion-voting-machines.md %}), which includes a video demonstration of how vote switching can occur by direct manipulation of the Dominion database.

{:.info}
For further reports by Patrick Colbeck, including detailed analyses of other third-party reports, [see below](#further-reports), or visit his website at **[LetsFixStuff.org](https://letsfixstuff.org/tag/elections/)**.


{% include 2000-mules-summary %}


## Michigan Senate Committee Hearing on Election Issues 

Many of the above issues, plus additional eyewitness accounts were given during the Senate Committee's hearing on Dec 1, 2020.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/eZXkAv7yKgw?start=107" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

{:.small.muted}
YouTube has censored the above clip. Please [let us know on Telegram]({{ site.data.globals.telegramChatLink }}) or via the comments section below if you are aware of a working, alternative link.

As mentioned [below](#michigan-senate-2020-election-report), the Michigan Senate later stated that "This Committee found no evidence of widespread or systemic fraud in Michigan’s prosecution of the 2020 election.” It appears that they ignored, overlooked or potentially "covered up" much of the testimony and evidence presented.


## Michigan 2020 Voter Analysis Report

A detailed statistical analysis of the ballots and anomalies uncovered within the 2020 election in Michigan was released by [Election-Integrity.info](https://election-integrity.info){:target="_blank"}, with contributions by Dr. Louis Anthony Cox, Jr., Tom Davis, S. Stanley Young, PhD, Dr. Eric Quinnell, Robert Wilgus, Dr. William M. Briggs, Dr. Robert Hancock and compiled by physicist John Droz, Jnr.

[Read the PDF Report](https://election-integrity.info/MI_2020_Voter_Analysis_Report.pdf){:target="_blank"}{:.button}



## The DePerno Reports

Attorney Matthew DePerno collaborated with several cyber security firms and forensic experts as part of his election-related lawsuits and has amassed at least 20 technical reports on security vulnerabilities and other evidence of fraud during the election, with a special focus on Michigan.

The 20+ reports are published at on [his website](https://www.depernolaw.com/all-expert-reports.html), with latest updates found on [his Twitter page](https://twitter.com/mdeperno).

> If we extrapolate the findings in Antrim County --- which match almost identically with what they come out with in Arizona --- if we extrapolate those results to the entire state, not counting Wayne County which is Detroit, there's *500,000 fraudulent votes*, that's *500,000 phantom ballots* in the state."
>
> <small>--- Attorney Matt DePerno [^17]</small>


{% include election_integrity_scorecard %}


## The Halderman Report

J. Alex Halderman, a professor of computer science and engineering at the University of Michigan, wrote a report ([view PDF](https://www.michigan.gov/documents/sos/Antrim_720623_7.pdf)) released and approved by the Michigan Department of State that argued human error and poor practices led to early tabulation errors in Antrim County but that the results were fully rectified and eventually accurate. While Halderman acknowledged vulnerabilities in the election technology, he stressed that "there is no evidence that any of these problems was ever exploited in Antrim County."

Several other sources dispute this claim, including Attorney Matthew DePerno and Analysts James Thomas Penrose IV and Jeffrey Lenberg. They released an [8-page report](https://www.scribd.com/document/513548171/JPenrose-JLenberg-AssessmentofHalderman-06-23-2021-Final-Signed2) and affidavit citing inaccuracies and glaring omissions in Halderman's report, and rebutting his conclusions.


## Michigan Senate 2020 Election Report

On June 23, 2021, the Michigan Senate Oversight Committee approved the release of their own report on the November 2020 Election in Michigan ([view PDF](https://misenategopcdn.s3.us-east-1.amazonaws.com/99/doccuments/20210623/SMPO_2020ElectionReport_2.pdf)). The Committee is Chaired by Senator Ed McBroom (R) and includes Vice Chair Senator Lana Theis (R), Senator John Bizon (R), and Senator Jeff Irwin (D). 

> This Committee found no evidence of widespread or systemic fraud in Michigan’s prosecution of the 2020 election.”
>
> <small>-- MI Senate Oversight Committee</small>

Yet, as Patrick Colbeck points out, the same committee members then proceeded to claim that there were “severe weaknesses in our election system”. He wrote [a detailed critique of the report](https://letsfixstuff.org/2021/06/mi-senate-2020-election-report-biglie/){:.target="_blank"} and its many flaws.

State Representative Daire Rendon wrote a letter shortly after stating she is "in receipt of evidence reflecting systemic election fraud in Michigan that occurred in the November 2020 election" and that the Senate Oversight Committee did not examine appropriate evidence, expert reports, and testimony. [^8]

Donald Trump labelled the Senate's actions as a "cover up":

![Donald Trump Statement to Michigan Senate](https://100percentfedup.com/wp-content/uploads/2021/06/E4qXi5bXEAU5Pb3.jpeg){:.image-border}

Attorney (and Attorney General candidate) Matt DePerno reviewed the report and responded that:

{:.small}
> With this report, the Michigan Senate is attempting to **cover up evidence** of election fraud in the November 2020 general election. They are also using the mantle of government to proactively **intimidate** anyone from speaking out about election fraud. ...
> 
> The Michigan Senate has **refused** to meet with our attorneys and team of forensic experts to review actual evidence of election fraud. Reportedly, Senator McBroom (who has been accused in the past of violating people’s constitutional rights) has gone so far as to **instruct the Republican caucus to not review evidence for themselves**. If they don’t review the evidence, they can continue to say they have seen no evidence. Nevertheless, we have so far released 19 reports on election fraud through multiple legal briefs filed with the 13th Circuit Court in Antrim County. We are not done. Additional reports will be released soon. **The Michigan Senate failed to properly address *any* of the evidence submitted** in the 19 reports available for everyone to review at [DePernoLaw.com](https://www.depernolaw.com). You can also see a great deal of the evidence at [LetsFixStuff.org](https://letsfixstuff.org). These reports expose the inherent vulnerabilities and weak or non-existent security protocols of voting machines. But more importantly, these reports also expose how **the voting system and election in Antrim County was actually and definitively subverted through fraud and intentional manipulation of the voting machines; and by extrapolation, the State of Michigan**. [^16]


## Key Allegations By Trump Legal Team

Back in November and December of 2020, the Trump legal team had already raised numerous concerns about the results:

* Vote counting rooms were announced as “closed down”, and yet counting continued, without independent observers present [^1]

* Statistical anomalies, including a notorious "vote dump" whereby Biden gained 141,258 votes compared with Trump's 5,968 in a single update on November 4. [^6]

  On November 7, there was another one-sided spike of 54,199 votes:

  <img src="/images/spike-michigan.png" class="image-border" width="3300" height="2550" alt="Michigan Election Vote Spike Chart">

  {:.caption}
Source: DeepCapture.com [^7]

* In Michigan's largest county, Wayne County (south and central Detroit):  [^1]
  - 71 of the precincts could not balance their books - there were more votes than people who voted
  - County board refused to certify because of the number that couldn’t be balanced
  - They were threatened, and recalled their concerns believing there would be a forensic audit. The audit never occurred.
  - Governor Whitlas ignored their refusal to certify
  - Secretary of State has said that records should be destroyed

* 270,000 votes were counted without independent observation [^1]

* The state used [Dominion voting machines](/in-detail/dominion-voting-machines/), which permit tampering and overriding of results. [^1] A forensic audit of the Antrim County vote tabulation found that the Dominion system had an astonishing error rate of 68 percent. By way of comparison, the Federal Election Committee requires that election systems must have an error rate no larger than 0.0008 percent. [^3]

* In some areas, including Antrim County, 82% of votes were flagged for adjudication, meaning an unobserved adjudicator was able to potentially change the outcome of votes [^1]

* All machines deserve to be audited, yet Michigan officials have obstructed attempts to audit their machines [^1]


## Lack of Turnout Statistics

[Dr. Doug Frank](/dr-frank-reports/) pointed out that as of Jan 2022, Michigan Secretary of State has still not published voter turnout statistics for 2020, despite publishing every election from 1948 to 2016. [^18] [See their website](https://www.michigan.gov/sos/0,4670,7-127-1633_8722-29616--,00.html). Dr. Frank and others have noted anomalies in the numbers of registrations and voters in Michigan.


{% include zuckerberg_interference.html %}


## Court Hearings

{% include court_case_link %}

Specific to Michigan:

* On November 13, 2020, the Circuit Court of Michigan denied the presented claims of fraud, citing that witness affidavits were not credible, and that enforcing an independent audit would add undue delays to the election process. [Read full report](https://web.archive.org/web/20210604180948/https://www.democracydocket.com/wp-content/uploads/sites/45/2020/11/Scanned-from-a-Xerox-Multifunction-Printer.pdf).

* The state’s highest court ruled to deny the appeal and disagreed with the lawsuit’s premise that an audit should take place before certification---instead, it could (and should) take place after. It an order ([see PDF](https://www.courthousenews.com/wp-content/uploads/2020/11/mich-supreme.pdf)) it acknowledged that:

  - There was presence of “troubling and serious allegations of fraud and irregularities” 
  - The plaintiffs presented evidence to substantiate their allegations, including in support of claims that ballots were counted from voters whose names were not on poll books and that instructions were given by elections officials to “disobey election laws and regulations.”
  
  While denying Costantino and McCall’s calls to halt the audit, the Michigan Supreme Court said its decision does not preclude plaintiffs from seeking a future “results audit”. [^4]
  
The Michigan State Board of Canvassers subsequently voted to certify the election results. Michigan’s Secretary of State has said that her agency will conduct a post-election performance audit in Wayne County. [^4]


## Further Reports

#### Patrick Colbeck

Patrick Colbeck formerly served two terms in the Michigan Senate and was investigating election integrity issues prior to the 2020 elections. He has written extensively about the integrity issues discovered during this election, with most published on his website [LetsFixStuff.org](https://letsfixstuff.org/). The following articles are especially recommended for further research:

* [Michigan Election Fraud Evidence](https://letsfixstuff.org/2021/03/michigan-election-fraud-affidavits/) - complete list of affidavits and other items of evidence

* [Antrim County: The Thread That Unravels 2020 Election Theft](https://letsfixstuff.org/2021/04/antrim-county-the-thread-that-unravels-2020-election-theft/) - summary of the major issues in Antrim County, drawing on numerous external reports: Doug Frank Report, Allied Security Operations Group (ASOG) Report, Halderman Report, Jim Penrose Report, Cyber Ninja Report, Benjamin Cotton Report, and others.

* [Antrim County: Technology Expert Witness Comparisons](https://letsfixstuff.org/2021/04/antrim-county-technology-expert-witness-comparisons/) - detailed coverage of witness testimonies and where they agree or disagree

* [Modem Chips Embedded in Voting System Computer Motherboards](https://letsfixstuff.org/2021/04/modem-chips-embedded-in-voting-system-computer-motherboards/) - an analysis of how voting machines had wireless access to the internet


{% include seth_state_page_summary headingLevel=4 %}

{% include dr-frank/state-summary 
    headingLevel=4
    correlation=0.997
    registrationsOver100=true
    extra1=nil
    exampleCounty=nil
    exampleDate=nil
   %}

{% include bloated_voter_rolls headingLevel=4 %}


#### Edward Solomon

After finding large statistical impossibilities in Georgia [^9], Mathematician Edward Solomon found that Pennsylvania and Michigan also had similar indications of a fraudulent algorithm. His report [Analysis of Michigan Precinct Timeseries Voting Data](https://docs.google.com/document/d/1UxxiGqcozWIE6w877yTByQPKO3QDU07HcxBc-Eglzso/edit) alleges that 287,980 votes were flipped from Trump to Biden in Michigan.


## Election Audits

#### 1. Internal Audits

Following the election, Jocelyn Benson, Michigan Secretary of State requested that election results be audited, and released a report ([view PDF](https://www.michigan.gov/documents/sos/BOE_2020_Post_Election_Audit_Report_04_21_21_723005_7.pdf)) on the findings. The report states that most counties *audited themselves*, and in some cases the same County Clerk responsible for programming election machines and printing of ballots were the same ones who performed the audit:

{:.small}
> The majority of post-election audits are conducted by Michigan’s 83 county clerks. County clerks do not administer elections directly on election day, but they do serve several critical election functions including the programming of election equipment and printing of ballots. The remainder are conducted by the Michigan Bureau of Elections on behalf of the Secretary of State."

She concluded that "After the most extensive audits in state history, no evidence of intentional misconduct of fraud by election officials was discovered."

The Secretary of State has published an [Election Fact Check Page](https://www.michigan.gov/sos/0,4670,7-127-1633_100423_102534_102535---,00.html) that attempts to debunk claims of election fraud, however some of the content appears dubious and misleading. [^12]


#### 2. Cheboygan County Audit

In June, Cheboygan County board of commissioners voted in favor of requesting a 2020 election audit:

"As commissioners, we have heard from many of our consistuents expressing concerns/questions related to the November 3, 2020 election... We believe we have a responsibility to address these concerns/questions."

They submitted a request to Michigan Department of State Elections, officially requesting that an audit be conducted, specifically requesting: [^15]

  1. A "hand recount" to confirm the numbers

  2. Confirmation that the vote tally was accurately reported to the state

  3. Confirm whether the county's Dominion vote tabulator and/or election system had a modem installed capable of connecting to the internet, or whether there is any evidence that any election machine was in communication with any unauthorized computer or manipulated the actual vote tally

They requested an "accredited election auditor such as from Pro V&V" [^15], however as [the Arizona Senators discovered](/fraud-summary-by-state/arizona/#telling-the-story) when planning the forensic audit in Maricopa, there is no such thing as an "accredited election auditor", only accredited [voting system test laboratories (VSTL)](/faqs/voting-system-test-laboratories-vstl/) which are contracted by voting machine companies like Dominion to grant them <abbr title="U.S. Election Assistance Commission (eac.gov)">EAC</abbr> certification, and as such would be inherently biased against finding issues in machines that they themselves were paid to certify.

In any case, their request was officially denied by the Department of State Elections. The Director reasoned in a response letter that "the commissioners lack the authority under state law to either conduct or supervise post-election audits". According to Michigan state law, only the Secretary of State and County Clerks can carry out election audits. "In light of these costs and the lack of any actual evidence that voting systems have been compromised, the Bureau would not instruct or advise the County Clerk to conduct any additional review." [^14] [^13]

The letter did lay out the possibility of submitting a FOIA (Freedom of Information Act) request: "Through this process, it would be possible for any member of the public to compare the votes on viewed paper ballots to the totals on tabulator tapes (which are also public records that may be inspected), canvass records and reported totals to verify the numbers matched." [^14] [^13]

The chairman of the county's commission said:	"I'm not disappointed and I think he's following the statutes."

It's unclear whether the county will yet pursue the FOIA request.

#### 3. Forensic Audits

**As of August 2021, no full *forensic* audits have yet been initiated.**

The movement towards initiating forensic audits is being discussed on Telegram at [@MichiganAudit](https://t.me/MichiganAudit) and [@MichiganAuditChat](https://t.me/MichiganAuditChat).


## Grassroots Canvass

{% include canvass/michigan %}

{% include canvass/link %}

{% include canvass/get-involved %}

{% include news %}

{% include state_telegram_channels %}

{% include state_rep_support %}

{% include raw_data %}

{% include further_leads %}


### Footnotes & References

[^1]: <span class="pill">Censored</span> "The Current State Of Our Country", Rudy Giuliani, Trump Legal Team, <https://youtu.be/NAzCECx8XeE?t=1320> (around 22min mark). Unfortunately, this video has been censored by YouTube, and no longer appears to be accessible via this link. We will update the address once we have a new link.

[^3]: “[The Immaculate Deception: Six Key Dimensions of Election Irregularities](https://bannonswarroom.com/wp-content/uploads/2020/12/The-Immaculate-Deception-12.15.20-1.pdf)", Peter Navarro, Director of the Office of Trade and Manufacturing Policy and Assistant to the President.

    See also Navarro's follow-up reports:  
    "[The Art of the Steal](https://www.priestsforlife.org/elections/pdf/navarro-art-of-the-Steal.pdf)" (Volume 2 of 3)  
    "[Yes, President Trump Won](https://mdrnrepublican.files.wordpress.com/2021/04/thenavarroreportvolumeiiifinal1.13.21-0001.pdf)" (Volume 3 of 3)

[^4]: "[Michigan Supreme Court Urges Lower Court to Quickly Assess Claims of Election Fraud](https://www.theepochtimes.com/mkt_app/michigan-supreme-court-urges-lower-court-to-quickly-assess-claims-of-election-fraud_3591465.html)", The Epoch Times, November 24, 2020

[^6]: Report: "[2020 Presidential Election Startling Vote Spikes: Statistical Analysis of State Vote Dumps in the 2020 Presidential Election](https://www.scribd.com/document/496106864/Vote-Spikes-Report)" by Eric Quinnell (Engineer); Stan Young (Statistician); Tony Cox (Statistician); Tom Davis (IT Expert); Ray Blehar (Government Analyst, ret’d); John Droz (Physicist); and Anonymous Expert.

[^7]: Patrick Byrne, traditionally a Libertarian, developed a team of computer specialists to investigate election integrity issues including the notorious one-sided "spikes" found in several states and has written extensively about them on his website *[DeepCapture.com](https://deepcapture.com)*. See "[Evidence Grows: ’20 Election Was Rigged](https://www.deepcapture.com/2020/11/election-2020-was-rigged-the-evidence/)" (Nov 24, 2020) and his book "[The Deep Rig](https://www.amazon.com/Deep-Rig-Election-friends-integrity-ebook/dp/B08X1Z9FHP)".

[^8]: Free Press International News Service: "[Storm clouds in Michigan over forensic audit](https://freepressers.com/articles/storm-clouds-in-michigan-over-forensic-audit)", July 2, 2021.

[^9]: Edward Solomon's 12min video "[Geometric Proof for Georgia](https://rumble.com/vdnfcf-edward-solomon-geometric-proof-for-georgia.html)" is available on Rumble. See also [his official report and affidavit](https://drive.google.com/file/d/1-j3msC3EQ0-dxpbN2lsKQm_SoaRrvXSo/view) or [screenshots of his spreadsheets](https://imgbb.com/m5YG58c). Statistician Dr Frank [also believes that Edward Solomon's numbers are correct](https://t.me/FollowTheData/552).

[^10]: American Voters Alliance & The Amistad Project: "[The Legitimacy and Effect of Private Funding in Federal and State Electoral Processes](https://americanvotersalliance.org/wp-content/uploads/2021/06/2.-The-Legitimacy-and-Effect-of-Private-Funding-in-Federal-and-State-Electoral-Processes.pdf)", Dec 14, 2020.

[^11]: The [film from Mike Lindell's Cyber Symposium](https://rumble.com/vkze5h-mike-lindell-cyber-symposium-your-wake-up-call-col-phil-waldron.html) (Aug 10, 2021) showed quite clearly that some machines had internet connectivity. Poll watcher Patrick Colbeck also gave [witness testimony](https://rumble.com/vkzga8-mike-lindell-cyber-symposium-patrick-colbeck-most-unsecure-election-in-hist.html) at the symposium to this fact. Internet connectivity combined with poor overall security left them wide open to fraudulent manipulation.

[^12]: The Secretary of State's [Election Fact Check Page](https://www.michigan.gov/sos/0,4670,7-127-1633_100423_102534_102535---,00.html) is dubious and misleading. For example, the first section claims that batteries do not contain data, and thus the removal of batteries cannot wipe data from a machine. This is misleading and ill-informed, as it's not the batteries themselves that contain data, but that removal of batteries from any device that contains "volatile memory" will cause that memory to be wiped. Volatile memory keeps information only during the time it is powered up. In other words, volatile memory requires power to maintain the information. Unless it's confirmed that the machines could not possibly contain crucial forensic evidence on volatile memory, the SoS's claim should be disputed.

[^13]: Facts Matter with Roman Balmakov: "[Lawmaker Issues Official Subpoenas for Ballots and Election Machines from 2 Counties](https://www.youtube.com/watch?v=X99DELK21zc)", Aug 14, 2021. [Alternate link](https://www.theepochtimes.com/facts-matter-aug-13-election-materials-demanded-from-2-counties-lawmaker-issues-official-subpoenas_3948300.html).

[^14]: Detroit Free Press: "[Bureau of Elections denies Cheboygan County Commission's request to conduct election audit](https://www.freep.com/story/news/politics/elections/2021/08/05/state-elections-director-denies-cheboygan-county-commission-audit/7992370002/)", Aug 5, 2021.

[^15]: Cheboygan County Board of Commissioners: [Committee Meeting Notes, June 22, 2021](https://is0.gaslightmedia.com/cheboygancounty/_ORIGINAL_/fs73-1624278422-70380.pdf) (PDF), see pages 34-36.

[^16]: Matthew DePerno: "[June 24, 2021 Press Release](https://www.depernolaw.com/press-june24-2021.html)".

[^17]: Interview on Bannon's War Room: [Matt DePerno: Audit Officials Were Threatened - Forced to Water Down Audit Report](https://rumble.com/vmyamp-matt-deperno-audit-officials-were-threatened-forced-to-water-down-audit-rep.html), Sep 25, 2021

[^18]: Telegram Post: <https://t.me/ElectionHQ2024/2585>, Dec 19, 2021


