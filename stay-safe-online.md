---
title: Staying Safe Online
breadcrumb: "[Further Articles](#colophon) ›"
last_updated: 21 Sep 2021
---

The opposition against those speaking out against election fraud has become quite intense. Here are a few simple steps to stay anonymous and safer while online:

1. Use a separate browser for research and communications on these issues. We recommend [Firefox](https://www.mozilla.org/en-US/firefox/new/), since it leaks less information than Chrome. 

   For further safety/security, we recommend that you:

   - Do not login to Google or Facebook in this browser, as they may track you
   - Block tracking cookies via *Settings > Privacy > Strict Mode*
   - Install these browser extensions: [uBlock Origin](https://ublockorigin.com/) and [EFF Privacy Badger](https://www.eff.org/pages/privacy-badger) which assist in blocking ads and tracking networks

2. Whenever possible, search using [DuckDuckGo](https://duckduckgo.com) instead of Google. Google is known for detailed tracking of users and also appears to be colluding by suppressing election integrity websites such as ours from search results.  

2. [ProtonMail](https://protonmail.com) is a great, secure, encrypted alternative to Gmail, Outlook and other email providers  
<small>(although note that they have occasionally been forced to reveal customer IP addresses when legally subpoenaed, so use a VPN, see below)</small>

3. We recommend using a VPN for all sensitive research and communications. [Mullvad VPN](https://mullvad.net/en/) is a good option with support for mobiles and desktops. **Avoid** *Express VPN* which has been implicated in surveillance scandals.

4. Install a firewall on your computer, one with *outgoing* protection, and learn how to use it. For Windows machines [TinyWall](http://tinywall.pados.hu/) is quite effective. It blocks all apps (known and unknown) from connecting to the internet unless specifically allowed. This can stop *some* types of malware and electronic surveillance in their tracks.

5. Avoid providing personal information wherever necessary, especially your phone number, email address, full name, or anything that can be easily tied to you. One way to hide your email address is through a service like [AnonAddy](https://anonaddy.com).

   Be aware that anything that can be uniquely identified to you may possibly be used against you: your IP address, credit card number, phone number, email address, social media accounts, etc.