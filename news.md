---
title: Election Integrity News
comments_enabled: false
---

{% assign news = site.data.news %}
{% if news.size > 0 %}

<table class="news-table" style="font-size: 90%">
<tbody>

{% for item in news %}

<tr>
    <td class="small muted" style="white-space: nowrap; padding-top: 0.9em;">{{ item.date | date: "%b %e, %Y" }}</td>
    <td>
        <div><a href="{{ item.url }}">{{ item.title }}</a></div>
        {% if item.description %}{{ item.description | markdownify }}{% endif %}
        <small class="muted">{{ item.source }}</small>
    </td>
</tr>

{% endfor %}

</tbody></table>

{% endif %}

