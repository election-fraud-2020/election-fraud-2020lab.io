---
title: Nebraska Election Analysis by Dr. Douglas Frank
parent_page: /dr-frank-reports/
last_updated: 18 Feb 2022
state: Nebraska
list_of_counties:
---

{% include see_main_article %}

{% include dr-frank/state_report_brief %}

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vhxik3/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Watch on Rumble](https://rumble.com/vkjopd-introduction-to-nebraskas-key-w-dr.-douglas-frank-phd-omaha-ne.html), July 30, 2021. A similar presentation in NE was [recorded here](https://rumble.com/vknulc-an-analytical-assessment-of-whats-brewing-in-nebraska-by-dr.-douglas-frank-.html).

The following video shows the charts for each county (slides only, no audio):

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vhnxpy/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Watch on Rumble](https://rumble.com/vka3v8-nebraska-registration-key.html)


{% include dr-frank/notes %}


## Registrants and Voters by County

{% include dr-frank/nebraska-variations %}



{% include dr-frank/compared-to-other-states %}


## Further Update

{% include tg-embed url="https://t.me/FollowTheData/1614" %}
