---
title: Arizona Election Analysis by Dr. Douglas Frank
parent_page: /dr-frank-reports/
last_updated: 28 Jan 2022
state: Arizona
list_of_counties:
---

{% include see_main_article %}

{% include dr-frank/state_report_briefer %}

## Video Presentation: Fifteen Arizona Counties

Runtime: 42mins

<iframe width="640" height="360" scrolling="no" frameborder="0" style="border: none;" src="https://www.bitchute.com/embed/ivieM8JuFtJe/" allowfullscreen></iframe>


{% include dr-frank/compared-to-other-states %}
