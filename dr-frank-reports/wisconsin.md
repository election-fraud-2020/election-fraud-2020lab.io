---
title: Wisconsin Election Analysis by Dr. Douglas Frank
parent_page: /dr-frank-reports/
last_updated: 28 Jan 2022
state: Wisconsin
list_of_counties:
---

{% include see_main_article %}

<!-- % include dr-frank/state_report % -->


## Dr. Frank's Presentation to Wisconsin Assembly Committee on Campaigns and Elections

Dec 8, 2021. Runtime: 33mins

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vnx0vv/?pub=4" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Watch on Rumble](https://rumble.com/vqj701-dr-frank-speaking-at-election-committee-12-8-21.html)


## Statistical Analysis

{% include dr-frank/state_report_brief %}

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vhgz89/?pub=4" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Watch on Rumble](https://rumble.com/vk35dj-wisconsin-election-demographics-investigation.html)


{% include dr-frank/notes %}

{% include dr-frank/compared-to-other-states %}



## Voter Roll Changes in Wisconsin Counties 2009--2021

The following charts show the synchronized increases and decreases of voters added to and removed from the voter rolls, which appear somewhat questionable. What causes the rolls to all increase together at a certain time and then all decrease together later on? Are they being centrally controlled and manipulated?

![](/images/dr-frank/wisconsin/voter-charts/1.jpg)

![](/images/dr-frank/wisconsin/voter-charts/2.jpg)

![](/images/dr-frank/wisconsin/voter-charts/4.jpg)

“Normalized” means that the maximum value for each county is set to 100%, so that you can see the percent changes, not just the absolute changes. The same data is shown in both graphs above, just displayed differently.

People often ask, "How long has this been going on, Dr Frank. And is it happening everywhere?"

This graph illustrates that it is essentially everywhere, and for at least twelve years, at least in Wisconsin.

![](/images/dr-frank/wisconsin/voter-charts/3.jpg)

He has data going back to the 1990's for [PA](../pennsylvania/) showing it going on there too.

{:.small}
Source: [Telegram Post](https://t.me/FollowTheData/1257), Dec 10, 2021


## Further Update on Wisconsin

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="FollowTheData/1263" data-width="100%"></script>


