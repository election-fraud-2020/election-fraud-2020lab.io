---
title: Missouri Election Analysis by Dr. Douglas Frank
parent_page: /dr-frank-reports/
last_updated: 28 Jan 2022
state: Missouri
list_of_counties:
---

{% include see_main_article %}

{% include dr-frank/state_report_brief %}

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vi4mxe/?pub=4" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Watch on Rumble](https://rumble.com/vkqt2o-the-registration-key-for-missouri.html)

{% include dr-frank/notes %}

{% include dr-frank/compared-to-other-states %}



## Further Update

<script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="FollowTheData/1232" data-width="100%"></script>