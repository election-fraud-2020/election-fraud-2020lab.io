---
title: Where can I find reliable news about election integrity and audits?
TODOs:
last_updated: 28 Jul 2021
comment_intro: Know of any other reliable sources of news? Let us know in the comments below.
---

It can be difficult to sort through the haystack of conflicting news in the media. We generally recommend the following outlets that generally maintain a balance in reporting, without promoting spurious claims:

* [The Epoch Times](https://www.theepochtimes.com/c-election-integrity)
* [RealClear Investigations](https://www.realclearinvestigations.com/)
* [American Greatness](https://amgreatness.com/category/elections/)
* [Steve Bannon - The War Room Podcast](https://pandemic.warroom.org/)
* [Just the News](https://justthenews.com/)

[Defending the Republic](https://defendingtherepublic.org) also recommends the following outlets, although accuracy may vary:

* [Zerohedge](https://www.zerohedge.com/)
* [Undercover DC](https://uncoverdc.com/)
* [The Conservative Treehouse](https://theconservativetreehouse.com/)
* [Sara Carter](https://saraacarter.com/)
* [Daily Wire](https://www.dailywire.com/)
* [Gateway Pundit](https://www.thegatewaypundit.com/)
* [LindellTV](https://lindelltv.com/)
* [FrankSpeech](https://www.frankspeech.com/)

Since many voices have been heavily censored on Twitter, Facebook, and other "big tech" platforms, [Telegram](https://telegram.org/) and [Gab](https://gab.com) are a more reliable source of free, uncensored speech.

## Recommended Telegram Channels

* [General Michael Flynn](https://t.me/RealGenFlynn)
* [Patrick Byrne](https://t.me/PatrickByrne)
* [Sidney Powell](https://t.me/SidneyPowell)
* [Man in America](https://t.me/maninamerica)
* [ElectionEvidence.com](https://t.me/electionevidence)
* [CodeMonkeyZ](https://t.me/CodeMonkeyZ)
* [National Audit Watch Channel](https://t.me/AuditWatch)
* [Seth Keshel](https://t.me/ElectionHQ2024)
* [Captain K's Maps and Stats](https://t.me/CaptainKMapsandStats)
* [President Donald J. Trump](https://t.me/real_DonaldJTrump)
* [Congresswoman Lauren Boebert](https://t.me/LaurenBoebert_CO)
* [Arizona Senator Wendy Rogers](https://t.me/wendyrogersaz)
* [Follow the Data with Dr Frank](https://t.me/FollowTheData)
* [Praying Medic](https://t.me/praying_medic)
* [Arizona Conservatives Take Action](https://t.me/ArizonaConservatives)
* [James O'Keefe](https://t.me/JamesOKeefeIII) (from Project Veritas)
* [Charlie Kirk](https://t.me/CharlieKirk)
* [Donald Trump Jr](https://t.me/TrumpJr)
* [Taking Back America](https://t.me/TakingBackAmerica)
* [Mike Lindell](https://t.me/MichaelJLindell)
* [David Harris Jr](https://t.me/DavidJHarris)