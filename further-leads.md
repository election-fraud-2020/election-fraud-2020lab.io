---
title: Further Leads
---

{% assign leads = site.data.hereistheevidence_com_items | where: 'State', 'Nationwide' %}

<p>The following list of election allegations was collated by <a href="https://HereIsTheEvidence.com">HereIsTheEvidence.com</a> with public contributions. Items specific to a certain state are listed at the bottom of each of <a href="#colophon">our state reports</a>. The list below contains only items that are of <em>national</em> relevance.</p>

<p>Some items have not been reviewed for accuracy, but they are provided for thoroughness and in case they contain important information that we have not covered elsewhere. Remember to use your own discernment.</p>
  
<div class="responsive-table">
<table style="font-size: 85%">
    <tbody>
    {% for item in leads %}
    {% assign isLink = item.Source | slice: 0,4 %}
    {% if item.Source.size < 150 %}
    <tr{% if item.Category == 'Unverified' %} class="muted" style="opacity: 0.7"{% endif %}>
        <td>
            {% if isLink == "http" %}
            <a href="{{ item.Source }}">{{ item.Claim }}</a>
            {% else %}
            {{ item.Claim }} <br>
            {{ item.Source }}
            {% endif %}
        </td>
        <td><span class="pill">{{ item.Category }}</span></td>
    </tr>
    {% endif %}
    {% endfor %}
    </tbody>
</table>
</div>

