---
title: Door-to-Door Canvassing Shows Alarming Results in Eleven States
meta_desc: We explain the process of canvassing voters, door-to-door, and present the eleven states that are already showing alarming results.
parent_page: /canvassing/overview/
last_updated: 27 Mar 2022
comment_intro: Do you know of other canvassing efforts going on around the country? Or know of details we might have missed? Let us know in the comments below.
# states_with_results_below:
#   - Arizona
#   - Colorado
#   - Georgia
#   - Florida
#   - Michigan
#   - Washington
#   - Delaware
collapse_headings: h3
permalink: /canvassing/
---

{:.small.muted}
Last updated {{ page.last_updated }}

For an introduction into canvassing and how it works, see *[Grassroots Canvassing Efforts](/canvassing/overview/)*.

Thanks to the diligent canvassing efforts in the following states, alarming anomalies and irregularities are being uncovered. Here are the states that have reported at least some preliminary results from their canvassing of voters after the 2020 election.



### Arizona

{% include canvass/arizona %}

[Read our full report on Arizona](/fraud-summary-by-state/arizona/){:.button}


### Colorado

{% include canvass/colorado %}

[Read our full report on Colorado](/fraud-summary-by-state/colorado/){:.button}


### Delaware

{% include canvass/delaware %}

[Read our full report on Delaware](/fraud-summary-by-state/delaware/){:.button}


### Florida

{% include canvass/florida %}

[Read our full report on Florida](/fraud-summary-by-state/florida/){:.button}


### Georgia

{% include canvass/georgia-braynard %}

[Read our full report on Georgia](/fraud-summary-by-state/georgia/){:.button}


### Indiana

On March 21, 2022, Indiana First Action released an hour-long presentation on findings in Indiana, including results from canvassing in Hamilton County, Johnson County, which begin at the 19:00 mark.

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vvhh1p/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Watch on Rumble](https://rumble.com/vy3n3n-expose-on-indianas-elections.html)

They found that 34-46% of addresses surveyed had some kind of election-related anomaly whether it was a voter who didn't exist at the address, a vote that was not correctly recorded, or some other kind of concerning record-keeping error.

[Read our full report on Indiana](/fraud-summary-by-state/indiana/){:.button}


### Michigan

{% include canvass/michigan %}

[Read our full report on Michigan](/fraud-summary-by-state/michigan/){:.button}


### New Mexico

{% include canvass/new-mexico %}

[Read our full report on New Mexico](/fraud-summary-by-state/new-mexico/){:.button}


### Pennsylvania

{% include canvass/pennsylvania %}

[Read our full report on Pennsylvania](/fraud-summary-by-state/pennsylvania/){:.button}


### South Carolina

{% include canvass/south-carolina %}

[Read our full report on South Carolina](/fraud-summary-by-state/south-carolina/){:.button}


### Washington

{% include canvass/washington %}

[Read our full report on Washington](/fraud-summary-by-state/washington/){:.button}



<!-- Other states:

    - Texas (Audit Force Channel?)
    - North Carolina (Audit Force Channel)
    - Pennsylvania (Don't Tread Pennsylvania)
      https://t.me/DontTreadPennsylvania/14461
      https://t.me/auditthevotepa      
    - Missouri
    - Not sure about Georgia yet
    - Nevada is training teams, in preparation for canvassing.

    (These should be added to Telegram list, also.)

-->


{% include contact-your-officials-box %}



{% assign canvassPage = site.pages | find: 'url', '/canvassing/get-involved/' %}
{% assign numberOfStates = canvassPage.canvass_teams_by_state | size %}

<div class="info expandable-excluded" markdown="1">
## Get Involved
  
  Canvassing is a great way to verify whether voting records are accurate and free of fraud. There are already teams canvassing in at least {{ numberOfStates }} states, and volunteers are needed.
  
  [Learn more](/canvassing/get-involved/){:.button}
</div>



{:.expandable-excluded}
### More to Come

*If you're aware of other states that are canvassing, let us know in the comments below.*


### Footnotes & References
