---
title: Change Log
---

We strive to keep you up to date with the latest, most pertinent and accurate information, while making it easy for people to find out what has changed since their last visit.

<!-- This page will keep records of all the changes we make to the website and will be updated regularly. -->

A complete history with every change highlighted is also [available on Gitlab](https://gitlab.com/election-fraud-2020/election-fraud-2020.gitlab.io/-/commits/master). If only our elections were this transparent. 😉

<div class="info" markdown=1>

*Due to recent time constraints, and the volume of new information being released, we have not been able to maintain our changelog here on this page.*

*Instead, you can see the [full change history on Gitlab](https://gitlab.com/election-fraud-2020/election-fraud-2020.gitlab.io/-/commits/master).*

</div>


## 4 Oct 2021

* New article: [Door-to-Door Canvassing Results](/canvassing/) which collates all the major canvassing results that we know of so far
* [Washington](/fraud-summary-by-state/washington/) - added Skagit County report (outrageous fraud!)
* [Delaware](/fraud-summary-by-state/delaware/) - added Preliminary Audit Results (also alarming!)
* [North Carolina](/fraud-summary-by-state/north-carolina/) - added Barcode Transparency Issues & Use of Uncertified Machines
* [Kentucky](/fraud-summary-by-state/kentucky/) - new comment from Dr. Frank on difficulty getting access to data 
* [Seth Keshel reports](/seth-keshel-reports/)
  - [Florida](/seth-keshel-reports/florida/) - two new videos from Seth explaining county statistics
  - [Arizona](/seth-keshel-reports/arizona/) - new video from Seth
  - [Texas](/seth-keshel-reports/texas/) - new video from Seth
  - Several new upcoming events
* All state pages now have a link to verifiedvoting.org for more info about what voting equipment was used + other minor tweaks


## 25 Sep - 3 Oct 2021

*Several updates were made during this time period, but due to time constraints we have not yet had a chance to summarize them here.*


## 24 Sep 2021

* [Arizona](/fraud-summary-by-state/arizona/) -- freshly released updates on the [Maricopa Forensic Audit Results](/in-detail/maricopa-arizona-forensic-audit-report-results/)


## 22 Sep 2021

* [North Carolina](/fraud-summary-by-state/north-carolina/) -- added recent statement from Lynn Bernstein
* [Seth Keshel: Montana](/seth-keshel-reports/montana/) -- new chart and analysis from Seth

## 21 Sep 2021

* [Colorado](/fraud-summary-by-state/colorado/) -- went for a much deeper dive into the murky and illegal issues going on in the state. Many updates and additions.
* [North Carolina](/fraud-summary-by-state/north-carolina/#weakened-voter-id-laws) -- an update on the recent [weakening of voter ID laws](/fraud-summary-by-state/north-carolina/#weakened-voter-id-laws)

## 19 Sep 2021

* [Arizona](/fraud-summary-by-state/arizona/) -- updates on the Maricopa audit results, including answers to [Common Criticisms of the Audit](/in-detail/maricopa-arizona-forensic-audit-report/#common-criticisms-of-the-audit), as well as news about the [Settlement Regarding Routers and Splunk Logs](/in-detail/maricopa-arizona-forensic-audit-report/#settlement-regarding-routers-and-splunk-logs)

* [Draza Smith's Election Analysis](/draza-reports/) -- added a better summary of Draza Smith's findings, as well as links to key videos
  - [Anomalies in California Governor Recall Election](/draza-reports/california-recall-election/) -- further updates from Draza, including links to the raw time-series data

* [Minnesota](/fraud-summary-by-state/minnesota/#illegal-ballot-harvesting) -- added a summary of Project Veritas' exposé on [ballot harvesting](/fraud-summary-by-state/minnesota/#illegal-ballot-harvesting)

* [Washington](/fraud-summary-by-state/washington/#skagit-county-public-hearing) -- added mention about important Skagit County Public Hearing with some key presenters Dr. Frank and Draza Smith. Videos available via the link.

<!-- there were more edits in this gap which I haven't yet recorded! -->

## 13 Sep 2021

* Publishing a new series on [How to Identify Electoral Fraud for Yourself, with Trend Analysis](/trend-analysis/), including two new articles:
  * [The Battle for the Largest Counties](/in-detail/battle-for-largest-counties/)
  * [Historical County General Election Trends](/trend-analysis/3-historical-county-trends/)
* [Seth Keshel's Reports](/seth-keshel-reports/):
  * [Oregon](/seth-keshel-reports/oregon/) - added new video from Seth about Linn County, Oregon

## 12 Sep 2021

* [Seth Keshel's Reports](/seth-keshel-reports/):
  * [Oregon](/seth-keshel-reports/oregon/) - corrected the stats on the page, which were incorrect, and also improved some wording for better readability

## 10 Sep 2021

* New "Reports by State" section which includes the existing 6 battleground states, plus new pages for the other 44 states, to provide a central portal for exploring each state. See:
[AL](/fraud-summary-by-state/alabama/)
[AK](/fraud-summary-by-state/alaska/)
[AR](/fraud-summary-by-state/arkansas/)
[CA](/fraud-summary-by-state/california/)
[CO](/fraud-summary-by-state/colorado/)
[CT](/fraud-summary-by-state/connecticut/)
[DE](/fraud-summary-by-state/delaware/)
[FL](/fraud-summary-by-state/florida/)
[HI](/fraud-summary-by-state/hawaii/)
[ID](/fraud-summary-by-state/idaho/)
[IL](/fraud-summary-by-state/illinois/)
[IN](/fraud-summary-by-state/indiana/)
[IA](/fraud-summary-by-state/iowa/)
[KS](/fraud-summary-by-state/kansas/)
[KY](/fraud-summary-by-state/kentucky/)
[LA](/fraud-summary-by-state/louisiana/)
[ME](/fraud-summary-by-state/maine/)
[MD](/fraud-summary-by-state/maryland/)
[MA](/fraud-summary-by-state/massachusetts/)
[MN](/fraud-summary-by-state/minnesota/)
[MS](/fraud-summary-by-state/mississippi/)
[MO](/fraud-summary-by-state/missouri/)
[MT](/fraud-summary-by-state/montana/)
[NE](/fraud-summary-by-state/nebraska/)
[NH](/fraud-summary-by-state/new-hampshire/)
[NJ](/fraud-summary-by-state/new-jersey/)
[NM](/fraud-summary-by-state/new-mexico/)
[NY](/fraud-summary-by-state/new-york/)
[NC](/fraud-summary-by-state/north-carolina/)
[ND](/fraud-summary-by-state/north-dakota/)
[OH](/fraud-summary-by-state/ohio/)
[OK](/fraud-summary-by-state/oklahoma/)
[OR](/fraud-summary-by-state/oregon/)
[RI](/fraud-summary-by-state/rhode-island/)
[SC](/fraud-summary-by-state/south-carolina/)
[SD](/fraud-summary-by-state/south-dakota/)
[TN](/fraud-summary-by-state/tennessee/)
[TX](/fraud-summary-by-state/texas/)
[UT](/fraud-summary-by-state/utah/)
[VT](/fraud-summary-by-state/vermont/)
[VA](/fraud-summary-by-state/virginia/)
[WA](/fraud-summary-by-state/washington/)
[WV](/fraud-summary-by-state/west-virginia/)
[WY](/fraud-summary-by-state/wyoming/)
* Reorganized site structure and navigation in the footer (main header navigation will be redone soon to match)
* [Seth Keshel's Reports](/seth-keshel-reports/):
   - Now listing upcoming events for Seth on the main page
   - [Montana](/seth-keshel-reports/montana/) - new upcoming event
* [Arizona](/fraud-summary-by-state/arizona/) - updated canvass report link to updated version
* [Election Fraud Documentaries](/in-detail/2020-election-fraud-documentaries/) - added new trailer and link to Reawakening Docuseries
* [Telegram Channels](/telegram-channels/) - new channels for North Carolina
* Our [NYT Time-Series Data Explorer](/data/explorer/) is now linked in footer of site

## 9 Sep 2021

* [Arizona](/fraud-summary-by-state/arizona/) - updated article to include details about the recently released Voter Canvassing Report, by Liz Harris.

## 8 Sep 2021

* [Seth Keshel's Reports](/seth-keshel-reports/):
	- [Wisconsin](/seth-keshel-reports/wisconsin/) - new video analysis for Rock County, from Seth's Telegram
	- [Texas](/seth-keshel-reports/texas/) - new video analysis for Wichita County, from Seth's Telegram 
* [The Fall of the Bellwether Counties](/in-detail/bellwether-states-counties-2020/) - wording improvements and corrections
* Our [NYT Time-Series Data Explorer](/data/explorer/) now has three CSV export options: raw, enhanced, and precinct level.

## 6 Sep 2021

* [Seth Keshel's Reports](/seth-keshel-reports/):
  - [Kansas](/seth-keshel-reports/kansas/) - new videos analyzing Johnson County and Sedgwick County
* New section [Down Ballot Audit Force Guide](/audit-force/), helping people analyze the down ballot data for anomalies
* New links to Ashe in America's excellent articles on the situation in Colorado, added to [Dominion Voting Machines](/in-detail/dominion-voting-machines/) and [Colorado](/seth-keshel-reports/colorado/) page

## 5 Sep 2021

* [Pennsylvania](/fraud-summary-by-state/pennsylvania/) - numerous updates to highlight the status of the audits in the state
* [Wisconsin](/fraud-summary-by-state/wisconsin/) - added mention of new video by Representative Timothy Ramthun where he explains why there's 3 separate audits going on in the state
* [Dominion Voting Machines](/in-detail/dominion-voting-machines/) - new details about what's included in Dominion's legal contracts with counties
* Several updates to [Seth Keshel's Reports](/seth-keshel-reports/):
  - [Colorado](/seth-keshel-reports/colorado/) - new chart showing mail-in ballot statistics
  - [Illinois](/seth-keshel-reports/illinois/) - showing Dominion Voting Systems contract
  - [North Carolina](/seth-keshel-reports/north-carolina/) - showing upcoming events for Seth
  - [Texas](/seth-keshel-reports/texas/) - a new video on "Why Are Audits So Important In Texas?" as well as some upcoming events/presentations
  - [Utah](/seth-keshel-reports/utah/) - new chart showing historical growth
  - [Virginia](/seth-keshel-reports/virginia/) - new upcoming event
  - [Washington](/seth-keshel-reports/washington/) - a few updates on happenings in the state
* [Data Explorer](/data/explorer/) - added mention of USEIP's Election Data Analyzer Tool to bottom of the page
* There's a new [Telegram Channels](/telegram-channels/) page that lists all channels relating to election integrity and audits.

## 3 Sep 2021

* [Arizona](/fraud-summary-by-state/arizona/) - small wording tweaks and improvements
* [The Fall of the Bellwether Counties](/in-detail/bellwether-states-counties-2020/) - wording improvements and corrections
* Released a new tool: [NYT Data Explorer](/data/explorer/) which allows you to download the raw New York Times data feeds, as well as see some key stats from each state (and eventually each county)

## 1 Sep 2021

* Several updates to [Seth Keshel's Reports](/seth-keshel-reports/):
	- [Florida](/seth-keshel-reports/florida/) - minor mention that Lake County is showing some movement towards an audit of the election ([source](https://t.me/ElectionHQ2024/1162))
	- [Massachusetts](/seth-keshel-reports/massachusetts/) - improved article wording, and added note about Caroline Colarusso's Investigations
	- [Missouri](/seth-keshel-reports/missouri/) - minor mention that St Charles County is showing some movement towards an audit of the election ([source](https://t.me/ElectionHQ2024/1162))
	- [Pennsylvania](/seth-keshel-reports/pennsylvania/) - new video posted analyzing Berks County
	- [South Carolina](/seth-keshel-reports/south-carolina/) - new video recording of Seth's Presentation in Beaufort, South Carolina
	- [Utah](/seth-keshel-reports/utah/) - minor formatting improvements, and mention of Seth's upcoming presentation in Farmington, Utah on Sept 2, 2021. See [his Telegram post](https://t.me/ElectionHQ2024/1160).

## 30 Aug 2021

* A new article: [The Fall of the Bellwether Counties](/in-detail/bellwether-states-counties-2020/) which looks at some key anomalies around how the bellwether counties voted in 2020
* Numerous updates to [Seth Keshel's Reports](/seth-keshel-reports/):
	- Added map and image of his Top 100 worst counties
	- Added mention of his recent video interview: [The Daily 302 Show 2A with Guest Captain Seth Keshel](https://rumble.com/vlukw3-the-daily-302-show-2a-with-guest-captain-seth-keshel.html)
	- [Alaska](/seth-keshel-reports/alaska/) - link to the new [video](https://rumble.com/vlukw3-the-daily-302-show-2a-with-guest-captain-seth-keshel.html), as mentioned above, which discusses Alaska
	- [Colorado](/seth-keshel-reports/colorado/) - added mention of upcoming event, see <https://t.me/ElectionHQ2024/1115>
	- [Kansas](/seth-keshel-reports/kansas/) - link to the new [video](https://rumble.com/vlukw3-the-daily-302-show-2a-with-guest-captain-seth-keshel.html), as mentioned above, which discusses Kansas
	- [Louisiana](/seth-keshel-reports/louisiana/) - uploaded and embedded recent Telegram video covering St. Tammany County
	- [New Mexico](/seth-keshel-reports/new-mexico/) - link to the new [video](https://rumble.com/vlukw3-the-daily-302-show-2a-with-guest-captain-seth-keshel.html), as mentioned above, which discusses New Mexico
	- [North Carolina](/seth-keshel-reports/north-carolina/) - mentioned upcoming event in High Point, North Carolina on Sept 17, 2021. [More info here](https://t.me/ElectionHQ2024/1100).
	- [Pennsylvania](/seth-keshel-reports/pennsylvania/) - uploaded and embedded recent Telegram video covering Westmoreland County
	- [Wisconsin](/seth-keshel-reports/wisconsin/) - uploaded and embedded recent Telegram video covering St. Croix County

## 24 Aug 2021

* [Seth Keshel Reports](/seth-keshel-reports/):
	* [Colorado](/seth-keshel-reports/colorado/) - added additional powerpoint presentation by USEIP, and links to their blog which contain more detailed reports on the battle between Secretary of State and Mesa County Clerk Tina Peters.

	  Also added mention of:  Retired US Air Force Colonel Shawn Smith presented scathing election integrity concerns to the Secretary of State on August 3. [Read the transcript here](/in-detail/shawn-smith-statement-colorado/). He also presented at [Mike Lindell's Cyber Symposium](/in-detail/cyber-symposium-mike-lindell/), along with Tina Peters, and also [demonstrated on video how insecure the voting machines are](https://rumble.com/vl7y6g-cyber-symposium-simulation-of-how-your-votes-can-be-flipped.html).

	* [Pennsylvania](/seth-keshel-reports/pennsylvania/) - new link to [AuditTheVotePA.com](https://www.auditthevotepa.com/) has further information, data, petitions and interviews
* New banner in the footer of every page: "Learn. Pray. Share. Mobilize." to help distill the ways that everybody can get involved in the integrity effort.
* [Pray for Election Integrity](/pray/) - added this new page with links to the [America First Audits Prayer Warrior Group](https://t.me/joinchat/ZUOl1QKriC04MDRh)
* New link to <a href="https://useip.org/">The US Election Integrity Plan</a> in the footer. Their site contains further news, analysis, videos and practical guides to getting involved.
* [Shawn Smith Testifies to Colorado Secretary of State](/in-detail/shawn-smith-statement-colorado/) - new page documenting the audio testimony given on Aug 3

## 23 Aug 2021

* [Arizona](/fraud-summary-by-state/arizona/) -- added note about eyewitness account of possible voter fraud operation happening between Arizona and Georgia, [see article](https://creativedestructionmedia.com/investigations/2021/07/21/breaking-evidence-to-soon-be-presented-to-citizens-grand-jury-of-interstate-conspiracy-to-manufacture-harvest-counterfeit-ballots-for-use-in-2020-election/)
* [Georgia](/fraud-summary-by-state/georgia/) -- as above, added note about eyewitness account of possible voter fraud operation happening between Arizona and Georgia, [see article](https://creativedestructionmedia.com/investigations/2021/07/21/breaking-evidence-to-soon-be-presented-to-citizens-grand-jury-of-interstate-conspiracy-to-manufacture-harvest-counterfeit-ballots-for-use-in-2020-election/)
* [Seth Keshel Reports](/seth-keshel-reports/):
	* [Georgia](/seth-keshel-reports/georgia/) - added latest analysis by Seth: a detailed Georgia Voter Roll Analysis report, as well as links to the raw data for anyone interested in looking further
	* [Florida](/seth-keshel-reports/florida/) - added latest analysis by Seth: a deep dive video on Tampa / Hillsborough County
	* [Texas](/seth-keshel-reports/texas/) - some cleanup, reformatting and additional explanations of charts, plus a link to further stat analysis (draft version)
	* [New Hampshire](/seth-keshel-reports/new-hampshire/) - reformatted report for easier reading
	* [Arizona](/seth-keshel-reports/arizona/) - added a link to our further statistical analysis (draft version)
	* [Colorado](/seth-keshel-reports/colorado/) - added a link to our further statistical analysis (draft version)
	* [New Mexico](/seth-keshel-reports/new-mexico/) - added a link to our further statistical analysis (draft version)
	* [North Carolina](/seth-keshel-reports/north-carolina/) - added a link to our further statistical analysis (draft version)
	* [Pennsylvania](/seth-keshel-reports/pennsylvania/) - added a link to our further statistical analysis (draft version)
	* [Washington](/seth-keshel-reports/washington/) - added a link to our further statistical analysis (draft version)
	* On some of the Seth Keshel report pages the Telegram videos were not able to be played. We've updated some of these with Rumble videos to make viewing easier.

## 20 Aug 2021

* [Dominion Voting Systems Page](/in-detail/dominion-voting-machines/) - new video and a couple of new points as raised by David Clements at the [Cyber Symposium](/in-detail/cyber-symposium-mike-lindell/):
	* According to Professor David K. Clements, legal contracts between counties and Dominion specify that county clerks must not cooperate with outside organizations, including auditors. They need to “Let them handle it”. Dominion, strangely, even offer to pay the legal fees in such scenarios.
	* Dominion repeatedly refuse in-depth audits based on the legal defense of "protecting their intellectual property" (IP). They've been accused of using this as a way of hiding their malfeasance.
* Updates to various state pages with latest news and analysis, as well as some wording improvements to help make the statistics more understandable to the average person:
	- [Alaska](/seth-keshel-reports/alaska/)
	- [Arizona](/seth-keshel-reports/arizona/)
	- [Colorado](/seth-keshel-reports/colorado/)
	- [Georgia](/seth-keshel-reports/georgia/)
	- [Kansas](/seth-keshel-reports/kansas/)
	- [New Hampshire](/seth-keshel-reports/new-hampshire/)
	- [Oregon](/seth-keshel-reports/oregon/)
	- [South Carolina](/seth-keshel-reports/south-carolina/)
	- [Texas](/seth-keshel-reports/texas/)
	- [Washington](/seth-keshel-reports/washington/)
* Seth Keshel - his powerpoint slides from the Symposium can be downloaded from [this Telegram link](https://t.me/ElectionHQ2024/984).


## 18 Aug 2021

* Added a bold summary box to the top of the 6 battleground state pages, which provide the key points "at a glance"
* Greatly expanded the information about forensic audits underway, see:
  * [Arizona > Election Audits](/fraud-summary-by-state/arizona/#election-audits)
  * [Michigan > Election Audits](/fraud-summary-by-state/michigan/#election-audits)
  * [Pennsylvania > Election Audits](/fraud-summary-by-state/pennsylvania/#election-audits)
  * [Wisconsin > Ballot Recounts](/fraud-summary-by-state/wisconsin/#ballot-recounts)
* New intro quote from Pat Colbeck on [Michigan](/fraud-summary-by-state/michigan/) page
* Added a link to the [US Election Atlas](https://uselectionatlas.org/BOTTOM/store_data.php), where election data can be purchased. It appears that this is what Seth is using for his analysis.
* Added a note to dozens of state pages that most state election and voter registration stats can be obtained from either the Secretary of State website or State Board of Elections.
* Found an additional investigative [report by RealClear Investigations](https://www.realclearinvestigations.com/articles/2021/05/26/how_zuckerberg_millions_paid_for_progressives_to_work_with_2020_vote_officials_nationwide_778300.html) which dives into the controversial Zuckerberg donations. Added this link into a few relevant places.


## 16 Aug 2021

#### Added

* Added [David K. Clements – A Vote Trafficking Parable](/in-detail/cyber-symposium/david-clements-vote-trafficking-parable/) - with notes from his talk at the Cyber Symposium
* Added [Initial Forensic Analysis of Mesa County Machine Images at Cyber Symposium ](/in-detail/cyber-symposium/codemonkeyz-technical-forensic-session/) - with notes from the forensic/technical talk

#### Updates

* Updated [Cyber Symposium Summary](/in-detail/cyber-symposium-mike-lindell/) with:
  - Links to video replays for all 3 days
  - Telegram/Twitter links for most of the presenters
  - A link to the [50min video](https://home.frankspeech.com/video/real-state-state-numbers-president-trumps-victory) where Mike Lindell goes through all 50 states presenting what he believes to be the "real" vote counts
  - Moved most of the notes from the forensic technical session onto [its own page](/in-detail/cyber-symposium/codemonkeyz-technical-forensic-session/), to prevent the "summary" page getting too long
  - Added a [link to the video](https://rumble.com/vl0pki-mike-lindells-cyber-symposium-day-2-live-continued.html) (starting from 2hr 55min mark) for the segment where state legislators each shared an update from their state
  - Several updates to the [Day 3 notes](/in-detail/cyber-symposium-mike-lindell/#day-3-summary), surrounding Mike Lindell's attack, the reasoning behind not being able to share the packet captures, and the mentioned Presidential Executive Orders.
  - Added a video showing the voting maching hack demonstration
  - New link to the notes and outline from David Clements' presentation
* Updates to Seth Keshel's reports
  - Added several new video links to the [main page](/seth-keshel-reports/)
  - Updated [Colorado](/seth-keshel-reports/colorado/), [North Carolina](/seth-keshel-reports/north-carolina/), [Pennsylvania](/seth-keshel-reports/pennsylvania/), [South Carolina](/seth-keshel-reports/south-carolina/), [Texas](/seth-keshel-reports/texas/) and [Washington](/seth-keshel-reports/washington/), with the latest posts, videos and updates from Seth.
* Other minor wording tweaks and corrections



## 13 Aug 2021

* Updated [Cyber Symposium Day 3](/in-detail/cyber-symposium-mike-lindell/#day-3-summary)
	* Added initial summary and clips
	* Later in the day, additional video clips were added of Joe Oltmann and David K. Clements' presentations
* Added raw data URL sources for [Kansas](/seth-keshel-reports/kansas/) and [Arizona](/seth-keshel-reports/arizona/)


## 12 Aug 2021

* New link to [ElectionEvidence.com](https://ElectionEvidence.com) in footer.
* Added prominent link to the Cyber Symposium in the header, at least while it's a current event
* Added many new points, quotes and links to the [Cyber Symposium Summary](/in-detail/cyber-symposium-mike-lindell/)
* Updates to [Dominion Voting Systems](/in-detail/dominion-voting-machines/) thanks to Joe Oltmann's reports about Eric Coomer. Highly concerning. Includes two new videos:
  - [ITALY STOLE U.S. ELECTION – HERE’S WHY THEY DID IT](https://rumble.com/vcz2lr-italy-stole-u.s.-election-heres-why-they-did-it.html) 
  - [Joe Oltmann EXPOSES Dominion Voting's Eric Coomer --- Ties to Antifa, Skinheads, and Election LIES](https://rumble.com/vfudkf-joe-oltmann-exposes-dominion-votings-eric-coomer-ties-to-antifa-skinheads-a.html)
* New documentary video (from a while back): [The Plot to Steal America](https://rumble.com/embed/v9gws7/?pub=m4ux1)
* Added a few new posts from Seth Keshel to his [Arizona](/seth-keshel-reports/arizona/) report
* Added a new post from Seth Keshel to his [Minnesota](/seth-keshel-reports/minnesota/) report




## 11 Aug 2021

* Added [Day 1 summary of Cyber Symposium](/in-detail/cyber-symposium-mike-lindell/) with highlights and quotes.
* Updated Seth Keshel's [Arizona](/seth-keshel-reports/arizona/) page with his latest analysis on Cochise county.
	* Includes a screenshot that reproduces his data
* Updated [documentaries page](/in-detail/2020-election-fraud-documentaries/) with new video from the Cyber Symposium

## 9 Aug 2021

* Update [documentaries page](/in-detail/2020-election-fraud-documentaries/). Add link to Patrick Bryne's "Deep Rig" ebook and mention [TheDeepRig.movie](https://thedeeprig.movie/) is now available for free
* Update Seth Keshel's [Pennsylvania](/seth-keshel-reports/pennsylvania/) page with voter registration rolls with analysis on *five counties* holding an estimated 92,000 extra Biden votes
* Update Seth Keshel's [South Carolina](/seth-keshel-reports/south-carolina/) page with **Lynz Piper-Loomis** pledge to investigate election fraud
* Update [Georgia](/fraud-summary-by-state/georgia/)
	* New statement from a staff worker at State Farm Arena
	* New links and references
* Update [Michigan](/fraud-summary-by-state/michigan/) with a new section on alleged interference from the Zuckerberg's
* Update [Pennsylvania](/fraud-summary-by-state/pennsylvania/) 
	* New section on alleged interference from the Zuckerberg's
	* New paragraph highlighting blatant censorship from Twitter
* Update [Wisconsin](/fraud-summary-by-state/wisconsin/) 
	* Mention voter roll cleanup.
	* New section on alleged interference from the Zuckerberg's
* Update [capital riots](/in-detail/capitol-riots/)
	* Link to detailed list of 540 people arrested and charged in relation to the January 6 riots
	* Link to The Patriot Freedom Project
	* Link to Sidney Powell's offer of support
* Update footer with excellent [American Voters Alliance](https://americanvotersalliance.org/2020-election/) site

## 8 Aug 2021

* Update Seth Keshel's [Michigan](/seth-keshel-reports/michigan/) page with his detailed PDF report
* Updates to nearly all Seth Keshel's states with revised numbers and plots
	* Update the [Seth Keshel's](/seth-keshel-reports/) index page with some of his quotes
* Add a new **Other Reports** section to highlight 3rd party reports for the following states:
	* [Arizona](/seth-keshel-reports/arizona/)
	* [California](/seth-keshel-reports/california/)
	* [Colorado](/seth-keshel-reports/colorado/)
	* [Connecticut](/seth-keshel-reports/connecticut/)
	* [Florida](/seth-keshel-reports/florida/)
	* [Georgia](/seth-keshel-reports/georgia/)
	* [Hawaii](/seth-keshel-reports/hawaii/)
	* [Illinois](/seth-keshel-reports/illinois/)
	* [Maine](/seth-keshel-reports/maine/)
	* [Maryland](/seth-keshel-reports/maryland/)
	* [Massachusetts](/seth-keshel-reports/massachusetts/)
	* [Michigan](/seth-keshel-reports/michigan/)
	* [Minnesota](/seth-keshel-reports/minnesota/)
	* [Missouri](/seth-keshel-reports/missouri/)
	* [New Jersey](/seth-keshel-reports/new-jersey/)
	* [New York](/seth-keshel-reports/new-york/)
	* [Ohio](/seth-keshel-reports/ohio/)
	* [Pennsylvania](/seth-keshel-reports/pennsylvania/)
	* [Texas](/seth-keshel-reports/texas/)
	* [Virginia](/seth-keshel-reports/virginia/)
	* [Washington](/seth-keshel-reports/washington/)
	* [Wisconsin](/seth-keshel-reports/wisconsin/)
* Update [Nevada](/fraud-summary-by-state/nevada/) with new "Vote Count Irregularities" section

## 6 Aug 2021

* Update [Arizona](/fraud-summary-by-state/arizona/) with new links and references
* Update [Georgia](/fraud-summary-by-state/georgia/) with new "VoterGA Ballot Audit" section
