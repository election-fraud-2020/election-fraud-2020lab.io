---
title: "Case Study: Florida Voter Registration Trends"
meta_desc: Examining the trends in voter registrations in Brevard County, Florida.
parent_page: /trend-analysis/historical-county-analysis-registration-data/
last_updated: 7 Oct 2021
draft_status: true  # link is hidden from main trend analysis page
# draft_until_date: '2021-09-29'
state: Florida
comment_intro: Have any thoughts or feedback on the data presented above? Let us know in the comments below.
---

<div class="tip emoji" markdown="1">
📃 This case study is based on the method we described in *[How to Predict Election Results Using Registration Data](/trend-analysis/historical-county-analysis-registration-data/)*.
</div>

## Brevard County, Florida

Brevard is a fairly large Republican county with 361,653 total votes in 2020.

{% include large_image url="/images/trend-analysis/Brevard.png" maxWidth="1900" %}

**Observations:**

- PGap (the margin of Republican registrations over Democrats) has been consistently increasing since 2008, which should translate into a larger lead in Republican votes; but strangely, it doesn't.
- T/DR (Democrat turnout rate): Makes a large jump from 0.91 to set a new record in 2020 at 1.07.
- DVI: We see a Democrat vote increase of 28,870 in 2020, despite previous elections being negative. ⚠️

**Analysis:**

If we were to use a more realistic T/DR turnout ratio of 0.89 (which is in line with the historical trend), we can estimate the Democrat excess votes.

Estimate Democrat Votes (EDV): | DR * T/DR | = | 138,527 * 0.89    | = | 123,289
Excess Democrat Votes:         | DV - EDV  | = | 148,549 - 123,289 | = | **24,960**

Again, as we noted above, this number is **conservative** as it does not account for the unusual and excessive new Democrat and "Other" registrations in 2020.

<div class="info emoji" markdown="1">
🧐
We have seen a troubling trend across many counties where the increase in "Other" registrants (ORI column) seem to heavily favor the Democrat party. Are the voter rolls being padded or "stuffed" with fake unaffiliated voters which can be used to legitimize fraudulent Democrat votes? 

Only with thorough audits and canvassing will we obtain conclusive answers.
</div>

----

For further similar case studies, see: {% include other_sibling_pages %}

----

{% include article_series from="/trend-analysis/" %}
