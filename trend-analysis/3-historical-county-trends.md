---
title: When Winning Margins Go “Off the Charts”
meta_desc: Learn how to "normalize" a county's winning margin to identify abnormalities. What does this reveal about the 2020 election?
parent_page: /trend-analysis/
last_updated: 20 Sep 2021
# draft_status: true
# draft_until_date: '2021-09-18'
comment_intro: Did you find this article helpful? Did you dig in and review the data for your county or state? What did you find? Let us know in the comments below.
---

<div class="tip emoji" markdown="1">
📃 This article is part of a series on [Identifying Electoral Fraud Using Trend Analysis](/trend-analysis/). 
</div>

The simplest (yet possibly the most important) trends to start with were covered in our previous articles:

- [The Fall of the Bellwether Counties](/in-detail/bellwether-states-counties-2020/) 

- [The Battle of the Largest Counties](/in-detail/battle-for-largest-counties/)

- [The Curious Case of the 2020 “Voting Rate” Blowouts](/trend-analysis/historical-voting-rate-trends/)

The articles mentioned above will make the following article easier to comprehend.

{% include toc %}


## Historical County General Election Trends

We will first look at the historical county General Election trends to identify potential issues, and then augment the analysis with "registration" data to derive a formula for calculating excess votes, in the next article.

This analysis is fairly simple and expands on the *[The Fall of the Bellwether Counties](/in-detail/bellwether-states-counties-2020/)* concepts; to turn the historical election results for any county, into a highly accurate predictor of election outcomes.

If we look more closely at what makes a "bellwether county" special, it is simply the fact that it consistently votes for the winning candidate at every election. The key is that it will vote for either party depending on the merits of each election. This means that after several elections we can derive two ranges of "winning percentages". One for each party.

The results for a bellwether county (in this case Warren County, IL) will look like this: 

{% include large_image url="/images/trend-analysis/WarrenCountyFinal.png" maxWidth="1300" %}

The numbers surrounding the rectangles, represent the year of the election.

The X-Axis represents that "percentage vote" for the winning party at each election. 

The results for each election are shown on the left hand side of the image.

Although it's rarer, when a county has a strong 3rd party presence, it's possible for a party to win that county with less than 50% of the votes. (As is the case for Warren County, IL, in 1992 and 1996).

Here is an animated example showing how the ranges are updated after each election:

{% include large_image url="/images/trend-analysis/WarrenCounty.gif" maxWidth="1300" %}

{:.tip.emoji}
💡 **TIP:**  
A picture of the 10 best bellwether's "winning ranges" can be found [here](/trend-analysis/bellwether-winning-ranges/).

If we were to take this concept to the extremes we would obtain the following scenarios. (Note, the size of the ranges are exaggerated to illustrate a point):

- A county that only voted Democrat:

  ![](/images/trend-analysis/DemocratStronghold.png)

- A county that only voted Republican:

  ![](/images/trend-analysis/RepublicanStronghold.png)

Needless to say that there are a multitude of different permutations, one of which could look like this:

![](/images/trend-analysis/TypicalCounty.png)

{:.tip.emoji}
💡 **TIP:**  
**This "Winning Range" is an important concept that gives us a lot of valuable information.**

For example, if a county always votes Republican, it's typical to see it vote less for the Republican candidate when a Democrat candidate wins the general election. And vice-versa.

So even though not all counties are technically "bellwether" counties, by carefully looking at their **winning ranges** and understanding where an election result is located within that range, we can determine if the outcome of an election makes sense or not.

In order to facilitate the analysis and compare counties with one another we will distill all this information into a single number. This number tells us where in the range of previous results did this specific election fall --- did the party win a county by an average margin, a lower-than-average margin, or did they set a new winning margin record, and by how much? We'll call it the "Location". (The choice of this name will become clearer shortly.)

If a party wins a county by a margin that ends up in the middle of the range, the "Location" value will be close to 50%. If they only win by a small margin, that is closer to the "low" end of the range, the "Location" value will be closer to 0%. If the winning margin is lower than the "low" end of the range (i.e. a new "low" end record is set), the "Location" value will be negative. If they win with a large margin, the "Location" value will be closer to 100% or even greater than 100% if they set a new winning record. The values above 100% are the ones we'll be particularly interested in, as they reveal which counties set new winning records, and by how much.

The different possible values are illustrated below:

![](/images/trend-analysis/LocationValues.png)


{% capture insideBox %}
This is how the "Location" number is calculated.

- For each county we look at the winning party percentage for each election since 1988.

- As we process each election, we build a range for the Democrat party wins, and a separate range for the Republican party wins.

- A range is made up of a "Highest Winning Percentage" and "Lowest Winning Percentage" value.

- We are only interested in the winning party percentages. (Irrespective of whether it agrees with the party that won the general election).

- If the Democrat party won, we will update the "High" or "Low" value for the Democrat range if necessary. (We do the same for the Republican range if the county voted for the Republican party.)

- After several elections, each county will have a "winning range" for each party. These ranges give us a guide for what to expect in future elections.

- The "Low" range value will tell us the lowest percentage value a party needed, in order to win a county.

Based on the balance of probabilities, we can assume that for any "typical" election, the winning party will be "located" close to the **middle** of the corresponding party range. (i.e. it should have a "Location" value that is close to 50%).

The "span" of the range (i.e. High - Low) also gives us an indication of the "volatility" of the voting patterns of the electorate. Smaller spans typically represent more predictable, stable, voting patterns.

Now that we have defined all the important terms the formula for calculating the "Location", will depend on a few different scenarios:

- If the winning percentage (WP) is located within the range: Location = (WP - Low) / Span. (i.e. the value will be between 0% and 100%. It gives us a sense of whether the the current WP is closer to the Low end of the range (0%), or closer to the High end of the range (100%), and is therefore an indicator of the popularity of a given candidate.)

- If WP is greater than the High value: Location = 1 + (WP - High) / Span. (i.e the value will always be greater than 100%)

- If WP is less than the Low value: Location = (WP - Low) / Span. (i.e. the value will always be negative.)

In a sense, the Location value is "normalized" by the span and therefore makes it more meaningful to compare between counties.

There are still a few more steps to consider. Once the "Location" value has been calculated:

- If the Location value is greater than 100% we will update the "High" value of the range to be equal to WP. 

- If the Location value is negative, we will update the Low value of the range to be equal to WP.

There are two corner cases we need to address: when a county votes for a party only once or twice within our dataset.

1. When the county votes for a party for the first time, we set the "Location" value to be equal to 0.999 (or 99.9%)

2. On the second occasion we still don't have a proper range, we only have a single "point", so:

   - If WP is greater than the previous point value: Location = 1 + (WP + point) / point (i.e. the Location will be greater than 100%)

   - If WP is less than the previous point value: Location = 1 + (WP - point) / point (i.e the Location will be negative)

{% endcapture %}

{% include expandable_box
    title="Learn how we calculate the \"Location\""
    content=insideBox %}

Each county's range is updated (if required) *after* calculating the location value for a given election. The new range will then be used to calculate the location value at the next election.


## Case Study: Arkansas

Let's now look at the results for all the counties in Arkansas for all the elections since 2000. Arkansas is one of the ***cleanest*** states in the 2020 election, so it will give us a "baseline" against which to measure other states against. (It isn't perfect, as we will soon see, but it is close.)

Concentrate on the `Loc.20` column (the "Location" value for 2020 election), and pay attention to the trends.

In the following tables, you'll see these columns. Here's what they mean:

{:.small}
TV20     |   Total Votes in 2020
Gap     |   Is the gap between the Winning Party Percentage - Loosing Party Percentage. This is always positive and is always relative to the winning party it is associated with, shown in column "P".
P     |   Winning party (either "R" or "D" for "Republican" or "Democrat")
Mov     |   Movement (or change) in winning percentage from previous election. i.e. Current Gap - Previous Gap
Loc     |   Location of the current winning percentage (as we explained above)
04, 08, 12, ...     |   True or False. Indicates whether the winning party in that county was also the winning party for the overall election. Did the county vote for the winner?

The counties are sorted from largest to smallest total votes (i.e. the most populated counties are at the top).

As you look at the data below, think critically, and ask yourself these questions:

- What do you notice about the Republican counties Loc.20 values?
- What do you notice about the Democrat counties Loc.20 values?
- Are there any counties that are bucking the trend? Which ones?

Try and answer these questions for yourself before looking at the answers (which are given in the "Observations" section below). 

(Two screenshots were needed to capture all the counties. Click an image to enlarge it.)

{% include large_image url="/images/trend-analysis/ArkansasCounties1.png" maxWidth="1550" %}
{% include large_image url="/images/trend-analysis/ArkansasCounties2.png" maxWidth="1550" %}


#### Observations

- Most counties voted very strongly Republican, setting a new record margin, with a Loc.20 value over 100%.
- The 3 most populated counties buck the trend.
- The Loc.20 value for Pulaski is much higher than all the other Democrat county values.
- The Loc.20 value for Benton and Washington is much lower than all the other Republican county values.
- All the other counties strongly voted Republican in 2020 with a Loc.20 value exceeding 100% across the board. This means they set individual new record highs for the Republican winning range.

Here are the same images as before, but this time with a new "Movement" column. This represents the change in "Gap" value from the previous election to the current election. This column helps quantify the shift in sentiment between elections and is simply added as a reference to help people understand the usefulness of the "Location" column to quickly identify **anomalies**.

{% include large_image url="/images/trend-analysis/ArkansasCounties1WithMovement.png" maxWidth="2100" %}
{% include large_image url="/images/trend-analysis/ArkansasCounties2WithMovement.png" maxWidth="2100" %}


## Case Study: Counties with Over 200K Votes

*We recommend reading [The Battle of the Largest Counties](/in-detail/battle-for-largest-counties/) article first, if you haven't already. It will provide the necessary background to assist you in finding meaningful trends.*

So now let's look at the counties with a total vote greater than 200K, sorted by descending order of their Loc.20 value (from highest to lowest). These are the large counties that set huge **new winning margin** records in the 2020 election.

Take some time to look at the data. Try and identify some trends on your own, before reading the observations.

{% include large_image url="/images/trend-analysis/HighestLoc20_200K_1.png" maxWidth="1550" %}
{% include large_image url="/images/trend-analysis/HighestLoc20_200K_2.png" maxWidth="1550" %}


So what did you notice? 

Here is what we observed:

- There is no trickery in the tables above. There really are **no Republican counties** with over 200K votes that have a Loc.20 value over 100%. (In other words, no Republican county with over 200K votes, increased their percentage lead from 2016 to 2020.)

- The fact that all these counties are uniformly Democrat is completely abnormal (especially when considering that the Republican party increased their votes by a record 10+ million over the previous election).

- Why did all the **larger** Democrat counties perform so well at the 2020 election and all the *smaller* Democrat counties perform so poorly? (From a trend analysis point of view, this is one of the basic yet most compelling "tell-tale" signs that something does not add up.)

- Counties with a Loc.20 value over 120% warrant further investigation. Expanding a range by 20% is suspicious, especially for counties with over 200K votes. (It just so happens that the first image only contains counties with a Loc.20 value greater than 120% in 2020.)

- In the second image, we highlighted counties that voted Democrat for the first time in 2020. Notice their total votes in 2020 (the TV20 column). Do you notice a trend?

- Notice how many counties used to vote Republican in 2000, and have gradually turned blue by 2020. (Is this part of a master plan to gradually turn all the largest counties blue?)

- Do all these trends make sense to you?

<div class="hint" markdown="1">
[Here is the list](/trend-analysis/most-populated-counties/) of all the most populated counties sorted by their total votes in 2020. You will notice the Republican counties are sparse, and their LOC.20 value is always less than 100%. We find this abnormal considering Trump had an extra 10+ million people vote for him in 2020. The highest of any incumbent President, ever.

What this means is that no Republican counties with over 200K votes increased their *percentage lead* from 2016 to 2020. It does not mean the total Republican votes did not increase. **All of them did!** It just means the total Democrat votes, in each of the large counties, increased **more** than the Republican votes (even within Republican strongholds!).

What makes this all the more confounding is that this trend only applies to populated counties. In smaller Democrat stronghold counties, with stable population growth, we observe large swings towards the Republican party.

Something does not add up.
</div>

We will leave you with a quote from Seth Keshel to ponder...

> How is California turning every state in the west more blue, blue, or purple, while doubling it's own blue votes since 2000?
>
> Let the one with wisdom understand.
>
> <small>--- [Seth Keshel](https://t.me/ElectionHQ2024/1369)</small>

## Summary

Hopefully you've learnt a few new tools for analyzing election results by comparing the results with earlier years, and calculating a number that represents how the result compared to previous years.

This "Historical County Range Analysis" can provide compelling information about the historical trend for an individual county, as well as a state wide trend for how the counties voted for a particular party.

You should have already noticed a few intriguing statistics that deserve a closer look. In the following article we'll dig deeper and incorporate voter registration data, which will allow us to derive a formula for estimating excess votes.

----

{% include article_series from="/trend-analysis/" %}
