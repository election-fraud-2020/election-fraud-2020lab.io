---
title: List of Large, Anomalous Democrat Vote Increases in 2020
meta_desc: 
parent_page: /trend-analysis/large-democrat-vote-increases/
last_updated: 08 Nov 2021
wide_layout: false
---

From our article on *[Investigating the Large Democrat Vote Increases](/trend-analysis/large-democrat-vote-increases/)*, here is the full list of counties where, in 2020, the increase in Democrat votes over the previous 2 elections was very high --- where the DVI#20 values are greater than 50%.

Counties are grouped by state and sorted from largest to smallest (in number of votes, TV20).


{% include large_image url="/images/trend-analysis/trends/DVITrend1.png" maxWidth="900" %}

{% include large_image url="/images/trend-analysis/trends/DVITrend2.png" maxWidth="900" %}

{% include large_image url="/images/trend-analysis/trends/DVITrend3.png" maxWidth="900" %}

{% include large_image url="/images/trend-analysis/trends/DVITrend4.png" maxWidth="900" %}

{% include large_image url="/images/trend-analysis/trends/DVITrend5.png" maxWidth="900" %}

{% include large_image url="/images/trend-analysis/trends/DVITrend6.png" maxWidth="900" %}

{% include large_image url="/images/trend-analysis/trends/DVITrend7.png" maxWidth="900" %}

{% include large_image url="/images/trend-analysis/trends/DVITrend8.png" maxWidth="900" %}

{% include large_image url="/images/trend-analysis/trends/DVITrend9.png" maxWidth="900" %}

{% include large_image url="/images/trend-analysis/trends/DVITrend10.png" maxWidth="900" %}

{% include large_image url="/images/trend-analysis/trends/DVITrend11.png" maxWidth="900" %}

{% include large_image url="/images/trend-analysis/trends/DVITrend12.png" maxWidth="900" %}

{% include large_image url="/images/trend-analysis/trends/DVITrend13.png" maxWidth="900" %}

{% include large_image url="/images/trend-analysis/trends/DVITrend14.png" maxWidth="900" %}

{% include large_image url="/images/trend-analysis/trends/DVITrend15.png" maxWidth="900" %}

{% include large_image url="/images/trend-analysis/trends/DVITrend16.png" maxWidth="900" %}

{% include large_image url="/images/trend-analysis/trends/DVITrend17.png" maxWidth="900" %}


----

{% include article_series from="/trend-analysis/" %}

