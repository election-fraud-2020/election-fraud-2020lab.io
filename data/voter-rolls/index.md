---
title: Voter Rolls
breadcrumb: "[Follow the Data](#colophon) ›"
last_updated: 10 Nov 2021
articles:
  - /data/voter-rolls/national-index/
  - /data/voter-rolls/where-to-obtain/
  - /data/voter-rolls/standardized-format/
  - /data/voter-rolls/validating-the-data/
  - /data/voter-rolls/finding-anomalies-fraud/
---

Exploring voter rolls --- otherwise known as voter registration databases or a qualified voter file (QVF) --- is an important step in auditing the integrity of an election. 

It's been theorized, and in several states [confirmed](/canvassing/), that voter roll "stuffing" has occurred whereby fake or "phantom" voters have been inserted into the state and county records which can be used to submit fraudulent ballots without obvious detection.

The rolls of all US states need careful inspection and analysis. The following pages are a brief attempt at getting you started on this process.

----

{% include article_series title="Articles In This Series" from="/data/voter-rolls/" telegram=false %}
