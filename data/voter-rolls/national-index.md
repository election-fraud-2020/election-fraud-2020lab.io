---
title: US National Voter Roll Index
meta_desc: An index of how to access voter rolls and registration data for all states in the US.
parent_page: /data/voter-rolls/
last_updated: 10 Nov 2021
# draft_status: true
# wide_layout: true
---

A channel is available on Telegram for crowdsourcing the best information on voter rolls across each state of the US. Visit the [US National Voter Roll Index](https://t.me/NationalVoterRollIndex) for more.

<iframe id="preview" style="border:0px;height:500px;width:500px;margin:5px;box-shadow: 0 0 16px 3px rgba(0,0,0,.2);" src="https://xn--r1a.website/s/NationalVoterRollIndex"></iframe>

The crowdsourced comments under each state will not be visible from the preview above. You will need to view the channel from within Telegram to do so.

----

{% include article_series from="/data/voter-rolls/" telegram=false %}
