---
title: Finding Anomalies & Fraud
parent_page: /data/voter-rolls/
last_updated: 10 Nov 2021
draft_status: true
# wide_layout: true
---

* Ineligible voters 
  - Deceased voters 
  - Non citizens
  - Incarcerated
  - Voters with lapsed records
  - Underage voters
  - Interstate voters
  - People who voted from a vacant lot

* Registration date patterns
  - By day of week
  - Fluctuations over time
  - Or LACK of fluctuations over time 

  <!-- Other things:

  - Ballots received BEFORE being sent out
  - Ballots received on same day as being sent
  - Ballot sent and received within 24-48 hours
  - People who voted and do not live in the state

  https://publicinterestlegal.org/pilf-files/Report-Critical_Condition-Web-FINAL-FINAL.pdf

  https://www.americanthinker.com/articles/2021/11/meet_the_technology_thats_uncovering_2020s_voter_fraud.html
  https://www.americanthinker.com/articles/2021/07/want_to_see_election_fraud_in_24_hours_send_us_your_data.html

  https://www.americanthinker.com/articles/2021/06/want_to_end_electoral_fraud_light_up_the_infrastructure.html
  https://www.americanthinker.com/articles/2021/05/needed_a_national_election_fraud_database.html
  https://www.americanthinker.com/articles/2021/04/election_fraud_hotspots__10_of_the_data_are_70_of_the_fraud.html
  https://www.americanthinker.com/articles/2021/03/the_sovereign_crime_of_industrial_scale_vote_fraud.html
  https://www.americanthinker.com/articles/2021/01/you_know_there_was_industrialscale_election_fraud_what_can_be_done.html
  https://www.americanthinker.com/articles/2021/01/industrialscale_election_fraud__did_it_happen.html
  https://www.americanthinker.com/articles/2020/12/who_will_be_the_fraud_deniers.html
  https://www.americanthinker.com/articles/2020/11/why_industrial_scale_fraud_is_just_so_hard_to_hide.html
  https://www.americanthinker.com/blog/2020/11/there_are_no_software_glitches.html




   -->

----

{% include article_series from="/data/voter-rolls/" telegram=false %}
