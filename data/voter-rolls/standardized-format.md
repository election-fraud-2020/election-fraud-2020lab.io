---
title: Standardized Voter Roll File Format
meta_desc: Exploring a standardized CSV format for voter roll data to assist with tools that can work across state lines.
parent_page: /data/voter-rolls/
last_updated: 10 Nov 2021
wide_layout: true
---

Many voter rolls come in two separate files or databases: 

1. A list of registered voters
2. A history of which elections those voters actually voted in

While it can be helpful to retain the two separate lists for some kinds of analysis, for most general auditing of the 2020 election, we find it helpful to combine those two documents into a single master document that can be analyzed more easily and shared between people with different tools. The result is a list of voter information with a column indicating whether they voted in the Nov 2020 election or not.

{:.warning}
Draft version. Seeking feedback as to whether this file format is appropriate and useful.

CSV is the preferred file format as it can be read easily by Excel, databases, scripts, tools, humans, and exchanged between these as needed. Fields should be separated by commas, and optionally enclosed in "quotes" when needed (such as when there is a comma or quote inside the column itself).

While each state and county office may use varying column names, if enough researchers adopting the following columns as standard, it will help ensure the greatest compatibility and interoperability with other tools.


<div class="responsive-table main-table small" markdown="1">

Field/Column Name | Example | Notes
-|-
State Name | Alabama | 
County Name | Tallapoosa | 
County Code |  | Only used in some states
Precinct Name | DUNCAN MEMORIAL COMMUNITY CENTER | 
Precinct Code | 0103 | 
Date of Snapshot | 2020-02-22 | yyyy-mm-dd format if possible <br> This refers to the date at which the record was current. It's common for voter registrations to be active on one date and later become inactive, so it's important to know at what date this refers to.
Voter ID | 12345678 | Format may vary by state
Date of Registration | 2020-01-28 | yyyy-mm-dd format if possible, otherwise mm/dd/yyyy as a fallback
Date of Birth | 1990-03-27 | yyyy-mm-dd format if possible, otherwise mm/dd/yyyy as a fallback
Year of Birth | 1990 |  
Full Name | STEVEN JAMES MATTHEW O\'NEAL JR. | All names & suffix combined. Ideally first name comes at the start and last name at the end, before the suffix. Although some states may present this differently.
Last Name | O\'NEAL | 
First Name | STEVEN | 
Middle Names | JAMES MATTHEW | 
Name Suffix | JR. | Common suffixes include: 'J', 'JNR', 'JR', 'SR', 'J.', 'JNR.', 'JR.', 'SR.', 'I', 'II', 'III', 'IV', 'V'
Residential Address Full || The complete address, all in one cell. It can optionally be split out into separate cells, see below, if this information is easily available.
Residential Address Line 1 || 
Residential Address Line 2 || 
Residential Address City || 
Residential Address State || Two letter code if within US
Residential Address Zip Code || 
Mailing Address Full || The complete address, all in one cell. It can optionally be split out into separate cells, see below, if this information is easily available. <br> Note: may be outside the USA
Mailing Address Line 1 || 
Mailing Address Line 2 || 
Mailing Address City || 
Mailing Address State || Two letter code if within US
Mailing Address Zip Code || 
Mailing Address Country || 
Party Affiliation || 
Voter Roll Status (as at snapshot date) | Assumed Active | Either "Active", "Inactive", or "Assumed Active" - this last option may be where a data file does not specify the status, but where the data appears to be a list of qualified voters who would be "active" and eligible to vote in an election at the Date of Snapshot.
Voted in 3-Nov-2020 Election | Yes | "Yes" or "No" or blank if unknown
Flagged Issues || Internal notes can go here when issues are found with a particular voter.
Data Source | PUBLIC NOTICE STATE OF ALABAMA TALLAPOOSA COUNTY SUPPLEMENTAL LIST OF TALLAPOOSA COUNTY QUALIFIED VOTERS, Alexander City Outlook, dated Feb. 22, 2020, printed March 03, 2020. | A text description of where this data was obtained. Was it from a website? Newspaper? Sent from County Clerk? <br> This should be clear enough so that someone looking at the data for the first time will know where to go to verify that this record is indeed correct. <br> All records within a specific batch will likely have the same value for all 4 of these fields.
Data Source Filename | 2-22-2020-Tallapoosa-Supplemental-List.pdf | Record the original filename of the source file here, so that if you need to trace it back you can refer here.
Date Source URL || Was the file downloaded? Or is it available via an online link currently? Record that here.
Data Source Contact || An email, phone or social media URL for who to contact about this data if anomalies are found.

</div>

Other columns may be added as needed. Some states will have a column for "Phone Number", "Drivers Licence ID", "Social Security Number (SSN)", etc., but these are not required as standard.

If a column is not known, it should be left blank. Do not fill it in with a dash, question mark or "Unknown".

#### Example File

```
State Name,County Name,County Code,Precinct Name,Precinct Code,Date of Snapshot,Voter ID,Date of Registration,Date of Birth,Year of Birth,Full Name,Last Name,First Name,Middle Names,Name Suffix,Residential Address Full,Residential Address Line 1,Residential Address Line 2,Residential Address City,Residential Address State,Residential Address Zip Code,Mailing Address Full,Mailing Address Line 1,Mailing Address Line 2,Mailing Address City,Mailing Address State,Mailing Address Zip Code,Mailing Address Country,Party Affiliation,Voter Roll Status (as at snapshot date),Voted in 3-Nov-2020 Election,Flagged Issues,Data Source,Data Source Filename,Date Source URL,Data Source Contact
ALABAMA,TALLAPOOSA,,DUNCAN MEMORIAL COMMUNITY CENTER,0103,2020-02-22,12345678,2020-01-28,1990-03-27,1990,"STEVEN JAMES MATTHEW O'NEAL JR.","O'NEAL",STEVEN,JAMES MATTHEW,JR.,,,,,,,,,,,,,,,Assumed Active,Yes,,"PUBLIC NOTICE STATE OF ALABAMA TALLAPOOSA COUNTY SUPPLEMENTAL LIST OF TALLAPOOSA COUNTY QUALIFIED VOTERS, Alexander City Outlook, dated Feb. 22, 2020, printed March 03, 2020.","2_22_2020-Tallapoosa_Supplemental_List.pdf",,
```

#### Useful Tools

These CSV files can be opened in MS Excel, Google Sheets, [CSViewer](https://csviewer.com/) (recommended for large files), and even text editors like Notepad.

We aim to expand the list of tools mentioned here soon.


<style>
    .main-table td:first-child {
        font-weight: bold;
        width: 30%;
    }
    .main-table td:nth-child(2) {
        font-family: Consolas, monospace;
        /* font-size: 105%; */
        width: 30%;
    }
    @media (max-width: 420px) {
        .main-table td:nth-child(2) {
            font-size: 80%;
        }
    }
    .main-table td:nth-child(3) {
        width: 40%;
        font-size: 90%;
    }
</style>

----

{% include article_series from="/data/voter-rolls/" telegram=false %}