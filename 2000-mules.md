---
title: 2000 Mules
subtitle: A documentary covering True the Vote’s video evidence of a coordinated, funded, illegal ballot trafficking network across critical swing states
meta_desc: A documentary covering True the Vote’s video evidence of a coordinated, funded, illegal ballot trafficking network across critical swing states including Arizona, Georgia, Pennsylvania, Michigan, Wisconsin, and Texas.
breadcrumb: "[Further Articles](#colophon) › [Election Fraud Documentaries](/in-detail/2020-election-fraud-documentaries/)  › "
last_updated: 4 May 2022
---

[![2000 Mules Poster](/images/2000-mules-poster.jpg)](https://2000mules.com)

{:.small.muted}
Official website: [2000mules.com](https://2000mules.com)


## Film Trailers

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vqzk5k/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vzh2um/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>


## Overview

{:.tip.emoji}
📰 Reports below provided by *Liberty Overwatch*.  
Follow [@LibertyOverwatchChannel](https://t.me/LibertyOverwatchChannel) and [@DineshJDSouza](https://t.me/DineshJDSouza) on Telegram for breaking updates, or sign up via email at [2000mules.com](https://2000mules.com).

*2000 Mules* documents research from [True the Vote](https://t.me/LibertyOverwatchChannel/5334), a conservative election intelligence organization that has compiled evidence of [organized ballot trafficking](https://t.me/LibertyOverwatchChannel/6253) in at least [6 states](https://t.me/LibertyOverwatchChannel/6309): Georgia, Arizona, Pennsylvania, Wisconsin, Michigan, and Texas. In Georgia for example, True the Vote alleges there were [242 traffickers](https://t.me/LibertyOverwatchChannel/6387) who made [5,662 trips](https://t.me/LibertyOverwatchChannel/6256) to ballot drop boxes between the early morning hours of 12AM and 5AM, potentially unloading hundreds of thousands of illegally harvested ballots over the course of several weeks. 

State | Mules Identified | Drop Boxes | Estimated Illegal Votes
-|-
Pennsylvania | 1,150 | 45 | 275,000 *
Michigan | 500 | 50 | 125,000 
Wisconsin | 100 | 28 | 14,000 
Georgia | 250 | 24 | 30,000 *
Arizona | 200 | 20 | 20,000 *
**TOTAL** | | | **380,000**

\* These states showed **more illegal votes from this strategy alone than Biden's winning margin**, based on True the Vote's high bar in defining a mule (at least 10 visits to drop boxes). If the bar is lowered to count those making at least 5 visits, the estimated affected votes increases by more than double. [^mules1]

[^mules1]: Nick Moseder: "[Is ‘2000 Mules’ An Absolute Game Changer?](https://nickmoseder.substack.com/p/is-2000-mules-an-absolute-game-changer)", May 3, 2022

Writer and Director Dinesh D’Souza, who had been relatively silent on election fraud until 2022, said he was swayed by the thoroughness of the data, the fact that it was **“enough to tip the election” and “systematic, comprehensive, and well-presented enough” to convince any “reasonable person.”**


## True the Vote's Investigation

In a Charlie Kirk [interview](https://podcasts.apple.com/us/podcast/explosive-election-fraud-news-previewing-2-000-mules/id1460600818?i=1000550148412), D’Souza gave background information about the operation. “True the Vote has done their work in all the key states, and they focused on the heavily Democrat areas of Fulton County, Georgia, Maricopa County, Arizona, the Detroit area of Michigan, the Milwaukee area of Wisconsin, and the greater Philadelphia area in Pennsylvania,” he explained.

Gathering both geo-tracking (cell phone) data and video surveillance, True the Vote captured ballot trafficking activity by 2,000 “mules” in 6 states. These paid vote couriers visited drop boxes late at night, used gloves to conceal their fingerprints, took photos to secure payment, and made multiple trips to multiple drop boxes over multiple weeks. D’Souza points out that *the traffickers began wearing gloves just days after a ballot harvesting case in Arizona secured indictments based on fingerprint evidence*.

“There’s no question in any of these states that we are witnessing illegal activity,” D’Souza said. “Even in areas where [vote harvesting] was temporarily allowed… in no area was it legal to be paid to deliver votes.”

Furthermore, “Many of these mules are coming from non-profit centers, which are 501(c)(3) organizations, which means they are prohibited from election activities. These are people that have a tax exemption from the IRS conditioned upon them not getting directly involved in electioneering… You’re seeing a coordinated ring of corruption and the evidence for it is decisive,” he added. Kirk later noted that a number of the non-profits were “closely aligned with Stacey Abrams.”

Despite the extensiveness of the evidence — True the Vote has 27 terabytes of data, including 4 million minutes of video and 10 trillion cell phone pings, as well as a [whistleblower’s](https://t.me/LibertyOverwatchChannel/6256) confession — the leftstream media has been dead silent.

“I think this is going to be huge and make a huge difference, but it begins with simply getting the truth out there so that what happened in 2020 becomes undeniable,” D’Souza said in closing. 

The film is scheduled to be released in late April 2022. 


## True the Vote Explains The Ballot Laundering 'Cartel'

True the Vote’s Catherine Engelbrecht & Gregg Philips also sat down with Charlie Kirk for an [illuminating hour long interview](https://podcasts.apple.com/us/podcast/the-charlie-kirk-show/id1460600818?i=1000556504385) detailing their ultra high-tech investigation into an “organized crime” ballot trafficking operation that swung the 2020 election for Joe Biden. 

The drop box stuffing activity followed a consistent pattern, Gregg explained. Every operation involved “a set of collectors, a collection point or stash house for all the ballots, the bundling of those ballots, and then the casting of those ballots by, what we were calling, ‘mules’ in the drop boxes.”

“And as we began put the pieces and parts together, it really did dawn on us: 'Well, this sounds like what's happening in Atlanta or in San Luis, Arizona’... **This was a conspiracy. This was organized crime**,” he emphasized.

Catherine describes the “fateful moment” when she turned to Gregg and asked, **“How do we take down a cartel?**”

**“That's when we began to use the terms like stash houses, and drop points, and mules, and trafficking, and voter abuse because that's what we're looking at,”** she continued. 

Focusing on six states, Arizona, Georgia, Pennsylvania, Michigan, Wisconsin, and Texas, True the Vote spent $2 million to buy publicly available cell phone data that can pinpoint an individual’s location to within a few inches. They then narrowed their search to targets who began visiting drop boxes and NGO offices during the early voting weeks leading up to November 3rd, activity that was contrary to their prior “pattern of life.” In Georgia, the threshold was at least two dozen trips to drop boxes and five visits to a non-profit. 

Catherine described their filtering logic, they wanted parameters that were “outside the norm, so aberrant that it would just stick out like a sore thumb in terms of a dataset… We wanted to have such clear margins, such clear lines that we finally settled on groups that were going [to the drop boxes] in Georgia an average of 23 times — so distinctive.  And, as Gregg points out, they also had to go to the NGOs — so they had to meet both those criteria a certain number of times for us to really drill down and study.”

Through open records requests, True the Vote acquired and drop box surveillance video (when available), which helped them confirm the trafficking activity. They also evaluated chain of custody documents to identify what a typical day looked like at a given drop box. Those records showed normal ballot drop data punctuated by “spikes” in ballot receipts. This information helped them home in on specific dates and times buried in their 4 million minutes of footage. 

“This is how money laundering works,” Charlie observed. “This is not just a one-off thing, this is not some Democrat activist that really wanted Trump gone and might have had a couple friends do this. This was a machine.”

“Which state was the worst offender?,” asked Charlie. “Pennsylvania,” Gregg stated unequivocally. “The worst in every way: 1,155 people met our criteria… in Philadelphia.”

“What's even more insane,” Catherine said, “is watching the data, watching the pings come across the bridge in New Jersey and into Philly.” The traffickers actually crossed state lines to participate in the fraud. And it wasn’t just in Philadelphia. Two mules in Arizona made their way to Georgia for the runoffs. And a bartender in South Carolina came in to “help out” in Atlanta, Gregg added.

Even more disturbing, the National Republican Senatorial Committee (NRSA) knew about the systematic ballot stuffing and did nothing. “We learned that there were off-duty law enforcement officers, paid for by the Republican Party, that reported all of this. And [the NRSA] just covered it up,” Gregg said.

> This was a machine… this is institutional evil.”
>
> <small>--- Charlie Kirk</small>

The ballot trafficking fraud begins with dirty voter rolls, Catherine explained. The cartel is feeding off of inflated voter lists, packed with invalid registrations. 

For example, in Georgia, the illegal Stacey Abrams-Mark Elias “consent decree pushed Raffensperger to send ballots to both active and inactive voters. The rolls hadn't been cleaned in two years. We now know that ineligible voter records contributed to 75,000 of the votes in the General and 45,000 votes in the run-off” in Georgia, Catherine said. 

“The way that the ballots were collected becomes this multi-tentacled hydra... There's all manner of ways that those ballots came in but the important take away is that dirty voter rolls allow for a big portion of this grift,” she underscored. 

“Who's running this?,” Charlie asked. 

“Our current hypothesis,” Gregg said, “is that there are new money folks, like the Stacey Abrams of the world, who all of a sudden show up in Maricopa Country after the election, arm in arm with the SEIU and others, [where they thank] her for delivering the state… The second piece of this is there are old money ties… to some foundations that started in Chicago back some 80 years ago, in the 1960s.”

“So you're saying that there's a foundation, a 501(c)(3), that was potentially funding some of this activity?,” Charlie asked. 

“Yes. Many,” Catherine and Gregg responded. 

Is there a typical mule profile?, Charlie wondered. 

Every county is different, Gregg replied. “We had some incidents at this place in Atlanta called the Bluff, one of the heaviest heroin trafficking places in the U.S., very dangerous, one of the top five most dangerous places in the U.S. We interviewed some people down there… That same night, we also went to 201 Washington Street, which is an advocacy center attached to a church… in downtown Atlanta.”

But “in Arizona, the profile looked a little different,” Catherine noted. “It’s been happening in Arizona for an awfully long time. What we see there are people who really control communities. And you have people at the top of the pyramid coming in and doing everything from building underprivileged housing to controlling the full vertical of the contractors, and the banks, and the financing organizations, and all of those people are participating in rounding up ballots.”

As the ‘2,000 Mules’ movie will soon show, “we have informants who have come forward to describe exactly what happens,” Catherine said. 

“And these collectors and these mules are making between $10 and $40 a ballot here in Arizona, according to the testimony we have,” Gregg added. 

In Yuma County, Arizona the cartel’s days may be numbered. “There were two ballot harvesting arrests in Yuma. I'm hearing from the grapevine that there might be more happening there,” Charlie said. 

“We think so too,” Gregg said. “We believe there will be more [arrests in Yuma]. One of the interesting things about Yuma County, and San Luis in particular, is some of that old money that we talked about earlier… flows into some of these poor border communities.” There, it’s “less about electing a president with these harvesting techniques and more about electing themselves so they can stay in control over all the billions that are flowing in,” Gregg explained. 

“We think that here in Arizona your AG and others are tuned in enough to what's going on down there that you're going to see some action,” he added. 

> If elections are not truly fair, we're not truly free.”
>
> <small>--- Catherine Engelbrecht</small>


## What Can Be Done?

What can we do to defeat the ballot laundering cartel? The solution has “four pillars,” Gregg explained. We must:

1. **Clean the voter rolls**. True the Vote has set up a website, [www.iv3.us](https://www.iv3.us), that helps citizens review their voter rolls and challenge inaccurate records;

2. **Stop mass mail-in voting** and **abolish the drop boxes**;

3. **Eliminate private funding** of elections;

4. **Impose punishments** that fit the crimes. “Put some people in jail,” Gregg said. “If you put somebody in jail for 10 years for this, you'll stop it.”

We may soon start to see Zuckerbucks audits that lead to arrests, according to Gregg. “You're going to start seeing more and more of what's happening in Jackson, Mississippi,” he said. “There's a state auditor in Jackson, his name is Chad White. And he went out and audited some of these officials who were doling out this Zuckerberg money, and there's been four arrests already in Jackson for stealing the money. I think you're going to see state auditors, and others all over the country, now start to say, ‘Where did all of this money go?’”

“Zuckerberg money was a huge catalyst,”he continued. “You take dirty voter rolls, you mail everyone an application whether they ask for it or not, whether they're even legitimately on the rolls or not, and then you provide a means to stuff them in there.”

But “don’t feel helpless,” Catherine encouraged. “We are not victims because victims don't have a choice. We have a choice, this is happening on our watch. So we can choose to remain complicit and to watch this and watch the [2,000 Mules] movie and go pop a bag of popcorn and sit back and say, ‘Wow this is just horrible,’ and the band plays on. Or we can say, ‘Not on our watch,’ and get involved.”

“We are an exceptional nation that can pull this together and pull it together quickly. We just have to make it a priority... We've taken voting for granted, we've taken the process for granted, and that has to come to an end... Now is the time to wake up and demand standards locally, and then it will all roll uphill,” she concluded.

Catherine and Gregg plan to release their entire two petabyte data arsenal after the 2,000 Mules premier, which is now scheduled for the first week of May. They’ve also recently located video footage from the 14 cameras inside the Gwinnett County, Georgia counting facilities. “Our intention after the video runs, we're going to pull the ripcord, we're going to release all of this.” 

Founded in 2009, True the Vote has battled IRS attacks under Obama, lawsuits from Mark Elias and Stacey Abrams, and most recently sabotage from Governor Kemp. 

“[Kemp] led a fight alright - against us,” Catherine said. “I personally briefed Kemp's team,” added Gregg. “They not only refused [to investigate]… they sent one of their henchmen, the guy that runs the GBI, down to the FBI office where our data lived — not to see the data, but to get into the metadata and figure out who the analysts were and then burn me and a couple of my analysts,” Gregg explained. “Releasing it all to the press... They did everything they could to stop us,” Catherine said. 

Despite the setbacks and attacks, True the Vote has never given up and has now amassed, arguably, the most comprehensive and incontrovertible evidence to date of a criminal conspiracy to subvert the 2020 election. Please consider supporting these warriors and American heroes at [TrueTheVote.org](https://truethevote.org).

{:.small.muted}
Source: [Liberty Overwatch's excellent summary](https://t.me/LibertyOverwatchChannel/6754) of the [Charlie Kirk interview](https://podcasts.apple.com/us/podcast/the-charlie-kirk-show/id1460600818?i=1000556504385), April 9, 2022.


## How to Watch

**Theater Special Events:** On two days, Monday May 2 and Wednesday May 4, they are renting 250 theaters around the country which will show the movie at the 7 pm showing. You will be able to purchase tickets for these showings from a link that will be posted on the [2000Mules website](https://2000mules.com/). This is a LIMITED theatrical release so there will be certain places that aren’t showing the movie in theaters.

**Virtual Premiere:** On Friday May 6 they are having a virtual premiere. This is a very exciting way for you to buy tickets and log into a live event out of a studio in Las Vegas. This virtual premiere will include exclusive content with President Trump, the screening of the movie, and a live Q&A with Dinesh D'Souza and leading figures in the movie. Additionally, for a higher price, around 300 VIPs will be able to attend this event in person.

**Digital Downloads:** On Saturday May 7 you will be able to stream the film from two platforms, [SalemNow.com](https://salemnow.com) and [Rumble/Locals](https://rumble.com).

Details for all of this will be on the website [2000Mules.com](https://2000mules.com) soon. 

We’re very excited about this movie. 2000Mules exposes the widespread, coordinated voter fraud in the 2020 election, sufficient to change the overall outcome. So get ready!

**[Dinesh D’Souza](https://t.me/DineshJDSouza)**  
Director and Producer  
2000Mules  

{:.caption}
Source: [Liberty Overwatch Channel](https://t.me/LibertyOverwatchChannel) and [Tierny Real News Network](https://www.tierneyrealnewsnetwork.com/post/update-on-2000-mules-the-documentary-by-dinesh-truethevote-proving-widespread-election-fraud)   
Telegram: [@TiernyRealNews](https://t.me/TiernyRealNews)

[Visit Official Website: 2000mules.com](https://2000mules.com){:.button} [TrueTheVote.org](https://truethevote.org){:.button}


## Full Interviews with Further Background

As referred to above, this 20min interview from Feb 2022 with Dinesh D'Souza explains his approach to the film:

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vys9jf/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Watch on Rumble](https://rumble.com/v11efj5-the-charlie-kirk-show-2000-mules-and-dinesh-dsouza-02.04.22.html) or [View full text transcript](https://otter.ai/u/0bvl7_HJXE1q630VwNAbmTTqm1U)

The hour-long April 2022 interview with Catherine Engelbrecht and Gregg Phillips goes into much greater detail about how the evidence was gathered:

<iframe class="rumble" width="640" height="360" src="https://rumble.com/embed/vxodgi/?pub=m4ux1" frameborder="0" allowfullscreen></iframe>

{:.caption}
[Watch on Rumble](https://rumble.com/v10ajh2-how-the-did-it-true-the-votes-catherine-engelbrecht-and-gregg-phillips-on-t.html) or [View full text transcript](https://otter.ai/u/qtUQSw4cQupaguY3OkmodyLtbzY)

This Epoch Times "Facts Matter" episode explains some of True the Vote's evidence collected, and the pending investigation in Georgia:

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/OZ5e9eht2Rk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

{:.caption}
[Watch on YouTube](https://www.youtube.com/watch?v=OZ5e9eht2Rk)



## Further Coverage

* [Liberty Overwatch: Breitbart Overview](https://t.me/LibertyOverwatchChannel/5334)
* [Liberty Overwatch: Ballot Trafficking Whistleblower ](https://t.me/LibertyOverwatchChannel/6256)
* [Liberty Overwatch: Ballot Trafficking in 6 States](https://t.me/LibertyOverwatchChannel/6309)
* [Liberty Overwatch: Gwinnett County Trafficker Footage](https://t.me/LibertyOverwatchChannel/6387)
* [Nick Moseder: Is '2000 Mules' An Absolute Game-Changer?](https://nickmoseder.substack.com/p/is-2000-mules-an-absolute-game-changer?r=1g6q8s&utm_medium=ios&s=r)
* [UncoverDC: True The Vote has RICO Ballot Trafficking Evidence: Americans Should Care](https://uncoverdc.com/2022/05/02/true-the-vote-has-rico-ballot-trafficking-evidence-americans-should-care/) -- a deeper dive into the state of Georgia's investigation (or lack thereof)


### Footnotes & References