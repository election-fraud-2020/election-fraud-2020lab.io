{% comment %}
Need to de-duplicate this info from:
- raw_data_known
- raw_data_unknown
- raw_data_general
- data/explorer.md

The Seth Keshel main page also links ONLY to the USElectionAtlas and

{% endcomment %}


## {{ include.title | default: "Raw Data" }}

We aim to publish links to both the raw election data and voter registration data for {{ page.state }} so that citizens and researchers can analyze this information for themselves. 

<a href="#raw-data"
    class="button"
    onclick="
        this.style.display='none'; 
        this.parentElement.nextElementSibling.style.display='';
        return false;
    ">Show Raw Data Links</a>

<div style="font-size: 87%; display: none">

    <div class="responsive-table">
        <table class="raw-data-table">
            <tbody>
                {% if site.data.rawDataSources[page.abbr].results %}
                <tr>
                    <th>
                        {{ site.data.rawDataSources[page.abbr].results.title | markdownify }}
                    </th>
                    <td>
                        {{ site.data.rawDataSources[page.abbr].results.description | markdownify }}
                    </td>
                </tr>
                {% else %}
                <tr>
                    <th>Certified Election Results</th>
                    <td>
                        {% if page.abbr %}<div class="pill">Currently unknown</div><br>{% endif %}
                        In most states, the certified election results are available from the Secretary of State or State Board of Elections. Check their website for details. States are also required by <a href="https://www.eac.gov/sites/default/files/eac_assets/1/6/HAVA41.PDF">HAVA law</a> to inform the public of how many absentee ballots were both sent and received to uniformed services and overseas voters.
                    </td>
                </tr>
                {% endif %}
                {% if site.data.rawDataSources[page.abbr].registrations %}
                <tr>
                    <th>
                        {{ site.data.rawDataSources[page.abbr].registrations.title | markdownify }}
                    </th>
                    <td>
                        {{ site.data.rawDataSources[page.abbr].registrations.description | markdownify }}
                    </td>
                </tr>
                {% else %}
                <tr>
                    <th>Voter Registration Rolls/Database</th>
                    <td>
                        {% if page.abbr %}<div class="pill">Currently unknown</div><br>{% endif %}
                        In some states, these are freely available from the Secretary of State or State Board of Elections. Check their website. In other states, voter rolls must be purchased and/or accessed via a signed legal agreement. Some officials are also obstructing access to the rolls, to make auditing difficult. Let us know via Telegram or via the comment section below if you experience issues.
                    </td>
                </tr>
                {% endif %}
                <tr>
                    <th width="35%" markdown="1">[Cast Vote Records](https://ordros.com/cvr/index.html)</th>
                    <td>Ordros Analytics has collated a repository of Cast Vote Records (CVRs) which list everyone who voted in the Nov 2020 election. Only some counties in some states are represented, but the list is growing.</td>
                </tr>
                <tr>
                    <th width="35%" markdown="1">[The New York Times 2020 Election Results](https://www.nytimes.com/interactive/2020/11/03/us/elections/results-president.html)</th>
                    <td>Results for all states, with several maps and charts.</td>
                </tr>
                {% if include.showExplorer != false %}
                <tr>
                    <th markdown="1">[Data Explorer Tool](/data/explorer/)</th>
                    <td>Our own tool for inspecting the 2020 New York Times data, including the time-series data of how the counting progressed. Also provides download links for raw JSON or CSV data, including counts for every precinct and county.</td>
                </tr>
                {% endif %}
                <tr>
                    <th><a href="https://uselectionatlas.org/">US Election Atlas</a></th>
                    <td>
                        {% if page.url contains '/seth-keshel-reports/' %}
                        <div class="pill" style="background-color: green">Recommended when doing trend analysis, as shown on this page</div><br>
                        {% endif %}
                        Detailed results for 2020 and previous years. Some data is freely accessible on their website, while some, such as <a href="https://uselectionatlas.org/BOTTOM/store_data.php">detailed historic results in CSV format</a>, are purchasable for a fee.
                        <br>
                        This appears to be the source commonly used by Seth Keshel for his analysis, although we have not officially confirmed this.
                    </td>
                </tr>
                {% for source in site.data.rawDataSources[page.abbr].other %}
                <tr>
                    <th>{{ source.title | markdownify }}</th>
                    <td>{{ source.description | markdownify }}</td>
                </tr>
                {% endfor %}
                <tr>
                    <th><a href="https://e1.pcloud.link/publink/show?code=kZn1iJZTuRElQ7DegFGQzSlAFMeOF2YtC3X">2020 General Election Data & Research</a></th>
                    <td>A broad collection of national stats, vote and registration counts, time-series data, voting machine information and manuals, PDF reports, and other research collated by citizen auditors.</td>
                </tr>
                <!-- Jeff O'Donnell's data -->
                <tr>
                    <th markdown="1">[Election Night Time-Series Data from Edison](https://magaraccoon.com/votereports.asp)</th>
                    <td>
                        <div class="pill">Unverified</div><br>
                        Provides more detail than is available from the New York Times, and includes numerous interactive charts.
                        <br>
                        <a href="https://magaraccoon.com/edison2020data.zip">Download ZIP of Raw Data</a>
                        <br>
                        Published by Jeff O’Donnell, <a href="https://magaraccoon.com/">MagaRaccoon.com</a>
                    </td>
                </tr>
                <tr>
                    <th markdown="1">[Weekly HAVV SSN Reports](https://magaraccoon.com/havv.asp)</th>
                    <td>
                        <div class="pill">Unverified</div><br>
                        Social Security Administration (SSA) Weekly Data for Help America Vote Verification (HAVV) Transactions by State.
                        <br>
                        <a href="https://www.ssa.gov/open/havv/">Learn more about this data</a>
                        <br>
                        Published by Jeff O’Donnell, <a href="https://magaraccoon.com/">MagaRaccoon.com</a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    
    <p>If you have additional sources of election data, please let us know via <a href="{{ site.data.globals.telegramChatLink }}">Telegram</a>, <a href="{{ site.data.globals.tweetUsLink }}">Twitter</a> or post a comment below to assist.</p>

</div>