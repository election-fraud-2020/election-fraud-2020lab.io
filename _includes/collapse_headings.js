/**
 * Collapsible/expandable headings, like on Wikipedia
 */
var collapsibleHeadingSelector = '{{ page.collapse_headings }}';
var collapsibleHeadingsExcludeSelector = '{{ page.collapse_headings_exclude }}';

document.querySelector('article').querySelectorAll(collapsibleHeadingSelector).forEach(function(heading) {

  // Exclude SOME headings
  if (heading.matches('.expandable-excluded, #footnotes--references, #visitor-comments')
    || (collapsibleHeadingsExcludeSelector && heading.matches(collapsibleHeadingsExcludeSelector)))
    return;

  var span = document.createElement('span');
  span.innerText = '';
  span.className = 'arrow';
  heading.prepend(span);

  // Create a <DIV> immediately after it to house its content
  var div = document.createElement('div');
  heading.after(div); // IE support? not sure

  // Loop through all elements following, until we reach a stop-element
  var nextElement = heading.nextElementSibling.nextElementSibling;
  while (nextElement) {
    // Break on certain elements
    if (nextElement.matches(collapsibleHeadingSelector + ',h1,h2,#footnotes--references,.post-meta,.expandable-excluded'))
      break;

    // Otherwise add to <div> above
    var thisElement = nextElement;
    nextElement = nextElement.nextElementSibling;
    div.append(thisElement);
  }
  
  // Set a class so we can style it as "expandable"
  heading.classList.add('expandable-heading');
  div.classList.add('expandable-content');

  // Add 'expanded' class when clicked
  heading.addEventListener('click', function(e){
    e.target.classList.toggle('expanded');
    e.target.nextElementSibling.classList.toggle('expanded');
  });

  // If URL contains the hash of this heading, open it by default
  if (
      heading.id == location.hash.substr(1)
      ||
      heading.id == 'footnotes--references' && location.hash.startsWith('#fn')
  ) {
      heading.classList.add('expanded');
      div.classList.add('expanded');
  }

  // If URL contains a footnote hash, be sure to expand that section
});

// If someone clicks on a footnote, we need to ensure the footnote section is also expanded
window.addEventListener('hashchange', function(e){
  console.log(e, location.hash);
  if (location.hash.startsWith('#fn')) {
    console.log('Running hashchange');
    var el = document.getElementById('footnotes--references');
    el.classList.add('expanded');
    el.nextElementSibling.classList.add('expanded');

    // Bug Workaround: reset the hash and apply it again so browser jumps there
    // var temp = location.hash;
    // location.hash = '';
    // location.hash = temp;
  }
}, true);